﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ResourceAccess.SQL
{
    /// <summary>
    /// 
    /// </summary>
    public class spBase
        : IDisposable
    {
        #region Private Members

        private ParamCollection parameters;
        private string name;
        private SqlConnection _cnn;
        private string _cnnstr;
        

        #endregion

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return _cnnstr;
            }
            set
            {
                _cnnstr = value;
            }

        }
        /// <summary>
        /// 
        /// </summary>
        public SqlConnection DBConnection
        {
            get
            {

                return _cnn;
            }
            set { _cnn = value; }
        }
        /// <summary>
        ///
        /// </summary>
        public ParamCollection Parameters
        {
            get
            {
                if (parameters == null)
                    parameters = new ParamCollection();
                return parameters;
            }
            set { parameters = value; }
        }
        /// <summary></summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }



        #endregion

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public spBase()
        {

            DBConnection = new SqlConnection();
            ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["conSQL"].ConnectionString;
            DBConnection.ConnectionString = ConnectionString;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        public spBase(string connection)
        {

            if (string.IsNullOrEmpty(connection))
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["oracleCon"].ConnectionString;
            }
            else
            {
                ConnectionString = connection;
            }


        }
        #endregion

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetSQLCommand()
        {
            
            string command = "EXEC " + name + " ";
            if (Parameters != null)
            {
                foreach (SqlParameter p in Parameters.Values)
                {
                    command += p.ParameterName + "= '" + F.Text.Input.IsNothing(p.Value, "NULL").ToString() + "',";
                }
            }
            command = command.Substring(0, command.Length - 1);
            return command;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="param"></param>
        public void SetParameter(string name, SqlParameter param)
        {
            if (Parameters != null)
            {
                if (Parameters.ContainsKey(name))
                {
                    Parameters[name] = param;
                }
                else
                {
                    Parameters.Add(name, param);
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void SetParameterValue(string name, object value)
        {
            if (parameters != null)
            {
                if (parameters.ContainsKey(name))
                {
                    parameters[name].Value = value;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int Execute()
         {
             int toRet = 0;
             try
             {
                 if (DBConnection != null)
                 {
                     OpenConnection();

                     if (parameters == null)
                         toRet = F.Data.SQL.DAL.ExecuteNonQuery(DBConnection, CommandType.StoredProcedure, name, null);

                     else
                         toRet = F.Data.SQL.DAL.ExecuteNonQuery(DBConnection, CommandType.StoredProcedure, name, parameters.ToArray());

                 }
                 else
                 {
                     if (parameters == null)
                         toRet = F.Data.SQL.DAL.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, name, null);
                     else
                         toRet = F.Data.SQL.DAL.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, name, parameters.ToArray());

                 }
             }
             catch (Exception ex)
             {
                 CloseConnection();
                 throw ex;
             }
             finally {
                 CloseConnection();
             }
             return toRet;
         }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataTable ToDataTable()
        {
            return ToDataTable(0);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public DataTable ToDataTable(int index)
        {
            DataSet ds = null;

            ds = ToDataSet();

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[index];
                }
            }

            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DataSet ToDataSet()
         {
             DataSet ds = null;
             try
             {
                 if (DBConnection != null)
                 {
                     OpenConnection();

                     if (parameters == null)
                         ds = F.Data.SQL.DAL.ExecuteDataset(DBConnection, CommandType.StoredProcedure, name, null);

                     else
                         ds = F.Data.SQL.DAL.ExecuteDataset(DBConnection, CommandType.StoredProcedure, name, parameters.ToArray());

                 }
                 else
                 {
                     if (parameters == null)
                         ds = F.Data.SQL.DAL.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, name, null);
                     else
                         ds = F.Data.SQL.DAL.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, name, parameters.ToArray());

                 }
             }
             catch (Exception ex)
             {
                 CloseConnection();
                 throw ex;
             }
             finally {
                 CloseConnection();
             }
             return ds;
         }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SqlDataReader ToDataReader()
        {
            SqlDataReader dr = null;
            try
            {
                if (DBConnection != null)
                {
                    OpenConnection();

                    if (parameters == null)
                        dr = F.Data.SQL.DAL.ExecuteReader(DBConnection, CommandType.StoredProcedure, name, null);
                    else
                        dr = F.Data.SQL.DAL.ExecuteReader(DBConnection, CommandType.StoredProcedure, name, parameters.ToArray());
                }
                else
                {
                    if (parameters == null)
                        dr = F.Data.SQL.DAL.ExecuteReader(ConnectionString, CommandType.StoredProcedure, name, null);
                    else
                        dr = F.Data.SQL.DAL.ExecuteReader(ConnectionString, CommandType.StoredProcedure, name, parameters.ToArray());
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw ex;
            }
            finally {
                CloseConnection();
            }
            return dr;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object ToScalar()
        {
            object val = null;
            try
            {
                if (DBConnection != null)
                {
                    OpenConnection();

                    if (parameters == null)
                        val = F.Data.SQL.DAL.ExecuteScalar(DBConnection, CommandType.StoredProcedure, name, null);
                    else
                        val = F.Data.SQL.DAL.ExecuteScalar(DBConnection, CommandType.StoredProcedure, name, parameters.ToArray());
                }
                else
                {
                    if (parameters == null)
                        val = F.Data.SQL.DAL.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, name, null);
                    else
                        val = F.Data.SQL.DAL.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, name, parameters.ToArray());
                }
            }
            catch (Exception ex)
            {
                CloseConnection();
                throw ex;
            }
            finally {
                CloseConnection();
            }
            return val;
        }
        #endregion

        #region Connection Handling
        /// <summary>
        /// 
        /// </summary>
        public  void OpenConnection()
        {
            if (DBConnection != null)
            {
                if (DBConnection.State != ConnectionState.Open)
                {
                    System.Data.SqlClient.SqlConnection.ClearAllPools();
                    DBConnection.Open();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public  void CloseConnection()
        {
            if (DBConnection != null)
            {
                if (DBConnection.State != ConnectionState.Closed)
                {
                    DBConnection.Close();
                    DBConnection.Dispose();
                }
            }
            System.Data.SqlClient.SqlConnection.ClearAllPools();
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            CloseConnection();

        }

        #endregion

    }

}
