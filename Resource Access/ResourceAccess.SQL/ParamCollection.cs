﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient ;

namespace ResourceAccess.SQL
{
    public class ParamCollection: Dictionary <string , SqlParameter >
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SqlParameter[] ToArray() {
            SqlParameter[] toret=new SqlParameter [this.Count];
            this.Values.CopyTo(toret, 0);
            return toret;
        } 

    }
}
