﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Coveg.Data
{
    public class OpenData
    {

        public static DataTable OpenQuery(string query)
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conSQL"].ConnectionString))
            {
                try
                {


                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.CommandType = CommandType.Text;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    if (conn != null)
                    {
                        conn.Close();
                        System.Data.SqlClient.SqlConnection.ClearAllPools();
                    }
                }
            }

            

        }
    }
}
