create view vwArea
As
SELECT     id_area, nombre_area, id_jefe, id_direccion, id_area_cargo, centro_costos
FROM         helpdesk.dbo.tbl_area

Go

Create View vwDireccion
AS
SELECT     id_direccion, nombre_direccion, id_jefe
FROM         helpdesk.dbo.tbl_direccion


go

create view vwUsuario
As
SELECT     id_usuario, nombre, apellidos, puesto, usuario, contrasena, telefono, extension, correo_electronico, correo_institucional, fecha_creacion, es_tecnico, 
                      id_area, id_perfil, id_puesto, jefe, jefe_directo, se_elimina, no_empleado
FROM         helpdesk.dbo.tbl_usuario