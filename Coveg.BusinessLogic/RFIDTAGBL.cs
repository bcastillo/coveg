﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;


namespace Coveg.BusinessLogic
{
    public class RFIDTAGBL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<RFIDTAG_BE> Insert(RFIDTAG_BE objBE)
        {
            Coveg.DataAccess.Procedures.spRFIDTAG sp = new Coveg.DataAccess.Procedures.spRFIDTAG();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.RFIDTAGID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAALTA, objBE.TAG);

            List<RFIDTAG_BE> lstRes = new List<RFIDTAG_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<RFIDTAG_BE>(dt, lstRes, new RFIDTAG_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<RFIDTAG_BE> Update(RFIDTAG_BE objBE)
        {
            Coveg.DataAccess.Procedures.spRFIDTAG sp = new Coveg.DataAccess.Procedures.spRFIDTAG();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.RFIDTAGID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAALTA, objBE.TAG);

            List<RFIDTAG_BE> lstRes = new List<RFIDTAG_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<RFIDTAG_BE>(dt, lstRes, new RFIDTAG_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(RFIDTAG_BE objBE)
        {
            Coveg.DataAccess.Procedures.spRFIDTAG sp = new Coveg.DataAccess.Procedures.spRFIDTAG();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.RFIDTAGID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAALTA, objBE.TAG);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<RFIDTAG_BE> SelectAll(RFIDTAG_BE objBE)
        {
            Coveg.DataAccess.Procedures.spRFIDTAG sp = new Coveg.DataAccess.Procedures.spRFIDTAG();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.RFIDTAGID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAALTA, objBE.TAG);

            List<RFIDTAG_BE> lstRes = new List<RFIDTAG_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<RFIDTAG_BE>(dt, lstRes, new RFIDTAG_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<RFIDTAG_BE> SearchKey(RFIDTAG_BE objBE)
        {
            Coveg.DataAccess.Procedures.spRFIDTAG sp = new Coveg.DataAccess.Procedures.spRFIDTAG();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.RFIDTAGID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAALTA, objBE.TAG);

            List<RFIDTAG_BE> lstRes = new List<RFIDTAG_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<RFIDTAG_BE>(dt, lstRes, new RFIDTAG_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
