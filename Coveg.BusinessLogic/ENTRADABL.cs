﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class ENTRADABL
    {
        #region Funciones

        public Boolean RegistrarEntrada(DETALLEENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spEFECTUAR_ENTRADA sp = new DataAccess.Procedures.spEFECTUAR_ENTRADA();
            Int32 intRes = 0;
            sp.Prepare(objBE.ACTIVOID, objBE.USUARIO, objBE.OBSERVACION);

            try
            {

                intRes = sp.Execute();
                if (intRes >= 0)
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ENTRADA_BE> Insert(ENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spENTRADA sp = new Coveg.DataAccess.Procedures.spENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO_ENTREGA);

            List<ENTRADA_BE> lstRes = new List<ENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ENTRADA_BE>(dt, lstRes, new ENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ENTRADA_BE> Update(ENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spENTRADA sp = new Coveg.DataAccess.Procedures.spENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO_ENTREGA);

            List<ENTRADA_BE> lstRes = new List<ENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ENTRADA_BE>(dt, lstRes, new ENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(ENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spENTRADA sp = new Coveg.DataAccess.Procedures.spENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO_ENTREGA);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ENTRADA_BE> SelectAll(ENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spENTRADA sp = new Coveg.DataAccess.Procedures.spENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO_ENTREGA);

            List<ENTRADA_BE> lstRes = new List<ENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ENTRADA_BE>(dt, lstRes, new ENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ENTRADA_BE> SearchKey(ENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spENTRADA sp = new Coveg.DataAccess.Procedures.spENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO_ENTREGA);

            List<ENTRADA_BE> lstRes = new List<ENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ENTRADA_BE>(dt, lstRes, new ENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Obtiene información de un activo para la entrada
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public ACTIVO_BUSQUEDA ActivoEntrada(ACTIVO_BUSQUEDA objBE)
        {
            Coveg.DataAccess.Procedures.spCONSULTA_ENTRADA_ACTIVO sp = new Coveg.DataAccess.Procedures.spCONSULTA_ENTRADA_ACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.CONSULTA_DATOS_ACTIVO_ENTRADA,objBE.CLAVE, objBE.DESC_RFIDTAG,objBE.ACTIVOID);

            List<ACTIVO_BUSQUEDA> lstRes = new List<ACTIVO_BUSQUEDA>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BUSQUEDA>(dt, lstRes, new ACTIVO_BUSQUEDA());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }

            if (lstRes != null)
                return lstRes[0];
            else
                return null;
        }


        public ACTIVO_BUSQUEDA InfoSolicitudEntrada(ACTIVO_BUSQUEDA objBE)
        {
            Coveg.DataAccess.Procedures.spCONSULTA_ENTRADA_ACTIVO sp = new Coveg.DataAccess.Procedures.spCONSULTA_ENTRADA_ACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.CONSULTA_DATOS_SOLICITUD_ENTRADA, objBE.CLAVE, objBE.DESC_RFIDTAG,objBE.ACTIVOID);

            List<ACTIVO_BUSQUEDA> lstRes = new List<ACTIVO_BUSQUEDA>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BUSQUEDA>(dt, lstRes, new ACTIVO_BUSQUEDA());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }

            if (lstRes != null)
                return lstRes[0];
            else
                return null;
        }


        #endregion

        #region Funciones para reporte de seguimiento

            public List<SEGUIMIENTO_SOLICITUDES_BE> Select_Pendientes_Salir(SEGUIMIENTO_SOLICITUDES_BE objBE)
            {
                Coveg.DataAccess.Procedures.spCONSULTAS_SEGUIMIENTO sp = new Coveg.DataAccess.Procedures.spCONSULTAS_SEGUIMIENTO();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_PENDIENTES_SALIR);

                List<SEGUIMIENTO_SOLICITUDES_BE> lstRes = new List<SEGUIMIENTO_SOLICITUDES_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<SEGUIMIENTO_SOLICITUDES_BE>(dt, lstRes, new SEGUIMIENTO_SOLICITUDES_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }

            public List<SEGUIMIENTO_SOLICITUDES_BE> Select_Pendientes_No_Vencidos(SEGUIMIENTO_SOLICITUDES_BE objBE)
            {
                Coveg.DataAccess.Procedures.spCONSULTAS_SEGUIMIENTO sp = new Coveg.DataAccess.Procedures.spCONSULTAS_SEGUIMIENTO();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_FUERA_NO_VENCIDOS);

                List<SEGUIMIENTO_SOLICITUDES_BE> lstRes = new List<SEGUIMIENTO_SOLICITUDES_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<SEGUIMIENTO_SOLICITUDES_BE>(dt, lstRes, new SEGUIMIENTO_SOLICITUDES_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }

            public List<SEGUIMIENTO_SOLICITUDES_BE> Select_Pendientes_Vencidos(SEGUIMIENTO_SOLICITUDES_BE objBE)
            {
                Coveg.DataAccess.Procedures.spCONSULTAS_SEGUIMIENTO sp = new Coveg.DataAccess.Procedures.spCONSULTAS_SEGUIMIENTO();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_VENCIDOS);

                List<SEGUIMIENTO_SOLICITUDES_BE> lstRes = new List<SEGUIMIENTO_SOLICITUDES_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<SEGUIMIENTO_SOLICITUDES_BE>(dt, lstRes, new SEGUIMIENTO_SOLICITUDES_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }

        #endregion
    }
}
