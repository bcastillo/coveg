﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class VEHICULOBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<VEHICULO_BE> Insert(VEHICULO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spVEHICULO sp = new DataAccess.Procedures.spVEHICULO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.VEHICULOID, objBE.ACTIVOID, objBE.CAPACIDAD, objBE.CILINDROS, objBE.CLAVE, objBE.COLOR, objBE.MARCA, objBE.MODELO, objBE.NUMERO_MOTOR, objBE.NUMERO_SERIE, objBE.PLACAS, objBE.PUERTAS, objBE.TIPO, objBE.TRANSMISION);

            List<VEHICULO_BE> lstRes = new List<VEHICULO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<VEHICULO_BE>(dt, lstRes, new VEHICULO_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>

        public List<VEHICULO_BE> Update(VEHICULO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spVEHICULO sp = new DataAccess.Procedures.spVEHICULO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.VEHICULOID, objBE.ACTIVOID, objBE.CAPACIDAD, objBE.CILINDROS, objBE.CLAVE, objBE.COLOR, objBE.MARCA, objBE.MODELO, objBE.NUMERO_MOTOR, objBE.NUMERO_SERIE, objBE.PLACAS, objBE.PUERTAS, objBE.TIPO, objBE.TRANSMISION);

            List<VEHICULO_BE> lstRes = new List<VEHICULO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<VEHICULO_BE>(dt, lstRes, new VEHICULO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(VEHICULO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spVEHICULO sp = new DataAccess.Procedures.spVEHICULO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.VEHICULOID, objBE.ACTIVOID, objBE.CAPACIDAD, objBE.CILINDROS, objBE.CLAVE, objBE.COLOR, objBE.MARCA, objBE.MODELO, objBE.NUMERO_MOTOR, objBE.NUMERO_SERIE, objBE.PLACAS, objBE.PUERTAS, objBE.TIPO, objBE.TRANSMISION);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<VEHICULO_BE> SelectAll(VEHICULO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spVEHICULO sp = new DataAccess.Procedures.spVEHICULO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.VEHICULOID, objBE.ACTIVOID, objBE.CAPACIDAD, objBE.CILINDROS, objBE.CLAVE, objBE.COLOR, objBE.MARCA, objBE.MODELO, objBE.NUMERO_MOTOR, objBE.NUMERO_SERIE, objBE.PLACAS, objBE.PUERTAS, objBE.TIPO, objBE.TRANSMISION);

            List<VEHICULO_BE> lstRes = new List<VEHICULO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<VEHICULO_BE>(dt, lstRes, new VEHICULO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<VEHICULO_BE> SearchKey(VEHICULO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spVEHICULO sp = new DataAccess.Procedures.spVEHICULO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.VEHICULOID, objBE.ACTIVOID, objBE.CAPACIDAD, objBE.CILINDROS, objBE.CLAVE, objBE.COLOR, objBE.MARCA, objBE.MODELO, objBE.NUMERO_MOTOR, objBE.NUMERO_SERIE, objBE.PLACAS, objBE.PUERTAS, objBE.TIPO, objBE.TRANSMISION);
            List<VEHICULO_BE> lstRes = new List<VEHICULO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<VEHICULO_BE>(dt, lstRes, new VEHICULO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        #endregion
    }
}
