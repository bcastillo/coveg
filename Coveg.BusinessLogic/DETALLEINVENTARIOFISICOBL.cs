﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class DETALLEINVENTARIOFISICOBL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLEINVENTARIOFISICO_BE> Insert(DETALLEINVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.DETALLEINVENTARIOFISICOID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.USUARIO);

            List<DETALLEINVENTARIOFISICO_BE> lstRes = new List<DETALLEINVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEINVENTARIOFISICO_BE>(dt, lstRes, new DETALLEINVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLEINVENTARIOFISICO_BE> Update(DETALLEINVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.DETALLEINVENTARIOFISICOID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.USUARIO);

            List<DETALLEINVENTARIOFISICO_BE> lstRes = new List<DETALLEINVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEINVENTARIOFISICO_BE>(dt, lstRes, new DETALLEINVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(DETALLEINVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.DETALLEINVENTARIOFISICOID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.USUARIO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLEINVENTARIOFISICO_BE> SelectAll(DETALLEINVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.DETALLEINVENTARIOFISICOID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.USUARIO);

            List<DETALLEINVENTARIOFISICO_BE> lstRes = new List<DETALLEINVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEINVENTARIOFISICO_BE>(dt, lstRes, new DETALLEINVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<DETALLEINVENTARIOFISICO_BE> SearchKey(DETALLEINVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spDETALLEINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.DETALLEINVENTARIOFISICOID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.USUARIO);

            List<DETALLEINVENTARIOFISICO_BE> lstRes = new List<DETALLEINVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEINVENTARIOFISICO_BE>(dt, lstRes, new DETALLEINVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
