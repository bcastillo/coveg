﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class SALIDABL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SALIDA_BE> Insert(SALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSALIDA sp = new Coveg.DataAccess.Procedures.spSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.SALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.SOLICITUDSALIDAID);

            List<SALIDA_BE> lstRes = new List<SALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SALIDA_BE>(dt, lstRes, new SALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SALIDA_BE> Update(SALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSALIDA sp = new Coveg.DataAccess.Procedures.spSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.SALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.SOLICITUDSALIDAID);

            List<SALIDA_BE> lstRes = new List<SALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SALIDA_BE>(dt, lstRes, new SALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(SALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSALIDA sp = new Coveg.DataAccess.Procedures.spSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.SALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.SOLICITUDSALIDAID);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SALIDA_BE> SelectAll(SALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSALIDA sp = new Coveg.DataAccess.Procedures.spSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.SALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.SOLICITUDSALIDAID);

            List<SALIDA_BE> lstRes = new List<SALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SALIDA_BE>(dt, lstRes, new SALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<SALIDA_BE> SearchKey(SALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSALIDA sp = new Coveg.DataAccess.Procedures.spSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.SALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.SOLICITUDSALIDAID);

            List<SALIDA_BE> lstRes = new List<SALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SALIDA_BE>(dt, lstRes, new SALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
