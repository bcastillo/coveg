﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class DETALLESALIDABL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLESALIDA_BE> Insert(DETALLESALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLESALIDA sp = new Coveg.DataAccess.Procedures.spDETALLESALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.DETALLESALIDAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHA, objBE.SALIDAID, objBE.USUARIO);

            List<DETALLESALIDA_BE> lstRes = new List<DETALLESALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLESALIDA_BE>(dt, lstRes, new DETALLESALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLESALIDA_BE> Update(DETALLESALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLESALIDA sp = new Coveg.DataAccess.Procedures.spDETALLESALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.DETALLESALIDAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHA, objBE.SALIDAID, objBE.USUARIO);

            List<DETALLESALIDA_BE> lstRes = new List<DETALLESALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLESALIDA_BE>(dt, lstRes, new DETALLESALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(DETALLESALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLESALIDA sp = new Coveg.DataAccess.Procedures.spDETALLESALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.DETALLESALIDAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHA, objBE.SALIDAID, objBE.USUARIO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLESALIDA_BE> SelectAll(DETALLESALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLESALIDA sp = new Coveg.DataAccess.Procedures.spDETALLESALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.DETALLESALIDAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHA, objBE.SALIDAID, objBE.USUARIO);

            List<DETALLESALIDA_BE> lstRes = new List<DETALLESALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLESALIDA_BE>(dt, lstRes, new DETALLESALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<DETALLESALIDA_BE> SearchKey(DETALLESALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLESALIDA sp = new Coveg.DataAccess.Procedures.spDETALLESALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.DETALLESALIDAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.FECHA, objBE.SALIDAID, objBE.USUARIO);

            List<DETALLESALIDA_BE> lstRes = new List<DETALLESALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLESALIDA_BE>(dt, lstRes, new DETALLESALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
