﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class USUARIOROLBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<USUARIOROLBE> Insert(USUARIOROLBE objBE)
        {
            Coveg.DataAccess.Procedures.spUSUARIOROL sp = new Coveg.DataAccess.Procedures.spUSUARIOROL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.USUARIOROLID, objBE.USUARIO, objBE.CVE_ROL);

            List<USUARIOROLBE> lstRes = new List<USUARIOROLBE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<USUARIOROLBE>(dt, lstRes, new USUARIOROLBE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<USUARIOROLBE> Update(USUARIOROLBE objBE)
        {
            Coveg.DataAccess.Procedures.spUSUARIOROL sp = new Coveg.DataAccess.Procedures.spUSUARIOROL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.USUARIOROLID, objBE.USUARIO, objBE.CVE_ROL);

            List<USUARIOROLBE> lstRes = new List<USUARIOROLBE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<USUARIOROLBE>(dt, lstRes, new USUARIOROLBE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(USUARIOROLBE objBE)
        {
            Coveg.DataAccess.Procedures.spUSUARIOROL sp = new Coveg.DataAccess.Procedures.spUSUARIOROL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.USUARIOROLID, objBE.USUARIO, objBE.CVE_ROL);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<USUARIOROLBE> SelectAll(USUARIOROLBE objBE)
        {
            Coveg.DataAccess.Procedures.spUSUARIOROL sp = new Coveg.DataAccess.Procedures.spUSUARIOROL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.USUARIOROLID, objBE.USUARIO, objBE.CVE_ROL);

            List<USUARIOROLBE> lstRes = new List<USUARIOROLBE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<USUARIOROLBE>(dt, lstRes, new USUARIOROLBE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<USUARIOROLBE> SearchKey(USUARIOROLBE objBE)
        {
            Coveg.DataAccess.Procedures.spUSUARIOROL sp = new Coveg.DataAccess.Procedures.spUSUARIOROL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.USUARIOROLID, objBE.USUARIO, objBE.CVE_ROL);

            List<USUARIOROLBE> lstRes = new List<USUARIOROLBE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<USUARIOROLBE>(dt, lstRes, new USUARIOROLBE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
