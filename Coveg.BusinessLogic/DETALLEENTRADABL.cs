﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class DETALLEENTRADABL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLEENTRADA_BE> Insert(DETALLEENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEENTRADA sp = new Coveg.DataAccess.Procedures.spDETALLEENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.DETALLEENTRADAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO);

            List<DETALLEENTRADA_BE> lstRes = new List<DETALLEENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEENTRADA_BE>(dt, lstRes, new DETALLEENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLEENTRADA_BE> Update(DETALLEENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEENTRADA sp = new Coveg.DataAccess.Procedures.spDETALLEENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.DETALLEENTRADAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO);

            List<DETALLEENTRADA_BE> lstRes = new List<DETALLEENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEENTRADA_BE>(dt, lstRes, new DETALLEENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(DETALLEENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEENTRADA sp = new Coveg.DataAccess.Procedures.spDETALLEENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.DETALLEENTRADAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DETALLEENTRADA_BE> SelectAll(DETALLEENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEENTRADA sp = new Coveg.DataAccess.Procedures.spDETALLEENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.DETALLEENTRADAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO);

            List<DETALLEENTRADA_BE> lstRes = new List<DETALLEENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEENTRADA_BE>(dt, lstRes, new DETALLEENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<DETALLEENTRADA_BE> SearchKey(DETALLEENTRADA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDETALLEENTRADA sp = new Coveg.DataAccess.Procedures.spDETALLEENTRADA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.DETALLEENTRADAID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ENTRADAID, objBE.FECHA, objBE.USUARIO);

            List<DETALLEENTRADA_BE> lstRes = new List<DETALLEENTRADA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DETALLEENTRADA_BE>(dt, lstRes, new DETALLEENTRADA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
