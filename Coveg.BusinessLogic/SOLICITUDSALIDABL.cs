﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;


namespace Coveg.BusinessLogic
{
    public class SOLICITUDSALIDABL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> Insert(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO,objBE.IDJEFEINMEDIATO,objBE.FECHASALIDA,objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> Update(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO,objBE.IDJEFEINMEDIATO,objBE.FECHASALIDA,objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        public bool Finalizar(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.FINALIZAR, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return (lstRes != null ? lstRes.Count :0)>0;
        }

        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO,objBE.IDJEFEINMEDIATO,objBE.FECHASALIDA,objBE.FECHARETORNO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> SelectAll(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch 
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> SearchKey(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Consulta registros de solicitudes de activos en resguardo
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> SolicitudesAutorizacion(SOLICITUDSALIDA_BE objBE,int intAccion)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare(intAccion, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion

        #region Metodos de consulta para documentos

        /// <summary>
        /// consulta solicitudes de salida de documentos
        /// </summary>
        /// <param name="objBE"></param>
        /// <param name="intAccion"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> SolicitudesAutorizacionDocumentos(SOLICITUDSALIDA_BE objBE, int intAccion)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare(intAccion, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Solicitudes por usuario no devueltas
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<SOLICITUDSALIDA_BE> SelectSolicitudesUsuarioNoDevueltas(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_SOLICITUDESUSUARIONODEVUELTAS, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

            List<SOLICITUDSALIDA_BE> lstRes = new List<SOLICITUDSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<SOLICITUDSALIDA_BE>(dt, lstRes, new SOLICITUDSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
    #endregion

        #region Solicitud y activos (activo, documento, vehiculo)

            public List<DETALLESOLICITUD_ACTIVOS_BE> SelectSolicitudActivos(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.DataAccess.Procedures.spSOLICITUDSALIDA sp = new Coveg.DataAccess.Procedures.spSOLICITUDSALIDA();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_INFO_SOLICITUD_ACTIVOS, objBE.SOLICITUDSALIDAID, objBE.ESTATUS, objBE.FECHA, objBE.MOTIVO, objBE.USUARIO, objBE.IDJEFEINMEDIATO, objBE.FECHASALIDA, objBE.FECHARETORNO);

                List<DETALLESOLICITUD_ACTIVOS_BE> lstRes = new List<DETALLESOLICITUD_ACTIVOS_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<DETALLESOLICITUD_ACTIVOS_BE>(dt, lstRes, new DETALLESOLICITUD_ACTIVOS_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }

        #endregion

            
    }
}
