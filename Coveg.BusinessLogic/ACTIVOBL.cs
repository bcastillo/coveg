﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class ACTIVOBL
    {
        #region Funciones


        /// <summary>
        /// Insert Activo tipo documento
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> InsertActivoDocumento(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERT_DOCUMENTO, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Inserar registro de activo para auto
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> InsertActivoAuto(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERT_VEHICULO, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> Insert(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA,objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch 
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        
        public List<ACTIVO_BE> Update(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> SelectAll(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Listado de activos para busqueda de solicitud
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BUSQUEDA> SelectActivosSolicitud(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_ACTIVOSSOLICITUD, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BUSQUEDA> lstRes = new List<ACTIVO_BUSQUEDA>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BUSQUEDA>(dt, lstRes, new ACTIVO_BUSQUEDA());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Listado de activos para busqueda de solicitud (Automoviles)
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BUSQUEDA> SelectActivosSolicitudAutomoviles(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_ACTIVOSSOLICITUDVEHICULOS, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BUSQUEDA> lstRes = new List<ACTIVO_BUSQUEDA>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BUSQUEDA>(dt, lstRes, new ACTIVO_BUSQUEDA());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Listado de activos para busqueda de solicitud (Documentos)
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BUSQUEDA> SelectActivosSolicitudDoctos(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SELECT_ACTIVOSSOLICITUDDOCTOS, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BUSQUEDA> lstRes = new List<ACTIVO_BUSQUEDA>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BUSQUEDA>(dt, lstRes, new ACTIVO_BUSQUEDA());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> SearchKey(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);
            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Consulta todos los registros de la tabla que son activos tipo vehiculo
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> SelectAllActivoVehiculo(ACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVO sp = new Coveg.DataAccess.Procedures.spACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.ACTIVO_VEHICULO, objBE.ACTIVOID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE, objBE.NUMEROSERIE, objBE.NUMNOMINA, objBE.REQUIERE_SOLICITUD, objBE.RFIDTAGID, objBE.TIPOACTIVOID, objBE.UNIDADID);

            List<ACTIVO_BE> lstRes = new List<ACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVO_BE>(dt, lstRes, new ACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        #endregion
    }
}
