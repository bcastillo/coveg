﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class TIPOACTIVOBL
    {
        #region Funciones


        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOACTIVO_BE> Insert(TIPOACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOACTIVO sp = new Coveg.DataAccess.Procedures.spTIPOACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.TIPOACTIVOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<TIPOACTIVO_BE> lstRes = new List<TIPOACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOACTIVO_BE>(dt, lstRes, new TIPOACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOACTIVO_BE> Update(TIPOACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOACTIVO sp = new Coveg.DataAccess.Procedures.spTIPOACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.TIPOACTIVOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<TIPOACTIVO_BE> lstRes = new List<TIPOACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOACTIVO_BE>(dt, lstRes, new TIPOACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(TIPOACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOACTIVO sp = new Coveg.DataAccess.Procedures.spTIPOACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.TIPOACTIVOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOACTIVO_BE> SelectAll(TIPOACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOACTIVO sp = new Coveg.DataAccess.Procedures.spTIPOACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.TIPOACTIVOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<TIPOACTIVO_BE> lstRes = new List<TIPOACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOACTIVO_BE>(dt, lstRes, new TIPOACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<TIPOACTIVO_BE> SearchKey(TIPOACTIVO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOACTIVO sp = new Coveg.DataAccess.Procedures.spTIPOACTIVO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.TIPOACTIVOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<TIPOACTIVO_BE> lstRes = new List<TIPOACTIVO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOACTIVO_BE>(dt, lstRes, new TIPOACTIVO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
