﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class ACTIVOSSOLICITADOSBL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVOSSOLICITADOS_BE> Insert(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS sp = new Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.ACTIVOSSOLICITADOSID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ESTATUS, objBE.SOLICITUDSALIDAID);

            List<ACTIVOSSOLICITADOS_BE> lstRes = new List<ACTIVOSSOLICITADOS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVOSSOLICITADOS_BE>(dt, lstRes, new ACTIVOSSOLICITADOS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVOSSOLICITADOS_BE> Update(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS sp = new Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.ACTIVOSSOLICITADOSID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ESTATUS, objBE.SOLICITUDSALIDAID);

            List<ACTIVOSSOLICITADOS_BE> lstRes = new List<ACTIVOSSOLICITADOS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVOSSOLICITADOS_BE>(dt, lstRes, new ACTIVOSSOLICITADOS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS sp = new Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.ACTIVOSSOLICITADOSID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ESTATUS, objBE.SOLICITUDSALIDAID);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<ACTIVOSSOLICITADOS_BE> SelectAll(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS sp = new Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.ACTIVOSSOLICITADOSID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ESTATUS, objBE.SOLICITUDSALIDAID);

            List<ACTIVOSSOLICITADOS_BE> lstRes = new List<ACTIVOSSOLICITADOS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVOSSOLICITADOS_BE>(dt, lstRes, new ACTIVOSSOLICITADOS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVOSSOLICITADOS_BE> SearchKey(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS sp = new Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.ACTIVOSSOLICITADOSID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ESTATUS, objBE.SOLICITUDSALIDAID);

            List<ACTIVOSSOLICITADOS_BE> lstRes = new List<ACTIVOSSOLICITADOS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVOSSOLICITADOS_BE>(dt, lstRes, new ACTIVOSSOLICITADOS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Buscar activos por solicitud
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVOSSOLICITADOS_BE> ListaActivosSolicitud(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS sp = new Coveg.DataAccess.Procedures.spACTIVOSSOLICITADOS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.ACTIVOS_SOLICITUD, objBE.ACTIVOSSOLICITADOSID, objBE.ACTIVOID, objBE.CANTIDAD, objBE.ESTATUS, objBE.SOLICITUDSALIDAID);

            List<ACTIVOSSOLICITADOS_BE> lstRes = new List<ACTIVOSSOLICITADOS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ACTIVOSSOLICITADOS_BE>(dt, lstRes, new ACTIVOSSOLICITADOS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
