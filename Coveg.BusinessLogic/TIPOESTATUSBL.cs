﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class TIPOESTATUSBL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOESTATUS_BE> Insert(TIPOESTATUS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOESTATUS sp = new Coveg.DataAccess.Procedures.spTIPOESTATUS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.TIPOESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE);

            List<TIPOESTATUS_BE> lstRes = new List<TIPOESTATUS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOESTATUS_BE>(dt, lstRes, new TIPOESTATUS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOESTATUS_BE> Update(TIPOESTATUS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOESTATUS sp = new Coveg.DataAccess.Procedures.spTIPOESTATUS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.TIPOESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE);

            List<TIPOESTATUS_BE> lstRes = new List<TIPOESTATUS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOESTATUS_BE>(dt, lstRes, new TIPOESTATUS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(TIPOESTATUS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOESTATUS sp = new Coveg.DataAccess.Procedures.spTIPOESTATUS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.TIPOESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOESTATUS_BE> SelectAll(TIPOESTATUS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOESTATUS sp = new Coveg.DataAccess.Procedures.spTIPOESTATUS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.TIPOESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE);

            List<TIPOESTATUS_BE> lstRes = new List<TIPOESTATUS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOESTATUS_BE>(dt, lstRes, new TIPOESTATUS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<TIPOESTATUS_BE> SearchKey(TIPOESTATUS_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOESTATUS sp = new Coveg.DataAccess.Procedures.spTIPOESTATUS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.TIPOESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE);

            List<TIPOESTATUS_BE> lstRes = new List<TIPOESTATUS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOESTATUS_BE>(dt, lstRes, new TIPOESTATUS_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
