﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class INVENTARIOBL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIO_BE> Insert(INVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIO sp = new Coveg.DataAccess.Procedures.spINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.INVENTARIOID, objBE.ACTIVOID, objBE.FECHA, objBE.SALDO, objBE.UBICACIONID);

            List<INVENTARIO_BE> lstRes = new List<INVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIO_BE>(dt, lstRes, new INVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIO_BE> Update(INVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIO sp = new Coveg.DataAccess.Procedures.spINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.INVENTARIOID, objBE.ACTIVOID, objBE.FECHA, objBE.SALDO, objBE.UBICACIONID);

            List<INVENTARIO_BE> lstRes = new List<INVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIO_BE>(dt, lstRes, new INVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(INVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIO sp = new Coveg.DataAccess.Procedures.spINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.INVENTARIOID, objBE.ACTIVOID, objBE.FECHA, objBE.SALDO, objBE.UBICACIONID);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIO_BE> SelectAll(INVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIO sp = new Coveg.DataAccess.Procedures.spINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.INVENTARIOID, objBE.ACTIVOID, objBE.FECHA, objBE.SALDO, objBE.UBICACIONID);
            

            List<INVENTARIO_BE> lstRes = new List<INVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIO_BE>(dt, lstRes, new INVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<INVENTARIO_BE> SearchKey(INVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIO sp = new Coveg.DataAccess.Procedures.spINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.INVENTARIOID, objBE.ACTIVOID, objBE.FECHA, objBE.SALDO, objBE.UBICACIONID);

            List<INVENTARIO_BE> lstRes = new List<INVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIO_BE>(dt, lstRes, new INVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
