﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class OBTENERCORREOSBL
    {
        /// <summary>
        /// Obtiene correo para notificar aprobación pendiente
        /// </summary>
        /// <param name="objData"></param>
        /// <returns></returns>
        public List<MailData> BuscarCorreos(MailData objData)
        {
            Coveg.DataAccess.Procedures.spOBTENERCORREOS sp = new Coveg.DataAccess.Procedures.spOBTENERCORREOS();
            sp.Prepare(objData.SOLICITUDSALIDAID);

            List<MailData> lstRes = new List<MailData>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<MailData>(dt, lstRes, new MailData());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
    }
}
