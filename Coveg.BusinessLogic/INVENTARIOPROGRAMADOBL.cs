﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class INVENTARIOPROGRAMADOBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIOPROGRAMADO_BE> Insert(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOPROGRAMADO sp = new DataAccess.Procedures.spINVENTARIOPROGRAMADO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.INVENTARIOPROGRAMADOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAFIN, objBE.fechainicio, objBE.FECHAPROGRAMADA, objBE.ID_AREA);

            List<INVENTARIOPROGRAMADO_BE> lstRes = new List<INVENTARIOPROGRAMADO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOPROGRAMADO_BE>(dt, lstRes, new INVENTARIOPROGRAMADO_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>

        public List<INVENTARIOPROGRAMADO_BE> Update(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOPROGRAMADO sp = new DataAccess.Procedures.spINVENTARIOPROGRAMADO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.INVENTARIOPROGRAMADOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAFIN, objBE.fechainicio, objBE.FECHAPROGRAMADA, objBE.ID_AREA);

            List<INVENTARIOPROGRAMADO_BE> lstRes = new List<INVENTARIOPROGRAMADO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOPROGRAMADO_BE>(dt, lstRes, new INVENTARIOPROGRAMADO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOPROGRAMADO sp = new DataAccess.Procedures.spINVENTARIOPROGRAMADO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.INVENTARIOPROGRAMADOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAFIN, objBE.fechainicio, objBE.FECHAPROGRAMADA, objBE.ID_AREA);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIOPROGRAMADO_BE> SelectAll(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOPROGRAMADO sp = new DataAccess.Procedures.spINVENTARIOPROGRAMADO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.INVENTARIOPROGRAMADOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAFIN, objBE.fechainicio, objBE.FECHAPROGRAMADA, objBE.ID_AREA);

            List<INVENTARIOPROGRAMADO_BE> lstRes = new List<INVENTARIOPROGRAMADO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOPROGRAMADO_BE>(dt, lstRes, new INVENTARIOPROGRAMADO_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<INVENTARIOPROGRAMADO_BE> SearchKey(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOPROGRAMADO sp = new DataAccess.Procedures.spINVENTARIOPROGRAMADO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.INVENTARIOPROGRAMADOID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.FECHAFIN, objBE.fechainicio, objBE.FECHAPROGRAMADA, objBE.ID_AREA);
            List<INVENTARIOPROGRAMADO_BE> lstRes = new List<INVENTARIOPROGRAMADO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOPROGRAMADO_BE>(dt, lstRes, new INVENTARIOPROGRAMADO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        #endregion
    }
}
