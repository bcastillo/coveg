﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class HISTORICO_SOLICITUDESBL
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<HISTORICO_SOLICITUDES_BE> HistoricoCancelados(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.DataAccess.Procedures.spHISTORICOSOL sp = new Coveg.DataAccess.Procedures.spHISTORICOSOL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.HISTORICO_CANCELADO, objBE.USUARIO_SOLICITA);

            List<HISTORICO_SOLICITUDES_BE> lstRes = new List<HISTORICO_SOLICITUDES_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<HISTORICO_SOLICITUDES_BE>(dt, lstRes, new HISTORICO_SOLICITUDES_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        /// <summary>
        /// Consulta historico de solicitudes aprobadas por usuario
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<HISTORICO_SOLICITUDES_BE> HistoricoAprobados(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.DataAccess.Procedures.spHISTORICOSOL sp = new Coveg.DataAccess.Procedures.spHISTORICOSOL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.HISTORICO_AUTORIZADO, objBE.USUARIO_SOLICITA);

            List<HISTORICO_SOLICITUDES_BE> lstRes = new List<HISTORICO_SOLICITUDES_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<HISTORICO_SOLICITUDES_BE>(dt, lstRes, new HISTORICO_SOLICITUDES_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Consulta historico de solicitudes rechazadas por usuario
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<HISTORICO_SOLICITUDES_BE> HistoricoRechazado(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.DataAccess.Procedures.spHISTORICOSOL sp = new Coveg.DataAccess.Procedures.spHISTORICOSOL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.HISTORICO_RECHAZADO, objBE.USUARIO_SOLICITA);

            List<HISTORICO_SOLICITUDES_BE> lstRes = new List<HISTORICO_SOLICITUDES_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<HISTORICO_SOLICITUDES_BE>(dt, lstRes, new HISTORICO_SOLICITUDES_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Consulta historico de solicitudes devueltas por usuario
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<HISTORICO_SOLICITUDES_BE> HistoricoDevuelto(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.DataAccess.Procedures.spHISTORICOSOL sp = new Coveg.DataAccess.Procedures.spHISTORICOSOL();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.HISTORICO_DEVUELTO, objBE.USUARIO_SOLICITA);

            List<HISTORICO_SOLICITUDES_BE> lstRes = new List<HISTORICO_SOLICITUDES_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<HISTORICO_SOLICITUDES_BE>(dt, lstRes, new HISTORICO_SOLICITUDES_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

    }
}
