﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class AUTORIZACIONSALIDABL
    {
        #region Funciones
        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<AUTORIZACIONSALIDA_BE> Insert(AUTORIZACIONSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            List<AUTORIZACIONSALIDA_BE> lstRes = new List<AUTORIZACIONSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<AUTORIZACIONSALIDA_BE>(dt, lstRes, new AUTORIZACIONSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<AUTORIZACIONSALIDA_BE> Update(AUTORIZACIONSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            List<AUTORIZACIONSALIDA_BE> lstRes = new List<AUTORIZACIONSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<AUTORIZACIONSALIDA_BE>(dt, lstRes, new AUTORIZACIONSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(AUTORIZACIONSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<AUTORIZACIONSALIDA_BE> SelectAll(AUTORIZACIONSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            List<AUTORIZACIONSALIDA_BE> lstRes = new List<AUTORIZACIONSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<AUTORIZACIONSALIDA_BE>(dt, lstRes, new AUTORIZACIONSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<AUTORIZACIONSALIDA_BE> SearchKey(AUTORIZACIONSALIDA_BE objBE)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            List<AUTORIZACIONSALIDA_BE> lstRes = new List<AUTORIZACIONSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<AUTORIZACIONSALIDA_BE>(dt, lstRes, new AUTORIZACIONSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }




        /// <summary>
        /// Autoriza resguardo
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<AUTORIZACIONSALIDA_BE> Autoriza(AUTORIZACIONSALIDA_BE objBE,int intAccion)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare(intAccion, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            List<AUTORIZACIONSALIDA_BE> lstRes = new List<AUTORIZACIONSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<AUTORIZACIONSALIDA_BE>(dt, lstRes, new AUTORIZACIONSALIDA_BE());
                else
                    lstRes = null;

            }
            catch (Exception ex)
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Rechaza resguardo
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<AUTORIZACIONSALIDA_BE> Rechaza(AUTORIZACIONSALIDA_BE objBE, int intAccion)
        {
            Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA sp = new Coveg.DataAccess.Procedures.spAUTORIZACIONSALIDA();
            sp.Prepare(intAccion, objBE.AUTORIZACIONSALIDAID, objBE.AUTORIZO, objBE.COMENTARIOS, objBE.ESTATUS, objBE.FECHAREVISION, objBE.SOLICITUDSALIDAID, objBE.TIPOAUTORIZADORID);

            List<AUTORIZACIONSALIDA_BE> lstRes = new List<AUTORIZACIONSALIDA_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<AUTORIZACIONSALIDA_BE>(dt, lstRes, new AUTORIZACIONSALIDA_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        #endregion
    }
}
