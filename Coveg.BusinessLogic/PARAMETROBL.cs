﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class PARAMETROBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<PARAMETRO_BE> Insert(PARAMETRO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spPARAMETRO sp = new Coveg.DataAccess.Procedures.spPARAMETRO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.PARAMETROID, objBE.CLAVE, objBE.ESTATUS, objBE.NOMBRE, objBE.VALORNUMERICO, objBE.VALORTEXTO);

            List<PARAMETRO_BE> lstRes = new List<PARAMETRO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<PARAMETRO_BE>(dt, lstRes, new PARAMETRO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<PARAMETRO_BE> Update(PARAMETRO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spPARAMETRO sp = new Coveg.DataAccess.Procedures.spPARAMETRO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.PARAMETROID, objBE.CLAVE, objBE.ESTATUS, objBE.NOMBRE, objBE.VALORNUMERICO, objBE.VALORTEXTO);

            List<PARAMETRO_BE> lstRes = new List<PARAMETRO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<PARAMETRO_BE>(dt, lstRes, new PARAMETRO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(PARAMETRO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spPARAMETRO sp = new Coveg.DataAccess.Procedures.spPARAMETRO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.PARAMETROID, objBE.CLAVE, objBE.ESTATUS, objBE.NOMBRE, objBE.VALORNUMERICO, objBE.VALORTEXTO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<PARAMETRO_BE> SelectAll(PARAMETRO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spPARAMETRO sp = new Coveg.DataAccess.Procedures.spPARAMETRO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.PARAMETROID, objBE.CLAVE, objBE.ESTATUS, objBE.NOMBRE, objBE.VALORNUMERICO, objBE.VALORTEXTO);

            List<PARAMETRO_BE> lstRes = new List<PARAMETRO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<PARAMETRO_BE>(dt, lstRes, new PARAMETRO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<PARAMETRO_BE> SearchKey(PARAMETRO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spPARAMETRO sp = new Coveg.DataAccess.Procedures.spPARAMETRO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.PARAMETROID, objBE.CLAVE, objBE.ESTATUS, objBE.NOMBRE, objBE.VALORNUMERICO, objBE.VALORTEXTO);

            List<PARAMETRO_BE> lstRes = new List<PARAMETRO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<PARAMETRO_BE>(dt, lstRes, new PARAMETRO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
