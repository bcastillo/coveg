﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class LOGINBL
    {
        /// <summary>
        /// Consulta usuario
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<LOGIN_BE> SelectLogin(LOGIN_BE objBE)
        {
            Coveg.DataAccess.Procedures.spLOGIN sp = new Coveg.DataAccess.Procedures.spLOGIN();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS,objBE.Usuario,objBE.Password);

            List<LOGIN_BE> lstRes = new List<LOGIN_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<LOGIN_BE>(dt, lstRes, new LOGIN_BE());

                if (lstRes != null)
                    if (lstRes.Count > 0)
                        return lstRes;
                    else
                        return null;
                else
                    return null;
                
            }
            catch
            {
                return null;
            }
        }


        public LOGIN_BE SelectJefe(LOGIN_BE objBE)
        {
            Coveg.DataAccess.Procedures.spJEFEINMEDIATO sp = new Coveg.DataAccess.Procedures.spJEFEINMEDIATO();
            sp.Prepare(objBE.id_usuario);

            List<LOGIN_BE> lstRes = new List<LOGIN_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<LOGIN_BE>(dt, lstRes, new LOGIN_BE());

                if (lstRes != null)
                    if (lstRes.Count > 0)
                        return lstRes[0];
                    else
                        return null;
                else
                    return null;

            }
            catch
            {
                return null;
            }
        }
    }
}
