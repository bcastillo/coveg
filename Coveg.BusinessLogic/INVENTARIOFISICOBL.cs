﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class INVENTARIOFISICOBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIOFISICO_BE> Insert(INVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.INVENTARIOFISICOID, objBE.FECHA, objBE.MOTIVO, objBE.NUMEMPLEADO, objBE.UBICACIONID);

            List<INVENTARIOFISICO_BE> lstRes = new List<INVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOFISICO_BE>(dt, lstRes, new INVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIOFISICO_BE> Update(INVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.INVENTARIOFISICOID, objBE.FECHA, objBE.MOTIVO, objBE.NUMEMPLEADO, objBE.UBICACIONID);

            List<INVENTARIOFISICO_BE> lstRes = new List<INVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOFISICO_BE>(dt, lstRes, new INVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(INVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.INVENTARIOFISICOID, objBE.FECHA, objBE.MOTIVO, objBE.NUMEMPLEADO, objBE.UBICACIONID);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<INVENTARIOFISICO_BE> SelectAll(INVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.INVENTARIOFISICOID, objBE.FECHA, objBE.MOTIVO, objBE.NUMEMPLEADO, objBE.UBICACIONID);

            List<INVENTARIOFISICO_BE> lstRes = new List<INVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOFISICO_BE>(dt, lstRes, new INVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<INVENTARIOFISICO_BE> SearchKey(INVENTARIOFISICO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spINVENTARIOFISICO sp = new Coveg.DataAccess.Procedures.spINVENTARIOFISICO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.INVENTARIOFISICOID, objBE.FECHA, objBE.MOTIVO, objBE.NUMEMPLEADO, objBE.UBICACIONID);
            List<INVENTARIOFISICO_BE> lstRes = new List<INVENTARIOFISICO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<INVENTARIOFISICO_BE>(dt, lstRes, new INVENTARIOFISICO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
