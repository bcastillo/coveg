﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class TIPOAUTORIZADORBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOAUTORIZADOR_BE> Insert(TIPOAUTORIZADOR_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR sp = new Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.TIPOAUTORIZADORID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS);

            List<TIPOAUTORIZADOR_BE> lstRes = new List<TIPOAUTORIZADOR_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOAUTORIZADOR_BE>(dt, lstRes, new TIPOAUTORIZADOR_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOAUTORIZADOR_BE> Update(TIPOAUTORIZADOR_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR sp = new Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.TIPOAUTORIZADORID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS);

            List<TIPOAUTORIZADOR_BE> lstRes = new List<TIPOAUTORIZADOR_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOAUTORIZADOR_BE>(dt, lstRes, new TIPOAUTORIZADOR_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(TIPOAUTORIZADOR_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR sp = new Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.TIPOAUTORIZADORID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<TIPOAUTORIZADOR_BE> SelectAll(TIPOAUTORIZADOR_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR sp = new Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.TIPOAUTORIZADORID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS);

            List<TIPOAUTORIZADOR_BE> lstRes = new List<TIPOAUTORIZADOR_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOAUTORIZADOR_BE>(dt, lstRes, new TIPOAUTORIZADOR_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<TIPOAUTORIZADOR_BE> SearchKey(TIPOAUTORIZADOR_BE objBE)
        {
            Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR sp = new Coveg.DataAccess.Procedures.spTIPOAUTORIZADOR();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.TIPOAUTORIZADORID, objBE.CLAVE, objBE.DESCRIPCION, objBE.ESTATUS);

            List<TIPOAUTORIZADOR_BE> lstRes = new List<TIPOAUTORIZADOR_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<TIPOAUTORIZADOR_BE>(dt, lstRes, new TIPOAUTORIZADOR_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
