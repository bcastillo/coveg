﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class FALTANTESINVENTARIOBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<FALTANTESINVENTARIO_BE> Insert(FALTANTESINVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO sp = new Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.FALTANTESINVENTARIOID, objBE.ACTIVOID, objBE.DIFERENCIA, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.MOTIVO);

            List<FALTANTESINVENTARIO_BE> lstRes = new List<FALTANTESINVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<FALTANTESINVENTARIO_BE>(dt, lstRes, new FALTANTESINVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<FALTANTESINVENTARIO_BE> Update(FALTANTESINVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO sp = new Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.FALTANTESINVENTARIOID, objBE.ACTIVOID, objBE.DIFERENCIA, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.MOTIVO);

            List<FALTANTESINVENTARIO_BE> lstRes = new List<FALTANTESINVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<FALTANTESINVENTARIO_BE>(dt, lstRes, new FALTANTESINVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(FALTANTESINVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO sp = new Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.FALTANTESINVENTARIOID, objBE.ACTIVOID, objBE.DIFERENCIA, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.MOTIVO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<FALTANTESINVENTARIO_BE> SelectAll(FALTANTESINVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO sp = new Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.FALTANTESINVENTARIOID, objBE.ACTIVOID, objBE.DIFERENCIA, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.MOTIVO);

            List<FALTANTESINVENTARIO_BE> lstRes = new List<FALTANTESINVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<FALTANTESINVENTARIO_BE>(dt, lstRes, new FALTANTESINVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<FALTANTESINVENTARIO_BE> SearchKey(FALTANTESINVENTARIO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO sp = new Coveg.DataAccess.Procedures.spFALTANTESINVENTARIO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.FALTANTESINVENTARIOID, objBE.ACTIVOID, objBE.DIFERENCIA, objBE.FECHAHORA, objBE.INVENTARIOFISICOID, objBE.MOTIVO);

            List<FALTANTESINVENTARIO_BE> lstRes = new List<FALTANTESINVENTARIO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<FALTANTESINVENTARIO_BE>(dt, lstRes, new FALTANTESINVENTARIO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
