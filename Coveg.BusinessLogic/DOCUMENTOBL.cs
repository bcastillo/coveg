﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class DOCUMENTOBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DOCUMENTO_BE> Insert(DOCUMENTO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDOCUMENTO sp = new DataAccess.Procedures.spDOCUMENTO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.DOCUMENTOID, objBE.ACTIVOID, objBE.AREA, objBE.CAJA, objBE.ESTANTE, objBE.FECHA_INGRESO, objBE.LEFFORT, objBE.TIPO);

            List<DOCUMENTO_BE> lstRes = new List<DOCUMENTO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DOCUMENTO_BE>(dt, lstRes, new DOCUMENTO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>

        public List<DOCUMENTO_BE> Update(DOCUMENTO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDOCUMENTO sp = new DataAccess.Procedures.spDOCUMENTO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.DOCUMENTOID, objBE.ACTIVOID, objBE.AREA, objBE.CAJA, objBE.ESTANTE, objBE.FECHA_INGRESO, objBE.LEFFORT, objBE.TIPO);

            List<DOCUMENTO_BE> lstRes = new List<DOCUMENTO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DOCUMENTO_BE>(dt, lstRes, new DOCUMENTO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(DOCUMENTO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDOCUMENTO sp = new DataAccess.Procedures.spDOCUMENTO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.DOCUMENTOID, objBE.ACTIVOID, objBE.AREA, objBE.CAJA, objBE.ESTANTE, objBE.FECHA_INGRESO, objBE.LEFFORT, objBE.TIPO);

            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<DOCUMENTO_BE> SelectAll(DOCUMENTO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDOCUMENTO sp = new DataAccess.Procedures.spDOCUMENTO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.DOCUMENTOID, objBE.ACTIVOID, objBE.AREA, objBE.CAJA, objBE.ESTANTE, objBE.FECHA_INGRESO, objBE.LEFFORT, objBE.TIPO);

            List<DOCUMENTO_BE> lstRes = new List<DOCUMENTO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DOCUMENTO_BE>(dt, lstRes, new DOCUMENTO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<DOCUMENTO_BE> SearchKey(DOCUMENTO_BE objBE)
        {
            Coveg.DataAccess.Procedures.spDOCUMENTO sp = new DataAccess.Procedures.spDOCUMENTO();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.DOCUMENTOID, objBE.ACTIVOID, objBE.AREA, objBE.CAJA, objBE.ESTANTE, objBE.FECHA_INGRESO, objBE.LEFFORT, objBE.TIPO);
            List<DOCUMENTO_BE> lstRes = new List<DOCUMENTO_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<DOCUMENTO_BE>(dt, lstRes, new DOCUMENTO_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        #endregion
    }
}
