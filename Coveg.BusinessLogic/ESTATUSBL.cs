﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;


namespace Coveg.BusinessLogic
{
    public class ESTATUSBL
    {
        #region Funciones

        /// <summary>
        /// Consulta lista de estatus para caralogo
        /// </summary>
        /// <returns></returns>
        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            ESTATUS_BE objBE = new ESTATUS_BE();
            Coveg.DataAccess.Procedures.spESTATUS sp = new Coveg.DataAccess.Procedures.spESTATUS();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.ESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE, objBE.TIPOESTATUSID, objBE.VALOR);

            List<ESTATUS_BE> lstRes = new List<ESTATUS_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<ESTATUS_BE>(dt, lstRes, new ESTATUS_BE());
                else
                    lstRes = null;

                if (lstRes != null)
                    lstRes = lstRes.FindAll(p => p.NOMBRE_TIPOESTATUS == strTipoEstatus);
            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

            /// <summary>
            /// Inserar registro
            /// </summary>
            /// <param name="objEstatus"></param>
            /// <returns></returns>
            public List<ESTATUS_BE> Insert(ESTATUS_BE objBE)
            {
                Coveg.DataAccess.Procedures.spESTATUS sp = new Coveg.DataAccess.Procedures.spESTATUS();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.ESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE, objBE.TIPOESTATUSID, objBE.VALOR);

                List<ESTATUS_BE> lstRes = new List<ESTATUS_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<ESTATUS_BE>(dt, lstRes, new ESTATUS_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }

            /// <summary>
            /// Actualizar registro
            /// </summary>
            /// <param name="objEstatus"></param>
            /// <returns></returns>
            public List<ESTATUS_BE> Update(ESTATUS_BE objBE)
            {
                Coveg.DataAccess.Procedures.spESTATUS sp = new Coveg.DataAccess.Procedures.spESTATUS();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.ESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE, objBE.TIPOESTATUSID, objBE.VALOR);

                List<ESTATUS_BE> lstRes = new List<ESTATUS_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<ESTATUS_BE>(dt, lstRes, new ESTATUS_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }


            /// <summary>
            /// Eliminar registro
            /// </summary>
            /// <param name="objEstatus"></param>
            /// <returns></returns>
            public bool Delete(ESTATUS_BE objBE)
            {
                Coveg.DataAccess.Procedures.spESTATUS sp = new Coveg.DataAccess.Procedures.spESTATUS();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.ESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE, objBE.TIPOESTATUSID, objBE.VALOR);

                bool bolRes = false;

                try
                {
                    int intRes = sp.Execute();
                    bolRes = true;

                }
                catch
                {
                    bolRes = false;
                }
                return bolRes;
            }

            /// <summary>
            /// Consulta todos los registros de la tabla
            /// </summary>
            /// <param name="objEstatus"></param>
            /// <returns></returns>
            public List<ESTATUS_BE> SelectAll(ESTATUS_BE objBE)
            {
                Coveg.DataAccess.Procedures.spESTATUS sp = new Coveg.DataAccess.Procedures.spESTATUS();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.ESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE, objBE.TIPOESTATUSID, objBE.VALOR);

                List<ESTATUS_BE> lstRes = new List<ESTATUS_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<ESTATUS_BE>(dt, lstRes, new ESTATUS_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }

            /// <summary>
            /// Buscar por llave de la tabla
            /// </summary>
            /// <param name="objBE"></param>
            /// <returns></returns>
            public List<ESTATUS_BE> SearchKey(ESTATUS_BE objBE)
            {
                Coveg.DataAccess.Procedures.spESTATUS sp = new Coveg.DataAccess.Procedures.spESTATUS();
                sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.ESTATUSID, objBE.DESCRIPCION, objBE.NOMBRE, objBE.TIPOESTATUSID, objBE.VALOR);

                List<ESTATUS_BE> lstRes = new List<ESTATUS_BE>();

                try
                {
                    System.Data.DataTable dt = new System.Data.DataTable();
                    dt = sp.ToDataTable();

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                        F.Runtime.Reflection.Reflecter.DataTableToList<ESTATUS_BE>(dt, lstRes, new ESTATUS_BE());
                    else
                        lstRes = null;

                }
                catch
                {
                    lstRes = null;
                }
                return lstRes;
            }
        #endregion
    }
}
