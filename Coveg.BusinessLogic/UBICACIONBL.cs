﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;


namespace Coveg.BusinessLogic
{
    public class UBICACIONBL
    {
        #region Funciones


        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<UBICACION_BE> Insert(UBICACION_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUBICACION sp = new Coveg.DataAccess.Procedures.spUBICACION();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.UBICACIONID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UBICACION_BE> lstRes = new List<UBICACION_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UBICACION_BE>(dt, lstRes, new UBICACION_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<UBICACION_BE> Update(UBICACION_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUBICACION sp = new Coveg.DataAccess.Procedures.spUBICACION();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.UBICACIONID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UBICACION_BE> lstRes = new List<UBICACION_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UBICACION_BE>(dt, lstRes, new UBICACION_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(UBICACION_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUBICACION sp = new Coveg.DataAccess.Procedures.spUBICACION();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.UBICACIONID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);
            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<UBICACION_BE> SelectAll(UBICACION_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUBICACION sp = new Coveg.DataAccess.Procedures.spUBICACION();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.UBICACIONID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UBICACION_BE> lstRes = new List<UBICACION_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UBICACION_BE>(dt, lstRes, new UBICACION_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<UBICACION_BE> SearchKey(UBICACION_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUBICACION sp = new Coveg.DataAccess.Procedures.spUBICACION();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.UBICACIONID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UBICACION_BE> lstRes = new List<UBICACION_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UBICACION_BE>(dt, lstRes, new UBICACION_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
