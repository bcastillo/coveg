﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.BusinessLogic
{
    public class UNIDADBL
    {
        #region Funciones

        /// <summary>
        /// Inserar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<UNIDAD_BE> Insert(UNIDAD_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUNIDAD sp = new Coveg.DataAccess.Procedures.spUNIDAD();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.INSERTRECORD, objBE.UNIDADID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UNIDAD_BE> lstRes = new List<UNIDAD_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UNIDAD_BE>(dt, lstRes, new UNIDAD_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Actualizar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<UNIDAD_BE> Update(UNIDAD_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUNIDAD sp = new Coveg.DataAccess.Procedures.spUNIDAD();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.UPDATERECORD, objBE.UNIDADID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UNIDAD_BE> lstRes = new List<UNIDAD_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UNIDAD_BE>(dt, lstRes, new UNIDAD_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }


        /// <summary>
        /// Eliminar registro
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public bool Delete(UNIDAD_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUNIDAD sp = new Coveg.DataAccess.Procedures.spUNIDAD();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.DELETERECORD, objBE.UNIDADID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);
            bool bolRes = false;

            try
            {
                int intRes = sp.Execute();
                bolRes = true;
            }
            catch
            {
                bolRes = false;
            }
            return bolRes;
        }

        /// <summary>
        /// Consulta todos los registros de la tabla
        /// </summary>
        /// <param name="objEstatus"></param>
        /// <returns></returns>
        public List<UNIDAD_BE> SelectAll(UNIDAD_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUNIDAD sp = new Coveg.DataAccess.Procedures.spUNIDAD();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.LISTRECORDS, objBE.UNIDADID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UNIDAD_BE> lstRes = new List<UNIDAD_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UNIDAD_BE>(dt, lstRes, new UNIDAD_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }

        /// <summary>
        /// Buscar por llave de la tabla
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<UNIDAD_BE> SearchKey(UNIDAD_BE objBE)
        {
            Coveg.DataAccess.Procedures.spUNIDAD sp = new Coveg.DataAccess.Procedures.spUNIDAD();
            sp.Prepare((int)Coveg.DataAccess.Enums.DBAction.SEARCHBYKEY, objBE.UNIDADID, objBE.DESCRIPCION, objBE.ESTATUS, objBE.NOMBRE);

            List<UNIDAD_BE> lstRes = new List<UNIDAD_BE>();

            try
            {
                System.Data.DataTable dt = new System.Data.DataTable();
                dt = sp.ToDataTable();

                if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    F.Runtime.Reflection.Reflecter.DataTableToList<UNIDAD_BE>(dt, lstRes, new UNIDAD_BE());
                else
                    lstRes = null;

            }
            catch
            {
                lstRes = null;
            }
            return lstRes;
        }
        #endregion
    }
}
