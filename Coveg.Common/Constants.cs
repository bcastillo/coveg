﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;

namespace Coveg.Common
{
    public class Constantes
    {

        //CONSTANTES DE TIPOS DE ESTATUS--------------------
        public const String TE_ACTIVO = "ESTATUS DE ACTIVO";
        public const String TE_PERMISO = "ESTATUS DE PERMISO";
        public const String TE_RFIDTAG = "ESTATUS DE RFIDTAG";
        public const String TE_ROL = "ESTATUS DE ROLES";
        public const String TE_TIPO_ACTIVO = "ESTATUS DE TIPO ACTIVO";
        public const String TE_UBICACION = "ESTATUS DE UBICACION";
        public const String TE_UNIDAD = "ESTATUS DE UNIDAD";
        public const String TE_SOLICITUD = "SOLICITUD";

        public const Int32 TA_DOCUMENTO = 1;
        public const Int32 TA_AUTOMOVIL = 2;
        public const Int32 TA_OTRO = 3;

        public const Int32 EST_SOL_DOCUMENTO = 10;
        public const Int32 EST_SOL_AUTOMOVIL = 11;
        public const Int32 EST_SOL_OTRO = 1;


        public const String TAD_DOCUMENTO = "DOCUMENTO";
        public const String TAD_AUTOMOVIL = "AUTO";

        //----------------------------------------------------
        
    }
}
