﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Web;
using System.IO;
using System.Net.Mime;
using System.Net.Mail;
using Coveg.Entities;

namespace Coveg.Common.Notifications
{
    public class Mail
    {
        #region Public Members

        public String Host { get; set; }
        public String User { get; set; }
        public String Password { get; set; }
        public String Domain { get; set; }
        public System.Net.Mail.MailMessage Correo { get; set; }
        public System.Net.Mail.SmtpClient Smtp { get; set; }
        public System.Net.NetworkCredential Credetials { get; set; }
        public String Bcc { get; set; }
        #endregion

        #region Constructor
        public Mail()
        {

            Host = ConfigurationManager.AppSettings["Host"];
            User = ConfigurationManager.AppSettings["User"];
            Password = ConfigurationManager.AppSettings["Password"];
            Domain = ConfigurationManager.AppSettings["Domain"];


            if (Host != string.Empty)
                Smtp = new System.Net.Mail.SmtpClient()
                {
                    Host = Host,
                    Port = 25
                };

            if (User != string.Empty && Password != string.Empty && Domain != string.Empty)
            {
                Credetials = new System.Net.NetworkCredential()
                {
                    UserName = User,
                    Password = Password,
                    Domain = Domain
                };

                Smtp.Credentials = Credetials;
            }
        }
        #endregion

        #region Public Methods
        //Values
        /// <param name="From">Correo origen de la notificación</param>
        /// <param name="To">Persona(s) a las que se les envía la notificación</param>
        /// <param name="CC">Copia de la notificación</param>
        /// <param name="Subject">Titulo del correo, con el número de folio recien creado</param>
        /// <param name="Message">Cuerpo del correo</param>
        /// 
        public void SendMail(String From, String To, String Cc, String Subject, String Message)
        {
            try
            {
                Correo = new System.Net.Mail.MailMessage();

                Correo.From = new System.Net.Mail.MailAddress(From.Trim());

                if (To != String.Empty)
                {
                    String[] Adress = To.Split(';');
                    foreach (String dir in Adress)
                    {
                        if (dir.Trim().Length > 0)
                            Correo.To.Add(dir.Trim());
                    }
                }

                if (Cc != String.Empty)
                {
                    String[] Adress = Cc.Split(';');
                    foreach (String dir in Adress)
                    {
                        if (dir.Trim().Length > 0)
                            Correo.CC.Add(dir.Trim());
                    }
                }


                //Subject
                Correo.Subject = Subject.Trim();

                //Body
                Correo.Body = Message.Trim();

                Correo.Priority = System.Net.Mail.MailPriority.High;

                //Send
                Smtp.Send(Correo);

            }
            catch (System.Net.Mail.SmtpException e)
            {
                throw e;
            }



        }

        /// <summary>
        /// Envia Correo Electronico con Formato en HTML
        /// </summary>
        /// <param name="mail" Type="Shell.DATA_MAIL" >Entidad Data_Mail</param>
        /// <returns>Enviado=True</returns>
        public Boolean SendMailWithFormat(MailStructure mail)
        {
            bool result = false;
            try
            {
                Correo = new System.Net.Mail.MailMessage();

                Correo.From = new System.Net.Mail.MailAddress(mail.From.Trim());

                if (mail.To != String.Empty)
                {
                    String[] Adress = mail.To.Split(';');
                    foreach (String dir in Adress)
                    {
                        if (dir.Trim().Length > 0)
                            Correo.To.Add(dir.Trim());
                    }
                }

                if (mail.Cc != String.Empty)
                {
                    String[] Adress = mail.Cc.Split(';');
                    foreach (String dir in Adress)
                    {
                        if (dir.Trim().Length > 0)
                            Correo.CC.Add(dir.Trim());
                    }
                }

                //Subject
                Correo.Subject = mail.Subject.Trim();

                // Añade la imagen desde un path local
                LinkedResource resLogoMabe = new LinkedResource(ConfigurationManager.AppSettings["SharedPath"].ToString() + "logo.png");
                resLogoMabe.ContentId = ("Logo").ToLower();
                resLogoMabe.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;


                LinkedResource resWarning = new LinkedResource(ConfigurationManager.AppSettings["SharedPath"].ToString() + "msgExclamation.png");
                resWarning.ContentId = ("resWarning").ToLower();
                resWarning.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;

                AlternateView htmlView = AlternateView.CreateAlternateViewFromString("<html>" + mail.Message + "</html>", null, "text/html");

                htmlView.LinkedResources.Add(resLogoMabe);
                htmlView.LinkedResources.Add(resWarning);

                Correo.AlternateViews.Add(htmlView);
                Correo.IsBodyHtml = true;
                Correo.Priority = System.Net.Mail.MailPriority.High;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["User"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                Smtp.Credentials = credentials;

                //Send
                Smtp.Send(Correo);
                result = true;
            }
            catch (Exception e)
            {
                result = false;

            }

            return result;
        }
        #endregion
    }
}
