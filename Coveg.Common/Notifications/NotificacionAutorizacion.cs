﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using Coveg.Common.Notifications;
using System.IO;

namespace Coveg.Common.Notifications
{
    public class NotificacionAutorizacion
    {
        public static bool CorreoAutorizacion(String To, String Cc, String strNOSOLICITUD, String strAUTORIZADOR, String strPUESTO, String strACTIVOSSOLLICITADOS)
        {
            String strTipoFolioVisible = String.Empty;
            bool result = false;
            try
            {
                string body = string.Empty;
                using (StreamReader reader = new StreamReader(System.Configuration.ConfigurationManager.AppSettings["RutaCorreos"].ToString() + "CorreoAutorizacion.htm"))
                {
                    body = reader.ReadToEnd();
                }

                Coveg.Entities.MailStructure DatosMail = new Coveg.Entities.MailStructure();
                DatosMail.From = System.Configuration.ConfigurationManager.AppSettings["From"].ToString();
                DatosMail.To = To;
                DatosMail.Cc = Cc;
                DatosMail.Subject = "Autorización de Salida";


                DatosMail.Message = body;

                //Valores a remplazar
                DatosMail.Message = DatosMail.Message.Replace("NOSOLICITUD", strNOSOLICITUD.ToUpper() ?? String.Empty);
                DatosMail.Message = DatosMail.Message.Replace("AUTORIZADOR", strAUTORIZADOR.ToUpper() ?? String.Empty);
                DatosMail.Message = DatosMail.Message.Replace("PUESTO", strPUESTO ?? String.Empty);
                DatosMail.Message = DatosMail.Message.Replace("ACTIVOSSOLLICITADOS", strACTIVOSSOLLICITADOS.ToUpper() ?? String.Empty);

                var mail = new Coveg.Common.Notifications.Mail();

                result = mail.SendMailWithFormat(DatosMail);

            }
            catch 
            {
                result = false;
            }
            return result;
        }
    }
}
