﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.DataAccess.Enums
{
    public enum DBAction : int
    {
        /// <summary>
        ///INSERTA UN REGISTRO 
        /// </summary>
        INSERTRECORD = 1,
        /// <summary>
        ///ACTUALIZA UN REGISTRO 
        /// </summary>
        UPDATERECORD = 2,
        /// <summary>
        ///BORRA UN REGISTRO 
        /// </summary>
        DELETERECORD = 3,
        /// <summary>
        ///ENLISTA REGISTROS 
        /// </summary>
        LISTRECORDS = 4,
        /// <summary>
        ///BUSCA POR LLAVE PRIMARIA UN REGISTRO 
        /// </summary>
        SEARCHBYKEY = 5,

        /// <summary>
        /// Activos de una solicitud
        /// </summary>
        ACTIVOS_SOLICITUD = 6,

        SOLICITUDES_RESGUARDO = 6,

        AUTORIZA_RESGUARDO = 6,

        RECHAZA_RESGUARDO = 7,

        SOLICITUDES_JEFE = 7,

        SOLICITUDES_RESGUARDO_DOCTOS = 9,

        SOLICITUDES_RESP_AUTOS = 10,

        AUTORIZA_JEFE = 8,

        RECHAZA_JEFE = 9,

        SOLICITUDES_DIRECTOR = 8,

        AUTORIZA_DIRECTOR = 10,

        RECHAZA_DIRECTOR = 11,

        AUTORIZA_ARCHIVO = 12,

        RECHAZA_ARCHIVO = 13,

        AUTORIZA_AUTO = 14,

        RECHAZA_AUTO = 15,

        /// <summary>
        /// Consulta la tabla de activos que son tipo vehiculos
        /// </summary>
        ACTIVO_VEHICULO = 6,

        /// <summary>
        /// Insertar actito tipo vehiculo
        /// </summary>
        INSERT_VEHICULO = 7,

        /// <summary>
        /// Consulta activos para llenar control de busqueda
        /// </summary>
        SELECT_ACTIVOSSOLICITUD = 8,

        /// <summary>
        /// Insertar registro en activo para tipo documento
        /// </summary>
        INSERT_DOCUMENTO = 9,

        /// <summary>
        /// Consulta activos para llenar control de busqueda tipo vehiculos
        /// </summary>
        SELECT_ACTIVOSSOLICITUDVEHICULOS = 10,

        /// <summary>
        /// Consulta activos para llenar control de busqueda tipo documentos
        /// </summary>
        SELECT_ACTIVOSSOLICITUDDOCTOS = 11,

        /// <summary>
        /// Solicitudes por usuario para pagina de inicio
        /// </summary>
        SELECT_SOLICITUDESUSUARIONODEVUELTAS = 11,

        /// <summary>
        /// Accion para consultar información de activo para entrada
        /// </summary>
        CONSULTA_DATOS_ACTIVO_ENTRADA = 1,

        /// <summary>
        /// Acción para consultar información de una solicitud
        /// </summary>
        CONSULTA_DATOS_SOLICITUD_ENTRADA = 2,

        /// <summary>
        /// Acciones de reporte historico de solicitudes
        /// </summary>
        HISTORICO_RECHAZADO = 1,
        HISTORICO_AUTORIZADO = 2,
        HISTORICO_DEVUELTO = 3,
        HISTORICO_CANCELADO = 4,
        /// <summary>
        /// Consulta información de una solicitud y los activos que la componen
        /// </summary>
        SELECT_INFO_SOLICITUD_ACTIVOS=12,

        /// <summary>
        /// Acciones de consultas de pendientes
        /// </summary>
        SELECT_PENDIENTES_SALIR	=1,
	    SELECT_FUERA_NO_VENCIDOS =2,
		SELECT_VENCIDOS =3,
        FINALIZAR   = 35
        

    }
}
