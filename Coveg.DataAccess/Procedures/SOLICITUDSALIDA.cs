namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spSOLICITUDSALIDA:spBase
    {
        public spSOLICITUDSALIDA() {
           base.Name = "USP_SOLICITUDSALIDA";
        }
        public void Prepare(object ACTION, object SOLICITUDSALIDAID, object ESTATUS, object FECHA, object MOTIVO, object USUARIO,object IDJEFEINMEDIATO,object FECHASALIDA,object FECHARETORNO)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(SOLICITUDSALIDAID))
            {
                Parameters.Add("SOLICITUDSALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SOLICITUDSALIDAID", DbType.Int32, SOLICITUDSALIDAID));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if ((DateTime)FECHA != new DateTime())
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }

            if ((DateTime)FECHASALIDA != new DateTime())
            {
                Parameters.Add("FECHASALIDA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHASALIDA", DbType.DateTime, FECHASALIDA));
            }

            if ((DateTime)FECHARETORNO != new DateTime())
            {
                Parameters.Add("FECHARETORNO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHARETORNO", DbType.DateTime, FECHARETORNO));
            }

            if (F.Text.Input.IsNotNull(MOTIVO))
            {
                Parameters.Add("MOTIVO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@MOTIVO", DbType.String, MOTIVO));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }
            if (F.Text.Input.IsNotNull(IDJEFEINMEDIATO))
            {
                Parameters.Add("IDJEFEINMEDIATO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@IDJEFEINMEDIATO", DbType.Int32, IDJEFEINMEDIATO));
            }
        }
    }
}