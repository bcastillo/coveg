namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spDETALLEENTRADA : spBase
    {
        public spDETALLEENTRADA()
        {
            base.Name = "USP_DETALLEENTRADA";
        }
        public void Prepare(object ACTION, object DETALLEENTRADAID, object ACTIVOID, object CANTIDAD, object ENTRADAID, object FECHA, object USUARIO)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(DETALLEENTRADAID))
            {
                Parameters.Add("DETALLEENTRADAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DETALLEENTRADAID", DbType.Int32, DETALLEENTRADAID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(CANTIDAD))
            {
                Parameters.Add("CANTIDAD", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CANTIDAD", DbType.Int32, CANTIDAD));
            }
            if (F.Text.Input.IsNotNull(ENTRADAID))
            {
                Parameters.Add("ENTRADAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ENTRADAID", DbType.Int32, ENTRADAID));
            }
            if (F.Text.Input.IsNotNull(FECHA))
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }

        }
    }
}