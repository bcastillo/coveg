namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spDETALLESALIDA : spBase
    {
        public spDETALLESALIDA()
        {
            base.Name = "USP_DETALLESALIDA";
        }
        public void Prepare(object ACTION, object DETALLESALIDAID, object ACTIVOID, object CANTIDAD, object FECHA, object SALIDAID, object USUARIO
 )
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(DETALLESALIDAID))
            {
                Parameters.Add("DETALLESALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DETALLESALIDAID", DbType.Int32, DETALLESALIDAID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(CANTIDAD))
            {
                Parameters.Add("CANTIDAD", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CANTIDAD", DbType.Int32, CANTIDAD));
            }
            if (F.Text.Input.IsNotNull(FECHA))
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }
            if (F.Text.Input.IsNotNull(SALIDAID))
            {
                Parameters.Add("SALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SALIDAID", DbType.Int32, SALIDAID));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }

        }
    }
}