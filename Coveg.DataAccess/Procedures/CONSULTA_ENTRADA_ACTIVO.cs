﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spCONSULTA_ENTRADA_ACTIVO : spBase
    {
        public spCONSULTA_ENTRADA_ACTIVO()
        {
            base.Name = "USP_CONSULTA_ENTRADA_ACTIVO";
        }

        public void Prepare(object ACTION ,object CLAVE, object TAG,object ACTIVOID)
        {
            Parameters.Clear();

            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }

            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.String, ACTIVOID));
            }

            if (F.Text.Input.IsNotNull(CLAVE))
            {
                Parameters.Add("CLAVE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CLAVE", DbType.String, CLAVE));
            }

            if (F.Text.Input.IsNotNull(TAG))
            {
                Parameters.Add("TAG", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TAG", DbType.String, TAG));
            }

        }
    }
}
