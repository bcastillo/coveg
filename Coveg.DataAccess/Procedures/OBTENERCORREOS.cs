﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;    

namespace Coveg.DataAccess.Procedures
{
    public class spOBTENERCORREOS : spBase
    {
        public spOBTENERCORREOS() {
           base.Name = "USP_OBTENERCORREOS";
        }
        public void Prepare(object SOLICITUDSALIDAID)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(SOLICITUDSALIDAID))
            {
                Parameters.Add("SOLICITUDSALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SOLICITUDSALIDAID", DbType.Int32, SOLICITUDSALIDAID));
            }
        }
    }
}
