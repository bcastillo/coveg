namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spENTRADA : spBase
    {
        public spENTRADA()
        {
            base.Name = "USP_ENTRADA";
        }
        public void Prepare(object ACTION, object ENTRADAID, object FECHA, object USUARIO_ENTREGA)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(ENTRADAID))
            {
                Parameters.Add("ENTRADAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ENTRADAID", DbType.Int32, ENTRADAID));
            }
            if (F.Text.Input.IsNotNull(FECHA))
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }
            if (F.Text.Input.IsNotNull(USUARIO_ENTREGA))
            {
                Parameters.Add("USUARIO_ENTREGA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO_ENTREGA", DbType.String, USUARIO_ENTREGA));
            }

        }
    }
}