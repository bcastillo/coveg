namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spDETALLEINVENTARIOFISICO : spBase
    {
        public spDETALLEINVENTARIOFISICO()
        {
            base.Name = "USP_DETALLEINVENTARIOFISICO";
        }
        public void Prepare(object ACTION, object DETALLEINVENTARIOFISICOID, object ACTIVOID, object CANTIDAD, object FECHAHORA, object INVENTARIOFISICOID, object USUARIO
 )
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(DETALLEINVENTARIOFISICOID))
            {
                Parameters.Add("DETALLEINVENTARIOFISICOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DETALLEINVENTARIOFISICOID", DbType.Int32, DETALLEINVENTARIOFISICOID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(CANTIDAD))
            {
                Parameters.Add("CANTIDAD", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CANTIDAD", DbType.Int32, CANTIDAD));
            }
            if (F.Text.Input.IsNotNull(FECHAHORA))
            {
                Parameters.Add("FECHAHORA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHAHORA", DbType.DateTime, FECHAHORA));
            }
            if (F.Text.Input.IsNotNull(INVENTARIOFISICOID))
            {
                Parameters.Add("INVENTARIOFISICOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@INVENTARIOFISICOID", DbType.Int32, INVENTARIOFISICOID));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }

        }
    }
}