﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spVEHICULO : spBase
    {
        public spVEHICULO()
        {
            base.Name = "USP_VEHICULO";
        }

        public void Prepare(object ACTION,object VEHICULOID, object ACTIVOID, object CAPACIDAD, object CILINDROS, object CLAVE, object COLOR, object MARCA, object MODELO,  object NUMERO_MOTOR, object NUMERO_SERIE, object PLACAS, object PUERTAS, object TIPO, object TRANSMISION)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(VEHICULOID))
            {
                Parameters.Add("VEHICULOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@VEHICULOID", DbType.Int32, VEHICULOID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(CAPACIDAD))
            {
                Parameters.Add("CAPACIDAD", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CAPACIDAD", DbType.String, CAPACIDAD));
            }
            if (F.Text.Input.IsNotNull(CILINDROS))
            {
                Parameters.Add("CILINDROS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CILINDROS", DbType.Int32, CILINDROS));
            }
            if (F.Text.Input.IsNotNull(COLOR))
            {
                Parameters.Add("COLOR", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@COLOR", DbType.String, COLOR));
            }
            if (F.Text.Input.IsNotNull(MARCA))
            {
                Parameters.Add("MARCA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@MARCA", DbType.String, MARCA));
            }
            if (F.Text.Input.IsNotNull(MODELO))
            {
                Parameters.Add("MODELO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@MODELO", DbType.String, MODELO));
            }
            if (F.Text.Input.IsNotNull(NUMERO_MOTOR))
            {
                Parameters.Add("NUMERO_MOTOR", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NUMERO_MOTOR", DbType.String, NUMERO_MOTOR));
            }
            if (F.Text.Input.IsNotNull(NUMERO_SERIE))
            {
                Parameters.Add("NUMERO_SERIE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NUMERO_SERIE", DbType.String, NUMERO_SERIE));
            }
            if (F.Text.Input.IsNotNull(PLACAS))
            {
                Parameters.Add("PLACAS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@PLACAS", DbType.String, PLACAS));
            }
            if (F.Text.Input.IsNotNull(PUERTAS))
            {
                Parameters.Add("PUERTAS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@PUERTAS", DbType.Int32, PUERTAS));
            }
            if (F.Text.Input.IsNotNull(TIPO))
            {
                Parameters.Add("TIPO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TIPO", DbType.String, TIPO));
            }
            if (F.Text.Input.IsNotNull(TRANSMISION))
            {
                Parameters.Add("TRANSMISION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TRANSMISION", DbType.String, TRANSMISION));
            }

        }


    }
}
