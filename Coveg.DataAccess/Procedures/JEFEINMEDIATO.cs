﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;


namespace Coveg.DataAccess.Procedures
{
    public class spJEFEINMEDIATO : spBase
    {
        public spJEFEINMEDIATO()
        {
            base.Name = "sp_jefe_inmediato";
        }

        public void Prepare(object id_usuario)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(id_usuario))
            {
                Parameters.Add("id_usuario", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@id_usuario", DbType.Int32, id_usuario));
            }
        }
    }
}
