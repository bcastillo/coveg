namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spAUTORIZACIONSALIDA : spBase
    {
        public spAUTORIZACIONSALIDA()
        {
            base.Name = "USP_AUTORIZACIONSALIDA";
        }
        public void Prepare(object ACTION, object AUTORIZACIONSALIDAID, object AUTORIZO, object COMENTARIOS, object ESTATUS, object FECHAREVISION, object SOLICITUDSALIDAID, object TIPOAUTORIZADORID)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(AUTORIZACIONSALIDAID))
            {
                Parameters.Add("AUTORIZACIONSALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@AUTORIZACIONSALIDAID", DbType.Int32, AUTORIZACIONSALIDAID));
            }
            if (F.Text.Input.IsNotNull(AUTORIZO))
            {
                Parameters.Add("AUTORIZO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@AUTORIZO", DbType.String, AUTORIZO));
            }
            if (F.Text.Input.IsNotNull(COMENTARIOS))
            {
                Parameters.Add("COMENTARIOS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@COMENTARIOS", DbType.String, COMENTARIOS));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (F.Text.Input.IsNotNull(FECHAREVISION))
            {
                Parameters.Add("FECHAREVISION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHAREVISION", DbType.DateTime, FECHAREVISION));
            }
            if (F.Text.Input.IsNotNull(SOLICITUDSALIDAID))
            {
                Parameters.Add("SOLICITUDSALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SOLICITUDSALIDAID", DbType.Int32, SOLICITUDSALIDAID));
            }
            if (F.Text.Input.IsNotNull(TIPOAUTORIZADORID))
            {
                Parameters.Add("TIPOAUTORIZADORID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TIPOAUTORIZADORID", DbType.Int32, TIPOAUTORIZADORID));
            }

        }
    }
}