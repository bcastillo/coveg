namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spACTIVO : spBase
    {
        public spACTIVO()
        {
            base.Name = "USP_ACTIVO";
        }
        public void Prepare(object ACTION, object ACTIVOID, object CLAVE, object DESCRIPCION, object ESTATUS, object NOMBRE, object NUMEROSERIE, object NUMNOMINA, object REQUIERE_SOLICITUD, object RFIDTAGID, object TIPOACTIVOID, object UNIDADID
 )
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(CLAVE))
            {
                Parameters.Add("CLAVE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CLAVE", DbType.String, CLAVE));
            }
            if (F.Text.Input.IsNotNull(DESCRIPCION))
            {
                Parameters.Add("DESCRIPCION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DESCRIPCION", DbType.String, DESCRIPCION));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (F.Text.Input.IsNotNull(NOMBRE))
            {
                Parameters.Add("NOMBRE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NOMBRE", DbType.String, NOMBRE));
            }
            if (F.Text.Input.IsNotNull(NUMEROSERIE))
            {
                Parameters.Add("NUMEROSERIE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NUMEROSERIE", DbType.String, NUMEROSERIE));
            }
            if (F.Text.Input.IsNotNull(NUMNOMINA))
            {
                Parameters.Add("NUMNOMINA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NUMNOMINA", DbType.String, NUMNOMINA));
            }
            if (F.Text.Input.IsNotNull(REQUIERE_SOLICITUD))
            {
                Parameters.Add("REQUIERE_SOLICITUD", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@REQUIERE_SOLICITUD", DbType.Boolean, REQUIERE_SOLICITUD));
            }
            if (F.Text.Input.IsNotNull(RFIDTAGID))
            {
                Parameters.Add("RFIDTAGID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@RFIDTAGID", DbType.Int32, RFIDTAGID));
            }
            if (F.Text.Input.IsNotNull(TIPOACTIVOID))
            {
                Parameters.Add("TIPOACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TIPOACTIVOID", DbType.Int32, TIPOACTIVOID));
            }
            if (F.Text.Input.IsNotNull(UNIDADID))
            {
                Parameters.Add("UNIDADID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@UNIDADID", DbType.Int32, UNIDADID));
            }

        }
    }
}