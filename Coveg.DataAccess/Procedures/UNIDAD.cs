namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spUNIDAD:spBase
    {
        public spUNIDAD() {
           base.Name = "USP_UNIDAD";
        }
        public void Prepare(object ACTION, object UNIDADID, object DESCRIPCION, object ESTATUS, object NOMBRE)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(UNIDADID))
            {
                Parameters.Add("UNIDADID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@UNIDADID", DbType.Int32, UNIDADID));
            }
            if (F.Text.Input.IsNotNull(DESCRIPCION))
            {
                Parameters.Add("DESCRIPCION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DESCRIPCION", DbType.String, DESCRIPCION));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (F.Text.Input.IsNotNull(NOMBRE))
            {
                Parameters.Add("NOMBRE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NOMBRE", DbType.String, NOMBRE));
            }
        }
    }
}