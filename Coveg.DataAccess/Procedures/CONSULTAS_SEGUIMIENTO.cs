﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spCONSULTAS_SEGUIMIENTO: spBase
    {
        public spCONSULTAS_SEGUIMIENTO()
        {
            base.Name = "USP_CONSULTAS_SEGUIMIENTO";
        }

        public void Prepare(object ACTION)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
        }
    }
}
