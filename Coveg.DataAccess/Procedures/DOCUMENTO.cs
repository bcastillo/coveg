﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spDOCUMENTO : spBase
    {
        public spDOCUMENTO()
        {
            base.Name = "USP_DOCUMENTO";
        }
        public void Prepare(object ACTION, object DOCUMENTOID, object ACTIVOID, object AREA, object CAJA, object ESTANTE, object FECHA_INGRESO, object LEFFORT, object TIPO)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(DOCUMENTOID))
            {
                Parameters.Add("DOCUMENTOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DOCUMENTOID", DbType.Int32, DOCUMENTOID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(AREA))
            {
                Parameters.Add("AREA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@AREA", DbType.String, AREA));
            }
            if (F.Text.Input.IsNotNull(CAJA))
            {
                Parameters.Add("CAJA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CAJA", DbType.String, CAJA));
            }
            if (F.Text.Input.IsNotNull(ESTANTE))
            {
                Parameters.Add("ESTANTE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTANTE", DbType.String, ESTANTE));
            }
            if (Convert.ToDateTime(FECHA_INGRESO)!= new DateTime())
            {
                Parameters.Add("FECHA_INGRESO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA_INGRESO", DbType.DateTime, FECHA_INGRESO));
            }
            if (F.Text.Input.IsNotNull(LEFFORT))
            {
                Parameters.Add("LEFFORT", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@LEFFORT", DbType.String, LEFFORT));
            }
            if (F.Text.Input.IsNotNull(TIPO))
            {
                Parameters.Add("TIPO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TIPO", DbType.String, TIPO));
            }

        }
    }
}
