namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spPARAMETRO:spBase
    {
        public spPARAMETRO() {
           base.Name = "USP_PARAMETRO";
        }
        public void Prepare(object ACTION, object PARAMETROID, object CLAVE, object ESTATUS, object NOMBRE, object VALORNUMERICO, object VALORTEXTO
 )
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(PARAMETROID))
            {
                Parameters.Add("PARAMETROID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@PARAMETROID", DbType.Int32, PARAMETROID));
            }
            if (F.Text.Input.IsNotNull(CLAVE))
            {
                Parameters.Add("CLAVE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CLAVE", DbType.String, CLAVE));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (F.Text.Input.IsNotNull(NOMBRE))
            {
                Parameters.Add("NOMBRE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NOMBRE", DbType.String, NOMBRE));
            }
            if (F.Text.Input.IsNotNull(VALORNUMERICO))
            {
                Parameters.Add("VALORNUMERICO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@VALORNUMERICO", DbType.Double, VALORNUMERICO));
            }
            if (F.Text.Input.IsNotNull(VALORTEXTO))
            {
                Parameters.Add("VALORTEXTO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@VALORTEXTO", DbType.String, VALORTEXTO));
            }

        }
    }
}