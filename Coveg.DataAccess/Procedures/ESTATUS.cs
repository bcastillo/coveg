namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spESTATUS : spBase
    {
        public spESTATUS()
        {
            base.Name = "USP_ESTATUS";
        }
        public void Prepare(object ACTION, object ESTATUSID, object DESCRIPCION, object NOMBRE, object TIPOESTATUSID, object VALOR)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(ESTATUSID))
            {
                Parameters.Add("ESTATUSID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUSID", DbType.Int32, ESTATUSID));
            }
            if (F.Text.Input.IsNotNull(DESCRIPCION))
            {
                Parameters.Add("DESCRIPCION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DESCRIPCION", DbType.String, DESCRIPCION));
            }
            if (F.Text.Input.IsNotNull(NOMBRE))
            {
                Parameters.Add("NOMBRE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NOMBRE", DbType.String, NOMBRE));
            }
            if (F.Text.Input.IsNotNull(TIPOESTATUSID))
            {
                Parameters.Add("TIPOESTATUSID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TIPOESTATUSID", DbType.Int32, TIPOESTATUSID));
            }
            if (F.Text.Input.IsNotNull(VALOR))
            {
                Parameters.Add("VALOR", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@VALOR", DbType.Int32, VALOR));
            }

        }
    }
}