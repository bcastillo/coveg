﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spEFECTUAR_ENTRADA: spBase
    {
        public spEFECTUAR_ENTRADA()
        {
            base.Name = "USP_EFECTUAR_ENTRADA";
        }

        public void Prepare(object activoid, object numnomina, object observacion)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(activoid))
            {
                Parameters.Add("activoid", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@activoid", DbType.Int32, activoid));
            }

            if (F.Text.Input.IsNotNull(numnomina))
            {
                Parameters.Add("numnomina", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@numnomina", DbType.String, numnomina));
            }

            if (F.Text.Input.IsNotNull(observacion))
            {
                Parameters.Add("observacion", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@observacion", DbType.String, observacion));
            }
        }

    }
}
