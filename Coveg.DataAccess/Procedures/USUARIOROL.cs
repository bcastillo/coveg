namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spUSUARIOROL:spBase
    {
        public spUSUARIOROL()
        {
           base.Name = "USP_USUARIOROL";
        }
        public void Prepare(object ACTION, object USUARIOROLID, object USUARIO, object CVE_ROL)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(USUARIOROLID))
            {
                Parameters.Add("USUARIOROLID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIOROLID", DbType.Int32, USUARIOROLID));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }
            if (F.Text.Input.IsNotNull(CVE_ROL))
            {
                Parameters.Add("CVE_ROL", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CVE_ROL", DbType.String, CVE_ROL));
            }

        }
    }
}