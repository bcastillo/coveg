namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spRFIDTAG:spBase
    {
        public spRFIDTAG() {
           base.Name = "USP_RFIDTAG";
        }
        public void Prepare(object ACTION, object RFIDTAGID, object DESCRIPCION, object ESTATUS, object FECHAALTA, object TAG)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(RFIDTAGID))
            {
                Parameters.Add("RFIDTAGID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@RFIDTAGID", DbType.Int32, RFIDTAGID));
            }
            if (F.Text.Input.IsNotNull(DESCRIPCION))
            {
                Parameters.Add("DESCRIPCION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DESCRIPCION", DbType.String, DESCRIPCION));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if ((DateTime)FECHAALTA != new DateTime())
            {
                Parameters.Add("FECHAALTA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHAALTA", DbType.DateTime, FECHAALTA));
            }
            if (F.Text.Input.IsNotNull(TAG))
            {
                Parameters.Add("TAG", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TAG", DbType.String, TAG));
            }

        }
    }
}