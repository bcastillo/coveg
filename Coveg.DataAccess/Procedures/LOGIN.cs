﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spLOGIN : spBase
    {
        public spLOGIN()
        {
            base.Name = "sp_login";
        }

        public void Prepare(object ACTION, object USUARIO, object PASSWORD)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }
            if (F.Text.Input.IsNotNull(PASSWORD))
            {
                Parameters.Add("CONTRASENA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CONTRASENA", DbType.String, PASSWORD));
            }
        }
    }
}
