namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spACTIVOSSOLICITADOS : spBase
    {
        public spACTIVOSSOLICITADOS()
        {
            base.Name = "USP_ACTIVOSSOLICITADOS";
        }
        public void Prepare(object ACTION, object ACTIVOSSOLICITADOSID, object ACTIVOID, object CANTIDAD, object ESTATUS, object SOLICITUDSALIDAID
 )
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(ACTIVOSSOLICITADOSID))
            {
                Parameters.Add("ACTIVOSSOLICITADOSID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOSSOLICITADOSID", DbType.Int32, ACTIVOSSOLICITADOSID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(CANTIDAD))
            {
                Parameters.Add("CANTIDAD", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CANTIDAD", DbType.Int32, CANTIDAD));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (F.Text.Input.IsNotNull(SOLICITUDSALIDAID))
            {
                Parameters.Add("SOLICITUDSALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SOLICITUDSALIDAID", DbType.Int32, SOLICITUDSALIDAID));
            }

        }
    }
}