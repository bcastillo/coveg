namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spINVENTARIO : spBase
    {
        public spINVENTARIO()
        {
            base.Name = "USP_INVENTARIO";
        }
        public void Prepare(object ACTION, object INVENTARIOID, object ACTIVOID, object FECHA, object SALDO, object UBICACIONID)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(INVENTARIOID))
            {
                Parameters.Add("INVENTARIOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@INVENTARIOID", DbType.Int32, INVENTARIOID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(FECHA))
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }
            if (F.Text.Input.IsNotNull(SALDO))
            {
                Parameters.Add("SALDO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SALDO", DbType.Int32, SALDO));
            }
            if (F.Text.Input.IsNotNull(UBICACIONID))
            {
                Parameters.Add("UBICACIONID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@UBICACIONID", DbType.Int32, UBICACIONID));
            }

        }
    }
}