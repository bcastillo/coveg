namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spSALIDA:spBase
    {
        public spSALIDA() {
           base.Name = "USP_SALIDA";
        }
        public void Prepare(object ACTION, object SALIDAID, object ESTATUS, object FECHA, object SOLICITUDSALIDAID)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(SALIDAID))
            {
                Parameters.Add("SALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SALIDAID", DbType.Int32, SALIDAID));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (F.Text.Input.IsNotNull(FECHA))
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }
            if (F.Text.Input.IsNotNull(SOLICITUDSALIDAID))
            {
                Parameters.Add("SOLICITUDSALIDAID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@SOLICITUDSALIDAID", DbType.Int32, SOLICITUDSALIDAID));
            }
        }
    }
}