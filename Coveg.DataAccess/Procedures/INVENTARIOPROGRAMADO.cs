﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;

namespace Coveg.DataAccess.Procedures
{
    public class spINVENTARIOPROGRAMADO : spBase
    {
        public spINVENTARIOPROGRAMADO()
        {
            base.Name = "USP_INVENTARIOPROGRAMADO";
        }

        public void Prepare(object ACTION,object INVENTARIOPROGRAMADOID, object DESCRIPCION, object ESTATUS, object FECHAFIN, object fechainicio, object FECHAPROGRAMADA, object ID_AREA)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(INVENTARIOPROGRAMADOID))
            {
                Parameters.Add("INVENTARIOPROGRAMADOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@INVENTARIOPROGRAMADOID", DbType.Int32, INVENTARIOPROGRAMADOID));
            }
            if (F.Text.Input.IsNotNull(DESCRIPCION))
            {
                Parameters.Add("DESCRIPCION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DESCRIPCION", DbType.String, DESCRIPCION));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
            if (Convert.ToDateTime(FECHAFIN)!= new DateTime())
            {
                Parameters.Add("FECHAFIN", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHAFIN", DbType.DateTime, FECHAFIN));
            }
            if (Convert.ToDateTime(fechainicio) != new DateTime())
            {
                Parameters.Add("fechainicio", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@fechainicio", DbType.DateTime, fechainicio));
            }
            if (Convert.ToDateTime(FECHAPROGRAMADA) != new DateTime())
            {
                Parameters.Add("FECHAPROGRAMADA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHAPROGRAMADA", DbType.DateTime, FECHAPROGRAMADA));
            }
            if (F.Text.Input.IsNotNull(ID_AREA))
            {
                Parameters.Add("ID_AREA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ID_AREA", DbType.Int32, ID_AREA));
            }

        }
    }
}
