namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spTIPOAUTORIZADOR : spBase
    {
        public spTIPOAUTORIZADOR()
        {
            base.Name = "USP_TIPOAUTORIZADOR";
        }
        public void Prepare(object ACTION, object TIPOAUTORIZADORID, object CLAVE, object DESCRIPCION, object ESTATUS)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(TIPOAUTORIZADORID))
            {
                Parameters.Add("TIPOAUTORIZADORID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@TIPOAUTORIZADORID", DbType.Int32, TIPOAUTORIZADORID));
            }
            if (F.Text.Input.IsNotNull(CLAVE))
            {
                Parameters.Add("CLAVE", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@CLAVE", DbType.String, CLAVE));
            }
            if (F.Text.Input.IsNotNull(DESCRIPCION))
            {
                Parameters.Add("DESCRIPCION", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DESCRIPCION", DbType.String, DESCRIPCION));
            }
            if (F.Text.Input.IsNotNull(ESTATUS))
            {
                Parameters.Add("ESTATUS", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ESTATUS", DbType.Int32, ESTATUS));
            }
        }
    }
}