namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spFALTANTESINVENTARIO : spBase
    {
        public spFALTANTESINVENTARIO()
        {
            base.Name = "USP_FALTANTESINVENTARIO";
        }
        public void Prepare(object ACTION, object FALTANTESINVENTARIOID, object ACTIVOID, object DIFERENCIA, object FECHAHORA, object INVENTARIOFISICOID, object MOTIVO)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(FALTANTESINVENTARIOID))
            {
                Parameters.Add("FALTANTESINVENTARIOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FALTANTESINVENTARIOID", DbType.Int32, FALTANTESINVENTARIOID));
            }
            if (F.Text.Input.IsNotNull(ACTIVOID))
            {
                Parameters.Add("ACTIVOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ACTIVOID", DbType.Int32, ACTIVOID));
            }
            if (F.Text.Input.IsNotNull(DIFERENCIA))
            {
                Parameters.Add("DIFERENCIA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@DIFERENCIA", DbType.Int32, DIFERENCIA));
            }
            if (F.Text.Input.IsNotNull(FECHAHORA))
            {
                Parameters.Add("FECHAHORA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHAHORA", DbType.DateTime, FECHAHORA));
            }
            if (F.Text.Input.IsNotNull(INVENTARIOFISICOID))
            {
                Parameters.Add("INVENTARIOFISICOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@INVENTARIOFISICOID", DbType.Int32, INVENTARIOFISICOID));
            }
            if (F.Text.Input.IsNotNull(MOTIVO))
            {
                Parameters.Add("MOTIVO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@MOTIVO", DbType.String, MOTIVO));
            }

        }
    }
}