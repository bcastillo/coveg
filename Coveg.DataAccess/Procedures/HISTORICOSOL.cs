﻿using System;
using System.Text;
using System.Data;
using System.Data.Common;
using F;
using F.Data;
using ResourceAccess.SQL;
using System.Collections.Generic;


namespace Coveg.DataAccess.Procedures
{
    public class spHISTORICOSOL : spBase
    {
        public spHISTORICOSOL()
        {
            base.Name = "USP_HISTORICOSOL";
        }

        public void Prepare(object ACTION, object USUARIO)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(USUARIO))
            {
                Parameters.Add("USUARIO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@USUARIO", DbType.String, USUARIO));
            }
        }

    }
}
