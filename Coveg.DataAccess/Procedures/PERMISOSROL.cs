namespace Coveg.DataAccess.Procedures
{
    using System;
    using System.Text;
    using System.Data;
    using System.Data.Common;
    using F;
    using F.Data;
    using ResourceAccess.SQL;
    using System.Collections.Generic;

    public class spPERMISOSROL : spBase
    {
        public spPERMISOSROL()
        {
            base.Name = "USP_PERMISOSROL";
        }
        public void Prepare(object ACTION, object PERMISOID, object ROLID)
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(PERMISOID))
            {
                Parameters.Add("PERMISOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@PERMISOID", DbType.Int32, PERMISOID));
            }
            if (F.Text.Input.IsNotNull(ROLID))
            {
                Parameters.Add("ROLID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@ROLID", DbType.Int32, ROLID));
            }
        }
    }
}