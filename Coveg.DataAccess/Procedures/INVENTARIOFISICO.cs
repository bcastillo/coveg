namespace Coveg.DataAccess.Procedures
{
	using System;
	using System.Text;
	using System.Data;
	using System.Data.Common;
	using F;
	using F.Data;
	using ResourceAccess.SQL;
	using System.Collections.Generic;    

    public class spINVENTARIOFISICO:spBase
    {
        public spINVENTARIOFISICO() {
           base.Name = "USP_INVENTARIOFISICO";
        }
        public void Prepare(object ACTION, object INVENTARIOFISICOID, object FECHA, object MOTIVO, object NUMEMPLEADO, object UBICACIONID
 )
        {
            Parameters.Clear();
            if (F.Text.Input.IsNotNull(ACTION))
            {
                Parameters.Add("Action", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@Action", DbType.Int32, ACTION));
            }
            if (F.Text.Input.IsNotNull(INVENTARIOFISICOID))
            {
                Parameters.Add("INVENTARIOFISICOID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@INVENTARIOFISICOID", DbType.Int32, INVENTARIOFISICOID));
            }
            if (F.Text.Input.IsNotNull(FECHA))
            {
                Parameters.Add("FECHA", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@FECHA", DbType.DateTime, FECHA));
            }
            if (F.Text.Input.IsNotNull(MOTIVO))
            {
                Parameters.Add("MOTIVO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@MOTIVO", DbType.String, MOTIVO));
            }
            if (F.Text.Input.IsNotNull(NUMEMPLEADO))
            {
                Parameters.Add("NUMEMPLEADO", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@NUMEMPLEADO", DbType.String, NUMEMPLEADO));
            }
            if (F.Text.Input.IsNotNull(UBICACIONID))
            {
                Parameters.Add("UBICACIONID", F.Data.Common.DataObjectsHelper.CreateSqlParameter("@UBICACIONID", DbType.Int32, UBICACIONID));
            }

        }
    }
}