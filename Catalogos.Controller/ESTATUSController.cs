﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class ESTATUSController
    {
        public List<ESTATUS_BE> Insert(ESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.Insert(objBE);
        }

        public List<ESTATUS_BE> Update(ESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.Update(objBE);
        }

        public bool Delete(ESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.Delete(objBE);
        }

        public List<ESTATUS_BE> SelectAll(ESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.SelectAll(objBE);
        }

        public List<ESTATUS_BE> SearchKey(ESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.SearchKey(objBE);
        }

        public List<TIPOESTATUS_BE> ListTipoEstatus(TIPOESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.TIPOESTATUSBL objBL = new Coveg.BusinessLogic.TIPOESTATUSBL();
            return objBL.SelectAll(objBE);
        }

    }
}
