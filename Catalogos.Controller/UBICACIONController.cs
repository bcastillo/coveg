﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class UBICACIONController
    {

        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
        }

        public List<UBICACION_BE> Insert(UBICACION_BE objBE)
        {
            Coveg.BusinessLogic.UBICACIONBL objBL = new Coveg.BusinessLogic.UBICACIONBL();
            return objBL.Insert(objBE);
        }

        public List<UBICACION_BE> Update(UBICACION_BE objBE)
        {
            Coveg.BusinessLogic.UBICACIONBL objBL = new Coveg.BusinessLogic.UBICACIONBL();
            return objBL.Update(objBE);
        }

        public bool Delete(UBICACION_BE objBE)
        {
            Coveg.BusinessLogic.UBICACIONBL objBL = new Coveg.BusinessLogic.UBICACIONBL();
            return objBL.Delete(objBE);
        }

        public List<UBICACION_BE> SelectAll(UBICACION_BE objBE)
        {
            Coveg.BusinessLogic.UBICACIONBL objBL = new Coveg.BusinessLogic.UBICACIONBL();
            return objBL.SelectAll(objBE);
        }

        public List<UBICACION_BE> SearchKey(UBICACION_BE objBE)
        {
            Coveg.BusinessLogic.UBICACIONBL objBL = new Coveg.BusinessLogic.UBICACIONBL();
            return objBL.SearchKey(objBE);
        }

    }
}
