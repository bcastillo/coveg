﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class TIPOESTATUSController
    {
        public List<TIPOESTATUS_BE> Insert(TIPOESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.TIPOESTATUSBL objBL = new Coveg.BusinessLogic.TIPOESTATUSBL();
            return objBL.Insert(objBE);
        }

        public List<TIPOESTATUS_BE> Update(TIPOESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.TIPOESTATUSBL objBL = new Coveg.BusinessLogic.TIPOESTATUSBL();
            return objBL.Update(objBE);
        }

        public bool Delete(TIPOESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.TIPOESTATUSBL objBL = new Coveg.BusinessLogic.TIPOESTATUSBL();
            return objBL.Delete(objBE);
        }

        public List<TIPOESTATUS_BE> SelectAll(TIPOESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.TIPOESTATUSBL objBL = new Coveg.BusinessLogic.TIPOESTATUSBL();
            return objBL.SelectAll(objBE);
        }

        public List<TIPOESTATUS_BE> SearchKey(TIPOESTATUS_BE objBE)
        {
            Coveg.BusinessLogic.TIPOESTATUSBL objBL = new Coveg.BusinessLogic.TIPOESTATUSBL();
            return objBL.SearchKey(objBE);
        }

    }
}
