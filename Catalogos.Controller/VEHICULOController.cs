﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class VEHICULOController
    {
        public List<VEHICULO_BE> Insert(VEHICULO_BE objBE,ACTIVO_BE objBEActivo)
        {

            Coveg.BusinessLogic.VEHICULOBL objBL = new Coveg.BusinessLogic.VEHICULOBL();
            Coveg.BusinessLogic.ACTIVOBL objBLActivo = new Coveg.BusinessLogic.ACTIVOBL();
            List<ACTIVO_BE> lstActivoRes = new List<ACTIVO_BE>();

            lstActivoRes = objBLActivo.InsertActivoAuto(objBEActivo);

            if (lstActivoRes != null)
            {
                objBE.ACTIVOID = lstActivoRes[0].ACTIVOID;
                return objBL.Insert(objBE);
            }
            else
                return null;
        }

        public Boolean Update(VEHICULO_BE objBE, ACTIVO_BE objBEActivo)
        {
            Boolean bolRes=false;
            Coveg.BusinessLogic.VEHICULOBL objBL = new Coveg.BusinessLogic.VEHICULOBL();
            Coveg.BusinessLogic.ACTIVOBL objBLActivo = new BusinessLogic.ACTIVOBL();

            if(objBL.Update(objBE)!=null)
                if(objBLActivo.Update(objBEActivo)!=null)
                    bolRes=true;

            return bolRes;
        }

        public bool Delete(VEHICULO_BE objBE)
        {
            Coveg.BusinessLogic.VEHICULOBL objBL = new Coveg.BusinessLogic.VEHICULOBL();
            return objBL.Delete(objBE);
        }

        public List<VEHICULO_BE> SelectAll(VEHICULO_BE objBE)
        {
            Coveg.BusinessLogic.VEHICULOBL objBL = new Coveg.BusinessLogic.VEHICULOBL();
            return objBL.SelectAll(objBE);
        }

        public List<VEHICULO_BE> SearchKey(VEHICULO_BE objBE)
        {
            Coveg.BusinessLogic.VEHICULOBL objBL = new Coveg.BusinessLogic.VEHICULOBL();
            return objBL.SearchKey(objBE);
        }


        /// <summary>
        /// Consulta lista de tabla de activos
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BE> SelectAllActivoVehiculos(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new Coveg.BusinessLogic.ACTIVOBL();
            return objBL.SelectAllActivoVehiculo(objBE);
        }


        /// <summary>
        /// Consulta lista de estatus por tipo de estatus para catalogo de activos
        /// </summary>
        /// <param name="strTipoEstatus"></param>
        /// <returns></returns>
        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clave"></param>
        /// <returns></returns>
        public ACTIVO_BE GetActivo(string clave)
        {
            ACTIVO_BE activo = new ACTIVO_BE();
            Coveg.BusinessLogic.ACTIVOBL actBL = new BusinessLogic.ACTIVOBL();
            var lst = actBL.SearchKey(new ACTIVO_BE() { CLAVE = clave });

            if (lst != null && lst.Count > 0)
            {
                activo = lst[0];
            }
            return activo;
        }
    }
}
