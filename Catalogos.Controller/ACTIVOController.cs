﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class ACTIVOController
    {

        /// <summary>
        /// Consulta lista de TAGs
        /// </summary>
        /// <returns></returns>
        public List<RFIDTAG_BE> ListaTAG()
        {
            Coveg.BusinessLogic.RFIDTAGBL objBL = new Coveg.BusinessLogic.RFIDTAGBL();
            return objBL.SelectAll(new RFIDTAG_BE());
        }

        /// <summary>
        /// Consulta lista de unidades
        /// </summary>
        /// <returns></returns>
        public List<UNIDAD_BE> ListaUnidad()
        {
            Coveg.BusinessLogic.UNIDADBL objBL = new Coveg.BusinessLogic.UNIDADBL();
            return objBL.SelectAll(new UNIDAD_BE());
        }

        /// <summary>
        /// Consulta lista de personal
        /// </summary>
        /// <returns></returns>
        public List<PERSONAL_BE> ListaPersonal()
        {
            return null;
        }

        /// <summary>
        /// Consulta lista de tipos de activo
        /// </summary>
        /// <returns></returns>
        public List<TIPOACTIVO_BE> ListaTipoActivo()
        {
            Coveg.BusinessLogic.TIPOACTIVOBL objBL = new Coveg.BusinessLogic.TIPOACTIVOBL();
            return objBL.SelectAll(new TIPOACTIVO_BE());
        }

        /// <summary>
        /// Consulta lista de estatus por tipo de estatus para catalogo de activos
        /// </summary>
        /// <param name="strTipoEstatus"></param>
        /// <returns></returns>
        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
        }

        public List<ACTIVO_BE> Insert(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new BusinessLogic.ACTIVOBL();
            return objBL.Insert(objBE);
        }

        public List<ACTIVO_BE> Update(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new BusinessLogic.ACTIVOBL();
            return objBL.Update(objBE); 
        }

        public bool Delete(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new BusinessLogic.ACTIVOBL();
            return objBL.Delete(objBE);  
        }

        public List<ACTIVO_BE> SelectAll(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new BusinessLogic.ACTIVOBL();
            return objBL.SelectAll(objBE);  
        }

        public List<ACTIVO_BE> SearchKey(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new BusinessLogic.ACTIVOBL();
            return objBL.SearchKey(objBE);   
        }


    }
}
