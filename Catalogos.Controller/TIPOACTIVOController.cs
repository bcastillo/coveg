﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class TIPOACTIVOController
    {

        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
        }


        public List<TIPOACTIVO_BE> Insert(TIPOACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.TIPOACTIVOBL objBL = new Coveg.BusinessLogic.TIPOACTIVOBL();
            return objBL.Insert(objBE);
        }

        public List<TIPOACTIVO_BE> Update(TIPOACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.TIPOACTIVOBL objBL = new Coveg.BusinessLogic.TIPOACTIVOBL();
            return objBL.Update(objBE);
        }

        public bool Delete(TIPOACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.TIPOACTIVOBL objBL = new Coveg.BusinessLogic.TIPOACTIVOBL();
            return objBL.Delete(objBE);
        }

        public List<TIPOACTIVO_BE> SelectAll(TIPOACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.TIPOACTIVOBL objBL = new Coveg.BusinessLogic.TIPOACTIVOBL();
            return objBL.SelectAll(objBE);
        }

        public List<TIPOACTIVO_BE> SearchKey(TIPOACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.TIPOACTIVOBL objBL = new Coveg.BusinessLogic.TIPOACTIVOBL();
            return objBL.SearchKey(objBE);
        }

    }
}
