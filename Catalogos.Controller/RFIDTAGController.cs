﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class RFIDTAGController
    {
        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
        }

        public List<RFIDTAG_BE> Insert(RFIDTAG_BE objBE)
        {
            Coveg.BusinessLogic.RFIDTAGBL objBL = new Coveg.BusinessLogic.RFIDTAGBL();
            return objBL.Insert(objBE);
        }

        public List<RFIDTAG_BE> Update(RFIDTAG_BE objBE)
        {
            Coveg.BusinessLogic.RFIDTAGBL objBL = new Coveg.BusinessLogic.RFIDTAGBL();
            return objBL.Update(objBE);
        }

        public bool Delete(RFIDTAG_BE objBE)
        {
            Coveg.BusinessLogic.RFIDTAGBL objBL = new Coveg.BusinessLogic.RFIDTAGBL();
            return objBL.Delete(objBE);
        }

        public List<RFIDTAG_BE> SelectAll(RFIDTAG_BE objBE)
        {
            Coveg.BusinessLogic.RFIDTAGBL objBL = new Coveg.BusinessLogic.RFIDTAGBL();
            return objBL.SelectAll(objBE);
        }

        public List<RFIDTAG_BE> SearchKey(RFIDTAG_BE objBE)
        {
            Coveg.BusinessLogic.RFIDTAGBL objBL = new Coveg.BusinessLogic.RFIDTAGBL();
            return objBL.SearchKey(objBE);
        }

    }
}
