﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class UNIDADController
    {
        public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
        {
            Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
            return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
        }


        public List<UNIDAD_BE> Insert(UNIDAD_BE objBE)
        {
            Coveg.BusinessLogic.UNIDADBL objBL = new Coveg.BusinessLogic.UNIDADBL();
            return objBL.Insert(objBE);
        }

        public List<UNIDAD_BE> Update(UNIDAD_BE objBE)
        {
            Coveg.BusinessLogic.UNIDADBL objBL = new Coveg.BusinessLogic.UNIDADBL();
            return objBL.Update(objBE);
        }

        public bool Delete(UNIDAD_BE objBE)
        {
            Coveg.BusinessLogic.UNIDADBL objBL = new Coveg.BusinessLogic.UNIDADBL();
            return objBL.Delete(objBE);
        }

        public List<UNIDAD_BE> SelectAll(UNIDAD_BE objBE)
        {
            Coveg.BusinessLogic.UNIDADBL objBL = new Coveg.BusinessLogic.UNIDADBL();
            return objBL.SelectAll(objBE);
        }

        public List<UNIDAD_BE> SearchKey(UNIDAD_BE objBE)
        {
            Coveg.BusinessLogic.UNIDADBL objBL = new Coveg.BusinessLogic.UNIDADBL();
            return objBL.SearchKey(objBE);
        }

    }
}
