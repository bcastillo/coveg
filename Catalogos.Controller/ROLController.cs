﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using F;
using F.Data;
using System.Data;

namespace Coveg.Catalogos.Controller
{
    public class USUARIOROLController
    {

        public List<USUARIOROLBE> Insert(USUARIOROLBE objBE)
        {
            Coveg.BusinessLogic.USUARIOROLBL objBL = new Coveg.BusinessLogic.USUARIOROLBL();
            return objBL.Insert(objBE);
        }

        public List<USUARIOROLBE> Update(USUARIOROLBE objBE)
        {
            Coveg.BusinessLogic.USUARIOROLBL objBL = new Coveg.BusinessLogic.USUARIOROLBL();
            return objBL.Update(objBE);
        }

        public bool Delete(USUARIOROLBE objBE)
        {
            Coveg.BusinessLogic.USUARIOROLBL objBL = new Coveg.BusinessLogic.USUARIOROLBL();
            return objBL.Delete(objBE);
        }

        public List<USUARIOROLBE> SelectAll(USUARIOROLBE objBE)
        {
            Coveg.BusinessLogic.USUARIOROLBL objBL = new Coveg.BusinessLogic.USUARIOROLBL();
            return objBL.SelectAll(objBE);
        }

        public List<USUARIOROLBE> SearchKey(USUARIOROLBE objBE)
        {
            Coveg.BusinessLogic.USUARIOROLBL objBL = new Coveg.BusinessLogic.USUARIOROLBL();
            return objBL.SearchKey(objBE);
        }

    }
}
