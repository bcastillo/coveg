﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;

namespace Coveg.Reportes.Controller
{
    public class DETALLESOLICITUDController
    {
        #region Consulta solicitud y activos de solicitud

        /// <summary>
        /// Obtiene lista de activos para busqueda
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<DETALLESOLICITUD_ACTIVOS_BE> SelectSolicitudActivos(SOLICITUDSALIDA_BE objBE)
        {
            Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
            List<DETALLESOLICITUD_ACTIVOS_BE> objListaActivos = new List<DETALLESOLICITUD_ACTIVOS_BE>();
            objListaActivos = objBL.SelectSolicitudActivos(objBE);
            return objListaActivos;
        }

        #endregion
    }
}
