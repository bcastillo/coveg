﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;

namespace Coveg.Reportes.Controller
{
    public class HISTORICOSOLController
    {
        public List<HISTORICO_SOLICITUDES_BE> HistoricoCancelados(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL objBL = new Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL();
            return objBL.HistoricoCancelados(objBE);
        }

        public List<HISTORICO_SOLICITUDES_BE> HistoricoAprobados(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL objBL = new Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL();
            return objBL.HistoricoAprobados(objBE);
        }

        public List<HISTORICO_SOLICITUDES_BE> HistoricoDevuelto(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL objBL = new Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL();
            return objBL.HistoricoDevuelto(objBE);
        }

        public List<HISTORICO_SOLICITUDES_BE> HistoricoRechazado(HISTORICO_SOLICITUDES_BE objBE)
        {
            Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL objBL = new Coveg.BusinessLogic.HISTORICO_SOLICITUDESBL();
            return objBL.HistoricoRechazado(objBE);
        }
    }
}
