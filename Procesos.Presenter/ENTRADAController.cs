﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class ENTRADAController
    {
        public List<ENTRADA_BE> Insert(ENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.Insert(objBE);
        }

        public List<ENTRADA_BE> Update(ENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.Update(objBE);
        }

        public bool Delete(ENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.Delete(objBE);
        }

        public List<ENTRADA_BE> SelectAll(ENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.SelectAll(objBE);
        }

        public List<ENTRADA_BE> SearchKey(ENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.SearchKey(objBE);
        }

        public ACTIVO_BUSQUEDA ActivoEntrada(ACTIVO_BUSQUEDA objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.ActivoEntrada(objBE);
        }

        public ACTIVO_BUSQUEDA InfoSolicitudEntrada(ACTIVO_BUSQUEDA objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.InfoSolicitudEntrada(objBE);
        }



        public Boolean RegistrarEntrada(DETALLEENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
            return objBL.RegistrarEntrada(objBE);
        }

        #region consultas para reporte de pendientes

            public List<SEGUIMIENTO_SOLICITUDES_BE> Select_Pendientes_Salir(SEGUIMIENTO_SOLICITUDES_BE objBE)
            {
                Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
                return objBL.Select_Pendientes_Salir(objBE);
            }

            public List<SEGUIMIENTO_SOLICITUDES_BE> Select_Pendientes_No_Vencidos(SEGUIMIENTO_SOLICITUDES_BE objBE)
            {
                Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
                return objBL.Select_Pendientes_No_Vencidos(objBE);
            }

            public List<SEGUIMIENTO_SOLICITUDES_BE> Select_Pendientes_Vencidos(SEGUIMIENTO_SOLICITUDES_BE objBE)
            {
                Coveg.BusinessLogic.ENTRADABL objBL = new Coveg.BusinessLogic.ENTRADABL();
                return objBL.Select_Pendientes_Vencidos(objBE);
            }


        #endregion
    }
}
