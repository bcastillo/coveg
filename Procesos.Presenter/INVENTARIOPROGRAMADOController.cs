﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class INVENTARIOPROGRAMADOController
    {
        public List<INVENTARIOPROGRAMADO_BE> Insert(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL objBL = new Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL();
            return objBL.Insert(objBE);
        }

        public List<INVENTARIOPROGRAMADO_BE> Update(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL objBL = new Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL();
            return objBL.Update(objBE);
        }

        public bool Delete(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL objBL = new Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL();
            return objBL.Delete(objBE);
        }

        public List<INVENTARIOPROGRAMADO_BE> SelectAll(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL objBL = new Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL();
            return objBL.SelectAll(objBE);
        }

        public List<INVENTARIOPROGRAMADO_BE> SearchKey(INVENTARIOPROGRAMADO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL objBL = new Coveg.BusinessLogic.INVENTARIOPROGRAMADOBL();
            return objBL.SearchKey(objBE);
        }

    }
}
