﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class SALIDAController
    {
        public List<SALIDA_BE> Insert(SALIDA_BE objBE)
        {
            Coveg.BusinessLogic.SALIDABL objBL = new Coveg.BusinessLogic.SALIDABL();
            return objBL.Insert(objBE);
        }

        public List<SALIDA_BE> Update(SALIDA_BE objBE)
        {
            Coveg.BusinessLogic.SALIDABL objBL = new Coveg.BusinessLogic.SALIDABL();
            return objBL.Update(objBE);
        }

        public bool Delete(SALIDA_BE objBE)
        {
            Coveg.BusinessLogic.SALIDABL objBL = new Coveg.BusinessLogic.SALIDABL();
            return objBL.Delete(objBE);
        }

        public List<SALIDA_BE> SelectAll(SALIDA_BE objBE)
        {
            Coveg.BusinessLogic.SALIDABL objBL = new Coveg.BusinessLogic.SALIDABL();
            return objBL.SelectAll(objBE);
        }

        public List<SALIDA_BE> SearchKey(SALIDA_BE objBE)
        {
            Coveg.BusinessLogic.SALIDABL objBL = new Coveg.BusinessLogic.SALIDABL();
            return objBL.SearchKey(objBE);
        }

    }
}
