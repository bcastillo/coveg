﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class LOGINController
    {
        public List<LOGIN_BE> SelectAll(LOGIN_BE objBE)
        {
            Coveg.BusinessLogic.LOGINBL objBL = new Coveg.BusinessLogic.LOGINBL();
            return objBL.SelectLogin(objBE);
        }

        public LOGIN_BE ObtenerJefeInmediato(LOGIN_BE objBE)
        {
            Coveg.BusinessLogic.LOGINBL objBL = new Coveg.BusinessLogic.LOGINBL();
            return objBL.SelectJefe(objBE);
        }
    }
}
