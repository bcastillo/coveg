﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class AUTORIZACIONSALIDAController
    {

        #region Autoriza rechaza director

            public List<AUTORIZACIONSALIDA_BE> AutorizaDirector(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.AUTORIZA_DIRECTOR);
            }

            public List<AUTORIZACIONSALIDA_BE> RechazaDirector(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.RECHAZA_DIRECTOR);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesDireccion(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_DIRECTOR);
            }

        #endregion

        #region Autoriza rechaza Jefe

            public List<AUTORIZACIONSALIDA_BE> AutorizaJefe(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.AUTORIZA_JEFE);
            }

            public List<AUTORIZACIONSALIDA_BE> RechazaJefe(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Rechaza(objBE, (int)Coveg.DataAccess.Enums.DBAction.RECHAZA_JEFE);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesJefe(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_JEFE);
            }

        #endregion

        #region Autoriza rechaza resguardo

            public List<AUTORIZACIONSALIDA_BE> RechazaResguardo(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Rechaza(objBE, (int)Coveg.DataAccess.Enums.DBAction.RECHAZA_RESGUARDO);
            }

            public List<AUTORIZACIONSALIDA_BE> AutorizaResguardo(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.AUTORIZA_RESGUARDO);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesResguardo(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_RESGUARDO);
            }

        #endregion

        #region Acciones comun

            public List<AUTORIZACIONSALIDA_BE> Insert(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Insert(objBE);
            }

            public List<AUTORIZACIONSALIDA_BE> Update(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Update(objBE);
            }

            public bool Delete(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Delete(objBE);
            }

            public List<AUTORIZACIONSALIDA_BE> SelectAll(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.SelectAll(objBE);
            }

            public List<AUTORIZACIONSALIDA_BE> SearchKey(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.SearchKey(objBE);
            }

            public List<ESTATUS_BE> ListaEstatusTipoEstatus(String strTipoEstatus)
            {
                Coveg.BusinessLogic.ESTATUSBL objBL = new Coveg.BusinessLogic.ESTATUSBL();
                return objBL.ListaEstatusTipoEstatus(strTipoEstatus);
            }


            public List<ACTIVOSSOLICITADOS_BE> ListaActivosSolicitados(ACTIVOSSOLICITADOS_BE be)
            {
                Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
                return objBL.ListaActivosSolicitud(be);
            }


            /// <summary>
            /// Notificar autorizadores
            /// </summary>
            /// <param name="intSolicitudID"></param>
            /// <returns></returns>
            public Boolean NotificarAutorizadores(Int32 intSolicitudID)
            {
                try
                {
                    Coveg.BusinessLogic.OBTENERCORREOSBL objBL = new Coveg.BusinessLogic.OBTENERCORREOSBL();
                    Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objActivosBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
                    ACTIVOSSOLICITADOS_BE objActivos = new ACTIVOSSOLICITADOS_BE();
                    List<ACTIVOSSOLICITADOS_BE> lstActivos = new List<ACTIVOSSOLICITADOS_BE>();

                    MailData objData = new MailData();
                    MailStructure objEstructuraCorreo = new MailStructure();
                    List<MailData> lstListaNotificar = new List<MailData>();
                    String strACTIVOSSOLLICITADOS = String.Empty;


                    objData.SOLICITUDSALIDAID = intSolicitudID;
                    objActivos.SOLICITUDSALIDAID = intSolicitudID;

                    lstListaNotificar = objBL.BuscarCorreos(objData);
                    ///Implementar llamado de correo   
                    if (lstListaNotificar != null)
                    {
                        //Llamada a correo
                        lstActivos = objActivosBL.ListaActivosSolicitud(objActivos);
                        foreach (ACTIVOSSOLICITADOS_BE objItem in lstActivos)
                        {
                            if (strACTIVOSSOLLICITADOS == String.Empty)
                                strACTIVOSSOLLICITADOS = objItem.ACTIVO_DESC;
                            else
                                strACTIVOSSOLLICITADOS = strACTIVOSSOLLICITADOS + "<BR>" + objItem.ACTIVO_DESC;
                        }

                        Coveg.Common.Notifications.NotificacionAutorizacion.CorreoAutorizacion(lstListaNotificar[0].correo_institucional, lstListaNotificar[0].correo_electronico, intSolicitudID.ToString(), lstListaNotificar[0].Nombre_Completo, lstListaNotificar[0].Puesto, strACTIVOSSOLLICITADOS);

                    }
                    else
                        return false;

                    return true;
                }
                catch
                {
                    return false;
                }
            }

        #endregion


        #region Responsable de archivo

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesDocumentos(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_RESGUARDO_DOCTOS);
            }

            public List<AUTORIZACIONSALIDA_BE> AutorizaResArchivo(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.AUTORIZA_ARCHIVO);
            }

            public List<AUTORIZACIONSALIDA_BE> RechazaResArchivo(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.RECHAZA_ARCHIVO);
            }

        #endregion

        #region Responsable de autos

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesAutos(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_RESP_AUTOS);
            }

            public List<AUTORIZACIONSALIDA_BE> AutorizaResAutos(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.AUTORIZA_AUTO);
            }

            public List<AUTORIZACIONSALIDA_BE> RechazaResAutos(AUTORIZACIONSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.AUTORIZACIONSALIDABL objBL = new Coveg.BusinessLogic.AUTORIZACIONSALIDABL();
                return objBL.Autoriza(objBE, (int)Coveg.DataAccess.Enums.DBAction.RECHAZA_AUTO);
            }

        #endregion


    }
}
