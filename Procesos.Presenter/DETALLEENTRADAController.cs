﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class DETALLEENTRADAController
    {
        public List<DETALLEENTRADA_BE> Insert(DETALLEENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLEENTRADABL objBL = new Coveg.BusinessLogic.DETALLEENTRADABL();
            return objBL.Insert(objBE);
        }

        public List<DETALLEENTRADA_BE> Update(DETALLEENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLEENTRADABL objBL = new Coveg.BusinessLogic.DETALLEENTRADABL();
            return objBL.Update(objBE);
        }

        public bool Delete(DETALLEENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLEENTRADABL objBL = new Coveg.BusinessLogic.DETALLEENTRADABL();
            return objBL.Delete(objBE);
        }

        public List<DETALLEENTRADA_BE> SelectAll(DETALLEENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLEENTRADABL objBL = new Coveg.BusinessLogic.DETALLEENTRADABL();
            return objBL.SelectAll(objBE);
        }

        public List<DETALLEENTRADA_BE> SearchKey(DETALLEENTRADA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLEENTRADABL objBL = new Coveg.BusinessLogic.DETALLEENTRADABL();
            return objBL.SearchKey(objBE);
        }

    }
}
