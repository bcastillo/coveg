﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class DETALLESALIDController
    {
        public List<DETALLESALIDA_BE> Insert(DETALLESALIDA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLESALIDABL objBL = new Coveg.BusinessLogic.DETALLESALIDABL();
            return objBL.Insert(objBE);
        }

        public List<DETALLESALIDA_BE> Update(DETALLESALIDA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLESALIDABL objBL = new Coveg.BusinessLogic.DETALLESALIDABL();
            return objBL.Update(objBE);
        }

        public bool Delete(DETALLESALIDA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLESALIDABL objBL = new Coveg.BusinessLogic.DETALLESALIDABL();
            return objBL.Delete(objBE);
        }

        public List<DETALLESALIDA_BE> SelectAll(DETALLESALIDA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLESALIDABL objBL = new Coveg.BusinessLogic.DETALLESALIDABL();
            return objBL.SelectAll(objBE);
        }

        public List<DETALLESALIDA_BE> SearchKey(DETALLESALIDA_BE objBE)
        {
            Coveg.BusinessLogic.DETALLESALIDABL objBL = new Coveg.BusinessLogic.DETALLESALIDABL();
            return objBL.SearchKey(objBE);
        }

    }
}
