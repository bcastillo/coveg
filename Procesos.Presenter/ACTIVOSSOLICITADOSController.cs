﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class ACTIVOSSOLICITADOSController
    {
        public List<ACTIVOSSOLICITADOS_BE> Insert(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
            return objBL.Insert(objBE);
        }

        public List<ACTIVOSSOLICITADOS_BE> Update(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
            return objBL.Update(objBE);
        }

        public bool Delete(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
            return objBL.Delete(objBE);
        }

        public List<ACTIVOSSOLICITADOS_BE> SelectAll(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
            return objBL.SelectAll(objBE);
        }

        public List<ACTIVOSSOLICITADOS_BE> SearchKey(ACTIVOSSOLICITADOS_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
            return objBL.SearchKey(objBE);
        }

    }
}
