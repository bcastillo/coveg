﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class SOLICITUDSALIDAController
    {

        #region SolicitudesAutorizarResumen

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesAutos(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_RESP_AUTOS);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesDocumentos(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_RESGUARDO_DOCTOS);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesResguardo(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_RESGUARDO);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesJefe(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_JEFE);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesDireccion(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SolicitudesAutorizacion(objBE, (int)Coveg.DataAccess.Enums.DBAction.SOLICITUDES_DIRECTOR);
            }

        #endregion

        #region Funciones transaccionales

            public bool FinalizarSolicitud(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.Finalizar(objBE);
            }

            public List<SOLICITUDSALIDA_BE> Insert(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.Insert(objBE);
            }

            public List<SOLICITUDSALIDA_BE> Update(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.Update(objBE);
            }

            public bool Delete(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.Delete(objBE);
            }

            public List<SOLICITUDSALIDA_BE> SelectAll(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SelectAll(objBE);
            }

            public List<SOLICITUDSALIDA_BE> SearchKey(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SearchKey(objBE);
            }

            public List<SOLICITUDSALIDA_BE> SelectSolicitudesUsuarioNoDevueltas(SOLICITUDSALIDA_BE objBE)
            {
                Coveg.BusinessLogic.SOLICITUDSALIDABL objBL = new Coveg.BusinessLogic.SOLICITUDSALIDABL();
                return objBL.SelectSolicitudesUsuarioNoDevueltas(objBE);
            }


            public List<ACTIVOSSOLICITADOS_BE> InsertActivosSolicitados(ACTIVO_BUSQUEDA objBE, Int32 intSolicitudID)
            {
                Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
                ACTIVOSSOLICITADOS_BE objNuevo = new ACTIVOSSOLICITADOS_BE();
                objNuevo.ACTIVOID = objBE.ACTIVOID;
                objNuevo.CANTIDAD = 1;
                objNuevo.SOLICITUDSALIDAID = intSolicitudID;

                return objBL.Insert(objNuevo);
            }

        #endregion

        #region Consulta solicitudes por tipo de activo

        /// <summary>
        /// Obtiene lista de activos para busqueda
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BUSQUEDA> ListActivosSolicitud(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new Coveg.BusinessLogic.ACTIVOBL();
            List<ACTIVO_BUSQUEDA> objListaActivos = new List<ACTIVO_BUSQUEDA>();
            objListaActivos = objBL.SelectActivosSolicitud(objBE);
            return objListaActivos;
        }


        /// <summary>
        /// Obtiene lista de activos para busqueda (Vehiculos)
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BUSQUEDA> ListActivosSolicitudVehiculos(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new Coveg.BusinessLogic.ACTIVOBL();
            List<ACTIVO_BUSQUEDA> objListaActivos = new List<ACTIVO_BUSQUEDA>();
            objListaActivos = objBL.SelectActivosSolicitudAutomoviles(objBE);
            return objListaActivos;
        }

        /// <summary>
        /// Obtiene lista de activos para busqueda (Doctos)
        /// </summary>
        /// <param name="objBE"></param>
        /// <returns></returns>
        public List<ACTIVO_BUSQUEDA> ListActivosSolicitudDoctos(ACTIVO_BE objBE)
        {
            Coveg.BusinessLogic.ACTIVOBL objBL = new Coveg.BusinessLogic.ACTIVOBL();
            List<ACTIVO_BUSQUEDA> objListaActivos = new List<ACTIVO_BUSQUEDA>();
            objListaActivos = objBL.SelectActivosSolicitudDoctos(objBE);
            return objListaActivos;
        }


        #endregion

        #region Notificaciones por correo

            /// <summary>
            /// Notificar autorizadores
            /// </summary>
            /// <param name="intSolicitudID"></param>
            /// <returns></returns>
            public Boolean NotificarAutorizadores(Int32 intSolicitudID)
            {
                try
                {
                    Coveg.BusinessLogic.OBTENERCORREOSBL objBL = new Coveg.BusinessLogic.OBTENERCORREOSBL();
                    Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL objActivosBL = new Coveg.BusinessLogic.ACTIVOSSOLICITADOSBL();
                    ACTIVOSSOLICITADOS_BE objActivos = new ACTIVOSSOLICITADOS_BE();
                    List<ACTIVOSSOLICITADOS_BE> lstActivos = new List<ACTIVOSSOLICITADOS_BE>();

                    MailData objData = new MailData();
                    MailStructure objEstructuraCorreo = new MailStructure();
                    List<MailData> lstListaNotificar = new List<MailData>();
                    String strACTIVOSSOLLICITADOS= String.Empty;


                    objData.SOLICITUDSALIDAID = intSolicitudID;
                    objActivos.SOLICITUDSALIDAID = intSolicitudID;

                    lstListaNotificar = objBL.BuscarCorreos(objData);
                    ///Implementar llamado de correo   
                    if (lstListaNotificar != null)
                    {
                        //Llamada a correo
                        lstActivos = objActivosBL.ListaActivosSolicitud(objActivos);
                        foreach (ACTIVOSSOLICITADOS_BE objItem in lstActivos)
                        {
                            if (strACTIVOSSOLLICITADOS == String.Empty)
                                strACTIVOSSOLLICITADOS = objItem.ACTIVO_DESC;
                            else
                                strACTIVOSSOLLICITADOS = strACTIVOSSOLLICITADOS + "<BR>" + objItem.ACTIVO_DESC;
                        }

                        Coveg.Common.Notifications.NotificacionAutorizacion.CorreoAutorizacion(lstListaNotificar[0].correo_institucional, lstListaNotificar[0].correo_electronico, intSolicitudID.ToString(), lstListaNotificar[0].Nombre_Completo, lstListaNotificar[0].Puesto, strACTIVOSSOLLICITADOS);    

                    }
                    else
                        return false;

                    return true;
                }
                catch 
                {
                    return false;
                }
            }

        #endregion



            
    }
}
