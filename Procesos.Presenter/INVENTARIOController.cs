﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Coveg.Entities;
using System.Data;

namespace Coveg.Procesos.Controller
{
    public class INVENTARIOController
    {
        public List<INVENTARIO_BE> Insert(INVENTARIO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOBL objBL = new Coveg.BusinessLogic.INVENTARIOBL();
            return objBL.Insert(objBE);
        }

        public List<INVENTARIO_BE> Update(INVENTARIO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOBL objBL = new Coveg.BusinessLogic.INVENTARIOBL();
            return objBL.Update(objBE);
        }

        public bool Delete(INVENTARIO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOBL objBL = new Coveg.BusinessLogic.INVENTARIOBL();
            return objBL.Delete(objBE);
        }

        public List<INVENTARIO_BE> SelectAll(INVENTARIO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOBL objBL = new Coveg.BusinessLogic.INVENTARIOBL();
            return objBL.SelectAll(objBE);
        }

        public List<INVENTARIO_BE> SearchKey(INVENTARIO_BE objBE)
        {
            Coveg.BusinessLogic.INVENTARIOBL objBL = new Coveg.BusinessLogic.INVENTARIOBL();
            return objBL.SearchKey(objBE);
        }

    }
}
