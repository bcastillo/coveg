namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class UNIDAD_BE{

	#region Properties and privates

		
	private Int32 _UNIDADID;
	private String _DESCRIPCION;
	private Int32 _ESTATUS;
	private String _NOMBRE;
    private String _DESC_ESTATUS;

    public String DESC_ESTATUS
    {
        get { return _DESC_ESTATUS; }
        set { _DESC_ESTATUS = value; }
    }
	
///
public Int32 UNIDADID{
get{
return _UNIDADID;
}
set{
_UNIDADID = value;
}
}
///
public String DESCRIPCION{
get{
return _DESCRIPCION;
}
set{
_DESCRIPCION = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}
///
public String NOMBRE{
get{
return _NOMBRE;
}
set{
_NOMBRE = value;
}
}

	#endregion

	#region Constructors
public UNIDAD_BE()
{

		}


	#endregion
	}
}