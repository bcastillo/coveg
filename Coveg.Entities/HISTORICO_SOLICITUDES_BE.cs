﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class HISTORICO_SOLICITUDES_BE
    {
        #region Properties and privates

        private Int32 _SOLICITUDSALIDAID;
        private String _USUARIO_SOLICITA;
        private DateTime _FECHA_SOLICITUD;
        private String _MOTIVO;
        private String _COMENTARIOS_RECHAZO;
        private DateTime _FECHA_RECHAZO;
        private String _USUARIO_RECHAZO;
        private String _TIPO_USUARIO_RECHAZA;

        private Int32 _ACTIVOS_SOLICITADOS;
        private Int32 _ACTIVOS_DEVUELTOS;
        private Int32 _DIFERENCIA;
        private DateTime _FECHASALIDA;
        private DateTime _FECHARETORNO;


        public Int32 ACTIVOS_SOLICITADOS
        {
            get { return _ACTIVOS_SOLICITADOS; }
            set { _ACTIVOS_SOLICITADOS = value; }
        }

        public Int32 ACTIVOS_DEVUELTOS
        {
            get { return _ACTIVOS_DEVUELTOS; }
            set { _ACTIVOS_DEVUELTOS = value; }
        }

        public Int32 DIFERENCIA
        {
            get { return _DIFERENCIA; }
            set { _DIFERENCIA = value; }
        }
        public DateTime FECHASALIDA
        {
            get { return _FECHASALIDA; }
            set { _FECHASALIDA = value; }
        }
        public DateTime FECHARETORNO
        {
            get { return _FECHARETORNO; }
            set { _FECHARETORNO = value; }
        }


        public String COMENTARIOS_RECHAZO
        {
            get { return _COMENTARIOS_RECHAZO; }
            set { _COMENTARIOS_RECHAZO = value; }
        }

        public DateTime FECHA_RECHAZO
        {
            get { return _FECHA_RECHAZO; }
            set { _FECHA_RECHAZO = value; }
        }

        public String USUARIO_RECHAZO
        {
            get { return _USUARIO_RECHAZO; }
            set { _USUARIO_RECHAZO = value; }
        }

        public String TIPO_USUARIO_RECHAZA
        {
            get { return _TIPO_USUARIO_RECHAZA; }
            set { _TIPO_USUARIO_RECHAZA = value; }
        }



        public Int32 SOLICITUDSALIDAID
        {
            get { return _SOLICITUDSALIDAID; }
            set { _SOLICITUDSALIDAID = value; }
        }

        public String USUARIO_SOLICITA        
        {
            get { return _USUARIO_SOLICITA; }
            set { _USUARIO_SOLICITA = value; }
        }

        public DateTime FECHA_SOLICITUD
        {
            get { return _FECHA_SOLICITUD; }
            set { _FECHA_SOLICITUD = value; }
        }

        
		
		
		
		
		public DateTime FECHA {get;set;}

		public string AUTORIZADOR {get;set;}

		public string TIPO_AUTORIZADOR {get;set;}

        public string SOLICITUD_ESTATUS { get; set; }
        ///
        public String MOTIVO
        {
            get{ return _MOTIVO;}
            set{_MOTIVO = value;}
        }
        
        

        #endregion

        #region Constructors

        public HISTORICO_SOLICITUDES_BE()
            {

		    }

	    #endregion
    }
}
