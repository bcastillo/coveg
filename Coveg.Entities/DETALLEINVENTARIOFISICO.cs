namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class DETALLEINVENTARIOFISICO_BE{

	#region Properties and privates

		
	private Int32 _DETALLEINVENTARIOFISICOID;
	private Int32 _ACTIVOID;
	private Int32 _CANTIDAD;
	private DateTime _FECHAHORA;
	private Int32 _INVENTARIOFISICOID;
	private String _USUARIO;

	
///
public Int32 DETALLEINVENTARIOFISICOID{
get{
return _DETALLEINVENTARIOFISICOID;
}
set{
_DETALLEINVENTARIOFISICOID = value;
}
}
///
public Int32 ACTIVOID{
get{
return _ACTIVOID;
}
set{
_ACTIVOID = value;
}
}
///
public Int32 CANTIDAD{
get{
return _CANTIDAD;
}
set{
_CANTIDAD = value;
}
}
///
public DateTime FECHAHORA{
get{
return _FECHAHORA;
}
set{
_FECHAHORA = value;
}
}
///
public Int32 INVENTARIOFISICOID{
get{
return _INVENTARIOFISICOID;
}
set{
_INVENTARIOFISICOID = value;
}
}
///
public String USUARIO{
get{
return _USUARIO;
}
set{
_USUARIO = value;
}
}

	#endregion

	#region Constructors
public DETALLEINVENTARIOFISICO_BE()
{

		}


	#endregion
	}
}