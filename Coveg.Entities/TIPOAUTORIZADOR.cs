namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class TIPOAUTORIZADOR_BE{

	#region Properties and privates

		
	private Int32 _TIPOAUTORIZADORID;
	private String _CLAVE;
	private String _DESCRIPCION;
	private Int32 _ESTATUS;

	
///
public Int32 TIPOAUTORIZADORID{
get{
return _TIPOAUTORIZADORID;
}
set{
_TIPOAUTORIZADORID = value;
}
}
///
public String CLAVE{
get{
return _CLAVE;
}
set{
_CLAVE = value;
}
}
///
public String DESCRIPCION{
get{
return _DESCRIPCION;
}
set{
_DESCRIPCION = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}

	#endregion

	#region Constructors
public TIPOAUTORIZADOR_BE()
{

		}


	#endregion
	}
}