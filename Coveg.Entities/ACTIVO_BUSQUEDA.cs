﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class ACTIVO_BUSQUEDA
    {
        #region Properties and privates

            private Int32 _ACTIVOID;
            private String _CLAVE;
            private String _DESCRIPCION;
            private String _NUMEROSERIE;
            private String _RESPONSABLE;
            private Int32 _TIPOACTIVOID;
            private String _NOMBRE;

            private String _DESC_RFIDTAG;
            private String _DESC_TIPOACTIVO;

            private Int32 _DETALLESALIDAID;
            private String _USUARIO_SOLICITO;
            private DateTime _FECHA_SOLICITUD;
            private String _USUARIO_SALIDA;
            private DateTime _FECHA_SALIDA;
            private DateTime _FECHARETORNO;
            private String _MOTIVO;


            //--Atributos de activo tipo automovil
            private String _CAPACIDAD;
            private Int32 _CILINDROS;
            private String _COLOR;
            private String _MARCA;
            private String _MODELO;
            private String _NUMERO_MOTOR;
            private String _NUMERO_SERIE_AUTO;
            private String _PLACAS;
            private Int32 _PUERTAS;
            private String _TIPO;
            private String _TRANSMISION;
            //--------------------------------------------------

            //--Atributos de activo tipo Documento
            private String _AREA;
            private String _CAJA;
            private String _ESTANTE;
            private DateTime _FECHA_INGRESO;
            private String _LEFFORT;
            private String _TIPO_DOCUMENTO;
            //--------------------------------------------------


            #region Info Solicitud

            public Int32 DETALLESALIDAID
            {
                get { return _DETALLESALIDAID; }
                set { _DETALLESALIDAID = value; }
            }
            public String USUARIO_SOLICITO
            {
                get { return _USUARIO_SOLICITO; }
                set { _USUARIO_SOLICITO=value;}
            }
            public DateTime FECHA_SOLICITUD
            {
                get { return _FECHA_SOLICITUD; }
                set { _FECHA_SOLICITUD = value; }
            }

            public String USUARIO_SALIDA
            {
                get { return _USUARIO_SALIDA; }
                set { _USUARIO_SALIDA = value; }
            }

            public DateTime FECHA_SALIDA
            {
                get { return _FECHA_SALIDA; }
                set { _FECHA_SALIDA = value; }
            }

            public DateTime FECHARETORNO
            {
                get { return _FECHARETORNO; }
                set { _FECHARETORNO = value; }
            }

            public String MOTIVO
            {
                get { return _MOTIVO; }
                set { _MOTIVO = value; }
            }

            #endregion

            #region Basicos


            public String DESC_TIPOACTIVO
            {
                get { return _DESC_TIPOACTIVO; }
                set { _DESC_TIPOACTIVO = value; }
            }

            public String DESC_RFIDTAG
            {
                get { return _DESC_RFIDTAG; }
                set { _DESC_RFIDTAG = value; }
            }

            public String NOMBRE
            {
                get { return _NOMBRE; }
                set { _NOMBRE = value; }
            }

            public Int32 TIPOACTIVOID
            {
                get { return _TIPOACTIVOID; }
                set { _TIPOACTIVOID = value; }
            }

            public Int32 ACTIVOID
            {
                get { return _ACTIVOID; }
                set { _ACTIVOID = value; }
            }

            public String CLAVE
            {
                get { return _CLAVE; }
                set { _CLAVE = value; }
            }

            public String DESCRIPCION
            {
                get { return _DESCRIPCION; }
                set { _DESCRIPCION = value; }
            }

            public String NUMEROSERIE
            {
                get { return _NUMEROSERIE; }
                set { _NUMEROSERIE = value; }
            }

            public String RESPONSABLE
            {
                get { return _RESPONSABLE;}
                set { _RESPONSABLE = value; }
            }
            #endregion

            #region Automoviles

            ///
            public String CAPACIDAD
            {
                get
                {
                    return _CAPACIDAD;
                }
                set
                {
                    _CAPACIDAD = value;
                }
            }
            ///
            public Int32 CILINDROS
            {
                get
                {
                    return _CILINDROS;
                }
                set
                {
                    _CILINDROS = value;
                }
            }

            ///
            public String COLOR
            {
                get
                {
                    return _COLOR;
                }
                set
                {
                    _COLOR = value;
                }
            }
            ///
            public String MARCA
            {
                get
                {
                    return _MARCA;
                }
                set
                {
                    _MARCA = value;
                }
            }
            ///
            public String MODELO
            {
                get
                {
                    return _MODELO;
                }
                set
                {
                    _MODELO = value;
                }
            }

            ///
            public String NUMERO_MOTOR
            {
                get
                {
                    return _NUMERO_MOTOR;
                }
                set
                {
                    _NUMERO_MOTOR = value;
                }
            }
            ///
            public String NUMERO_SERIE_AUTOMOVIL
            {
                get
                {
                    return _NUMERO_SERIE_AUTO;
                }
                set
                {
                    _NUMERO_SERIE_AUTO = value;
                }
            }
            ///
            public String PLACAS
            {
                get
                {
                    return _PLACAS;
                }
                set
                {
                    _PLACAS = value;
                }
            }
            ///
            public Int32 PUERTAS
            {
                get
                {
                    return _PUERTAS;
                }
                set
                {
                    _PUERTAS = value;
                }
            }
            ///
            public String TIPO
            {
                get
                {
                    return _TIPO;
                }
                set
                {
                    _TIPO = value;
                }
            }
            ///
            public String TRANSMISION
            {
                get
                {
                    return _TRANSMISION;
                }
                set
                {
                    _TRANSMISION = value;
                }
            }


            #endregion

            #region Documentos

            ///
            public String AREA
            {
                get
                {
                    return _AREA;
                }
                set
                {
                    _AREA = value;
                }
            }
            ///
            public String CAJA
            {
                get
                {
                    return _CAJA;
                }
                set
                {
                    _CAJA = value;
                }
            }
            ///
            public String ESTANTE
            {
                get
                {
                    return _ESTANTE;
                }
                set
                {
                    _ESTANTE = value;
                }
            }
            ///
            public DateTime FECHA_INGRESO
            {
                get
                {
                    return _FECHA_INGRESO;
                }
                set
                {
                    _FECHA_INGRESO = value;
                }
            }
            ///
            public String LEFFORT
            {
                get
                {
                    return _LEFFORT;
                }
                set
                {
                    _LEFFORT = value;
                }
            }
            ///
            public String TIPO_DOCUMENTO
            {
                get
                {
                    return _TIPO_DOCUMENTO;
                }
                set
                {
                    _TIPO_DOCUMENTO = value;
                }
            }


            #endregion

            #region Constructor

            public ACTIVO_BUSQUEDA()
            {
 
            }

            public ACTIVO_BUSQUEDA(Int32 intACTIVOID, String strCLAVE, String strDESCRIPCION, String strNUMEROSERIE,String strRESPONSABLE,Int32 intTIPOACTIVO)
            {
                ACTIVOID = intACTIVOID;
                CLAVE = strCLAVE;
                DESCRIPCION = strDESCRIPCION;
                NUMEROSERIE = strNUMEROSERIE;
                RESPONSABLE = strRESPONSABLE;
                TIPOACTIVOID = intTIPOACTIVO;
            }

        #endregion

        #endregion
    }
}
