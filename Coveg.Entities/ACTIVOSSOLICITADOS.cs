namespace Coveg.Entities
{

    using System;
    using System.Text;
    using System.Data;
    using System.Collections;
    using System.Collections.Generic;


    public class ACTIVOSSOLICITADOS_BE
    {

        #region Properties and privates


        private Int32 _ACTIVOSSOLICITADOSID;
        private Int32 _ACTIVOID;
        private Int32 _CANTIDAD;
        private Int32 _ESTATUS;
        private Int32 _SOLICITUDSALIDAID;
        private String _ACTIVO_DESC;

        private String _TIPO_ACTIVO;
        private String _CLAVE;
        private String _RESPONSABLE;


        #region Propiedades de autos

            private String _MARCA;
            private String _MODELO;
            private String _COLOR;
            private String _PLACAS;
            private String _TIPO;

            public String TIPO
            {
                get { return _TIPO; }
                set { _TIPO = value; }
            }

            public String MARCA
            {
                get { return _MARCA; }
                set { _MARCA = value; }
            }

            public String MODELO
            {
                get { return _MODELO; }
                set { _MODELO = value; }
            }

            public String COLOR
            {
                get { return _COLOR; }
                set { _COLOR = value; }
            }

            public String PLACAS
            {
                get { return _PLACAS; }
                set { _PLACAS = value; }
            }

        #endregion

        #region Propiedades de documentos

            private String _AREA;
		    private String _CAJA;
		    private String _ESTANTE;
		    private String _FECHA_INGRESO;
		    private String _LEFFORT;
		    private String _TIPO_DOCUMENTO;

            public String AREA
            {
                get { return _AREA; }
                set { _AREA = value; }
            }

            public String CAJA
            {
                get { return _CAJA; }
                set { _CAJA = value; }
            }

            public String ESTANTE
            {
                get { return _ESTANTE; }
                set { _ESTANTE = value; }
            }

            public String FECHA_INGRESO
            {
                get { return _FECHA_INGRESO; }
                set { _FECHA_INGRESO = value; }
            }

            public String LEFFORT
            {
                get { return _LEFFORT; }
                set { _LEFFORT = value; }
            }

            public String TIPO_DOCUMENTO
            {
                get { return _TIPO_DOCUMENTO; }
                set { _TIPO_DOCUMENTO = value; }
            }

        #endregion


        public String RESPONSABLE
        {
            get { return _RESPONSABLE; }
            set { _RESPONSABLE = value; }
        }

        public String CLAVE
        {
            get { return _CLAVE; }
            set { _CLAVE = value; }
        }

        public String TIPO_ACTIVO
        {
            get { return _TIPO_ACTIVO; }
            set { _TIPO_ACTIVO = value; }
        }

        public String ACTIVO_DESC
        {
            get { return _ACTIVO_DESC; }
            set { _ACTIVO_DESC = value; }
        }


        ///
        public Int32 ACTIVOSSOLICITADOSID
        {
            get
            {
                return _ACTIVOSSOLICITADOSID;
            }
            set
            {
                _ACTIVOSSOLICITADOSID = value;
            }
        }
        ///
        public Int32 ACTIVOID
        {
            get
            {
                return _ACTIVOID;
            }
            set
            {
                _ACTIVOID = value;
            }
        }
        ///
        public Int32 CANTIDAD
        {
            get
            {
                return _CANTIDAD;
            }
            set
            {
                _CANTIDAD = value;
            }
        }
        ///
        public Int32 ESTATUS
        {
            get
            {
                return _ESTATUS;
            }
            set
            {
                _ESTATUS = value;
            }
        }
        ///
        public Int32 SOLICITUDSALIDAID
        {
            get
            {
                return _SOLICITUDSALIDAID;
            }
            set
            {
                _SOLICITUDSALIDAID = value;
            }
        }

        #endregion

        #region Constructors
        public ACTIVOSSOLICITADOS_BE()
        {

        }


        #endregion
    }
}