namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class DETALLESALIDA_BE{

	#region Properties and privates

		
	private Int32 _DETALLESALIDAID;
	private Int32 _ACTIVOID;
	private Int32 _CANTIDAD;
	private DateTime _FECHA;
	private Int32 _SALIDAID;
	private String _USUARIO;

	
///
public Int32 DETALLESALIDAID{
get{
return _DETALLESALIDAID;
}
set{
_DETALLESALIDAID = value;
}
}
///
public Int32 ACTIVOID{
get{
return _ACTIVOID;
}
set{
_ACTIVOID = value;
}
}
///
public Int32 CANTIDAD{
get{
return _CANTIDAD;
}
set{
_CANTIDAD = value;
}
}
///
public DateTime FECHA{
get{
return _FECHA;
}
set{
_FECHA = value;
}
}
///
public Int32 SALIDAID{
get{
return _SALIDAID;
}
set{
_SALIDAID = value;
}
}
///
public String USUARIO{
get{
return _USUARIO;
}
set{
_USUARIO = value;
}
}

	#endregion

	#region Constructors
public DETALLESALIDA_BE()
{

		}


	#endregion
	}
}