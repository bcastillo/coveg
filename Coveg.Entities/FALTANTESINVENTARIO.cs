namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class FALTANTESINVENTARIO_BE{

	#region Properties and privates

		
	private Int32 _FALTANTESINVENTARIOID;
	private Int32 _ACTIVOID;
	private Int32 _DIFERENCIA;
	private DateTime _FECHAHORA;
	private Int32 _INVENTARIOFISICOID;
	private String _MOTIVO;

	
///
public Int32 FALTANTESINVENTARIOID{
get{
return _FALTANTESINVENTARIOID;
}
set{
_FALTANTESINVENTARIOID = value;
}
}
///
public Int32 ACTIVOID{
get{
return _ACTIVOID;
}
set{
_ACTIVOID = value;
}
}
///
public Int32 DIFERENCIA{
get{
return _DIFERENCIA;
}
set{
_DIFERENCIA = value;
}
}
///
public DateTime FECHAHORA{
get{
return _FECHAHORA;
}
set{
_FECHAHORA = value;
}
}
///
public Int32 INVENTARIOFISICOID{
get{
return _INVENTARIOFISICOID;
}
set{
_INVENTARIOFISICOID = value;
}
}
///
public String MOTIVO{
get{
return _MOTIVO;
}
set{
_MOTIVO = value;
}
}

	#endregion

	#region Constructors
public FALTANTESINVENTARIO_BE()
{

		}


	#endregion
	}
}