﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class LOGIN_BE
    {
        private String _Usuario;
        private String _Password;
        private Int32 _id_usuario;
        private Int32 _jefe_directo;
        private String _Nombre;
        private String _Rol;


        public String Usuario
        {
            get { return _Usuario; }
            set { _Usuario = value; }
        }

        public String Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public Int32 id_usuario
        {
            get { return _id_usuario; }
            set { _id_usuario = value; }
        }

        public Int32 jefe_directo
        {
            get { return _jefe_directo; }
            set { _jefe_directo = value; }
        }

        public String Nombre_Completo
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public String Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }


    }
}
