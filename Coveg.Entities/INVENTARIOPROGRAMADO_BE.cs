﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class INVENTARIOPROGRAMADO_BE
    {
        #region Properties and privates


        private Int32 _INVENTARIOPROGRAMADOID;
        private String _DESCRIPCION;
        private Int32 _ESTATUS;
        private DateTime _FECHAFIN;
        private DateTime _fechainicio;
        private DateTime _FECHAPROGRAMADA;
        private Int32 _ID_AREA;
        private String _DESC_ESTATUS;

        private Int32 _PENDIENTES;
        private Int32 _CANTIDAD;
        private float _AVANCE;
        private String _AREA;

        public Int32 PENDIENTES
        {
            get { return _PENDIENTES; }
            set { _PENDIENTES = value; }
        }

        public Int32 CANTIDAD
        {
            get { return _CANTIDAD; }
            set { _CANTIDAD = value; }
        }


        public float AVANCE
        {
            get { return _AVANCE; }
            set { _AVANCE = value; }
        }

        public String AREA
        {
            get { return _AREA; }
            set { _AREA = value; }
        }


        public String DESC_ESTATUS
        {
            get { return _DESC_ESTATUS; }
            set { _DESC_ESTATUS = value; }
        }


        ///
        public Int32 INVENTARIOPROGRAMADOID
        {
            get
            {
                return _INVENTARIOPROGRAMADOID;
            }
            set
            {
                _INVENTARIOPROGRAMADOID = value;
            }
        }
        ///
        public String DESCRIPCION
        {
            get
            {
                return _DESCRIPCION;
            }
            set
            {
                _DESCRIPCION = value;
            }
        }
        ///
        public Int32 ESTATUS
        {
            get
            {
                return _ESTATUS;
            }
            set
            {
                _ESTATUS = value;
            }
        }
        ///
        public DateTime FECHAFIN
        {
            get
            {
                return _FECHAFIN;
            }
            set
            {
                _FECHAFIN = value;
            }
        }
        ///
        public DateTime fechainicio
        {
            get
            {
                return _fechainicio;
            }
            set
            {
                _fechainicio = value;
            }
        }
        ///
        public DateTime FECHAPROGRAMADA
        {
            get
            {
                return _FECHAPROGRAMADA;
            }
            set
            {
                _FECHAPROGRAMADA = value;
            }
        }
        ///
        public Int32 ID_AREA
        {
            get
            {
                return _ID_AREA;
            }
            set
            {
                _ID_AREA = value;
            }
        }

        #endregion

        #region Constructors
        public INVENTARIOPROGRAMADO_BE()
        {

        }


        #endregion
    }
}
