namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class ESTATUS_BE{

	#region Properties and privates

		
	private Int32 _ESTATUSID;
	private String _DESCRIPCION;
	private String _NOMBRE;
	private Int32 _TIPOESTATUSID;
	private Int32 _VALOR;
    private String _DESC_TIPOESTATUS;
    private String _NOMBRE_TIPOESTATUS;

    public String NOMBRE_TIPOESTATUS
    {
        get { return _NOMBRE_TIPOESTATUS; }
        set { _NOMBRE_TIPOESTATUS = value; }
    }

    public String DESC_TIPOESTATUS
    {
        get { return _DESC_TIPOESTATUS; }
        set { _DESC_TIPOESTATUS = value; }
    }
	
///
public Int32 ESTATUSID{
get{
return _ESTATUSID;
}
set{
_ESTATUSID = value;
}
}
///
public String DESCRIPCION{
get{
return _DESCRIPCION;
}
set{
_DESCRIPCION = value;
}
}
///
public String NOMBRE{
get{
return _NOMBRE;
}
set{
_NOMBRE = value;
}
}
///
public Int32 TIPOESTATUSID{
get{
return _TIPOESTATUSID;
}
set{
_TIPOESTATUSID = value;
}
}
///
public Int32 VALOR{
get{
return _VALOR;
}
set{
_VALOR = value;
}
}

	#endregion

	#region Constructors
public ESTATUS_BE()
{

		}


	#endregion
	}
}