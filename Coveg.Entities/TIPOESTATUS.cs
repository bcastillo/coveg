namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class TIPOESTATUS_BE{

	#region Properties and privates

		
	private Int32 _TIPOESTATUSID;
	private String _DESCRIPCION;
	private String _NOMBRE;

	
///
public Int32 TIPOESTATUSID{
get{
return _TIPOESTATUSID;
}
set{
_TIPOESTATUSID = value;
}
}
///
public String DESCRIPCION{
get{
return _DESCRIPCION;
}
set{
_DESCRIPCION = value;
}
}
///
public String NOMBRE{
get{
return _NOMBRE;
}
set{
_NOMBRE = value;
}
}

	#endregion

	#region Constructors
public TIPOESTATUS_BE()
{

		}


	#endregion
	}
}