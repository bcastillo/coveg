namespace Coveg.Entities
{

    using System;
    using System.Text;
    using System.Data;
    using System.Collections;
    using System.Collections.Generic;


    public class DETALLEENTRADA_BE
    {

        #region Properties and privates


        private Int32 _DETALLEENTRADAID;
        private Int32 _ENTRADAID;
        private Int32 _ACTIVOID;
        private Int32 _CANTIDAD;
        private String _USUARIO;
        private DateTime _FECHA;
        
        private Int32 _DETALLESALIDAID;
        private String _OBSERVACION;

        public Int32 DETALLESALIDAID
        {
            get { return _DETALLESALIDAID; }
            set { _DETALLESALIDAID = value; }
        }

        public String OBSERVACION
        {
            get { return _OBSERVACION; }
            set { _OBSERVACION = value; }
        }

        ///
        public Int32 DETALLEENTRADAID
        {
            get
            {
                return _DETALLEENTRADAID;
            }
            set
            {
                _DETALLEENTRADAID = value;
            }
        }
        ///
        public Int32 ACTIVOID
        {
            get
            {
                return _ACTIVOID;
            }
            set
            {
                _ACTIVOID = value;
            }
        }
        ///
        public Int32 CANTIDAD
        {
            get
            {
                return _CANTIDAD;
            }
            set
            {
                _CANTIDAD = value;
            }
        }
        ///
        public Int32 ENTRADAID
        {
            get
            {
                return _ENTRADAID;
            }
            set
            {
                _ENTRADAID = value;
            }
        }
        ///
        public DateTime FECHA
        {
            get
            {
                return _FECHA;
            }
            set
            {
                _FECHA = value;
            }
        }
        ///
        public String USUARIO
        {
            get
            {
                return _USUARIO;
            }
            set
            {
                _USUARIO = value;
            }
        }

        #endregion

        #region Constructors
        public DETALLEENTRADA_BE()
        {

        }


        #endregion
    }
}