﻿using System;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace Coveg.Entities
{
    public class USUARIOROLBE
    {
        #region Properties and privates

            private Int32 _USUARIOROLID;
            private String _USUARIO;
            private String _CVE_ROL;
            private String _DESC_ROL;

            public Int32 USUARIOROLID
            {
                get { return _USUARIOROLID; }
                set { _USUARIOROLID = value; }
            }

            public String USUARIO
            {
                get { return _USUARIO; }
                set { _USUARIO = value; }
            }

            public String CVE_ROL
            {
                get { return _CVE_ROL; }
                set { _CVE_ROL = value; }
            }

            public String DESC_ROL
            {
                get { return _DESC_ROL; }
                set { _DESC_ROL = value; }
            }

        #endregion


        #region Constructors
            public USUARIOROLBE()
            {

		    }


	#endregion

    }
}
