namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class INVENTARIOFISICO_BE{

	#region Properties and privates

		
	private Int32 _INVENTARIOFISICOID;
	private DateTime _FECHA;
	private String _MOTIVO;
	private String _NUMEMPLEADO;
	private Int32 _UBICACIONID;

	
///
public Int32 INVENTARIOFISICOID{
get{
return _INVENTARIOFISICOID;
}
set{
_INVENTARIOFISICOID = value;
}
}
///
public DateTime FECHA{
get{
return _FECHA;
}
set{
_FECHA = value;
}
}
///
public String MOTIVO{
get{
return _MOTIVO;
}
set{
_MOTIVO = value;
}
}
///
public String NUMEMPLEADO{
get{
return _NUMEMPLEADO;
}
set{
_NUMEMPLEADO = value;
}
}
///
public Int32 UBICACIONID{
get{
return _UBICACIONID;
}
set{
_UBICACIONID = value;
}
}

	#endregion

	#region Constructors
public INVENTARIOFISICO_BE()
{

		}


	#endregion
	}
}