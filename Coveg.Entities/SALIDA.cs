namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class SALIDA_BE{

	#region Properties and privates

		
	private Int32 _SALIDAID;
	private Int32 _ESTATUS;
	private DateTime _FECHA;
	private Int32 _SOLICITUDSALIDAID;

	
///
public Int32 SALIDAID{
get{
return _SALIDAID;
}
set{
_SALIDAID = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}
///
public DateTime FECHA{
get{
return _FECHA;
}
set{
_FECHA = value;
}
}
///
public Int32 SOLICITUDSALIDAID{
get{
return _SOLICITUDSALIDAID;
}
set{
_SOLICITUDSALIDAID = value;
}
}

	#endregion

	#region Constructors
public SALIDA_BE()
{

		}


	#endregion
	}
}