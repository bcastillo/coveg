﻿using System;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace Coveg.Entities
{
    public class DETALLESOLICITUD_ACTIVOS_BE
    {
        #region Properties and privates ACTIVO


        private Int32 _ACTIVOID;
        private String _NOMBRE_ACTIVO;
        private String _DESC_ACTIVO;
        private String _CLAVE_ACTIVO;
        private String _ESTATUS_ACTIVO;
        private String _SERIE_ACTIVO;
        private String _TIPO_ACTIVO;

        private String _TAG;
        private String _RESPONSABLE;

        public String TAG
        {
            get { return _TAG; }
            set { _TAG = value; }
        }

        public String RESPONSABLE
        {
            get { return _RESPONSABLE; }
            set { _RESPONSABLE = value; }
        }
        public String TIPO_ACTIVO
        {
            get { return _TIPO_ACTIVO; }
            set { _TIPO_ACTIVO = value; }
        }

        public Int32 ACTIVOID
        {
            get { return _ACTIVOID; }
            set { _ACTIVOID = value; }
        }

        public String NOMBRE_ACTIVO
        {
            get { return _NOMBRE_ACTIVO; }
            set { _NOMBRE_ACTIVO = value; }
        }

        public String DESC_ACTIVO
        {
            get { return _DESC_ACTIVO; }
            set { _DESC_ACTIVO = value; }
        }

        public String CLAVE_ACTIVO
        {
            get { return _CLAVE_ACTIVO; }
            set { _CLAVE_ACTIVO = value; }
        }

        public String ESTATUS_ACTIVO
        {
            get { return _ESTATUS_ACTIVO; }
            set { _ESTATUS_ACTIVO = value; }
        }

        ///
        public String SERIE_ACTIVO
        {
            get
            {
                return _SERIE_ACTIVO;
            }
            set
            {
                _SERIE_ACTIVO = value;
            }
        }

        #endregion

        #region Properties and privates SOLICITUD

        private Int32 _SOLICITUDSALIDAID;
        private String _ESTATUS_SOLICITUD;
        private DateTime _FECHA;
        private String _MOTIVO;
        private String _SOLICITANTE;
        private DateTime _FECHASALIDA;
        private DateTime _FECHARETORNO;


        public Int32 SOLICITUDSALIDAID
        {
            get
            {
                return _SOLICITUDSALIDAID;
            }
            set
            {
                _SOLICITUDSALIDAID = value;
            }
        }

        public String ESTATUS_SOLICITUD
        {
            get { return _ESTATUS_SOLICITUD; }
            set { _ESTATUS_SOLICITUD = value; }
        }

        public DateTime FECHA
        {
            get
            {
                return _FECHA;
            }
            set
            {
                _FECHA = value;
            }
        }

        public String MOTIVO
        {
            get
            {
                return _MOTIVO;
            }
            set
            {
                _MOTIVO = value;
            }
        }

        public String SOLICITANTE
        {
            get { return _SOLICITANTE; }
            set { _SOLICITANTE = value; }
        }

        public DateTime FECHASALIDA
        {
            get { return _FECHASALIDA; }
            set { _FECHASALIDA = value; }
        }

        public DateTime FECHARETORNO
        {
            get { return _FECHARETORNO; }
            set { _FECHARETORNO = value; }
        }

        public String MOTIVO_RECHAZO { get; set; }
        #endregion

        #region Properties and privates DOCUMENTO


        private Int32 _DOCUMENTOID;
        private String _AREA;
        private String _CAJA;
        private String _ESTANTE;
        private DateTime _FECHA_INGRESO_DOCUMENTO;
        private String _LEFFORT;
        private String _TIPO_DOCUMENTO;

        public Int32 DOCUMENTOID
        {
            get
            {
                return _DOCUMENTOID;
            }
            set
            {
                _DOCUMENTOID = value;
            }
        }

        public String AREA
        {
            get
            {
                return _AREA;
            }
            set
            {
                _AREA = value;
            }
        }
        
        public String CAJA
        {
            get
            {
                return _CAJA;
            }
            set
            {
                _CAJA = value;
            }
        }
        
        public String ESTANTE
        {
            get
            {
                return _ESTANTE;
            }
            set
            {
                _ESTANTE = value;
            }
        }
        
        public DateTime FECHA_INGRESO_DOCUMENTO
        {
            get
            {
                return _FECHA_INGRESO_DOCUMENTO;
            }
            set
            {
                _FECHA_INGRESO_DOCUMENTO = value;
            }
        }
        
        public String LEFFORT
        {
            get
            {
                return _LEFFORT;
            }
            set
            {
                _LEFFORT = value;
            }
        }
        ///
        public String TIPO_DOCUMENTO
        {
            get
            {
                return _TIPO_DOCUMENTO;
            }
            set
            {
                _TIPO_DOCUMENTO = value;
            }
        }

        #endregion

        #region Properties and privates

        private Int32 _VEHICULOID;
        private String _CAPACIDAD;
        private Int32 _CILINDROS;
        private String _COLOR;
        private String _MARCA;
        private String _MODELO;
        private String _NUMERO_MOTOR;
        private String _NUMERO_SERIE_VEHICULO;
        private String _PLACAS;
        private Int32 _PUERTAS;
        private String _TIPO_VEHICULO;
        private String _TRANSMISION;


        public Int32 VEHICULOID
        {
            get
            {
                return _VEHICULOID;
            }
            set
            {
                _VEHICULOID = value;
            }
        }
        
        public String CAPACIDAD
        {
            get
            {
                return _CAPACIDAD;
            }
            set
            {
                _CAPACIDAD = value;
            }
        }
        
        public Int32 CILINDROS
        {
            get
            {
                return _CILINDROS;
            }
            set
            {
                _CILINDROS = value;
            }
        }
        
        public String COLOR
        {
            get
            {
                return _COLOR;
            }
            set
            {
                _COLOR = value;
            }
        }
        
        public String MARCA
        {
            get
            {
                return _MARCA;
            }
            set
            {
                _MARCA = value;
            }
        }
        
        public String MODELO
        {
            get
            {
                return _MODELO;
            }
            set
            {
                _MODELO = value;
            }
        }

        public String NUMERO_MOTOR
        {
            get
            {
                return _NUMERO_MOTOR;
            }
            set
            {
                _NUMERO_MOTOR = value;
            }
        }

        public String NUMERO_SERIE_VEHICULO
        {
            get
            {
                return _NUMERO_SERIE_VEHICULO;
            }
            set
            {
                _NUMERO_SERIE_VEHICULO = value;
            }
        }
        
        public String PLACAS
        {
            get
            {
                return _PLACAS;
            }
            set
            {
                _PLACAS = value;
            }
        }
        
        public Int32 PUERTAS
        {
            get
            {
                return _PUERTAS;
            }
            set
            {
                _PUERTAS = value;
            }
        }

        public String TIPO_VEHICULO
        {
            get
            {
                return _TIPO_VEHICULO;
            }
            set
            {
                _TIPO_VEHICULO = value;
            }
        }
        
        public String TRANSMISION
        {
            get
            {
                return _TRANSMISION;
            }
            set
            {
                _TRANSMISION = value;
            }
        }

        #endregion

        #region Constructor

            public DETALLESOLICITUD_ACTIVOS_BE()
            {
            }

        #endregion

    }
}

