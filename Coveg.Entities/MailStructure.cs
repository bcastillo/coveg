﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class MailStructure
    {
        public String From { get; set; }
        public String To { get; set; }
        public String Cc { get; set; }
        public String Subject { get; set; }
        public String Message { get; set; }
        public Boolean Html { get; set; }
    }
}
