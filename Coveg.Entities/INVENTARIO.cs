namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class INVENTARIO_BE{

	#region Properties and privates

		
	private Int32 _INVENTARIOID;
	private Int32 _ACTIVOID;
	private DateTime _FECHA;
	private Int32 _SALDO;
	private Int32 _UBICACIONID;

	
///
public Int32 INVENTARIOID{
get{
return _INVENTARIOID;
}
set{
_INVENTARIOID = value;
}
}
///
public Int32 ACTIVOID{
get{
return _ACTIVOID;
}
set{
_ACTIVOID = value;
}
}
///
public DateTime FECHA{
get{
return _FECHA;
}
set{
_FECHA = value;
}
}
///
public Int32 SALDO{
get{
return _SALDO;
}
set{
_SALDO = value;
}
}
///
public Int32 UBICACIONID{
get{
return _UBICACIONID;
}
set{
_UBICACIONID = value;
}
}

	#endregion

	#region Constructors
public INVENTARIO_BE()
{

		}


	#endregion
	}
}