﻿using System;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace Coveg.Entities
{
    public class VEHICULO_BE
    {
        #region Properties and privates


        private Int32 _VEHICULOID;
        private Int32 _ACTIVOID;
        private String _CAPACIDAD;
        private Int32 _CILINDROS;
        private String _COLOR;
        private String _MARCA;
        private String _MODELO;
        private String _NUMERO_MOTOR;
        private String _NUMERO_SERIE;
        private String _PLACAS;
        private Int32 _PUERTAS;
        private String _TIPO;
        private String _TRANSMISION;

        //Propiedades de ACTIVO
        private String _DESCRIPCION;
        private Int32 _ESTATUS;
        private String _NUMNOMINA;
        private Boolean _REQUIERE_SOLICITUD;
        private String _RESPONSABLE;
        private String _CLAVE;

        public String RESPONSABLE
        {
            get { return _RESPONSABLE; }
            set { _RESPONSABLE = value; }
        }

         ///
        public Boolean REQUIERE_SOLICITUD
        {
            get
            {
                return _REQUIERE_SOLICITUD;
            }
            set
            {
                _REQUIERE_SOLICITUD = value;
            }
        }

        ///
        public String NUMNOMINA
        {
            get
            {
                return _NUMNOMINA;
            }
            set
            {
                _NUMNOMINA = value;
            }
        }

        ///
        public String DESCRIPCION
        {
            get
            {
                return _DESCRIPCION;
            }
            set
            {
                _DESCRIPCION = value;
            }
        }
        ///
        public Int32 ESTATUS
        {
            get
            {
                return _ESTATUS;
            }
            set
            {
                _ESTATUS = value;
            }
        }


        ///
        public Int32 VEHICULOID
        {
            get
            {
                return _VEHICULOID;
            }
            set
            {
                _VEHICULOID = value;
            }
        }
        ///
        public Int32 ACTIVOID
        {
            get
            {
                return _ACTIVOID;
            }
            set
            {
                _ACTIVOID = value;
            }
        }
        ///
        public String CAPACIDAD
        {
            get
            {
                return _CAPACIDAD;
            }
            set
            {
                _CAPACIDAD = value;
            }
        }
        ///
        public Int32 CILINDROS
        {
            get
            {
                return _CILINDROS;
            }
            set
            {
                _CILINDROS = value;
            }
        }
        ///
        public String CLAVE
        {
            get
            {
                return _CLAVE;
            }
            set
            {
                _CLAVE = value;
            }
        }
        ///
        public String COLOR
        {
            get
            {
                return _COLOR;
            }
            set
            {
                _COLOR = value;
            }
        }
        ///
        public String MARCA
        {
            get
            {
                return _MARCA;
            }
            set
            {
                _MARCA = value;
            }
        }
        ///
        public String MODELO
        {
            get
            {
                return _MODELO;
            }
            set
            {
                _MODELO = value;
            }
        }

        ///
        public String NUMERO_MOTOR
        {
            get
            {
                return _NUMERO_MOTOR;
            }
            set
            {
                _NUMERO_MOTOR = value;
            }
        }
        ///
        public String NUMERO_SERIE
        {
            get
            {
                return _NUMERO_SERIE;
            }
            set
            {
                _NUMERO_SERIE = value;
            }
        }
        ///
        public String PLACAS
        {
            get
            {
                return _PLACAS;
            }
            set
            {
                _PLACAS = value;
            }
        }
        ///
        public Int32 PUERTAS
        {
            get
            {
                return _PUERTAS;
            }
            set
            {
                _PUERTAS = value;
            }
        }
        ///
        public String TIPO
        {
            get
            {
                return _TIPO;
            }
            set
            {
                _TIPO = value;
            }
        }
        ///
        public String TRANSMISION
        {
            get
            {
                return _TRANSMISION;
            }
            set
            {
                _TRANSMISION = value;
            }
        }

        #endregion

        #region Constructors
        public VEHICULO_BE()
        {

        }


        #endregion

    }
}
