namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class UBICACION_BE{

	#region Properties and privates

		
	private Int32 _UBICACIONID;
	private String _DESCRIPCION;
	private Int32 _ESTATUS;
	private String _NOMBRE;
    private String _DESC_ESTATUS;

    public String DESC_ESTATUS
    {
        get { return _DESC_ESTATUS; }
        set { _DESC_ESTATUS = value; }
    }

	
///
public Int32 UBICACIONID{
get{
return _UBICACIONID;
}
set{
_UBICACIONID = value;
}
}
///
public String DESCRIPCION{
get{
return _DESCRIPCION;
}
set{
_DESCRIPCION = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}
///
public String NOMBRE{
get{
return _NOMBRE;
}
set{
_NOMBRE = value;
}
}

	#endregion

	#region Constructors
public UBICACION_BE()
{

		}


	#endregion
	}
}