namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class RFIDTAG_BE{

	#region Properties and privates

		
	private Int32 _RFIDTAGID;
	private String _DESCRIPCION;
	private Int32 _ESTATUS;
	private DateTime _FECHAALTA;
	private String _TAG;
    private String _DESC_ESTATUS;

    public String DESC_ESTATUS
    {
        get { return _DESC_ESTATUS; }
        set { _DESC_ESTATUS = value; } 
    }
	
///
public Int32 RFIDTAGID{
get{
return _RFIDTAGID;
}
set{
_RFIDTAGID = value;
}
}
///
public String DESCRIPCION{
get{
return _DESCRIPCION;
}
set{
_DESCRIPCION = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}
///
public DateTime FECHAALTA{
get{
return _FECHAALTA;
}
set{
_FECHAALTA = value;
}
}
///
public String TAG{
get{
return _TAG;
}
set{
_TAG = value;
}
}

	#endregion

	#region Constructors
public RFIDTAG_BE()
{

		}


	#endregion
	}
}