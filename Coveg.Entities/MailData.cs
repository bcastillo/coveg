﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class MailData
    {
        public Int32 SOLICITUDSALIDAID { get; set; }
        public String Nombre_Completo { get; set; }
        public String Puesto { get; set; }
        public String correo_electronico { get; set; }
        public String correo_institucional { get; set; }
    }
}
