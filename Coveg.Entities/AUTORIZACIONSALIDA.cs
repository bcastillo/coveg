namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class AUTORIZACIONSALIDA_BE{

	#region Properties and privates

		
	private Int32 _AUTORIZACIONSALIDAID;
	private String _AUTORIZO;
	private String _COMENTARIOS;
	private Int32 _ESTATUS;
	private DateTime _FECHAREVISION;
	private Int32 _SOLICITUDSALIDAID;
	private Int32 _TIPOAUTORIZADORID;

	
///
public Int32 AUTORIZACIONSALIDAID{
get{
return _AUTORIZACIONSALIDAID;
}
set{
_AUTORIZACIONSALIDAID = value;
}
}
///
public String AUTORIZO{
get{
return _AUTORIZO;
}
set{
_AUTORIZO = value;
}
}
///
public String COMENTARIOS{
get{
return _COMENTARIOS;
}
set{
_COMENTARIOS = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}
///
public DateTime FECHAREVISION{
get{
return _FECHAREVISION;
}
set{
_FECHAREVISION = value;
}
}
///
public Int32 SOLICITUDSALIDAID{
get{
return _SOLICITUDSALIDAID;
}
set{
_SOLICITUDSALIDAID = value;
}
}
///
public Int32 TIPOAUTORIZADORID{
get{
return _TIPOAUTORIZADORID;
}
set{
_TIPOAUTORIZADORID = value;
}
}

	#endregion

	#region Constructors
public AUTORIZACIONSALIDA_BE()
{

		}


	#endregion
	}
}