﻿using System;
using System.Text;
using System.Data;
using System.Collections;
using System.Collections.Generic;

namespace Coveg.Entities
{
    public class DOCUMENTO_BE
    {
        #region Properties and privates


        private Int32 _DOCUMENTOID;
        private Int32 _ACTIVOID;
        private String _AREA;
        private String _CAJA;
        private String _ESTANTE;
        private DateTime _FECHA_INGRESO;
        private String _LEFFORT;
        private String _TIPO;

        //Propiedades de ACTIVO
        private String _DESCRIPCION;
        private Int32 _ESTATUS;
        private Boolean _REQUIERE_SOLICITUD;
        private String _CLAVE;
        private String _DESC_ESTATUS;

        public String DESC_ESTATUS
        {
            get
            {
                return _DESC_ESTATUS;
            }
            set
            {
                _DESC_ESTATUS = value;
            }
        }

        ///
        public Boolean REQUIERE_SOLICITUD
        {
            get
            {
                return _REQUIERE_SOLICITUD;
            }
            set
            {
                _REQUIERE_SOLICITUD = value;
            }
        }

        ///
        public String CLAVE
        {
            get
            {
                return _CLAVE;
            }
            set
            {
                _CLAVE = value;
            }
        }

        ///
        public String DESCRIPCION
        {
            get
            {
                return _DESCRIPCION;
            }
            set
            {
                _DESCRIPCION = value;
            }
        }
        ///
        public Int32 ESTATUS
        {
            get
            {
                return _ESTATUS;
            }
            set
            {
                _ESTATUS = value;
            }
        }


        ///
        public Int32 DOCUMENTOID
        {
            get
            {
                return _DOCUMENTOID;
            }
            set
            {
                _DOCUMENTOID = value;
            }
        }
        ///
        public Int32 ACTIVOID
        {
            get
            {
                return _ACTIVOID;
            }
            set
            {
                _ACTIVOID = value;
            }
        }
        ///
        public String AREA
        {
            get
            {
                return _AREA;
            }
            set
            {
                _AREA = value;
            }
        }
        ///
        public String CAJA
        {
            get
            {
                return _CAJA;
            }
            set
            {
                _CAJA = value;
            }
        }
        ///
        public String ESTANTE
        {
            get
            {
                return _ESTANTE;
            }
            set
            {
                _ESTANTE = value;
            }
        }
        ///
        public DateTime FECHA_INGRESO
        {
            get
            {
                return _FECHA_INGRESO;
            }
            set
            {
                _FECHA_INGRESO = value;
            }
        }
        ///
        public String LEFFORT
        {
            get
            {
                return _LEFFORT;
            }
            set
            {
                _LEFFORT = value;
            }
        }
        ///
        public String TIPO
        {
            get
            {
                return _TIPO;
            }
            set
            {
                _TIPO = value;
            }
        }

        #endregion

        #region Constructors
        public DOCUMENTO_BE()
        {

        }


        #endregion

    }
}
