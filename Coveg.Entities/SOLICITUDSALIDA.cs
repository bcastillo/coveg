namespace Coveg.Entities
{

    using System;
    using System.Text;
    using System.Data;
    using System.Collections;
    using System.Collections.Generic;


    public class SOLICITUDSALIDA_BE
    {

        #region Properties and privates


        private Int32 _SOLICITUDSALIDAID;
        private Int32 _ESTATUS;
        private DateTime _FECHA;

        private DateTime _FECHASALIDA;
        private DateTime _FECHARETORNO;

        private String _MOTIVO;
        private String _USUARIO;
        private String _NOMBRE;
        private String _DESC_ESTATUS;
        private Int32 _IDJEFEINMEDIATO;
        private String _COMENTARIOS_RESGUARDO;
        private String _COMENTARIOS_JEFE;
        private String _SOLICITANTE;
        private String _TIPO_AUTORIZACION;
        private String _TIPO;

        public String TIPO
        {
            get { return _TIPO; }
            set { _TIPO = value; }
        }

        public String SOLICITANTE
        {
            get { return _SOLICITANTE; }
            set { _SOLICITANTE = value; }
        }

        public String TIPO_AUTORIZACION
        {
            get { return _TIPO_AUTORIZACION; }
            set { _TIPO_AUTORIZACION = value; }
        }

        public DateTime FECHASALIDA
        {
            get { return _FECHASALIDA; }
            set { _FECHASALIDA = value; }
        }

        public DateTime FECHARETORNO
        {
            get { return _FECHARETORNO; }
            set { _FECHARETORNO = value; }
        }


        public String COMENTARIOS_JEFE
        {
            get { return _COMENTARIOS_JEFE; }
            set { _COMENTARIOS_JEFE = value; }
        }

        public String COMENTARIOS_RESGUARDO
        {
            get { return _COMENTARIOS_RESGUARDO; }
            set { _COMENTARIOS_RESGUARDO = value; }
        }

        public String NOMBRE
        {
            get { return _NOMBRE; }
            set { _NOMBRE = value; }
        }

        public Int32 IDJEFEINMEDIATO
        {
            get { return _IDJEFEINMEDIATO; }
            set { _IDJEFEINMEDIATO = value; }
        }

        public String DESC_ESTATUS
        {
            get { return _DESC_ESTATUS; }
            set { _DESC_ESTATUS = value; }
        }


        ///
        public Int32 SOLICITUDSALIDAID
        {
            get
            {
                return _SOLICITUDSALIDAID;
            }
            set
            {
                _SOLICITUDSALIDAID = value;
            }
        }
        ///
        public Int32 ESTATUS
        {
            get
            {
                return _ESTATUS;
            }
            set
            {
                _ESTATUS = value;
            }
        }
        ///
        public DateTime FECHA
        {
            get
            {
                return _FECHA;
            }
            set
            {
                _FECHA = value;
            }
        }
        ///
        public String MOTIVO
        {
            get
            {
                return _MOTIVO;
            }
            set
            {
                _MOTIVO = value;
            }
        }
        ///
        public String USUARIO
        {
            get
            {
                return _USUARIO;
            }
            set
            {
                _USUARIO = value;
            }
        }

        #endregion

        #region Constructors
        public SOLICITUDSALIDA_BE()
        {

        }

        public SOLICITUDSALIDA_BE(int intSOLICITUD,DateTime dtFECHA,String strSOLICITANTE,String strTIPO,String strTipoSolicitud)
        {
            SOLICITUDSALIDAID = intSOLICITUD;
            FECHA = dtFECHA;
            SOLICITANTE = strSOLICITANTE;
            TIPO_AUTORIZACION = strTIPO;
            TIPO = strTipoSolicitud;
        }

        #endregion
    }
}