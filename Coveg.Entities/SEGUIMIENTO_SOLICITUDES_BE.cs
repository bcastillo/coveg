﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Coveg.Entities
{
    public class SEGUIMIENTO_SOLICITUDES_BE
    {
        #region Properties and privates

            private String _USUARIO;
            private DateTime _FECHA_RETORNO;
            private String _TIPO_ACTIVO;
            private String _DESCRIPCION;
            private String _MOTIVO;

            private String _CLAVE;

            public String CLAVE
            {
                get { return _CLAVE; }
                set { _CLAVE = value; }
            }

        public String USUARIO
        {
            get { return _USUARIO; }
            set { _USUARIO = value; }
        }

        public DateTime FECHA_RETORNO
        {
            get { return _FECHA_RETORNO; }
            set { _FECHA_RETORNO = value; }
        }

        public String TIPO_ACTIVO
        {
            get { return _TIPO_ACTIVO; }
            set { _TIPO_ACTIVO = value; }
        }

        public String DESCRIPCION
        {
            get { return _DESCRIPCION; }
            set { _DESCRIPCION = value; }
        }

        public String MOTIVO
        {
            get { return _MOTIVO; }
            set { _MOTIVO = value; }
        }


        #endregion

        #region Constructors
            public SEGUIMIENTO_SOLICITUDES_BE()
            {

            }
        #endregion
    }
}
