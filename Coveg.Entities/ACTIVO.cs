namespace Coveg.Entities
{

    using System;
    using System.Text;
    using System.Data;
    using System.Collections;
    using System.Collections.Generic;


    public class ACTIVO_BE
    {

        #region Properties and privates


        private Int32 _ACTIVOID;
        private String _CLAVE;
        private String _DESCRIPCION;
        private Int32 _ESTATUS;
        private String _NOMBRE;
        private String _NUMEROSERIE;
        private String _NUMNOMINA;
        private String _RESPONSABLE;
        private Boolean _REQUIERE_SOLICITUD;
        private Int32 _RFIDTAGID;
        private Int32 _TIPOACTIVOID;
        private Int32 _UNIDADID;

        private String _DESC_ESTATUS;
        private String _DESC_RFIDTAG;
        private String _DESC_TIPOACTIVO;
        private String _DESC_UNIDAD;


        public String RESPONSABLE
        {
            get { return _RESPONSABLE; }
            set { _RESPONSABLE = value; }
        }

        public String DESC_ESTATUS
        {
            get { return _DESC_ESTATUS; }
            set { _DESC_ESTATUS = value; }
        }

        public String DESC_RFIDTAG
        {
            get { return _DESC_RFIDTAG; }
            set { _DESC_RFIDTAG = value; }
        }

        public String DESC_TIPOACTIVO
        {
            get { return _DESC_TIPOACTIVO; }
            set { _DESC_TIPOACTIVO = value; }
        }

        public String DESC_UNIDAD
        {
            get { return _DESC_UNIDAD; }
            set { _DESC_UNIDAD = value; }
        }

        ///
        public Int32 ACTIVOID
        {
            get
            {
                return _ACTIVOID;
            }
            set
            {
                _ACTIVOID = value;
            }
        }
        ///
        public String CLAVE
        {
            get
            {
                return _CLAVE;
            }
            set
            {
                _CLAVE = value;
            }
        }
        ///
        public String DESCRIPCION
        {
            get
            {
                return _DESCRIPCION;
            }
            set
            {
                _DESCRIPCION = value;
            }
        }
        ///
        public Int32 ESTATUS
        {
            get
            {
                return _ESTATUS;
            }
            set
            {
                _ESTATUS = value;
            }
        }
        ///
        public String NOMBRE
        {
            get
            {
                return _NOMBRE;
            }
            set
            {
                _NOMBRE = value;
            }
        }
        ///
        public String NUMEROSERIE
        {
            get
            {
                return _NUMEROSERIE;
            }
            set
            {
                _NUMEROSERIE = value;
            }
        }
        ///
        public String NUMNOMINA
        {
            get
            {
                return _NUMNOMINA;
            }
            set
            {
                _NUMNOMINA = value;
            }
        }
        ///
        public Boolean REQUIERE_SOLICITUD
        {
            get
            {
                return _REQUIERE_SOLICITUD;
            }
            set
            {
                _REQUIERE_SOLICITUD = value;
            }
        }
        ///
        public Int32 RFIDTAGID
        {
            get
            {
                return _RFIDTAGID;
            }
            set
            {
                _RFIDTAGID = value;
            }
        }
        ///
        public Int32 TIPOACTIVOID
        {
            get
            {
                return _TIPOACTIVOID;
            }
            set
            {
                _TIPOACTIVOID = value;
            }
        }
        ///
        public Int32 UNIDADID
        {
            get
            {
                return _UNIDADID;
            }
            set
            {
                _UNIDADID = value;
            }
        }


        #endregion

        #region Constructors
        public ACTIVO_BE()
        {

        }


        #endregion
    }
}