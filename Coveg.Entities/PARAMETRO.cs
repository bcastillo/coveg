namespace Coveg.Entities
{

	using System;
	using System.Text;
	using System.Data;
	using System.Collections;
	using System.Collections.Generic;
	

	public class PARAMETRO_BE{

	#region Properties and privates

		
	private Int32 _PARAMETROID;
	private String _CLAVE;
	private Int32 _ESTATUS;
	private String _NOMBRE;
	private Double _VALORNUMERICO;
	private String _VALORTEXTO;

	
///
public Int32 PARAMETROID{
get{
return _PARAMETROID;
}
set{
_PARAMETROID = value;
}
}
///
public String CLAVE{
get{
return _CLAVE;
}
set{
_CLAVE = value;
}
}
///
public Int32 ESTATUS{
get{
return _ESTATUS;
}
set{
_ESTATUS = value;
}
}
///
public String NOMBRE{
get{
return _NOMBRE;
}
set{
_NOMBRE = value;
}
}
///
public Double VALORNUMERICO{
get{
return _VALORNUMERICO;
}
set{
_VALORNUMERICO = value;
}
}
///
public String VALORTEXTO{
get{
return _VALORTEXTO;
}
set{
_VALORTEXTO = value;
}
}

	#endregion

	#region Constructors
public PARAMETRO_BE()
{

		}


	#endregion
	}
}