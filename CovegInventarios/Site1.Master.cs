﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CovegInventarios
{
    public partial class Site1 : System.Web.UI.MasterPage
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            string strPath = HttpContext.Current.Request.Url.AbsolutePath;
            AplicarSeguridad();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSalir_Click(object sender, ImageClickEventArgs e)
        {
            
        }

        public void AplicarSeguridad()
        {
            int intAuto, intDoctos, intUsuario, intAdmin, intRFID;

            if (Session["ROL"] == null)
                Response.Redirect("~/Views/Login.aspx");

            intAuto = Session["ROL"].ToString().LastIndexOf("AUTO");
            intDoctos = Session["ROL"].ToString().LastIndexOf("DOCTOS");
            intUsuario = Session["ROL"].ToString().LastIndexOf("USUARIO");
            intAdmin = Session["ROL"].ToString().LastIndexOf("ADMIN");
            intRFID = Session["ROL"].ToString().LastIndexOf("RFID");


            if (intAdmin != -1)
            {
                mnuAdministracion.Visible = true;
                mnuCatalogos.Visible = true;
                mnunInventarioProgramado.Visible = true;
                mnuAutorizacionSalidaDoctos.Visible = true;
                mnuAutorizacionSalidaAutos.Visible = true;
                mnuEntradaActivos.Visible = true;
                mnuInventario.Visible = true;
            }
            else
            {
                mnuInventario.Visible = false;
                mnuAdministracion.Visible = false;
                mnuCatalogos.Visible = false;
                mnunInventarioProgramado.Visible = false;
                mnuAutorizacionSalidaDoctos.Visible = false;
                mnuAutorizacionSalidaAutos.Visible = false;
                mnuEntradaActivos.Visible = false;

                if(intAuto!=-1)
                    mnuAutorizacionSalidaAutos.Visible = true;
                if(intDoctos!=-1)
                    mnuAutorizacionSalidaDoctos.Visible = true;
                if (intRFID != -1)
                    mnuEntradaActivos.Visible = true; ;
            }


            lblRolesValor.Text = Session["ROL"].ToString();
            lblUsuarioValor.Text = Session["NOMBRE"].ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void b_btnConfirmOK_Click(object sender, EventArgs e)
        {
            try
            {
                Session["ROL"] = null;
                Session["IDUSUARIO"] = null;
                Session["USUARIO"] = null;
                Session["NOMBRE"] = null;
                Session["IDJEFE"] = null;
                Session.Clear();
                Session.Abandon();
            }
            finally
            {
                Response.Redirect("~/Views/Login.aspx");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void b_btnConfirmCancel_Click(object sender, EventArgs e)
        {

        }


    }
}