﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace CovegInventarios
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }


        public void SetUpSession(String strRoles)
        {
            String[] roles = strRoles.Split(',');

            foreach (String rol in roles)
            {
                if (rol == "ADMIN") PerfilesUsuario.Add(CovegInventarios.Enums.UserProfile.ADMIN);
                else if (rol == "DOCTOS") PerfilesUsuario.Add(CovegInventarios.Enums.UserProfile.DOCTOS);
                else if (rol == "AUTO") PerfilesUsuario.Add(CovegInventarios.Enums.UserProfile.AUTO);
            }
            if (strRoles == String.Empty)
                PerfilesUsuario.Add(CovegInventarios.Enums.UserProfile.GUEST);
        }


        /// <summary>
        /// Lista de roles
        /// </summary>
        public static List<CovegInventarios.Enums.UserProfile> PerfilesUsuario
        {
            get
            {
                if (HttpContext.Current.Session["ROL"] == null)
                {
                    HttpContext.Current.Session["ROL"] = new List<CovegInventarios.Enums.UserProfile>();
                    (HttpContext.Current.Session["ROL"] as List<CovegInventarios.Enums.UserProfile>).Add(CovegInventarios.Enums.UserProfile.GUEST);
                }
                return (List<CovegInventarios.Enums.UserProfile>)HttpContext.Current.Session["ROL"];
            }
            set
            {
                HttpContext.Current.Session["ROL"] = value;
            }
        }

    }
}
