﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CovegInventarios.Views.Reportes
{
    public partial class ReporteInvProgDetallePop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LlenarReporteInvProgDetalle();
            }
        }

        private DataTable ValoresReporte()
        {
            String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptInvProgIDDetalle"].ToString();
            strConsulta = strConsulta.Replace("XXXXX", Request.QueryString["INVENTARIOPROGRAMADOID"].ToString());
            return Coveg.Data.OpenData.OpenQuery(strConsulta);

        }

        private void LlenarReporteInvProgDetalle()
        {
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Microsoft.Reporting.WebForms.LocalReport objRpt = ReportViewer1.LocalReport;
            objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReporteInvProgDetalle.rdlc";
            Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("InvProgDetalleDataSet", ValoresReporte());
            objRpt.DataSources.Clear();
            objRpt.DataSources.Add(rds);
            ReportViewer1.LocalReport.Refresh();
        }
    }
}