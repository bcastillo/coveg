﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CovegInventarios.Views.Reportes
{
    public partial class ReporteSolicitudes : System.Web.UI.Page
    {
        #region Eventos

        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);

        }

        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            panelMessageBox.Visible = false;
        }

        /// <summary>
        /// Ver reporte
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgVerReporte_Click(object sender, EventArgs e)
        {
            if (ValidateControls())
                LlenarReporteSolicitudes();
            else
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errFechasReportes"]);
        }

        #endregion

        #region Funciones
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strFechaIni"></param>
        /// <param name="strFechaFin"></param>
        /// <returns></returns>
        private DataTable ValoresSolicitudes(String strFechaIni, String strFechaFin)
        {
            String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptSolicitudes"].ToString();
            strConsulta = strConsulta.Replace("XFECHAINIX", strFechaIni);
            strConsulta = strConsulta.Replace("XFECHAFINAX", strFechaFin);
            return Coveg.Data.OpenData.OpenQuery(strConsulta);
        }
        /// <summary>
        /// 
        /// </summary>
        private void LlenarReporteSolicitudes()
        {
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Microsoft.Reporting.WebForms.LocalReport objRpt = ReportViewer1.LocalReport;
            objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReporteSolicitudes.rdlc";
            Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", ValoresSolicitudes(txtFechaInicio.Text, txtFechaFin.Text));
            objRpt.DataSources.Clear();
            objRpt.DataSources.Add(rds);
            ReportViewer1.LocalReport.Refresh();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Boolean ValidateControls()
        {
            Boolean bolRes = false;
            if (txtFechaInicio.Text != String.Empty)
                if (txtFechaFin.Text != String.Empty)
                    bolRes = true;
            return bolRes;
        }
        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Mensaje", message);
            panelMessageBox.Visible = true;
        }

        #endregion
    }
}