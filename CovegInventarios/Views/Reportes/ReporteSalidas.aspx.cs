﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CovegInventarios.Views.Reportes
{
    public partial class ReporteSalidas : System.Web.UI.Page
    {

        #region Eventos

            protected void Page_Load(object sender, EventArgs e)
            {
                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            }

            protected void imgVerReporte_Click(object sender, EventArgs e)
            {
                if (ValidateControls())
                    LlenarReporteSalidas(txtFechaInicio.Text, txtFechaFin.Text);
                else
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errFechasReportes"].ToString());
            }


            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

        #endregion

        #region Funciones

            private void LlenarReporteSalidas(String strFechaIni, String strFechaFin)
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                Microsoft.Reporting.WebForms.LocalReport objRpt = ReportViewer1.LocalReport;
                objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReporteSalidas.rdlc";
                Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", ValoresSalidas(txtFechaInicio.Text, txtFechaFin.Text));
                objRpt.DataSources.Clear();
                objRpt.DataSources.Add(rds);
                ReportViewer1.LocalReport.Refresh();
            }

            private DataTable ValoresSalidas(String strFechaIni, String strFechaFin)
            {
                String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptSalidas"].ToString();
                strConsulta = strConsulta + " CONVERT(DATE,'" + strFechaIni + "',103) AND CONVERT(DATE,'" + strFechaFin + "',103)";
                return Coveg.Data.OpenData.OpenQuery(strConsulta);
            }


            private Boolean ValidateControls()
            {
                Boolean bolRes = false;
                if (txtFechaInicio.Text != String.Empty)
                    if (txtFechaFin.Text != String.Empty)
                        bolRes = true;
                return bolRes;
            }

        #endregion
        
        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "err0001", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Titulo", message);
            panelMessageBox.Visible = true;
        }

        #endregion
    }
}