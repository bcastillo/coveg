﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CovegInventarios.Views.Reportes
{
    public partial class ReporteFaltantes : System.Web.UI.Page
    {
        #region Eventos

        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
        }

        protected void imgVerReporte_Click(object sender, EventArgs e)
        {
            string usuario = txtUsuario.Text;
            string[] tokens = usuario.Split('|');
            if (tokens != null && tokens.Length > 0)
            {
                usuario = tokens[0];
            }

            if(ValidateControls())
                LlenarReporteFaltantes(txtFechaInicial.Text, txtFechaFinal.Text,usuario);
        }


        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            panelMessageBox.Visible = false;
        }

        #endregion

        #region Funciones


        private Boolean ValidateControls()
        {
            Boolean bolRes = true;
            if (txtFechaInicial.Text == String.Empty || txtFechaFinal.Text == String.Empty)
            {
                bolRes = false;
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errFechasReportes"]);
            }

            return bolRes;
        }

        private void LlenarReporteFaltantes(String strFechaInicial,String strFechaFinal, String strUsuario)
        {
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            Microsoft.Reporting.WebForms.LocalReport objRpt = ReportViewer1.LocalReport;
            objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReporteFaltantesInventario.rdlc";
            Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", ValoresFaltantes(strFechaInicial,strFechaFinal, strUsuario));
            objRpt.DataSources.Clear();
            objRpt.DataSources.Add(rds);
            ReportViewer1.LocalReport.Refresh();
        }

        private DataTable ValoresFaltantes(String strFechaInicial,String strFechaFinal, String strUsuario)
        {
            String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptFaltantes"].ToString();

            strConsulta = strConsulta.Replace("XFECHAINIX", strFechaInicial);
            strConsulta = strConsulta.Replace("XFECHAFINAX", strFechaFinal);

            if(strUsuario != String.Empty)
                strConsulta = strConsulta + "AND UPPER(INVENTARIOFISICO.NUMEMPLEADO) = UPPER('" + strUsuario + "')";

            return Coveg.Data.OpenData.OpenQuery(strConsulta);
        }


        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "err0001", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Titulo", message);
            panelMessageBox.Visible = true;
        }

        #endregion

        #region Search Methods
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerUsuarios(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("select usuario + ' | ' + nombre +' ' + apellidos  usuario from vwUsuario  WITH(NOLOCK) WHERE usuario LIKE '" + prefixText + "%' or nombre LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["usuario"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        #endregion
    }
}