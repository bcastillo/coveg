﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views.Reportes
{
    public partial class DetalleSolicitud : System.Web.UI.Page
    {

        #region Properties

        Coveg.Reportes.Controller.DETALLESOLICITUDController _controller;

        /// <summary>
        ///
        /// </summary>
        protected Coveg.Reportes.Controller.DETALLESOLICITUDController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Reportes.Controller.DETALLESOLICITUDController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.DETALLESOLICITUD_ACTIVOS_BE> lista
        {
            get
            {
                return (List<Coveg.Entities.DETALLESOLICITUD_ACTIVOS_BE>)Page.Session["DETALLESOLICITUD_ACTIVOS_BE"];
            }
            set
            {
                Page.Session["DETALLESOLICITUD_ACTIVOS_BE"] = value;
            }
        }

        #endregion

        #region Eventos
        
            protected void Page_Load(object sender, EventArgs e)
            {
                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
                
                try
                {
                    if (!IsPostBack)
                    {
                        ConsultaSolicitud();
                    }
                }
                catch
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                }
            }


            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

        #endregion

        #region Funciones

            

            private void ClearControls()
            {
                txtEstatusActivo.Text=String.Empty;
                txtNombre.Text=String.Empty;
                txtDescripcionActivo.Text=String.Empty;
                txtNumSerieActivo.Text=String.Empty;
                txtClaveActivo.Text=String.Empty;

                txtEstatusDocumento.Text=String.Empty;
                txtFechaIngreso.Text=String.Empty;
                txtTipo.Text=String.Empty;
                txtArea.Text=String.Empty;
                txtCaja.Text=String.Empty;
                txtClave.Text=String.Empty;
                txtEstante.Text=String.Empty;
                txtCarpeta.Text=String.Empty;

                txtEstatusVehiculo.Text=String.Empty;
                txtNumeroSerie.Text=String.Empty;
                txtNumeroMotor.Text=String.Empty;
                txtNoPuertas.Text=String.Empty;
                txtCilindros.Text=String.Empty;
                txtCapacidad.Text=String.Empty;
                txtModelo.Text=String.Empty;
                txtPlacas.Text=String.Empty;
                txtMarca.Text=String.Empty;
                txtTipoAutomovil.Text=String.Empty;
                txtColor.Text=String.Empty;
                txtTransmision.Text = String.Empty;
            }

            private void ConsultaSolicitud()
            {
                SOLICITUDSALIDA_BE objBE = new SOLICITUDSALIDA_BE();
                objBE.SOLICITUDSALIDAID = Convert.ToInt32(Session["SOLICITUDIDDETALLE"].ToString());
                lista = Controller.SelectSolicitudActivos(objBE);

                if (lista != null)
                {

                    if (lista[0].TIPO_ACTIVO == "AUTO")
                    {
                        fsAutomovil.Visible = true;
                        fsActivo.Visible = false;
                        fsDocumento.Visible = false;
                    }
                    else
                    {
                        if (lista[0].TIPO_ACTIVO == "DOCUMENTO")
                        {
                            fsAutomovil.Visible = false;
                            fsActivo.Visible = false;
                            fsDocumento.Visible = true;
                        }
                        else
                        {
                            fsAutomovil.Visible = false;
                            fsActivo.Visible = true;
                            fsDocumento.Visible = false;
                        }
                    }
                    
                    ///Asignar valores de solicitud
                    txtEstatusSolicitud.Text = lista[0].ESTATUS_SOLICITUD;
                    txtSolicitante.Text = lista[0].SOLICITANTE;
                    txtFechaSalida.Text = lista[0].FECHASALIDA.ToShortDateString();
                    txtFecha.Text = lista[0].FECHA.ToShortDateString();
                    txtFechaRetorno.Text = lista[0].FECHARETORNO.ToShortDateString();
                    txtMotivo.Text = lista[0].MOTIVO;
                    txtMotivoRechazo.Text = lista[0].MOTIVO_RECHAZO;


                    ///asignar valores a grid
                    GridView1.DataSource = lista;
                    GridView1.DataBind();
                }
                else
                {
                    RaiseUserError("No existen datos para la solicitud seleccionada");
                }
            }


            private void SelectRecord(DETALLESOLICITUD_ACTIVOS_BE objBE)
            {
                ClearControls();
                if (objBE.TIPO_ACTIVO == "DOCUMENTO")
                {
                    ///Asignar valores de documento
                    txtEstatusDocumento.Text = objBE.ESTATUS_ACTIVO;
                    txtFechaIngreso.Text = objBE.FECHA_INGRESO_DOCUMENTO.ToShortDateString();
                    txtTipo.Text = objBE.TIPO_DOCUMENTO;
                    txtArea.Text = objBE.AREA;
                    txtCaja.Text = objBE.CAJA;
                    txtClave.Text = objBE.CLAVE_ACTIVO;
                    txtEstante.Text = objBE.ESTANTE;
                    txtCarpeta.Text = objBE.LEFFORT;
                }
                else
                {
                    if (objBE.TIPO_ACTIVO == "AUTO")
                    {
                        ///Asignar valores de vehiculo
                        txtEstatusVehiculo.Text = objBE.ESTATUS_ACTIVO;
                        txtNumeroSerie.Text = objBE.NUMERO_SERIE_VEHICULO;
                        txtNumeroMotor.Text = objBE.NUMERO_MOTOR;
                        txtNoPuertas.Text = objBE.PUERTAS.ToString();
                        txtCilindros.Text = objBE.CILINDROS.ToString();
                        txtCapacidad.Text = objBE.CAPACIDAD;
                        txtModelo.Text = objBE.MODELO;
                        txtPlacas.Text = objBE.PLACAS;
                        txtMarca.Text = objBE.MARCA;
                        txtTipoAutomovil.Text = objBE.TIPO_VEHICULO;
                        txtColor.Text = objBE.COLOR;
                        txtTransmision.Text = objBE.TRANSMISION;
                    }
                    else
                    {
                        ///Asignar valores de activo
                        txtEstatusActivo.Text = objBE.ESTATUS_ACTIVO;
                        txtNombre.Text = objBE.NOMBRE_ACTIVO;
                        txtDescripcionActivo.Text = objBE.DESC_ACTIVO;
                        txtNumSerieActivo.Text = objBE.SERIE_ACTIVO;
                        txtClaveActivo.Text = objBE.CLAVE_ACTIVO;
                    }
                }
            }

        #endregion

        #region ICatalogoView<> Members

            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error", "Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }

            #endregion

        #region Gridview Eventos


            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                }
            }


            /// <summary>
            /// Cambia pagina del grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                GridView1.PageIndex = e.NewPageIndex;
                GridView1.DataSource = lista;
                GridView1.DataBind();
            }

            /// <summary>
            /// Selecciona un elemento del grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (GridView1.SelectedRow != null)
                {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;
                    Coveg.Entities.DETALLESOLICITUD_ACTIVOS_BE objBE = lista[selectedRow];
                    SelectRecord(objBE);
                }
                
            }


            /// <summary>
            /// Ordenar grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (lista == null || lista.Count == 0)
                    return;

                if (e.SortExpression == "ACTIVOID")
                {
                    Page.Session["SortExpression"] = "ACTIVOID";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.ACTIVOID);
                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.ACTIVOID);
                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "RESPONSABLE")
                    {
                        Page.Session["SortExpression"] = "RESPONSABLE";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.RESPONSABLE);
                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.RESPONSABLE);
                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                else
                    if (e.SortExpression == "TAG")
                    {
                        Page.Session["SortExpression"] = "TAG";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.TAG);
                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.TAG);
                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                else
                    if (e.SortExpression == "AREA")
                    {
                        Page.Session["SortExpression"] = "AREA";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.AREA);
                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.AREA);
                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "CAJA")
                        {
                            Page.Session["SortExpression"] = "CAJA";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.CAJA);
                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.CAJA);
                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "CAPACIDAD")
                            {
                                Page.Session["SortExpression"] = "CAPACIDAD";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.CAPACIDAD);
                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.CAPACIDAD);
                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "CILINDROS")
                                {
                                    Page.Session["SortExpression"] = "CILINDROS";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.CILINDROS);
                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.CILINDROS);
                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "CLAVE_ACTIVO")
                                    {
                                        Page.Session["SortExpression"] = "CLAVE_ACTIVO";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.CLAVE_ACTIVO);
                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.CLAVE_ACTIVO);
                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "COLOR")
                                        {
                                            Page.Session["SortExpression"] = "COLOR";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.COLOR);
                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.COLOR);
                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "DESC_ACTIVO")
                                            {
                                                Page.Session["SortExpression"] = "DESC_ACTIVO";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.DESC_ACTIVO);
                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.DESC_ACTIVO);
                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "DOCUMENTOID")
                                                {
                                                    Page.Session["SortExpression"] = "DOCUMENTOID";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.DOCUMENTOID);
                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.DOCUMENTOID);
                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "ESTANTE")
                                                    {
                                                        Page.Session["SortExpression"] = "ESTANTE";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.ESTANTE);
                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.ESTANTE);
                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "ESTATUS_ACTIVO")
                                                        {
                                                            Page.Session["SortExpression"] = "ESTATUS_ACTIVO";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.ESTATUS_ACTIVO);
                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.ESTATUS_ACTIVO);
                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "ESTATUS_SOLICITUD")
                                                            {
                                                                Page.Session["SortExpression"] = "ESTATUS_SOLICITUD";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.ESTATUS_SOLICITUD);
                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.ESTATUS_SOLICITUD);
                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }

                                                            else
                                                                if (e.SortExpression == "FECHA")
                                                                {
                                                                    Page.Session["SortExpression"] = "FECHA";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.FECHA);
                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.FECHA);
                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }
                                                                else
                                                                    if (e.SortExpression == "FECHA_INGRESO_DOCUMENTO")
                                                                    {
                                                                        Page.Session["SortExpression"] = "FECHA_INGRESO_DOCUMENTO";
                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                        {
                                                                            var v = lista.OrderBy(p => p.FECHA_INGRESO_DOCUMENTO);
                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                            Page.Session["SortDirection"] = "Descending";
                                                                        }
                                                                        else
                                                                        {
                                                                            var v = lista.OrderByDescending(p => p.FECHA_INGRESO_DOCUMENTO);
                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                        }
                                                                    }
                                                                    else
                                                                        if (e.SortExpression == "FECHARETORNO")
                                                                        {
                                                                            Page.Session["SortExpression"] = "FECHARETORNO";
                                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                            {
                                                                                var v = lista.OrderBy(p => p.FECHARETORNO);
                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                Page.Session["SortDirection"] = "Descending";
                                                                            }
                                                                            else
                                                                            {
                                                                                var v = lista.OrderByDescending(p => p.FECHARETORNO);
                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                Page.Session["SortDirection"] = "Ascending";
                                                                            }
                                                                        }
                                                                        else
                                                                            if (e.SortExpression == "FECHASALIDA")
                                                                            {
                                                                                Page.Session["SortExpression"] = "FECHASALIDA";
                                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                {
                                                                                    var v = lista.OrderBy(p => p.FECHASALIDA);
                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                    Page.Session["SortDirection"] = "Descending";
                                                                                }
                                                                                else
                                                                                {
                                                                                    var v = lista.OrderByDescending(p => p.FECHASALIDA);
                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                                }
                                                                            }
                                                                            else
                                                                                if (e.SortExpression == "LEFFORT")
                                                                                {
                                                                                    Page.Session["SortExpression"] = "LEFFORT";
                                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                    {
                                                                                        var v = lista.OrderBy(p => p.LEFFORT);
                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                        Page.Session["SortDirection"] = "Descending";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        var v = lista.OrderByDescending(p => p.LEFFORT);
                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                                    }
                                                                                }
                                                                                else
                                                                                    if (e.SortExpression == "MARCA")
                                                                                    {
                                                                                        Page.Session["SortExpression"] = "MARCA";
                                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                        {
                                                                                            var v = lista.OrderBy(p => p.MARCA);
                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                            Page.Session["SortDirection"] = "Descending";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            var v = lista.OrderByDescending(p => p.MARCA);
                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                        if (e.SortExpression == "MODELO")
                                                                                        {
                                                                                            Page.Session["SortExpression"] = "MODELO";
                                                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                            {
                                                                                                var v = lista.OrderBy(p => p.MODELO);
                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                Page.Session["SortDirection"] = "Descending";
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                var v = lista.OrderByDescending(p => p.MODELO);
                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                Page.Session["SortDirection"] = "Ascending";
                                                                                            }
                                                                                        }
                                                                                        else
                                                                                            if (e.SortExpression == "MOTIVO")
                                                                                            {
                                                                                                Page.Session["SortExpression"] = "MOTIVO";
                                                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                {
                                                                                                    var v = lista.OrderBy(p => p.MOTIVO);
                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                    Page.Session["SortDirection"] = "Descending";
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    var v = lista.OrderByDescending(p => p.MOTIVO);
                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                                                }
                                                                                            }
                                                                                            else
                                                                                                if (e.SortExpression == "NOMBRE_ACTIVO")
                                                                                                {
                                                                                                    Page.Session["SortExpression"] = "NOMBRE_ACTIVO";
                                                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                    {
                                                                                                        var v = lista.OrderBy(p => p.NOMBRE_ACTIVO);
                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                        Page.Session["SortDirection"] = "Descending";
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        var v = lista.OrderByDescending(p => p.NOMBRE_ACTIVO);
                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                                                    }
                                                                                                }
                                                                                                else
                                                                                                    if (e.SortExpression == "NUMERO_MOTOR")
                                                                                                    {
                                                                                                        Page.Session["SortExpression"] = "NUMERO_MOTOR";
                                                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                        {
                                                                                                            var v = lista.OrderBy(p => p.NUMERO_MOTOR);
                                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                            Page.Session["SortDirection"] = "Descending";
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            var v = lista.OrderByDescending(p => p.NUMERO_MOTOR);
                                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                                                        }
                                                                                                    }
                                                                                                    else
                                                                                                        if (e.SortExpression == "NUMERO_SERIE_VEHICULO")
                                                                                                        {
                                                                                                            Page.Session["SortExpression"] = "NUMERO_SERIE_VEHICULO";
                                                                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                            {
                                                                                                                var v = lista.OrderBy(p => p.NUMERO_SERIE_VEHICULO);
                                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                Page.Session["SortDirection"] = "Descending";
                                                                                                            }
                                                                                                            else
                                                                                                            {
                                                                                                                var v = lista.OrderByDescending(p => p.NUMERO_SERIE_VEHICULO);
                                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                Page.Session["SortDirection"] = "Ascending";
                                                                                                            }
                                                                                                        }
                                                                                                        else
                                                                                                            if (e.SortExpression == "PLACAS")
                                                                                                            {
                                                                                                                Page.Session["SortExpression"] = "PLACAS";
                                                                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                {
                                                                                                                    var v = lista.OrderBy(p => p.PLACAS);
                                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                    Page.Session["SortDirection"] = "Descending";
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    var v = lista.OrderByDescending(p => p.PLACAS);
                                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                                                                }
                                                                                                            }
                                                                                                            else
                                                                                                                if (e.SortExpression == "PUERTAS")
                                                                                                                {
                                                                                                                    Page.Session["SortExpression"] = "PUERTAS";
                                                                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                    {
                                                                                                                        var v = lista.OrderBy(p => p.PUERTAS);
                                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                        Page.Session["SortDirection"] = "Descending";
                                                                                                                    }
                                                                                                                    else
                                                                                                                    {
                                                                                                                        var v = lista.OrderByDescending(p => p.PUERTAS);
                                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                                                                    }
                                                                                                                }
                                                                                                                else
                                                                                                                    if (e.SortExpression == "SERIE_ACTIVO")
                                                                                                                    {
                                                                                                                        Page.Session["SortExpression"] = "SERIE_ACTIVO";
                                                                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                        {
                                                                                                                            var v = lista.OrderBy(p => p.SERIE_ACTIVO);
                                                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                            Page.Session["SortDirection"] = "Descending";
                                                                                                                        }
                                                                                                                        else
                                                                                                                        {
                                                                                                                            var v = lista.OrderByDescending(p => p.SERIE_ACTIVO);
                                                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                                                                        }
                                                                                                                    }
                                                                                                                    else
                                                                                                                        if (e.SortExpression == "SOLICITANTE")
                                                                                                                        {
                                                                                                                            Page.Session["SortExpression"] = "SOLICITANTE";
                                                                                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                            {
                                                                                                                                var v = lista.OrderBy(p => p.SOLICITANTE);
                                                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                Page.Session["SortDirection"] = "Descending";
                                                                                                                            }
                                                                                                                            else
                                                                                                                            {
                                                                                                                                var v = lista.OrderByDescending(p => p.SOLICITANTE);
                                                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                Page.Session["SortDirection"] = "Ascending";
                                                                                                                            }
                                                                                                                        }
                                                                                                                        else
                                                                                                                            if (e.SortExpression == "SOLICITUDSALIDAID")
                                                                                                                            {
                                                                                                                                Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                                                                                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                                {
                                                                                                                                    var v = lista.OrderBy(p => p.SOLICITUDSALIDAID);
                                                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                    Page.Session["SortDirection"] = "Descending";
                                                                                                                                }
                                                                                                                                else
                                                                                                                                {
                                                                                                                                    var v = lista.OrderByDescending(p => p.SOLICITUDSALIDAID);
                                                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                                                                                }
                                                                                                                            }
                                                                                                                            else
                                                                                                                                if (e.SortExpression == "TIPO_ACTIVO")
                                                                                                                                {
                                                                                                                                    Page.Session["SortExpression"] = "TIPO_ACTIVO";
                                                                                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                                    {
                                                                                                                                        var v = lista.OrderBy(p => p.TIPO_ACTIVO);
                                                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                        Page.Session["SortDirection"] = "Descending";
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                    {
                                                                                                                                        var v = lista.OrderByDescending(p => p.TIPO_ACTIVO);
                                                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                else
                                                                                                                                    if (e.SortExpression == "TIPO_DOCUMENTO")
                                                                                                                                    {
                                                                                                                                        Page.Session["SortExpression"] = "TIPO_DOCUMENTO";
                                                                                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                                        {
                                                                                                                                            var v = lista.OrderBy(p => p.TIPO_DOCUMENTO);
                                                                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                            Page.Session["SortDirection"] = "Descending";
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                        {
                                                                                                                                            var v = lista.OrderByDescending(p => p.TIPO_DOCUMENTO);
                                                                                                                                            lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                    else
                                                                                                                                        if (e.SortExpression == "TIPO_VEHICULO")
                                                                                                                                        {
                                                                                                                                            Page.Session["SortExpression"] = "TIPO_VEHICULO";
                                                                                                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                                            {
                                                                                                                                                var v = lista.OrderBy(p => p.TIPO_VEHICULO);
                                                                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                                Page.Session["SortDirection"] = "Descending";
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                            {
                                                                                                                                                var v = lista.OrderByDescending(p => p.TIPO_VEHICULO);
                                                                                                                                                lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                                Page.Session["SortDirection"] = "Ascending";
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        else
                                                                                                                                            if (e.SortExpression == "TRANSMISION")
                                                                                                                                            {
                                                                                                                                                Page.Session["SortExpression"] = "TRANSMISION";
                                                                                                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                                                {
                                                                                                                                                    var v = lista.OrderBy(p => p.TRANSMISION);
                                                                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                                    Page.Session["SortDirection"] = "Descending";
                                                                                                                                                }
                                                                                                                                                else
                                                                                                                                                {
                                                                                                                                                    var v = lista.OrderByDescending(p => p.TRANSMISION);
                                                                                                                                                    lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            else
                                                                                                                                                if (e.SortExpression == "VEHICULOID")
                                                                                                                                                {
                                                                                                                                                    Page.Session["SortExpression"] = "VEHICULOID";
                                                                                                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                                                                                    {
                                                                                                                                                        var v = lista.OrderBy(p => p.VEHICULOID);
                                                                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                                        Page.Session["SortDirection"] = "Descending";
                                                                                                                                                    }
                                                                                                                                                    else
                                                                                                                                                    {
                                                                                                                                                        var v = lista.OrderByDescending(p => p.VEHICULOID);
                                                                                                                                                        lista = v.ToList<DETALLESOLICITUD_ACTIVOS_BE>();
                                                                                                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                                                                                                    }
                                                                                                                                                }
                GridView1.SelectedIndex = -1;
                GridView1.DataSource = lista;
                GridView1.DataBind();
                ClearControls();
            }

        #endregion
    }
}