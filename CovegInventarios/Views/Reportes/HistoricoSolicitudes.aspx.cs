﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views.Reportes
{
    public partial class HistoricoSolicitudes : System.Web.UI.Page
    {
        #region Properties

            Coveg.Reportes.Controller.HISTORICOSOLController _controller;

            /// <summary>
            ///
            /// </summary>
            protected Coveg.Reportes.Controller.HISTORICOSOLController Controller
            {
                get
                {
                    if (_controller == null)
                    {
                        _controller = new Coveg.Reportes.Controller.HISTORICOSOLController();
                    }

                    return _controller;
                }
                set
                {
                    _controller = value;
                }
            }

            /// <summary>
            ///
            /// </summary>
            private int selectedRow
            {
                get
                {
                    if (Page.Session["SelectedROW"] == null)
                    {
                        Page.Session["SelectedROW"] = -1;
                    }
                    return (Int32)Page.Session["SelectedROW"];
                }
                set
                {
                    Page.Session["SelectedROW"] = value;
                }
            }

            /// <summary>
            ///
            /// </summary>
            private List<Coveg.Entities.HISTORICO_SOLICITUDES_BE> lista
            {
                get
                {
                    return (List<Coveg.Entities.HISTORICO_SOLICITUDES_BE>)Page.Session["HISTORICOSOLICITUDES"];
                }
                set
                {
                    Page.Session["HISTORICOSOLICITUDES"] = value;
                }
            }

        #endregion

        #region Eventos
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void Page_Load(object sender, EventArgs e)
            {
                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
                try
                {
                    if (!IsPostBack)
                    {
                        Page.Session["SortDirection"] = "Ascending";
                        Page.Session["SortExpression"] = "";
                        CargarReporte();
                    }
                }
                catch
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void txtSearch_TextChanged(object sender, EventArgs e)
            {
                Filtrar();
            }

            /// <summary>
            /// Selección de reporte
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlReporte_SelectedIndexChanged(object sender, EventArgs e)
            {
                CargarReporte();
            }
            /// <summary>
            /// Boton de aceptar de caja de texto
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

        #endregion

        #region Eventos de rechazo
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvRechazadas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                }
            }
            /// <summary>
            /// Desplegar detalle de elemento seleccionado
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvRechazadas_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (grvRechazadas.SelectedRow != null)
                {
                    selectedRow = grvRechazadas.SelectedRow.DataItemIndex;
                    Session["SOLICITUDIDDETALLE"] = lista[selectedRow].SOLICITUDSALIDAID;
                    Session["SOLICITUDIDDETALLETIPO"] = ddlReporte.SelectedValue;
                    Response.Redirect("DetalleSolicitud.aspx");
                }

            }
            /// <summary>
            /// Paginacion del grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvRechazadas_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvRechazadas.PageIndex = e.NewPageIndex;
                grvRechazadas.DataSource = lista;
                grvRechazadas.DataBind();
            }

            /// <summary>
            /// Ordeamiento del grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvRechazadas_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (lista == null || lista.Count == 0)
                    return;

                
                        if (e.SortExpression == "COMENTARIOS_RECHAZO")
                        {
                            Page.Session["SortExpression"] = "COMENTARIOS_RECHAZO";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DIFERENCIA")
                            {
                                Page.Session["SortExpression"] = "DIFERENCIA";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "FECHA_RECHAZO")
                                {
                                    Page.Session["SortExpression"] = "FECHA_RECHAZO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "FECHA_SOLICITUD")
                                    {
                                        Page.Session["SortExpression"] = "FECHA_SOLICITUD";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "FECHARETORNO")
                                        {
                                            Page.Session["SortExpression"] = "FECHARETORNO";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.FECHARETORNO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.FECHARETORNO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "FECHASALIDA")
                                            {
                                                Page.Session["SortExpression"] = "FECHASALIDA";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "MOTIVO")
                                                {
                                                    Page.Session["SortExpression"] = "MOTIVO";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "SOLICITUDSALIDAID")
                                                    {
                                                        Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "TIPO_USUARIO_RECHAZA")
                                                        {
                                                            Page.Session["SortExpression"] = "TIPO_USUARIO_RECHAZA";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.TIPO_USUARIO_RECHAZA);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.TIPO_USUARIO_RECHAZA);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "USUARIO_RECHAZO")
                                                            {
                                                                Page.Session["SortExpression"] = "USUARIO_RECHAZO";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.USUARIO_RECHAZO);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.USUARIO_RECHAZO);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }
                                                            else
                                                                if (e.SortExpression == "USUARIO_SOLICITA")
                                                                {
                                                                    Page.Session["SortExpression"] = "USUARIO_SOLICITA";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }
                grvRechazadas.SelectedIndex = -1;
                grvRechazadas.DataSource = lista;
                grvRechazadas.DataBind();
            }
        #endregion

        #region Eventos de Aprobados
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvAutorizados_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                    if (Convert.ToDateTime(e.Row.Cells[5].Text) == new DateTime())
                        e.Row.Cells[5].Text = "--";
                    else
                        e.Row.Cells[5].Text = Convert.ToDateTime(e.Row.Cells[5].Text).ToShortDateString();

                    if (Convert.ToDateTime(e.Row.Cells[4].Text) == new DateTime())
                        e.Row.Cells[4].Text = "--";
                    else
                        e.Row.Cells[4].Text = Convert.ToDateTime(e.Row.Cells[4].Text).ToShortDateString();
                }
            }

            /// <summary>
            /// Desplegar detalle de elemento seleccionado
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvAutorizados_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (grvAutorizados.SelectedRow != null)
                {
                    selectedRow = grvAutorizados.SelectedRow.DataItemIndex;
                    Session["SOLICITUDIDDETALLE"] = lista[selectedRow].SOLICITUDSALIDAID;
                    Session["SOLICITUDIDDETALLETIPO"] = ddlReporte.SelectedValue;
                    Response.Redirect("DetalleSolicitud.aspx");
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvAutorizados_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvAutorizados.PageIndex = e.NewPageIndex;
                grvAutorizados.DataSource = lista;
                grvAutorizados.DataBind();
            }
            /// <summary>
            /// Ordeamiento del grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvAutorizados_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (lista == null || lista.Count == 0)
                    return;

                if (e.SortExpression == "ACTIVOS_DEVUELTOS")
                {
                    Page.Session["SortExpression"] = "ACTIVOS_DEVUELTOS";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.ACTIVOS_DEVUELTOS);
                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.ACTIVOS_DEVUELTOS);
                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "ACTIVOS_SOLICITADOS")
                    {
                        Page.Session["SortExpression"] = "ACTIVOS_SOLICITADOS";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.ACTIVOS_SOLICITADOS);
                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.ACTIVOS_SOLICITADOS);
                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }

                    else
                        if (e.SortExpression == "COMENTARIOS_RECHAZO")
                        {
                            Page.Session["SortExpression"] = "COMENTARIOS_RECHAZO";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DIFERENCIA")
                            {
                                Page.Session["SortExpression"] = "DIFERENCIA";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "FECHA_RECHAZO")
                                {
                                    Page.Session["SortExpression"] = "FECHA_RECHAZO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "FECHA_SOLICITUD")
                                    {
                                        Page.Session["SortExpression"] = "FECHA_SOLICITUD";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "FECHARETORNO")
                                        {
                                            Page.Session["SortExpression"] = "FECHARETORNO";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.FECHARETORNO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.FECHARETORNO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "FECHASALIDA")
                                            {
                                                Page.Session["SortExpression"] = "FECHASALIDA";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "MOTIVO")
                                                {
                                                    Page.Session["SortExpression"] = "MOTIVO";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "SOLICITUDSALIDAID")
                                                    {
                                                        Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "TIPO_USUARIO_RECHAZA")
                                                        {
                                                            Page.Session["SortExpression"] = "TIPO_USUARIO_RECHAZA";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.TIPO_USUARIO_RECHAZA);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.TIPO_USUARIO_RECHAZA);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "USUARIO_RECHAZO")
                                                            {
                                                                Page.Session["SortExpression"] = "USUARIO_RECHAZO";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.USUARIO_RECHAZO);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.USUARIO_RECHAZO);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }
                                                            else
                                                                if (e.SortExpression == "USUARIO_SOLICITA")
                                                                {
                                                                    Page.Session["SortExpression"] = "USUARIO_SOLICITA";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }
                grvAutorizados.SelectedIndex = -1;
                grvAutorizados.DataSource = lista;
                grvAutorizados.DataBind();
            }

            #endregion

            #region Eventos de Cancelaciones
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvCanceladas_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (lista == null || lista.Count == 0)
                    return;

                if (e.SortExpression == "ACTIVOS_DEVUELTOS")
                {
                    Page.Session["SortExpression"] = "ACTIVOS_DEVUELTOS";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.ACTIVOS_DEVUELTOS);
                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.ACTIVOS_DEVUELTOS);
                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "ACTIVOS_SOLICITADOS")
                    {
                        Page.Session["SortExpression"] = "ACTIVOS_SOLICITADOS";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.ACTIVOS_SOLICITADOS);
                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.ACTIVOS_SOLICITADOS);
                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }

                    else
                        if (e.SortExpression == "COMENTARIOS_RECHAZO")
                        {
                            Page.Session["SortExpression"] = "COMENTARIOS_RECHAZO";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DIFERENCIA")
                            {
                                Page.Session["SortExpression"] = "DIFERENCIA";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "FECHA_RECHAZO")
                                {
                                    Page.Session["SortExpression"] = "FECHA_RECHAZO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "FECHA_SOLICITUD")
                                    {
                                        Page.Session["SortExpression"] = "FECHA_SOLICITUD";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "COMENTARIOS_RECHAZO")
                                        {
                                            Page.Session["SortExpression"] = "COMENTARIOS_RECHAZO";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.COMENTARIOS_RECHAZO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.COMENTARIOS_RECHAZO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "FECHASALIDA")
                                            {
                                                Page.Session["SortExpression"] = "FECHASALIDA";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "MOTIVO")
                                                {
                                                    Page.Session["SortExpression"] = "MOTIVO";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "SOLICITUDSALIDAID")
                                                    {
                                                        Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "AUTORIZADOR")
                                                        {
                                                            Page.Session["SortExpression"] = "AUTORIZADOR";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.AUTORIZADOR);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.AUTORIZADOR);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "TIPO_AUTORIZADOR")
                                                            {
                                                                Page.Session["SortExpression"] = "TIPO_AUTORIZADOR";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.TIPO_AUTORIZADOR);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.TIPO_AUTORIZADOR);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }
                                                            else
                                                                if (e.SortExpression == "USUARIO_SOLICITA")
                                                                {
                                                                    Page.Session["SortExpression"] = "USUARIO_SOLICITA";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }
                grvAutorizados.SelectedIndex = -1;
                grvAutorizados.DataSource = lista;
                grvAutorizados.DataBind();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvCanceladas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvCanceladas_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvCanceladas.PageIndex = e.NewPageIndex;
                grvCanceladas.DataSource = lista;
                grvCanceladas.DataBind();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvCanceladas_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (grvCanceladas.SelectedRow != null)
                {
                    selectedRow = grvCanceladas.SelectedRow.DataItemIndex;
                    Session["SOLICITUDIDDETALLE"] = lista[selectedRow].SOLICITUDSALIDAID;
                    Session["SOLICITUDIDDETALLETIPO"] = ddlReporte.SelectedValue;
                    Response.Redirect("DetalleSolicitud.aspx");
                }
            }
            #endregion

            #region Eventos de Devoluciones
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvDevoluciones_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                }
            }
            /// <summary>
            /// Desplegar detalle de elemento seleccionado
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvDevoluciones_SelectedIndexChanged(object sender, EventArgs e)
            {
                if (grvDevoluciones.SelectedRow != null)
                {
                    selectedRow = grvDevoluciones.SelectedRow.DataItemIndex;
                    Session["SOLICITUDIDDETALLE"] = lista[selectedRow].SOLICITUDSALIDAID;
                    Session["SOLICITUDIDDETALLETIPO"] = ddlReporte.SelectedValue;
                    Response.Redirect("DetalleSolicitud.aspx");
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvDevoluciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvDevoluciones.PageIndex = e.NewPageIndex;
                grvDevoluciones.DataSource = lista;
                grvDevoluciones.DataBind();
            }
            /// <summary>
            /// Ordeamiento del grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void grvDevoluciones_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (lista == null || lista.Count == 0)
                    return;

                if (e.SortExpression == "ACTIVOS_DEVUELTOS")
                {
                    Page.Session["SortExpression"] = "ACTIVOS_DEVUELTOS";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.ACTIVOS_DEVUELTOS);
                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.ACTIVOS_DEVUELTOS);
                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "ACTIVOS_SOLICITADOS")
                    {
                        Page.Session["SortExpression"] = "ACTIVOS_SOLICITADOS";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.ACTIVOS_SOLICITADOS);
                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.ACTIVOS_SOLICITADOS);
                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }

                    else
                        if (e.SortExpression == "COMENTARIOS_RECHAZO")
                        {
                            Page.Session["SortExpression"] = "COMENTARIOS_RECHAZO";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.COMENTARIOS_RECHAZO);
                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DIFERENCIA")
                            {
                                Page.Session["SortExpression"] = "DIFERENCIA";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.DIFERENCIA);
                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "FECHA_RECHAZO")
                                {
                                    Page.Session["SortExpression"] = "FECHA_RECHAZO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.FECHA_RECHAZO);
                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "FECHA_SOLICITUD")
                                    {
                                        Page.Session["SortExpression"] = "FECHA_SOLICITUD";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.FECHA_SOLICITUD);
                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "FECHARETORNO")
                                        {
                                            Page.Session["SortExpression"] = "FECHARETORNO";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.FECHARETORNO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.FECHARETORNO);
                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "FECHASALIDA")
                                            {
                                                Page.Session["SortExpression"] = "FECHASALIDA";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.FECHASALIDA);
                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "MOTIVO")
                                                {
                                                    Page.Session["SortExpression"] = "MOTIVO";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.MOTIVO);
                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "SOLICITUDSALIDAID")
                                                    {
                                                        Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.SOLICITUDSALIDAID);
                                                            lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "TIPO_USUARIO_RECHAZA")
                                                        {
                                                            Page.Session["SortExpression"] = "TIPO_USUARIO_RECHAZA";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.TIPO_USUARIO_RECHAZA);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.TIPO_USUARIO_RECHAZA);
                                                                lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "USUARIO_RECHAZO")
                                                            {
                                                                Page.Session["SortExpression"] = "USUARIO_RECHAZO";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.USUARIO_RECHAZO);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.USUARIO_RECHAZO);
                                                                    lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }
                                                            else
                                                                if (e.SortExpression == "USUARIO_SOLICITA")
                                                                {
                                                                    Page.Session["SortExpression"] = "USUARIO_SOLICITA";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.USUARIO_SOLICITA);
                                                                        lista = v.ToList<HISTORICO_SOLICITUDES_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }
                grvDevoluciones.SelectedIndex = -1;
                grvDevoluciones.DataSource = lista;
                grvDevoluciones.DataBind();
            }
            #endregion

        #region Funciones

            /// <summary>
            ///
            /// </summary>
            public void Filtrar()
            {
                CargarReporte();
                if (grvAutorizados.DataSource is List<HISTORICO_SOLICITUDES_BE> || grvRechazadas.DataSource is List<HISTORICO_SOLICITUDES_BE> || grvDevoluciones.DataSource is List<HISTORICO_SOLICITUDES_BE>)
                {
                    string strValue = txtSearch.Text.ToLower();

                    lista = lista.FindAll(p => F.Text.Input.IsNothing(p.ACTIVOS_DEVUELTOS, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.ACTIVOS_SOLICITADOS, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.COMENTARIOS_RECHAZO, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.DIFERENCIA, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.FECHA_RECHAZO, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.FECHA_SOLICITUD, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.FECHARETORNO, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.FECHASALIDA, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.MOTIVO, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.SOLICITUDSALIDAID, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.TIPO_USUARIO_RECHAZA, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.USUARIO_RECHAZO, "").ToString().ToLower().Contains(strValue)
                        || F.Text.Input.IsNothing(p.USUARIO_SOLICITA, "").ToString().ToLower().Contains(strValue)
                        );

                    if (ddlReporte.SelectedValue == "Rechazadas")
                    {
                        grvRechazadas.DataSource = lista;
                        grvRechazadas.DataBind();
                    }

                    if (ddlReporte.SelectedValue == "Devoluciones")
                    {
                        grvDevoluciones.DataSource = lista;
                        grvDevoluciones.DataBind();
                    }

                    if (ddlReporte.SelectedValue == "Autorizadas")
                    {
                        grvAutorizados.DataSource = lista;
                        grvAutorizados.DataBind();
                    }
                }
            }
            /// <summary>
            /// 
            /// </summary>
            private void CargarReporte()
            {
                if (ddlReporte.SelectedValue == "Rechazadas")
                    CargarRechazadas();

                if (ddlReporte.SelectedValue == "Devoluciones")
                    CargarDevoluciones();

                if (ddlReporte.SelectedValue == "Autorizadas")
                    CargarAutorizadas();

                if (ddlReporte.SelectedValue == "Canceladas")
                    CargarCanceladas();
            }

            /// <summary>
            /// Carga grid de canceladas
            /// </summary>
            private void CargarCanceladas()
            {
                HISTORICO_SOLICITUDES_BE objBE = new HISTORICO_SOLICITUDES_BE();

                grvAutorizados.Visible = false;
                grvDevoluciones.Visible = false;
                grvRechazadas.Visible = false;
                grvCanceladas.Visible = true;

                objBE.USUARIO_SOLICITA = Session["USUARIO"].ToString();

                grvAutorizados.DataSource = null;
                grvAutorizados.DataBind();

                grvRechazadas.DataSource = null;
                grvRechazadas.DataBind();

                grvDevoluciones.DataSource = null;
                grvDevoluciones.DataBind();

                lista = Controller.HistoricoCancelados(objBE);
                grvCanceladas.DataSource = lista;
                grvCanceladas.DataBind();

            }
            /// <summary>
            /// Carga grid de autorizadas
            /// </summary>
            private void CargarAutorizadas()
            {
                HISTORICO_SOLICITUDES_BE objBE = new HISTORICO_SOLICITUDES_BE();

                grvAutorizados.Visible = true;
                grvDevoluciones.Visible = false;
                grvRechazadas.Visible = false;
                grvCanceladas.Visible = false;

                objBE.USUARIO_SOLICITA = Session["USUARIO"].ToString();

                grvRechazadas.DataSource = null;
                grvRechazadas.DataBind();

                grvDevoluciones.DataSource = null;
                grvDevoluciones.DataBind();

                grvCanceladas.DataSource = null;
                grvCanceladas.DataBind();

                lista=Controller.HistoricoAprobados(objBE);
                grvAutorizados.DataSource = lista;
                grvAutorizados.DataBind();

            }

            /// <summary>
            /// Cargar grid de rechazadas
            /// </summary>
            private void CargarRechazadas()
            {
                HISTORICO_SOLICITUDES_BE objBE = new HISTORICO_SOLICITUDES_BE();

                grvAutorizados.Visible = false;
                grvDevoluciones.Visible = false;
                grvRechazadas.Visible = true;
                grvCanceladas.Visible = false;

                objBE.USUARIO_SOLICITA = Session["USUARIO"].ToString();
                lista = Controller.HistoricoRechazado(objBE);

                grvRechazadas.DataSource = lista;
                grvRechazadas.DataBind();

                grvDevoluciones.DataSource = null;
                grvDevoluciones.DataBind();

                grvAutorizados.DataSource = null;
                grvAutorizados.DataBind();

                grvCanceladas.DataSource = null;
                grvCanceladas.DataBind();

            }

            /// <summary>
            /// Carga grid de devoluciones
            /// </summary>
            private void CargarDevoluciones()
            {
                HISTORICO_SOLICITUDES_BE objBE = new HISTORICO_SOLICITUDES_BE();

                grvAutorizados.Visible = false;
                grvDevoluciones.Visible = true;
                grvRechazadas.Visible = false;
                grvCanceladas.Visible = false;

                objBE.USUARIO_SOLICITA = Session["USUARIO"].ToString();
                lista = Controller.HistoricoDevuelto(objBE);

                grvRechazadas.DataSource = null;
                grvRechazadas.DataBind();

                grvDevoluciones.DataSource = lista;
                grvDevoluciones.DataBind();

                grvAutorizados.DataSource = null;
                grvAutorizados.DataBind();

                grvCanceladas.DataSource = null;
                grvCanceladas.DataBind();

            }

        #endregion

        #region ICatalogoView<> Members

            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error", "Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }

        #endregion

            
    }
}