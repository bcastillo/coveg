﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeguimientoSalidasVencidosPorvencer.aspx.cs" Inherits="CovegInventarios.Views.Reportes.SeguimientoSalidasVencidosPorvencer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<%@ Register src="../../Utils/DetalleActivo.ascx" tagname="DetalleActivo" tagprefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" />  
    <style type="text/css">
        .style1
        {
            font-family: Intro_Book;
            font-size: 11px;
            color: Black;
            font-weight: bold;
        }
        .style2
        {
        	color: #3466B7;
            font-size: 13px;
            font-weight: bold;
        }
        .style4
        {
        	color: Black;
            text-align: left;
        }
        .style5
        {
        	 color: #3466B7;
            font-size: 13px;
        }
        .style6
        {
            text-align: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="tksManager" runat="server" ScriptMode="Release">
        </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
            <ContentTemplate>
                <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
                    <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
                </asp:Panel>

                <asp:ModalPopupExtender ID="mpDetalleActivo"  PopupControlID="pnlConsulta" TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="ModalPopupBG" runat="server"/>
                <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>

                <center>
                    <%--Panel de detalle de activo --%>
                    <asp:Panel ID="pnlConsulta" runat="server" Width="1000px" CssClass="shadowPanel">
                        <asp:Panel ID="Panel4" runat="server"  Width="1000px" >
                        <table width="1000px" class="shadow" cellpadding="2" cellspacing="0">
                            <tr class="cpHeader">
                            <td >
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Update.png" Width="20px" />&nbsp;
                                <asp:Label ID="Label2" runat="server" Text="Detalle de Activo" Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" CssClass="whitetitle" />
                            </td>
                             <td align="right">
                                <asp:Button runat="server" ID="cmdCerrar" Text="Cerrar" Font-Bold="True" onclick="cmdCerrar_Click" CssClass="boton" />
                             </td>
                            </tr>                        
                            <tr class="cpBody">
                                <td align="center" colspan="2">
                                    <uc2:DetalleActivo ID="DetalleActivo1" runat="server" />
                                    
                                </td>
                            </tr>
                            </table>
                    </asp:Panel>
                    </asp:Panel>
                    
                    <%--Grids de reportes --%>
                    <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="1000px" >
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" Width="20px" />&nbsp;
                                <asp:Label ID="lblText" runat="server" Text="Seguimiento de Solicitudes" Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" CssClass="whitetitle" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                    <asp:Panel ID="pBody" runat="server" CssClass="cpBody" Width="1000px">
                    <table width="99%" cellspacing="5px">
                    <%-- Por salir--%>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel1" runat="server" CssClass="cpHeader" Width="99%" >
                            <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblPorSalir" runat="server" Text="Activos por Salir" 
                                        Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" 
                                        CssClass="whitetitle" />
                                </td>
                            </tr>
                            </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top">
                            <div class="grid" align="center" style="width:970px" >
                                <asp:GridView ID="grvPorSalir" 
                                    runat="server" 
                                    CellPadding="3" 
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="grvPorSalir_RowDataBound" 
                                    OnPageIndexChanging="grvPorSalir_PageIndexChanging"
                                    onsorting="grvPorSalir_Sorting"
                                    OnSelectedIndexChanged="grvPorSalir_SelectedIndexChanged"
                                    Font-Names="intro_book"
                                    Width="960px"
                                    >
                                    <Columns>
                                        <asp:BoundField DataField="USUARIO" HeaderText="USUARIO" SortExpression="USUARIO"/>
                                        <asp:BoundField DataField="FECHA_RETORNO" HeaderText="FECHA_RETORNO" SortExpression="FECHA_RETORNO" />
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                                        <asp:BoundField DataField="TIPO_ACTIVO" HeaderText="TIPO_ACTIVO" SortExpression="TIPO_ACTIVO" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                            <ControlStyle CssClass="boton" />
                                            <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <%--Por vencer --%>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" CssClass="cpHeader" Width="99%" >
                            <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblAutorizaciones" runat="server" Text="Activos por Vencer" 
                                        Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" 
                                        CssClass="whitetitle" />
                                </td>
                            </tr>
                            </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top">
                            <div class="grid" align="center" style="width:970px" >
                                <asp:GridView ID="grvPorVencer" 
                                    runat="server" 
                                    CellPadding="3" 
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="grvPorVencer_RowDataBound" 
                                    OnPageIndexChanging="grvPorVencer_PageIndexChanging"
                                    onsorting="grvPorVencer_Sorting"
                                    OnSelectedIndexChanged="grvPorVencer_SelectedIndexChanged"
                                    Font-Names="intro_book"
                                    Width="960px"
                                    >
                                    <Columns>
                                        <asp:BoundField DataField="USUARIO" HeaderText="USUARIO" SortExpression="USUARIO"/>
                                        <asp:BoundField DataField="FECHA_RETORNO" HeaderText="FECHA_RETORNO" SortExpression="FECHA_RETORNO" />
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                                        <asp:BoundField DataField="TIPO_ACTIVO" HeaderText="TIPO_ACTIVO" SortExpression="TIPO_ACTIVO" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                            <ControlStyle CssClass="boton" />
                                            <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <%--Vencidos --%>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel3" runat="server" CssClass="cpHeader" Width="99%" >
                            <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text="Activos Vencidos" 
                                        Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" 
                                        CssClass="whitetitle" />
                                </td>
                            </tr>
                            </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top">
                            <div class="grid" align="center" style="width:970px" >
                                <asp:GridView ID="grvVencidos" 
                                    runat="server" 
                                    CellPadding="3" 
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="grvVencidos_RowDataBound" 
                                    OnPageIndexChanging="grvVencidos_PageIndexChanging"
                                    onsorting="grvVencidos_Sorting"
                                    OnSelectedIndexChanged="grvVencidos_SelectedIndexChanged"
                                    Font-Names="intro_book"
                                    Width="960px"
                                    >
                                    <Columns>
                                        <asp:BoundField DataField="USUARIO" HeaderText="USUARIO" SortExpression="USUARIO" />
                                        <asp:BoundField DataField="FECHA_RETORNO" HeaderText="FECHA_RETORNO" SortExpression="FECHA_RETORNO" />
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                                        <asp:BoundField DataField="TIPO_ACTIVO" HeaderText="TIPO_ACTIVO" SortExpression="TIPO_ACTIVO" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                            <ControlStyle CssClass="boton" />
                                            <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
                </center>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>
