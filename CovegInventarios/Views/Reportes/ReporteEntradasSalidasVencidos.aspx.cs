﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CovegInventarios.Views.Reportes
{
    public partial class ReporteEntradasSalidasVencidos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LlenarReportePorSalir();
                LlenarReporteAfueraNoVencidos();
                LlenarReporteAfueraVencidos();
            }
        }

        #region ReportePorSalir

            private DataTable ValoresPendientesSalir()
            {
                String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptPendientesSalir"].ToString();
                return Coveg.Data.OpenData.OpenQuery(strConsulta);

            }

            private void LlenarReportePorSalir()
            {
                rvPorSalir.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                Microsoft.Reporting.WebForms.LocalReport objRpt = rvPorSalir.LocalReport;
                objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReportePendientesSalir.rdlc";
                Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("PendientesSalirDataSet", ValoresPendientesSalir());
                objRpt.DataSources.Clear();
                objRpt.DataSources.Add(rds);
                rvPorSalir.LocalReport.Refresh();
            }

        #endregion

        #region ReporteAfueraNoVencidos

            private DataTable ValoresAfueraNoVencidos()
            {
                String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptAfueraNoVencidos"].ToString();
                return Coveg.Data.OpenData.OpenQuery(strConsulta);

            }

            private void LlenarReporteAfueraNoVencidos()
            {
                rvAfueraNoVencidos.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                Microsoft.Reporting.WebForms.LocalReport objRpt = rvAfueraNoVencidos.LocalReport;
                objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReportePendientesRegresarNoVencidos.rdlc";
                Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("PendRegNoVencidosDataSet", ValoresAfueraNoVencidos());
                objRpt.DataSources.Clear();
                objRpt.DataSources.Add(rds);
                rvAfueraNoVencidos.LocalReport.Refresh();
            }

        #endregion

        #region ReporteAfueraVencidos

            private DataTable ValoresAfueraVencidos()
            {
                String strConsulta = System.Configuration.ConfigurationManager.AppSettings["rptAfueraVencidos"].ToString();
                return Coveg.Data.OpenData.OpenQuery(strConsulta);
            }

            private void LlenarReporteAfueraVencidos()
            {
                rvAfueraVencidos.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                Microsoft.Reporting.WebForms.LocalReport objRpt = rvAfueraVencidos.LocalReport;
                objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReportePendientesRegresarVencidos.rdlc";
                Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("PendientesVencidosDataSet", ValoresAfueraVencidos());
                objRpt.DataSources.Clear();
                objRpt.DataSources.Add(rds);
                rvAfueraVencidos.LocalReport.Refresh();
            }

        #endregion

    }
}