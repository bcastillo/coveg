﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Coveg.Entities;

namespace CovegInventarios.Views.Reportes
{
    public partial class SeguimientoSalidasVencidosPorvencer : System.Web.UI.Page
    {

        #region Properties

        Coveg.Procesos.Controller.ENTRADAController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Procesos.Controller.ENTRADAController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Procesos.Controller.ENTRADAController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// 

        private int selectedRowVencidas
        {
            get
            {
                if (Page.Session["selectedRowVencidas"] == null)
                {
                    Page.Session["selectedRowVencidas"] = -1;
                }
                return (Int32)Page.Session["selectedRowVencidas"];
            }
            set
            {
                Page.Session["selectedRowVencidas"] = value;
            }
        }

        private int selectedRowPorVencer
        {
            get
            {
                if (Page.Session["selectedRowPorVencer"] == null)
                {
                    Page.Session["selectedRowPorVencer"] = -1;
                }
                return (Int32)Page.Session["selectedRowPorVencer"];
            }
            set
            {
                Page.Session["selectedRowPorVencer"] = value;
            }
        }


        private int selectedRowPorSalir
        {
            get
            {
                if (Page.Session["selectedRowPorSalir"] == null)
                {
                    Page.Session["selectedRowPorSalir"] = -1;
                }
                return (Int32)Page.Session["selectedRowPorSalir"];
            }
            set
            {
                Page.Session["selectedRowPorSalir"] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private List<SEGUIMIENTO_SOLICITUDES_BE> SolicitudesSalir
        {
            get
            {
                return (List<SEGUIMIENTO_SOLICITUDES_BE>)Page.Session["SolicitudesSalir"];
            }
            set
            {
                Page.Session["SolicitudesSalir"] = value;
            }
        }

        private List<SEGUIMIENTO_SOLICITUDES_BE> SolicitudesPorVencer
        {
            get
            {
                return (List<SEGUIMIENTO_SOLICITUDES_BE>)Page.Session["SolicitudesPorVencer"];
            }
            set
            {
                Page.Session["SolicitudesPorVencer"] = value;
            }
        }

        private List<SEGUIMIENTO_SOLICITUDES_BE> SolicitudesVencidas
        {
            get
            {
                return (List<SEGUIMIENTO_SOLICITUDES_BE>)Page.Session["SolicitudesVencidas"];
            }
            set
            {
                Page.Session["SolicitudesVencidas"] = value;
            }
        }

        #endregion

        #region Eventos

            protected void Page_Load(object sender, EventArgs e)
            {
                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
                try
                {
                    if (!IsPostBack)
                    {
                        Page.Session["SortDirectionSalir"] = "Ascending";
                        Page.Session["SortExpressionSalir"] = "";

                        Page.Session["SortDirectionVencer"] = "Ascending";
                        Page.Session["SortExpressionVencer"] = "";

                        Page.Session["SortDirectionVencido"] = "Ascending";
                        Page.Session["SortExpressionVencido"] = "";

                        CargarSolicitudes();
                    }
                }
                catch
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                }
            }

            protected void cmdCerrar_Click(object sender, EventArgs e)
            {
                mpDetalleActivo.Hide();
            }


        #endregion

        #region Funciones

            /// <summary>
            /// Carga listas y grids
            /// </summary>
            private void CargarSolicitudes()
            {
                SolicitudesVencidas = null;
                SolicitudesSalir = null;
                SolicitudesPorVencer = null;

                ValoresAfueraNoVencidos();
                ValoresAfueraVencidos();
                ValoresPendientesSalir();

                grvPorSalir.DataSource = SolicitudesSalir;
                grvPorSalir.DataBind();

                grvPorVencer.DataSource = SolicitudesPorVencer;
                grvPorVencer.DataBind();

                grvVencidos.DataSource = SolicitudesVencidas;
                grvVencidos.DataBind();
            }

            /// <summary>
            /// Obtiene valores de pendientes por salir
            /// </summary>
            private void ValoresPendientesSalir()
            {
                SolicitudesSalir = Controller.Select_Pendientes_Salir(new SEGUIMIENTO_SOLICITUDES_BE());
            }

            /// <summary>
            /// Obtiene valores de no vencidos
            /// </summary>
            private void ValoresAfueraNoVencidos()
            {
                SolicitudesPorVencer = Controller.Select_Pendientes_No_Vencidos(new SEGUIMIENTO_SOLICITUDES_BE());
            }

            /// <summary>
            /// Obtiene valores de vencidos
            /// </summary>
            private void ValoresAfueraVencidos()
            {
                SolicitudesVencidas = Controller.Select_Pendientes_Vencidos(new SEGUIMIENTO_SOLICITUDES_BE());
            }
        #endregion

        #region ICatalogoView<> Members
            
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error", "Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }


            #endregion

        #region Eventos de grid por salir

            protected void grvPorSalir_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                    if (Convert.ToDateTime(e.Row.Cells[1].Text) == new DateTime())
                        e.Row.Cells[1].Text = "--";
                    else
                        e.Row.Cells[1].Text = Convert.ToDateTime(e.Row.Cells[1].Text).ToShortDateString();
                }
            }

            protected void grvPorSalir_SelectedIndexChanged(object sender, EventArgs e)
            {
                selectedRowPorSalir = grvPorSalir.SelectedRow.DataItemIndex;
                DetalleActivo1.Inicializar(SolicitudesSalir[selectedRowPorSalir].CLAVE);
                mpDetalleActivo.Show();
            }

            protected void grvPorSalir_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvPorSalir.PageIndex = e.NewPageIndex;
                grvPorSalir.DataSource = SolicitudesSalir;
                grvPorSalir.DataBind();
            }

            protected void grvPorSalir_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (SolicitudesSalir == null || SolicitudesSalir.Count == 0)
                    return;

                if (e.SortExpression == "DESCRIPCION")
                {
                    Page.Session["SortExpressionSalir"] = "DESCRIPCION";
                    if (Page.Session["SortDirectionSalir"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = SolicitudesSalir.OrderBy(p => p.DESCRIPCION);
                        SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                        Page.Session["SortDirectionSalir"] = "Descending";
                    }
                    else
                    {
                        var v = SolicitudesSalir.OrderByDescending(p => p.DESCRIPCION);
                        SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                        Page.Session["SortDirectionSalir"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "FECHA_RETORNO")
                    {
                        Page.Session["SortExpressionSalir"] = "FECHA_RETORNO";
                        if (Page.Session["SortDirectionSalir"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = SolicitudesSalir.OrderBy(p => p.FECHA_RETORNO);
                            SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                            Page.Session["SortDirectionSalir"] = "Descending";
                        }
                        else
                        {
                            var v = SolicitudesSalir.OrderByDescending(p => p.FECHA_RETORNO);
                            SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                            Page.Session["SortDirectionSalir"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "MOTIVO")
                        {
                            Page.Session["SortExpressionSalir"] = "MOTIVO";
                            if (Page.Session["SortDirectionSalir"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = SolicitudesSalir.OrderBy(p => p.MOTIVO);
                                SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                Page.Session["SortDirectionSalir"] = "Descending";
                            }
                            else
                            {
                                var v = SolicitudesSalir.OrderByDescending(p => p.MOTIVO);
                                SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                Page.Session["SortDirectionSalir"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "TIPO_ACTIVO")
                            {
                                Page.Session["SortExpressionSalir"] = "TIPO_ACTIVO";
                                if (Page.Session["SortDirectionSalir"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = SolicitudesSalir.OrderBy(p => p.TIPO_ACTIVO);
                                    SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                    Page.Session["SortDirectionSalir"] = "Descending";
                                }
                                else
                                {
                                    var v = SolicitudesSalir.OrderByDescending(p => p.TIPO_ACTIVO);
                                    SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                    Page.Session["SortDirectionSalir"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "USUARIO")
                                {
                                    Page.Session["SortExpressionSalir"] = "USUARIO";
                                    if (Page.Session["SortDirectionSalir"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = SolicitudesSalir.OrderBy(p => p.USUARIO);
                                        SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                        Page.Session["SortDirectionSalir"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = SolicitudesSalir.OrderByDescending(p => p.USUARIO);
                                        SolicitudesSalir = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                        Page.Session["SortDirectionSalir"] = "Ascending";
                                    }
                                }

                grvPorSalir.SelectedIndex = -1;
                grvPorSalir.DataSource = SolicitudesSalir;
                grvPorSalir.DataBind();

            }

        #endregion

        #region Eventos de grid por vencer

            protected void grvPorVencer_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                    
                    if (Convert.ToDateTime(e.Row.Cells[1].Text) == new DateTime())
                        e.Row.Cells[1].Text = "--";
                    else
                        e.Row.Cells[1].Text = Convert.ToDateTime(e.Row.Cells[1].Text).ToShortDateString();
                }
            }

            protected void grvPorVencer_SelectedIndexChanged(object sender, EventArgs e)
            {
                selectedRowPorVencer = grvPorVencer.SelectedRow.DataItemIndex;
                DetalleActivo1.Inicializar(SolicitudesPorVencer[selectedRowPorVencer].CLAVE);
                mpDetalleActivo.Show();
            }

            protected void grvPorVencer_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvPorVencer.PageIndex = e.NewPageIndex;
                grvPorVencer.DataSource = SolicitudesSalir;
                grvPorVencer.DataBind();
            }

            protected void grvPorVencer_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (SolicitudesPorVencer == null || SolicitudesPorVencer.Count == 0)
                    return;

                if (e.SortExpression == "DESCRIPCION")
                {
                    Page.Session["SortExpressionVencer"] = "DESCRIPCION";
                    if (Page.Session["SortDirectionVencer"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = SolicitudesPorVencer.OrderBy(p => p.DESCRIPCION);
                        SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                        Page.Session["SortDirectionVencer"] = "Descending";
                    }
                    else
                    {
                        var v = SolicitudesPorVencer.OrderByDescending(p => p.DESCRIPCION);
                        SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                        Page.Session["SortDirectionVencer"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "FECHA_RETORNO")
                    {
                        Page.Session["SortExpressionVencer"] = "FECHA_RETORNO";
                        if (Page.Session["SortDirectionVencer"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = SolicitudesPorVencer.OrderBy(p => p.FECHA_RETORNO);
                            SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                            Page.Session["SortDirectionVencer"] = "Descending";
                        }
                        else
                        {
                            var v = SolicitudesPorVencer.OrderByDescending(p => p.FECHA_RETORNO);
                            SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                            Page.Session["SortDirectionVencer"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "MOTIVO")
                        {
                            Page.Session["SortExpressionVencer"] = "MOTIVO";
                            if (Page.Session["SortDirectionVencer"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = SolicitudesPorVencer.OrderBy(p => p.MOTIVO);
                                SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                Page.Session["SortDirectionVencer"] = "Descending";
                            }
                            else
                            {
                                var v = SolicitudesPorVencer.OrderByDescending(p => p.MOTIVO);
                                SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                Page.Session["SortDirectionVencer"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "TIPO_ACTIVO")
                            {
                                Page.Session["SortExpressionVencer"] = "TIPO_ACTIVO";
                                if (Page.Session["SortDirectionVencer"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = SolicitudesPorVencer.OrderBy(p => p.TIPO_ACTIVO);
                                    SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                    Page.Session["SortDirectionVencer"] = "Descending";
                                }
                                else
                                {
                                    var v = SolicitudesPorVencer.OrderByDescending(p => p.TIPO_ACTIVO);
                                    SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                    Page.Session["SortDirectionVencer"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "USUARIO")
                                {
                                    Page.Session["SortExpressionVencer"] = "USUARIO";
                                    if (Page.Session["SortDirectionVencer"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = SolicitudesPorVencer.OrderBy(p => p.USUARIO);
                                        SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                        Page.Session["SortDirectionVencer"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = SolicitudesPorVencer.OrderByDescending(p => p.USUARIO);
                                        SolicitudesPorVencer = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                        Page.Session["SortDirectionVencer"] = "Ascending";
                                    }
                                }

                grvPorVencer.SelectedIndex = -1;
                grvPorVencer.DataSource = SolicitudesPorVencer;
                grvPorVencer.DataBind();

            }

        #endregion

        #region Eventos de grid vencidos

            protected void grvVencidos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                    
                    if (Convert.ToDateTime(e.Row.Cells[1].Text) == new DateTime())
                        e.Row.Cells[1].Text = "--";
                    else
                        e.Row.Cells[1].Text = Convert.ToDateTime(e.Row.Cells[1].Text).ToShortDateString();
                }
            }

            protected void grvVencidos_SelectedIndexChanged(object sender, EventArgs e)
            {
                selectedRowVencidas = grvVencidos.SelectedRow.DataItemIndex;
                DetalleActivo1.Inicializar(SolicitudesVencidas[selectedRowVencidas].CLAVE);
                mpDetalleActivo.Show();
            }

            protected void grvVencidos_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                grvVencidos.PageIndex = e.NewPageIndex;
                grvVencidos.DataSource = SolicitudesSalir;
                grvVencidos.DataBind();
            }

            protected void grvVencidos_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (SolicitudesVencidas == null || SolicitudesVencidas.Count == 0)
                    return;

                if (e.SortExpression == "DESCRIPCION")
                {
                    Page.Session["SortExpressionVencido"] = "DESCRIPCION";
                    if (Page.Session["SortDirectionVencido"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = SolicitudesVencidas.OrderBy(p => p.DESCRIPCION);
                        SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                        Page.Session["SortDirectionVencido"] = "Descending";
                    }
                    else
                    {
                        var v = SolicitudesVencidas.OrderByDescending(p => p.DESCRIPCION);
                        SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                        Page.Session["SortDirectionVencido"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "FECHA_RETORNO")
                    {
                        Page.Session["SortExpressionVencido"] = "FECHA_RETORNO";
                        if (Page.Session["SortDirectionVencido"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = SolicitudesVencidas.OrderBy(p => p.FECHA_RETORNO);
                            SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                            Page.Session["SortDirectionVencido"] = "Descending";
                        }
                        else
                        {
                            var v = SolicitudesVencidas.OrderByDescending(p => p.FECHA_RETORNO);
                            SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                            Page.Session["SortDirectionVencido"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "MOTIVO")
                        {
                            Page.Session["SortExpressionVencido"] = "MOTIVO";
                            if (Page.Session["SortDirectionVencido"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = SolicitudesVencidas.OrderBy(p => p.MOTIVO);
                                SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                Page.Session["SortDirectionVencido"] = "Descending";
                            }
                            else
                            {
                                var v = SolicitudesVencidas.OrderByDescending(p => p.MOTIVO);
                                SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                Page.Session["SortDirectionVencido"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "TIPO_ACTIVO")
                            {
                                Page.Session["SortExpressionVencido"] = "TIPO_ACTIVO";
                                if (Page.Session["SortDirectionVencido"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = SolicitudesVencidas.OrderBy(p => p.TIPO_ACTIVO);
                                    SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                    Page.Session["SortDirectionVencido"] = "Descending";
                                }
                                else
                                {
                                    var v = SolicitudesVencidas.OrderByDescending(p => p.TIPO_ACTIVO);
                                    SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                    Page.Session["SortDirectionVencido"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "USUARIO")
                                {
                                    Page.Session["SortExpressionVencido"] = "USUARIO";
                                    if (Page.Session["SortDirectionVencido"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = SolicitudesVencidas.OrderBy(p => p.USUARIO);
                                        SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                        Page.Session["SortDirectionVencido"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = SolicitudesVencidas.OrderByDescending(p => p.USUARIO);
                                        SolicitudesVencidas = v.ToList<SEGUIMIENTO_SOLICITUDES_BE>();
                                        Page.Session["SortDirectionVencido"] = "Ascending";
                                    }
                                }

                grvVencidos.SelectedIndex = -1;
                grvVencidos.DataSource = SolicitudesVencidas;
                grvVencidos.DataBind();

            }

        #endregion
    }
}