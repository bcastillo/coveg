﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="HistoricoSolicitudes.aspx.cs" Inherits="CovegInventarios.Views.Reportes.HistoricoSolicitudes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
            <ContentTemplate>
                <asp:Panel ID="panelMessageBox" runat="server" Visible="False">
                    <uc1:MessageBox ID="MessageBox1" runat="server" />
                </asp:Panel>
            
                <table>
                    <tr>
                        <td valign="middle" class="cpHeaderCatalogos" colspan="3">
                            <table width="100%">
                                <tr>
                                <td>
                                    <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" />
                                </td>
                                <td align="left" colspan="2">
                                    <asp:Label ID="lblTituloCatalogo" runat="server" Text="Historico de Solicitudes" Font-Bold="True" Font-Size="13px" />
                                </td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="panel6" valign="middle">
                            <asp:Label ID="lblReporte" runat="server" Style="font-weight: 700" 
                                Text="Elija el tipo de visualización " CssClass="Etiquetas" ></asp:Label>
                            <asp:DropDownList ID="ddlReporte" runat="server" AutoPostBack="True" 
                                OnSelectedIndexChanged="ddlReporte_SelectedIndexChanged" Height="20px" 
                                CssClass="comboSeleccion">
                                <asp:ListItem Text="Solicitudes Rechazadas" Value="Rechazadas" ></asp:ListItem>
                                <asp:ListItem Text="Devoluciones" Value="Devoluciones" ></asp:ListItem>
                                <asp:ListItem Text="Solicitudes Autorizadas" Value="Autorizadas" ></asp:ListItem>
                                <asp:ListItem Text="Solicitudes Canceladas" Value="Canceladas" ></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                            <asp:Label ID="lbl_ExpresionBusqueda" runat="server" Style="font-weight: 700" 
                                Text="Expresión de búsqueda " CssClass="Etiquetas" ></asp:Label>
                            <asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search" 
                                AutoPostBack="True" CssClass="textbox" OnTextChanged="txtSearch_TextChanged" 
                                Width="170px" Height="16px" ></asp:TextBox>
                        <br />
                        </td>
                        <td width="15px">
                        </td>
                        <td align="left">

                        </td>
                    </tr>
                    
                    <%-- GRID DE RECHAZADAS--%>
                    <tr>
                        <td align="center" colspan="3">
                            <div class="grid">
                                <asp:GridView ID="grvRechazadas" 
                                    runat="server" 
                                    CellPadding="3" 
                                    OnSelectedIndexChanged="grvRechazadas_SelectedIndexChanged"
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    OnPageIndexChanging="grvRechazadas_PageIndexChanging"
                                    onsorting="grvRechazadas_Sorting" 
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="grvRechazadas_RowDataBound" 
                                    Font-Names="intro_book">
                                    <Columns>
                                        <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID"/>
                                        <asp:BoundField DataField="USUARIO_SOLICITA" HeaderText="SOLICITANTE" SortExpression="USUARIO_SOLICITA" />
                                        <asp:BoundField DataField="FECHA_SOLICITUD" HeaderText="FECHA DE SOLICITUD" SortExpression="FECHA_SOLICITUD" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO DE SALIDA" SortExpression="MOTIVO" />
                                        <asp:BoundField DataField="FECHA_RECHAZO" HeaderText="FECHA DE RECHAZO" SortExpression="FECHA_RECHAZO" />
                                        <asp:BoundField DataField="USUARIO_RECHAZO" HeaderText="USUARIO QUE RECHAZO" SortExpression="USUARIO_RECHAZO" />
                                        <asp:BoundField DataField="COMENTARIOS_RECHAZO" HeaderText="MOTIVOS DE RECHAZO" SortExpression="COMENTARIOS_RECHAZO" />
                                        <asp:BoundField DataField="TIPO_USUARIO_RECHAZA" HeaderText="TIPO USUARIO QUE RECHAZA" SortExpression="TIPO_USUARIO_RECHAZA" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                        <ControlStyle CssClass="boton" />
                                        <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>    
                        </td>
                    </tr>

                    <%-- GRID DE DEVOLUCIONES--%>
                    <tr>
                        <td align="center" colspan="3">
                            <div class="grid">
                                <asp:GridView ID="grvDevoluciones" 
                                    runat="server" 
                                    CellPadding="3" 
                                    OnSelectedIndexChanged="grvDevoluciones_SelectedIndexChanged"
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    OnPageIndexChanging="grvDevoluciones_PageIndexChanging"
                                    onsorting="grvDevoluciones_Sorting" 
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="grvDevoluciones_RowDataBound" 
                                    Font-Names="intro_book">
                                    <Columns>
                                        <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID"/>
                                        <asp:BoundField DataField="USUARIO_SOLICITA" HeaderText="USUARIO SOLICITA" SortExpression="USUARIO_SOLICITA" />
                                        <asp:BoundField DataField="FECHA_SOLICITUD" HeaderText="FECHA SOLICITUD" SortExpression="FECHA_SOLICITUD" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                                        <asp:BoundField DataField="ACTIVOS_SOLICITADOS" HeaderText="ACTIVOS SOLICITADOS" SortExpression="ACTIVOS_SOLICITADOS" />
                                        <asp:BoundField DataField="ACTIVOS_DEVUELTOS" HeaderText="ACTIVOS DEVUELTOS" SortExpression="ACTIVOS_DEVUELTOS" />
                                        <asp:BoundField DataField="DIFERENCIA" HeaderText="DIFERENCIA" SortExpression="DIFERENCIA" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                        <ControlStyle CssClass="boton" />
                                        <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>    
                        </td>
                    </tr>

                    <%-- GRID DE AUTORIZADOS--%>
                    <tr>
                        <td align="center" colspan="3">
                            <div class="grid">
                                <asp:GridView ID="grvAutorizados" 
                                    runat="server" 
                                    CellPadding="3" 
                                    OnSelectedIndexChanged="grvAutorizados_SelectedIndexChanged"
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    OnPageIndexChanging="grvAutorizados_PageIndexChanging"
                                    onsorting="grvAutorizados_Sorting" 
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="grvAutorizados_RowDataBound" 
                                    Font-Names="intro_book">
                                    <Columns>
                                        <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID"/>
                                        <asp:BoundField DataField="USUARIO_SOLICITA" HeaderText="USUARIO SOLICITA" SortExpression="USUARIO_SOLICITA" />
                                        <asp:BoundField DataField="FECHA_SOLICITUD" HeaderText="FECHA SOLICITUD" SortExpression="FECHA_SOLICITUD" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                                        <asp:BoundField DataField="FECHASALIDA" HeaderText="FECHA SALIDA" SortExpression="FECHASALIDA" />
                                        <asp:BoundField DataField="FECHARETORNO" HeaderText="FECHA RETORNO" SortExpression="FECHARETORNO" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                        <ControlStyle CssClass="boton" />
                                        <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>                                
                            </div>    
                        </td>
                    </tr>
                    <%-- GRID DE AUTORIZADOS--%>
                    <tr>
                        <td align="center" colspan="3">
                            <div class="grid">
                                <asp:GridView ID="grvCanceladas" runat="server" AllowPaging="True" 
                                    AllowSorting="true" AutoGenerateColumns="False" BackColor="White" 
                                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                                    Font-Names="intro_book" OnPageIndexChanging="grvCanceladas_PageIndexChanging" 
                                    onrowdatabound="grvCanceladas_RowDataBound" 
                                    OnSelectedIndexChanged="grvCanceladas_SelectedIndexChanged" 
                                    onsorting="grvCanceladas_Sorting">
                                    <Columns>
                                        <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" 
                                            SortExpression="SOLICITUDSALIDAID" />
                                        <asp:BoundField DataField="USUARIO_SOLICITA" HeaderText="USUARIO SOLICITA" 
                                            SortExpression="USUARIO_SOLICITA" />
                                        <asp:BoundField DataField="FECHA_SOLICITUD" HeaderText="FECHA SOLICITUD" 
                                            SortExpression="FECHA_SOLICITUD" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" 
                                            SortExpression="MOTIVO" />
                                        <asp:BoundField DataField="COMENTARIOS_RECHAZO" HeaderText="COMENTARIOS" 
                                            SortExpression="COMENTARIOS_RECHAZO" />                                          
                                        <asp:BoundField DataField="AUTORIZADOR" HeaderText="AUTORIZADOR" 
                                            SortExpression="AUTORIZADOR" />
                                        <asp:BoundField DataField="TIPO_AUTORIZADOR" HeaderText="TIPO DE AUTORIZADOR" 
                                            SortExpression="TIPO_AUTORIZADOR" />
                                        <asp:BoundField DataField="SOLICITUD_ESTATUS" HeaderText="ULTIMO ESTATUS" 
                                            SortExpression="SOLICITUD_ESTATUS" />                                        
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                        <ControlStyle CssClass="boton" />
                                        <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <FooterStyle BackColor="White" Font-Names="Intro_Book" ForeColor="#000066" />
                                    <HeaderStyle CssClass="myheader" Font-Bold="True" Font-Names="Intro_Book" 
                                        Font-Size="9pt" ForeColor="#33333f" />
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle Font-Names="Intro_Book" Font-Size="9pt" ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" Font-Names="Intro_Book" 
                                        ForeColor="000066" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
