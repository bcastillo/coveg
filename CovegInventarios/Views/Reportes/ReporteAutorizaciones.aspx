﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ReporteAutorizaciones.aspx.cs" Inherits="CovegInventarios.Views.Reportes.ReporteAutorizaciones" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
        <ContentTemplate>
            <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
                <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
            </asp:Panel>

            <table cellspacing="0" cellpadding="5" width ="600px">
            <tr class="cpHeaderCatalogos">
                <td valign="middle"   colspan="3">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px"/>
                            </td>
                            <td align="left" colspan="3">
                                &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" Text="Reporte de Autorizaciones" Font-Bold="True" Font-Size="13px" /></td>
                            
                        </tr>
                    </table>
                </td>
                <td align="right">
                    <asp:Button ID="imgVerReporte" runat="server" Text="Reporte" onclick="imgVerReporte_Click" CssClass="boton" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFechaIni" runat="server" Text="Fecha Inicial" 
                        CssClass="Etiquetas"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtFechaInicial" runat="server" CssClass="textbox" 
                        Height="16px" Width="80px"></asp:TextBox>
                    &nbsp;<asp:ImageButton ID="imgFechaInicial" runat="server" 
                        ImageUrl="~/Images/calendar.png" Width="24px" />
                        <asp:CalendarExtender ID="calInicio" PopupButtonID="imgFechaInicial" 
                        runat="server" ClearTime="True" Format="dd/MM/yyyy" 
                        TargetControlID="txtFechaInicial" />
                </td>
                
                <td colspan="2" align="left">
                    &nbsp;&nbsp;
                    <asp:Label ID="lblUsuario" runat="server" CssClass="Etiquetas" 
                        Text="Usuario Solicitante"></asp:Label>
                    &nbsp;<asp:TextBox ID="txtUsuario" Width ="250px" runat="server" 
                        CssClass="textbox" Height="16px"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="txtUsuario_AutoCompleteExtender" runat="server" 
                        CompletionInterval="100" 
                        CompletionListCssClass="autocomplete_completionListElement" 
                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                        CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                        DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                        ServiceMethod="ObtenerUsuarios" ShowOnlyCurrentWordInCompletionListItem="True" 
                        TargetControlID="txtUsuario" UseContextKey="True">
                    </asp:AutoCompleteExtender>
                </td>           
                
            </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFechaAutorizacion0" runat="server" CssClass="Etiquetas" 
                            Text="Fecha Final"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFechaFinal" runat="server" CssClass="textbox" Height="16px" 
                            Width="80px"></asp:TextBox>
                        <asp:CalendarExtender ID="txtFechaFinal_CalendarExtender" runat="server" 
                            ClearTime="True" Format="dd/MM/yyyy" PopupButtonID="imgFechaFinal" 
                            TargetControlID="txtFechaFinal" />
                        &nbsp;<asp:ImageButton ID="imgFechaFinal" runat="server" 
                            ImageUrl="~/Images/calendar.png" Width="24px" />
                    </td>
                    <td align="left" colspan="2">
                        &nbsp;</td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<br />
<rsweb:ReportViewer id="ReportViewer1" runat="server" Width="1000px">
</rsweb:ReportViewer>
</asp:Content>
