﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ReporteInventarioFisico.aspx.cs" Inherits="CovegInventarios.Views.Reportes.ReporteInventarioFisico" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
        <ContentTemplate>
            <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
                <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
            </asp:Panel>
        
        <table cellspacing="0" cellpadding="5" width="800px">
            <tr class="cpHeaderCatalogos">
                <td valign="middle"   colspan="5">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px"/>
                            </td>
                            <td align="left" colspan="3">
                                &nbsp;
                                <asp:Label ID="lblTituloCatalogo" runat="server" Text="Reporte de Inventario Fisico" Font-Bold="True" Font-Size="13px" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:Button ID="imgVerReporte" runat="server" CssClass="boton" Text="Reporte" onclick="imgVerReporte_Click" />
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFechaInicial" runat="server" Text="Fecha Inicial" 
                            CssClass="Etiquetas"></asp:Label>
                    </td>
                    <td align="left">
                        &nbsp;
                        <asp:TextBox ID="txtFechaInicial" runat="server" CssClass="textbox" 
                            Height="16px" Width="80px"></asp:TextBox>                  
                        &nbsp;<asp:ImageButton ID="imgFechaInicial" runat="server" 
                            ImageUrl="~/Images/calendar.png" Width="24px" />
                        <asp:CalendarExtender ID="calFechaInventario" runat="server" 
                            PopupButtonID="imgFechaInicial" ClearTime="True" Format="dd/MM/yyyy" 
                            TargetControlID="txtFechaInicial" />
                    </td>
                   <td>
                        &nbsp;
                        <asp:Label ID="lblUsuario" runat="server" Text="Usuario" CssClass="Etiquetas"></asp:Label>
                        &nbsp;
                    </td>
                    <td colspan="3" align="left">
                        &nbsp;
                        <asp:TextBox ID="txtUsuario" runat="server" CssClass="textbox" Height="16px" 
                            Width="250px"></asp:TextBox>
                         <asp:AutoCompleteExtender ID="txtUsuario_AutoCompleteExtender" runat="server" 
                        CompletionInterval="100" 
                        CompletionListCssClass="autocomplete_completionListElement" 
                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                        CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                        DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                        ServiceMethod="ObtenerUsuarios" ShowOnlyCurrentWordInCompletionListItem="True" 
                        TargetControlID="txtUsuario" UseContextKey="True">
                    </asp:AutoCompleteExtender>
                    </td>
                    
                </tr>
            <tr>
                <td>
                    <asp:Label ID="lblFechaFinal" runat="server" CssClass="Etiquetas" 
                        Text="Fecha Final"></asp:Label>
                </td>
                <td align="left">
                    &nbsp;
                    <asp:TextBox ID="txtFechaFinal" runat="server" CssClass="textbox" Height="16px" 
                        Width="80px"></asp:TextBox>
                    <asp:CalendarExtender ID="txtFechaFinal_CalendarExtender" runat="server" 
                        ClearTime="True" Format="dd/MM/yyyy" PopupButtonID="imgFechaFinal" 
                        TargetControlID="txtFechaFinal" />&nbsp;<asp:ImageButton ID="imgFechaFinal" runat="server" 
                        ImageUrl="~/Images/calendar.png" Width="24px" />
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
    <br />
    <rsweb:ReportViewer id="ReportViewer1" runat="server" Width="1000px">
    </rsweb:ReportViewer>

</asp:Content>
