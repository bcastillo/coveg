﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteEntradasSalidasVencidos.aspx.cs" Inherits="CovegInventarios.Views.Reportes.ReporteEntradasSalidasVencidos" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="refresh" content="60">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Control de Inventario</title>
    <link href="../../Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Style1.css" rel="Stylesheet" type="text/css" />  
    <link rel="shortcut icon" href="http://www.guanajuato.gob.mx/favicon.ico">
    <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Quattrocento+Sans:regular&amp;subset=latin" media="all">
</head>

<body>
    <form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="tksManager" runat="server" ScriptMode="Release">
                                </asp:ToolkitScriptManager>

               <div id="header-new2" align="center" style="height:0px; top: 0px; left: 0px;">
                <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="100%" >
                                <div id="menu-new" align="center" style="vertical-align:bottom;">
                    REPORTE DE SEGUIMIENTO</div>
                </asp:Panel>

                <table width="100%">
                    <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="34%">
                                    <asp:Label ID="lblPorSalir" runat="server" Text="ACTIVOS POR SALIR" 
                                        Font-Bold="True" Font-Names="verdana" Font-Size="8pt" ForeColor="#0066CC"></asp:Label>
                                </td>
                                <td width="33%">
                                    <asp:Label ID="lblNoVencidos" runat="server" Text="ACTIVOS NO VENCIDOS" 
                                        Font-Bold="True" Font-Names="verdana" Font-Size="8pt" ForeColor="#0066CC"></asp:Label>
                                </td>
                                <td width="33%">
                                <asp:Label ID="lblVencidos" runat="server" Text="ACTIVOS VENCIDOS" Font-Bold="True" 
                                        Font-Names="verdana" Font-Size="8pt" ForeColor="#0066CC"></asp:Label>
                                </td>
                            </tr>

                            <tr>
                                <td width="34%">
                                    <rsweb:ReportViewer id="rvPorSalir" runat="server" Width="100%">
                                    </rsweb:ReportViewer></td>
                                <td width="33%">
                                    <rsweb:ReportViewer id="rvAfueraNoVencidos" runat="server" Width="100%">
                                    </rsweb:ReportViewer></td>                                    
                                <td width="33%">
                                    <rsweb:ReportViewer id="rvAfueraVencidos" runat="server" Width="100%">
                                    </rsweb:ReportViewer></td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                    <tr>"
                        <td colspan="3" align="center">
		                    <div id="footer-new2" align="center">
        	                    <asp:Label ID="Label1" runat="server" Font-Size="10pt" 
                                    Text="Conjunto Administrativo Pozuelos Vialidad 1 s/n 36000 Guanajuato, Gto.│ Teléfono:(473) 735-3803, 735-3800 y 01 800 7 10 54 65 "></asp:Label>
		                    </div><!--pie de página-->
                        </td>
                    </tr>
                </table>
        </div><!--encabezado-->

        <!--gobierno-de-guanajuato-->

    </form>
</body>
</html>
