﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteDetalleSolicitudPop.aspx.cs" Inherits="CovegInventarios.Views.Reportes.ReporteDetalleSolicitudPop" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ToolkitScriptManager ID="tksManager" runat="server" ScriptMode="Release">
        </asp:ToolkitScriptManager>
        <rsweb:ReportViewer id="ReportViewer1" runat="server" Width="1000px">
        </rsweb:ReportViewer>
    </div>
    </form>
</body>
</html>
