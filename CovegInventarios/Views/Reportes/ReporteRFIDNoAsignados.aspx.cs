﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CovegInventarios.Views.Reportes
{
    public partial class RFIDNoAsignados : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LlenarReporteRFIDNoAsignados();
            }
        }

        #region LlenadoDatos

            private DataTable ValoresRFIDNoAsignados()
            {
                return Coveg.Data.OpenData.OpenQuery(System.Configuration.ConfigurationManager.AppSettings["rptRFIDNoAsignados"].ToString());
            }

            private void LlenarReporteRFIDNoAsignados()
            {
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                Microsoft.Reporting.WebForms.LocalReport objRpt = ReportViewer1.LocalReport;

                objRpt.ReportPath = Server.MapPath(".") + "\\RDLC\\ReporteRFIDNoAsignados.rdlc";
                Microsoft.Reporting.WebForms.ReportDataSource rds = new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", ValoresRFIDNoAsignados());
                objRpt.DataSources.Clear();
                objRpt.DataSources.Add(rds);
                ReportViewer1.LocalReport.Refresh();
            }

        #endregion
    }
}