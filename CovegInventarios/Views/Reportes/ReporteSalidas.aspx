﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ReporteSalidas.aspx.cs" Inherits="CovegInventarios.Views.Reportes.ReporteSalidas" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
        <ContentTemplate>
            <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
                <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
            </asp:Panel>
            <table cellspacing="0" cellpadding="5" width="800px">
                <tr class="cpHeaderCatalogos">
                <td valign="middle"   colspan="5">
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px"/>
                            </td>
                            <td align="left" colspan="3">
                                &nbsp;
                                <asp:Label ID="lblTituloCatalogo" runat="server" Text="Reporte de Salidas" Font-Bold="True" Font-Size="13px" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <asp:Button ID="imgVerReporte" runat="server" CssClass="boton" Text="Reporte" onclick="imgVerReporte_Click" />
                </td>
            </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio" CssClass="Etiquetas"></asp:Label>
                    </td>
                    <td align="left">
                        &nbsp;
                        <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="textbox" 
                            Width="80px" Height="16px"></asp:TextBox>
                        &nbsp;
                        <asp:ImageButton ID="imgFechaInicio" runat="server" 
                            ImageUrl="~/Images/calendar.png" Width="24px" />
                        <asp:CalendarExtender ID="calInicio" PopupButtonID="imgFechaInicio" runat="server" ClearTime="True" Format="dd/MM/yyyy" TargetControlID="txtFechaInicio" />
                    </td>
                   <td>
                        &nbsp;&nbsp;
                        <asp:Label ID="lblFechaFin" runat="server" Text="Fecha Final" 
                            CssClass="Etiquetas"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                        <asp:TextBox ID="txtFechaFin" runat="server" CssClass="textbox" Height="16px" 
                            Width="80px"></asp:TextBox>
                        &nbsp;
                        <asp:ImageButton ID="imgFechaFin" runat="server" 
                            ImageUrl="~/Images/calendar.png" Width="24px" />
                        <asp:CalendarExtender ID="calFin" PopupButtonID="imgFechaFin" runat="server" ClearTime="True" Format="dd/MM/yyyy" TargetControlID="txtFechaFin" />
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
    <br />
    <rsweb:ReportViewer id="ReportViewer1" runat="server" Width="1000px">
    </rsweb:ReportViewer>
</asp:Content>
