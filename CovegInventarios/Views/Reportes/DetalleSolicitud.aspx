﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DetalleSolicitud.aspx.cs" Inherits="CovegInventarios.Views.Reportes.DetalleSolicitud" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/Utils/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
    <style type="text/css">
        .style1
        {
            font-family: Intro_Book;
            font-size: 11px;
            color: Black;
            font-weight: bold;
        }
        .style2
        {
        	color: #3466B7;
            font-size: 13px;
            font-weight: bold;
        }
        .style4
        {
        	color: Black;
            width: 87px;
        }
        .style5
        {
        	 color: #3466B7;
            font-size: 13px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">  
    <ContentTemplate>

        <asp:Panel ID="panelMessageBox" runat="server" Visible="False">
            <uc1:MessageBox ID="MessageBox1" runat="server" />
        </asp:Panel>

        <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="100%" >
            <table width="100%">
                <tr>
                    <td colspan="2" align="left">
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Detalle de la solicitud" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                </tr>
            </table>
            </asp:Panel>

        <asp:Panel ID="pBody" runat="server" CssClass="cpBody" BackColor="White" Width="100%">
            <table cellpadding="0" cellspacing="0" class="shadow" width="100%">
            <tr>
                <td align="left">

                    <fieldset id="fsSolicitud" runat="server"  style='background-color:White;'>
                    <legend class="style5"><b>Generales de la solicitud</b></legend>
                    <table cellpadding="2" cellspacing="0" width="100%">
                        <tr style="background-color:#ECF3F8">
                            <td width="130px">
                                <asp:Label ID="lblUsuario" runat="server" CssClass="style1" Text="Solicitante"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtSolicitante" runat="server"></asp:Label>
                            </td>
                            <td width="130px">
                                <asp:Label ID="lblEstatusSolicitud" runat="server" CssClass="style1" 
                                    Text="Estatus de la solicitud"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusSolicitud" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFecha" runat="server" CssClass="style1" 
                                    Text="Fecha de la solicitud"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFecha" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaSalida" runat="server" CssClass="style1" 
                                    Text="Fecha de salida"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaSalida" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="130px">
                                <asp:Label ID="lblMotivo" runat="server" CssClass="style1" 
                                    Text="Motivo de la salida"></asp:Label>
                            </td>
                            <td colspan="5">
                                <asp:Label ID="txtMotivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaRetorno" runat="server" CssClass="style1" 
                                    Text="Fecha de retorno" Width="100px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaRetorno" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr  style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblMotivoRechazo" runat="server" CssClass="style1" 
                                    Text="Motivo del rechazo"></asp:Label>
                            </td>
                            <td colspan="5">
                                <asp:Label ID="txtMotivoRechazo" runat="server"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    </fieldset>

                    <fieldset id="fsActivo" runat="server" style='background-color:White'>
                    <legend class="style2">Generales del activo</legend>
                        <table cellpadding="2" cellspacing="0" width="100%">
                        <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="Label6" runat="server" CssClass="style1" Text="Clave"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtClaveActivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label4" runat="server" CssClass="style1" Text="Número de serie"></asp:Label>
                            </td>
                            <td class="style4">
                                <asp:Label ID="txtNumSerieActivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblNombre" runat="server" CssClass="style1" 
                                    Text="Nombre"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNombre" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEstatusActivo" runat="server" CssClass="style1" 
                                    Text="Estatus del activo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusActivo" runat="server"></asp:Label>
                            </td>
                        </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDescripcion0" runat="server" CssClass="style1" 
                                        Text="Descripción"></asp:Label>
                                </td>
                                <td colspan="7">
                                    <asp:Label ID="txtDescripcionActivo" runat="server"></asp:Label>
                                </td>
                            </tr>
                    </table>
                    </fieldset>

                    <fieldset id="fsDocumento" runat="server" style='background-color:White'>
                    <legend class="style5"><b>Generales del&nbsp;documento</b></legend>
                    <table cellpadding="2" cellspacing="0" width="100%">
                        <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblClave" runat="server" CssClass="style1" Text="Clave"></asp:Label>
                            </td>
                            <td >
                                <asp:Label ID="txtClave" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTipo" runat="server" CssClass="style1" Text="Tipo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtTipo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaIngreso" runat="server" CssClass="style1" 
                                    Text="Fecha de ingreso"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaIngreso" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEstatusDocumento" runat="server" CssClass="style1" 
                                    Text="Estatus del documento"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusDocumento" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblArea" runat="server" CssClass="style1" Text="Área"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtArea" runat="server"></asp:Label>
                            </td>
                            <td width="80px">
                                <asp:Label ID="lblEstante" runat="server" CssClass="style1" Text="Estante"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstante" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCarpeta" runat="server" CssClass="style1" 
                                    Text="Carpeta/Leffort"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCarpeta" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCaja" runat="server" CssClass="style1" Text="Caja"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCaja" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </fieldset>

                    <fieldset id="fsAutomovil" runat="server" style='background-color:White;'>
                    <legend class="style5"><b>Generales del vehículo</b></legend>
                    <table cellpadding="2" cellspacing="0" width="100%">
                    <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblNumeroSerie" runat="server" CssClass="style1" 
                                    Text="Número de serie"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNumeroSerie" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCilindros" runat="server" CssClass="style1" 
                                    Text="Número de cílindros"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCilindros" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblMarca" runat="server" CssClass="style1" Text="Marca"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtMarca" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Vehiculo" runat="server" CssClass="style1" 
                                    Text="Estatus vehículo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusVehiculo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblNoMotor" runat="server" CssClass="style1" 
                                    Text="Número de motor"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNumeroMotor" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblModelo" runat="server" CssClass="style1" Text="Modelo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtModelo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="style1" Text="Tipo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtTipoAutomovil" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPlacas" runat="server" CssClass="style1" Text="Placas"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtPlacas" runat="server"></asp:Label>
                            </td>
                        </tr>
                    <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblNoPuertas" runat="server" CssClass="style1" 
                                    Text="Número de puertas"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNoPuertas" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCapacidad" runat="server" CssClass="style1" Text="Capacidad"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCapacidad" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblColor" runat="server" CssClass="style1" Text="Color"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtColor" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTransmision" runat="server" CssClass="style1" 
                                    Text="Transmisión"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtTransmision" runat="server"></asp:Label>
                            </td>
                        </tr>
                    

                    </table>
                    </fieldset>


                </td>
            </tr>
        </table>

        <table width="100%">
        <tr>
        <td valign="middle" class="cpHeaderCatalogos" >
            <table width="100%">
            <tr>
                <td align="left" colspan="3">
                    <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" />
                    &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" 
                        Text="Activos solicitados" Font-Bold="True" Font-Size="13px"/>
                </td>
            </tr>
            </table>
        </td>
        </tr>
        <tr>
            <td class="panel6" valign="middle">
            <div class="grid" align="center">
                <asp:GridView ID="GridView1" 
                            Width="98%"
                            runat="server" 
                            CellPadding="3" 
                            OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                            AllowPaging="True" 
                            AllowSorting="True"
                            OnPageIndexChanging="GridView1_PageIndexChanging"
                            onsorting="GridView1_Sorting" 
                            AutoGenerateColumns="False"  
                            BackColor="White" 
                            BorderColor="#CCCCCC" 
                            BorderStyle="None" 
                            BorderWidth="1px" 
                            onrowdatabound="GridView1_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="ESTATUS_ACTIVO" HeaderText="ESTATUS" SortExpression="ESTATUS_ACTIVO" />
                            <asp:BoundField DataField="DESC_ACTIVO" HeaderText="DESCRIPCION" SortExpression="DESC_ACTIVO" />
                            <asp:BoundField DataField="TAG" HeaderText="RFIDTAG" SortExpression="TAG" />
                            <asp:BoundField DataField="TIPO_ACTIVO" HeaderText="TIPO ACTIVO" SortExpression="TIPO_ACTIVO" />
                            <asp:BoundField DataField="CLAVE_ACTIVO" HeaderText="CLAVE" SortExpression="CLAVE_ACTIVO" />
                            <asp:BoundField DataField="RESPONSABLE" HeaderText="RESPONSABLE" SortExpression="RESPONSABLE" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>

        </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
