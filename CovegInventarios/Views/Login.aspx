﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CovegInventarios.Views.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="http://guanajuato.gob.mx/favicon.ico"/> 
    <title>Control de Inventario</title>
    <link rel="stylesheet" href="../Styles/EstiloLogin.css">
    <link rel="stylesheet" href="../Styles/reset.css">
	<link rel="stylesheet" href="../Styles/animate.css">
	<link rel="stylesheet" href="../Styles/styles.css">
</head>
<body background="../Images/fondodegradado.jpg;">
	<div id="container" style="height:450px;top:200px">
        <center>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/gto-logo.png" />
        </center>  
		<form runat="server">              
		<label for="name">Usuario:</label>
        <asp:TextBox ID="txtUser" runat="server" CssClass="cajatexto2"></asp:TextBox>
		<label for="username">Contraseña:</label>
		<p>
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        <asp:Label ID ="lblMensaje" runat="server" ForeColor="Red" Font-Size="10pt"></asp:Label>
		<div id="lower">
		<asp:Button ID="imgLogin" runat="server" type="submit"  Text="Aceptar" 
                onclick="imgLogin_Click"/>
		</div>
		</form>
	</div>
</body>
</html>
