﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CatActivoAuto.aspx.cs" Inherits="CovegInventarios.Views.Catalogos.CatActivoAuto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
<title>Control de Inventario</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
    <ContentTemplate>
    <center>
        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

        <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pnl1" TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
            BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" Enabled="True" />
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>

       <asp:Panel ID="pnl1" runat="server"  CssClass="shadowPanel">            
                <table cellpadding="2" cellspacing="0" style="border:solid 1px #333333; background-color:#ffffff">
                <tr class="cpHeader">
                    <td>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Detalles de Automovil" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdGuardar" Text="Guardar" Font-Bold="True" onclick="btnSave_Click" CssClass="boton" />&nbsp;
                        <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" Font-Bold="True" OnClick="btnEliminar_Click" CssClass="boton"/>&nbsp;
                        <asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>            
                <tr>
                <td colspan="2" align="left">                    
                    <table>
                        <tr style="display:none">
                            <td>
                                &nbsp;</td>
                            <td>
                                <asp:TextBox  ID="txtVehiculoID" runat="server" Height="16px" 
                                    CssClass="textbox" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr  style="height:35px">
                            <td>
                                <asp:Label ID="lblClave" runat="server" Text="Clave" 
                                    CssClass="Etiquetas"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido19" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave" runat="server" CssClass="textbox" Height="16px" 
                                    Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblNumeroSerie" runat="server" CssClass="Etiquetas" 
                                    Text="Num Serie"></asp:Label>
                                <asp:Label ID="lblRequerido7" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNumeroSerie" runat="server" CssClass="textbox" 
                                    Height="16px" Width="120px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblNoMotor" runat="server" CssClass="Etiquetas" 
                                    Text="No Motor"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido17" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNumeroMotor" runat="server" CssClass="textbox" 
                                    Height="16px" Width="150px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr  style="height:35px">
                            <td><asp:Label ID="Usuario" runat="server" CssClass="Etiquetas" 
                                    Text="Responsable"></asp:Label>&nbsp;
                                <asp:Label ID="lblRequerido4" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                                
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtUsuario" runat="server" CssClass="textbox" 
                                    Height="16px" Width="98%"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="txtUsuario_AutoCompleteExtender" runat="server" 
                                    CompletionInterval="100" 
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                                    DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                                    ServiceMethod="ObtenerResponsables" 
                                    ShowOnlyCurrentWordInCompletionListItem="True" TargetControlID="txtUsuario" 
                                    UseContextKey="True">
                                </asp:AutoCompleteExtender>
                            </td>
                            <td>
                                <asp:Label ID="lblEstatus" runat="server" CssClass="Etiquetas" Text="Estatus"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido2" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="cboEstatus" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>                                
                            </td>
                        </tr>
                        <tr  style="height:35px">
                            <td><asp:Label ID="lblDescripcion" runat="server" CssClass="Etiquetas" 
                                    Text="Descripción"></asp:Label>&nbsp;
                                <asp:Label ID="lblRequerido5" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                                
                            </td>
                            <td colspan="5" valign="top">
                                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="textbox" 
                                    Height="30px" TextMode="MultiLine" Width="75%"></asp:TextBox>
                                <asp:Label ID="lblRequiere" runat="server" CssClass="Etiquetas" 
                                    Text="Requiere Solicitud"></asp:Label>
                                <asp:CheckBox ID="chkRequereSolicitud" runat="server" Text=" " />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr  style="height:35px">
                            <td>
                                <asp:Label ID="lblModelo" runat="server" CssClass="Etiquetas" Text="Modelo"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido14" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtModelo" runat="server" CssClass="textbox" Height="16px" 
                                    Width="60px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblPlacas" runat="server" CssClass="Etiquetas" Text="Placas"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido18" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>

                            <td>
                                <asp:TextBox ID="txtPlacas" runat="server" CssClass="textbox" Height="16px" 
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;<asp:Label ID="lblMarca" runat="server" CssClass="Etiquetas" Text="Marca"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido9" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtMarca" runat="server" CssClass="textbox" Height="16px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="txtMarca_AutoCompleteExtender" runat="server" 
                                    CompletionInterval="100" 
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                                    DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                                    ServiceMethod="ObtenerMarcas" 
                                    ShowOnlyCurrentWordInCompletionListItem="True"
                                    ServicePath="" TargetControlID="txtMarca">
                                </asp:AutoCompleteExtender>
                            </td>

                            <td width="50px">
                                &nbsp;
                                </td>

                        </tr>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblTipo" runat="server" CssClass="Etiquetas" Text="Tipo"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido11" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTipo" runat="server" CssClass="textbox" Height="16px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="txtTipo_AutoCompleteExtender" runat="server" 
                                    CompletionInterval="100" 
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                                    DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                                    ServiceMethod="ObtenerTipoAuto" 
                                    ShowOnlyCurrentWordInCompletionListItem="True"
                                    ServicePath="" TargetControlID="txtTipo">
                                </asp:AutoCompleteExtender>
                            </td>
                            <td colspan="1">
                                <asp:Label ID="lblCilindros" runat="server" CssClass="Etiquetas" 
                                    Text="No Cilindros"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido10" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td valign="top">
                                <asp:TextBox ID="txtCilindros" runat="server" CssClass="textbox" Height="16px" 
                                    Width="50px"></asp:TextBox>
                                <asp:NumericUpDownExtender ID="txtCilindros_NumericUpDownExtender" 
                                    runat="server" Enabled="True" Maximum="20" Minimum="1" RefValues="" 
                                    ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" 
                                    TargetButtonDownID="" TargetButtonUpID="" TargetControlID="txtCilindros" 
                                    Width="50">
                                </asp:NumericUpDownExtender>
                            </td>
                            <td>
                                &nbsp;<asp:Label ID="lblNoPuertas" runat="server" CssClass="Etiquetas" 
                                    Text="No. Puertas"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido8" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td valign="top">
                                <asp:TextBox ID="txtNoPuertas" runat="server" CssClass="textbox" Height="16px" 
                                    Width="50px"></asp:TextBox>
                                <asp:NumericUpDownExtender ID="txtNoPuertas_NumericUpDownExtender" 
                                    runat="server" Enabled="True" Maximum="20" Minimum="1" RefValues="" 
                                    ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" 
                                    TargetButtonDownID="" TargetButtonUpID="" TargetControlID="txtNoPuertas" 
                                   
                                    Width="50">
                                </asp:NumericUpDownExtender>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblCapacidad" runat="server" CssClass="Etiquetas" 
                                    Text="Capacidad"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido12" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td valign="top">
                                <asp:TextBox ID="txtCapacidad" runat="server" CssClass="textbox" Height="16px" 
                                    Width="70px"></asp:TextBox>
                                <asp:NumericUpDownExtender ID="txtCapacidad_NumericUpDownExtender" 
                                    runat="server" Enabled="True" Maximum="200" Minimum="1" RefValues="" 
                                    ServiceDownMethod="" ServiceDownPath="" ServiceUpMethod="" Tag="" 
                                    TargetButtonDownID="" TargetButtonUpID="" TargetControlID="txtCapacidad" 
                                    Width="70">
                                </asp:NumericUpDownExtender>
                            </td>
                            <td colspan="1">
                                <asp:Label ID="lblColor" runat="server" CssClass="Etiquetas" Text="Color"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido13" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtColor" runat="server" CssClass="textbox" Height="16px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblTransmision" runat="server" CssClass="Etiquetas" 
                                    Text="Transmisión"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido15" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTransmision" runat="server" CssClass="textbox" 
                                    Height="16px"></asp:TextBox>
                                
                                <asp:AutoCompleteExtender ID="txtTransmision_AutoCompleteExtender" 
                                    runat="server" 
                                    CompletionInterval="100" 
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                                    DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                                    ServiceMethod="ObtenerTransmisiones" 
                                    ShowOnlyCurrentWordInCompletionListItem="True"
                                    TargetControlID="txtTransmision">
                                </asp:AutoCompleteExtender>
                                
                            </td>
                        </tr>
                    </table>
                   

                   
                </td>
            </tr>
        </table>
            </asp:Panel>
        

        
        <table>
            <tr>
            <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" 
                                    />
                            </td>
                            <td width="60%" align="left">
                                
                                &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" 
                                    Text="Catalogo de Automoviles" Font-Bold="True" Font-Size="13px" 
                                     /></td>
                            <td align="right">
                                
                                <asp:Button runat="server" ID="Button1" Text="Nuevo"  
                                    Font-Bold="True"
                                    onclick="btnNew_Click" CssClass="boton" 
                                    />
                                &nbsp;<asp:Button runat="server" ID="Button2" Text="Refrescar"  
                                    Font-Bold="True" OnClick="btnRefresh_Click" 
                                     CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
            <tr>
            <td class="panel6" valign="middle">
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="Label1" runat="server" 
                    Style="font-weight: 700" Text="Expresión de búsqueda" CssClass="Etiquetas" 
                    ></asp:Label>
                &nbsp;&nbsp;<asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search" 
                    AutoPostBack="True" CssClass="textbox" 
                    Width="170px" Height="16px" ></asp:TextBox>
                &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" 
                    ImageUrl="~/Images/search_find.png" onclick="ImageButton3_Click" Width="24px" />
&nbsp;
                <asp:Label ID="Label2" runat="server" CssClass="Etiquetas" 
                    Font-Bold="True" Text="Tamaño de página" 
                    ></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" Width="48px" 
                    CssClass="comboSeleccion" Height="20px" 
                    >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="30" Value="30" ></asp:ListItem>
                    <asp:ListItem Text="50" Value="50" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                    <asp:ListItem Text="300" Value="300" ></asp:ListItem>
                    <asp:ListItem Text="500" Value="500" ></asp:ListItem>
                    <asp:ListItem Text="---" Value="0" ></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
            <tr>
            <td align="center">
            <br />
            <div class="grid" style="width:1000px;">
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        Width="990px"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound" 
                    EmptyDataText="No se encontraron registros">
                        <Columns>
                            <asp:BoundField DataField="VEHICULOID" HeaderText="ID" SortExpression="VEHICULOID" Visible="false" />
                            <asp:BoundField DataField="RESPONSABLE" HeaderText="RESPONSABLE" SortExpression="RESPONSABLE" />
                            <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" SortExpression="CLAVE" />
                            <asp:BoundField DataField="PLACAS" HeaderText="PLACAS" SortExpression="PLACAS" />
                            <asp:BoundField DataField="MARCA" HeaderText="MARCA" SortExpression="MARCA" />
                            <asp:BoundField DataField="MODELO" HeaderText="MODELO" SortExpression="MODELO" />
                            <asp:BoundField DataField="CAPACIDAD" HeaderText="CAPACIDAD" SortExpression="CAPACIDAD" />
                            <asp:BoundField DataField="COLOR" HeaderText="COLOR" SortExpression="COLOR" />
                            <asp:BoundField DataField="PUERTAS" HeaderText="PUERTAS" SortExpression="PUERTAS" />
                            <asp:BoundField DataField="TIPO" HeaderText="TIPO" SortExpression="TIPO" />
                            <asp:BoundField DataField="TRANSMISION" HeaderText="TRANSMISION" SortExpression="TRANSMISION" />
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Editar">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                         <EmptyDataRowStyle Font-Bold="True" Font-Size="13px" ForeColor="Red" 
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
            </div>    
        </td>
        </tr>
        </table>
        </center>
        </ContentTemplate>
  </asp:UpdatePanel> 
</asp:Content>
