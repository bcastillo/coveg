﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;
using Coveg.Catalogos;
using F;

namespace CovegInventarios.Views.Catalogos
{
    public partial class CatActivo : System.Web.UI.Page
    {

        #region Properties

        Coveg.Catalogos.Controller.ACTIVOController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Catalogos.Controller.ACTIVOController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Catalogos.Controller.ACTIVOController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.ACTIVO_BE> lista
        {
            get
            {
                return (List<Coveg.Entities.ACTIVO_BE>)Page.Session["CatACTIVOCollection"];
            }
            set
            {
                Page.Session["CatACTIVOCollection"] = value;
            }
        }
        #endregion

        #region Initializers
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtActivoID.Attributes.Add("readonly", "readonly");

            try
            {
                if (!IsPostBack)
                {
                    Page.Session["SortDirection"] = "Ascending";
                    Page.Session["SortExpression"] = "";
                    Refresh(true);
                    CargarCombos();
                }
            }
            catch
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }



        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            if (Session["Recarga"] != null)
                if (Convert.ToBoolean(Session["Recarga"]))
                {
                    Session["Recarga"] = false;
                    mpEditarElemento.Show();
                }

            panelMessageBox.Visible = false;
        }

        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Información", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void ClearControls()
        {
            txtActivoID.Text = String.Empty;
            txtClave.Text = String.Empty;
            txtDescripcion.Text = String.Empty;
            txtNombre.Text = String.Empty;
            txtNumeroSerie.Text = String.Empty;
            txtNumNomina.Text = String.Empty;
            cboEstatus.SelectedIndex = -1;
            chkRequereSolicitud.Checked = false;

            //cboRFIDTAG.SelectedValue = "0";
            cboTipoActivo.SelectedIndex = -1;
            cboUnidad.SelectedIndex = -1;

            cboEstatus.SelectedIndex = -1;
            GridView1.SelectedIndex = -1;
        }

        /// <summary>
        ///
        /// </summary>
        public void SelectRecord()
        {
            if (GridView1.SelectedRow != null)
            {
                selectedRow = GridView1.SelectedRow.DataItemIndex;
                if (selectedRow < lista.Count())
                {
                    Coveg.Entities.ACTIVO_BE be = lista[selectedRow];
                    txtActivoID.Text = be.ACTIVOID.ToString();
                    txtClave.Text = be.CLAVE;
                    txtDescripcion.Text = be.DESCRIPCION;
                    txtNombre.Text = be.NOMBRE;
                    txtNumeroSerie.Text = be.NUMEROSERIE;
                    cboEstatus.SelectedValue = be.ESTATUS.ToString();
                    txtNumNomina.Text = be.NUMNOMINA;

                    //if (be.RFIDTAGID != 0)
                    //{
                    //    if (cboRFIDTAG.Items.FindByValue (be.RFIDTAGID.ToString())!=null)
                    //        cboRFIDTAG.SelectedValue = be.RFIDTAGID.ToString();
                    //}
                    //else
                    //    cboRFIDTAG.SelectedIndex = -1;

                    cboTipoActivo.SelectedValue = be.TIPOACTIVOID.ToString();
                    cboUnidad.SelectedValue = be.UNIDADID.ToString();

                    chkRequereSolicitud.Checked = be.REQUIERE_SOLICITUD;
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Refresh(bool requery)
        {
            if (requery || lista == null)
                lista = Controller.SelectAll(new ACTIVO_BE());

            selectedRow = -1;

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }
        /// <summary>
        ///
        /// </summary>
        public void Filtrar()
        {
            Refresh(true);
            if (GridView1.DataSource is List<ACTIVO_BE>)
            {
                string strValue = txtSearch.Text.ToLower();

                lista = lista.FindAll(p => 
                    F.Text.Input.IsNothing(p.CLAVE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESC_ESTATUS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESC_RFIDTAG, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESC_TIPOACTIVO, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESC_UNIDAD, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESCRIPCION, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.ESTATUS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NOMBRE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NUMEROSERIE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NUMNOMINA, "").ToString().ToLower().Contains(strValue)
                    
                    );

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Delete()
        {
            if (selectedRow >= 0)
            {
                if (lista[selectedRow] != null)
                {
                    ACTIVO_BE objBE = new ACTIVO_BE();
                    objBE.ACTIVOID = lista[selectedRow].ACTIVOID;

                    if (Controller.Delete(objBE))
                    {
                        Refresh(true);
                        ClearControls();
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoEliminado"]);
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Save()
        {
            if (ValidateControls())
            {
                ACTIVO_BE be = new ACTIVO_BE();

                if (txtActivoID.Text != String.Empty)
                    be.ACTIVOID = Convert.ToInt32(txtActivoID.Text);
                
                be.CLAVE = txtClave.Text;
                be.DESCRIPCION = txtDescripcion.Text;
                be.NOMBRE = txtNombre.Text;
                be.NUMEROSERIE = txtNumeroSerie.Text;

                be.ESTATUS = Convert.ToInt32(cboEstatus.SelectedValue);

                var tokens = Coveg.Common.Text.ObtenerTokens(txtNumNomina.Text);

                if (tokens != null && tokens.Length > 0)
                    be.NUMNOMINA = tokens[0];
                else
                    be.NUMNOMINA = txtNumNomina.Text;

                //if(cboRFIDTAG.SelectedValue!=String.Empty)
                //    be.RFIDTAGID = Convert.ToInt32(cboRFIDTAG.SelectedValue);

                be.TIPOACTIVOID = Convert.ToInt32(cboTipoActivo.SelectedValue);
                be.UNIDADID = Convert.ToInt32(cboUnidad.SelectedValue);
                be.REQUIERE_SOLICITUD = chkRequereSolicitud.Checked;

                if (lista != null && lista.Find(entity => entity.ACTIVOID == be.ACTIVOID) != null)
                {
                    if (Controller.Update(be) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoActualizado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errAtualizarRegistro"]);
                    }
                    ClearControls();
                }
                else
                {

                    if (Controller.Insert(be) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoInsertado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errInsertarRegistro"]);
                    }
                    ClearControls();
                }
            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                mpEditarElemento.Show();
                Session["Recarga"] = true;
            }
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        public void LoadCollection(List<ACTIVO_BE> collection)
        {
            lista = collection;
            Refresh(false);
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            return (
                cboTipoActivo.SelectedIndex!=-1
                &&!string.IsNullOrEmpty(txtClave.Text) 
                && !String.IsNullOrEmpty(txtNombre.Text)
                && !String.IsNullOrEmpty(txtDescripcion.Text)
                && cboEstatus.SelectedIndex != -1
                && !String.IsNullOrEmpty(txtNumNomina.Text)
                );
        }
        #endregion

        #region Eventos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Filtrar();
        }
        /// <summary>
        /// Cancelar edición
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdCancelarPop_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Hide();
        }

        /// <summary>
        /// Mouse OVER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
            }
        }

        /// <summary>
        /// Nuevo elemento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Show();
            cmdEliminar.Visible = false;
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        /// <summary>
        /// Actulizar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearControls();
            Refresh(true);
        }

        

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (F.Text.Input.IsNumeric(ddlPageSize.SelectedValue))
            {
                int psize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (psize == 0)
                {
                    GridView1.AllowPaging = false;
                }
                else
                {
                    GridView1.AllowPaging = true;
                    GridView1.PageSize = psize;
                    GridView1.PageIndex = 0;
                }
                Refresh(true);
                ClearControls();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectRecord();
            mpEditarElemento.Show();
            cmdEliminar.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }


        #endregion

        #region Sorting Commands

        /// <summary>
        /// Ordenar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (lista == null || lista.Count == 0)
                return;

            if (e.SortExpression == "ACTIVOID")
            {
                Page.Session["SortExpression"] = "ACTIVOID";
                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                {
                    var v = lista.OrderBy(p => p.ACTIVOID);
                    lista = v.ToList<ACTIVO_BE>();
                    Page.Session["SortDirection"] = "Descending";
                }
                else
                {
                    var v = lista.OrderByDescending(p => p.ACTIVOID);
                    lista = v.ToList<ACTIVO_BE>();
                    Page.Session["SortDirection"] = "Ascending";
                }
            }
            else
                if (e.SortExpression == "CLAVE")
                {
                    Page.Session["SortExpression"] = "CLAVE";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.CLAVE);
                        lista = v.ToList<ACTIVO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.CLAVE);
                        lista = v.ToList<ACTIVO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "DESC_ESTATUS")
                    {
                        Page.Session["SortExpression"] = "DESC_ESTATUS";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.DESC_ESTATUS);
                            lista = v.ToList<ACTIVO_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.DESC_ESTATUS);
                            lista = v.ToList<ACTIVO_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "DESC_RFIDTAG")
                        {
                            Page.Session["SortExpression"] = "DESC_RFIDTAG";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.DESC_RFIDTAG);
                                lista = v.ToList<ACTIVO_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.DESC_RFIDTAG);
                                lista = v.ToList<ACTIVO_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DESC_TIPOACTIVO")
                            {
                                Page.Session["SortExpression"] = "DESC_TIPOACTIVO";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.DESC_TIPOACTIVO);
                                    lista = v.ToList<ACTIVO_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.DESC_TIPOACTIVO);
                                    lista = v.ToList<ACTIVO_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "DESC_UNIDAD")
                                {
                                    Page.Session["SortExpression"] = "DESC_UNIDAD";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.DESC_UNIDAD);
                                        lista = v.ToList<ACTIVO_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.DESC_UNIDAD);
                                        lista = v.ToList<ACTIVO_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "DESCRIPCION")
                                    {
                                        Page.Session["SortExpression"] = "DESCRIPCION";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.DESCRIPCION);
                                            lista = v.ToList<ACTIVO_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.DESCRIPCION);
                                            lista = v.ToList<ACTIVO_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "ESTATUS")
                                        {
                                            Page.Session["SortExpression"] = "ESTATUS";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.ESTATUS);
                                                lista = v.ToList<ACTIVO_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.ESTATUS);
                                                lista = v.ToList<ACTIVO_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "NOMBRE")
                                            {
                                                Page.Session["SortExpression"] = "NOMBRE";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.NOMBRE);
                                                    lista = v.ToList<ACTIVO_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.NOMBRE);
                                                    lista = v.ToList<ACTIVO_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "NUMEROSERIE")
                                                {
                                                    Page.Session["SortExpression"] = "NUMEROSERIE";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.NUMEROSERIE);
                                                        lista = v.ToList<ACTIVO_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.NUMEROSERIE);
                                                        lista = v.ToList<ACTIVO_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "NUMNOMINA")
                                                    {
                                                        Page.Session["SortExpression"] = "NUMNOMINA";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.NUMNOMINA);
                                                            lista = v.ToList<ACTIVO_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.NUMNOMINA);
                                                            lista = v.ToList<ACTIVO_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "RFIDTAGID")
                                                        {
                                                            Page.Session["SortExpression"] = "RFIDTAGID";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.RFIDTAGID);
                                                                lista = v.ToList<ACTIVO_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.RFIDTAGID);
                                                                lista = v.ToList<ACTIVO_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "TIPOACTIVOID")
                                                            {
                                                                Page.Session["SortExpression"] = "TIPOACTIVOID";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.TIPOACTIVOID);
                                                                    lista = v.ToList<ACTIVO_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.TIPOACTIVOID);
                                                                    lista = v.ToList<ACTIVO_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }
                                                            else
                                                                if (e.SortExpression == "UNIDADID")
                                                                {
                                                                    Page.Session["SortExpression"] = "UNIDADID";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.UNIDADID);
                                                                        lista = v.ToList<ACTIVO_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.UNIDADID);
                                                                        lista = v.ToList<ACTIVO_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }


            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
            ClearControls();
        }


        #endregion

        #region Funciones

        /// <summary>
        /// Carga listado em combos
        /// </summary>
        private void CargarCombos()
        {
            List<ESTATUS_BE> lstEstatus = new List<ESTATUS_BE>();
            List<TIPOACTIVO_BE> lstTipoActivo = new List<TIPOACTIVO_BE>();
            List<UNIDAD_BE> lstUnidad = new List<UNIDAD_BE>();
            List<RFIDTAG_BE> lstRFIDTAG = new List<RFIDTAG_BE>();
            RFIDTAG_BE objNARFID = new RFIDTAG_BE();

            lstEstatus = Controller.ListaEstatusTipoEstatus(Coveg.Common.Constantes.TE_ACTIVO);
            lstTipoActivo = Controller.ListaTipoActivo();

            if (lstTipoActivo != null)
            {
                lstTipoActivo = lstTipoActivo.FindAll(p => !p.NOMBRE.Equals("DOCUMENTO") && !p.NOMBRE.Equals("AUTO"));
            }

            lstUnidad = Controller.ListaUnidad();
            lstRFIDTAG = Controller.ListaTAG();

            cboEstatus.DataSource = lstEstatus;
            cboEstatus.DataTextField = "NOMBRE";
            cboEstatus.DataValueField = "ESTATUSID";
            cboEstatus.DataBind();

            cboTipoActivo.DataSource = lstTipoActivo;
            cboTipoActivo.DataTextField = "DESCRIPCION";
            cboTipoActivo.DataValueField = "TIPOACTIVOID";
            cboTipoActivo.DataBind();


            cboUnidad.DataSource = lstUnidad;
            cboUnidad.DataTextField = "DESCRIPCION";
            cboUnidad.DataValueField = "UNIDADID";
            cboUnidad.DataBind();

            
            objNARFID.RFIDTAGID = 0;
            objNARFID.DESCRIPCION = "N/A";

            if (lstRFIDTAG == null)
                lstRFIDTAG = new List<RFIDTAG_BE>();

            lstRFIDTAG.Insert(0, objNARFID);
                

            //cboRFIDTAG.DataSource = lstRFIDTAG;
            //cboRFIDTAG.DataTextField = "DESCRIPCION";
            //cboRFIDTAG.DataValueField = "RFIDTAGID";
            //cboRFIDTAG.DataBind();

            
        }

        #endregion

        #region Search Methods
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerResponsables(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("SELECT NO_EMPLEADO+' | '+nombre+' '+apellidos EMPLEADO FROM vwUsuario  WITH(NOLOCK) WHERE NO_EMPLEADO LIKE '" + prefixText + "%' or NOMBRE LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["EMPLEADO"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        #endregion

       

    }
}