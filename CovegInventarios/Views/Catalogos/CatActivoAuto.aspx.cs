﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;
using Coveg.Catalogos;
using F;

namespace CovegInventarios.Views.Catalogos
{
    public partial class CatActivoAuto : System.Web.UI.Page
    {
        #region Properties

        Coveg.Catalogos.Controller.VEHICULOController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Catalogos.Controller.VEHICULOController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Catalogos.Controller.VEHICULOController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.VEHICULO_BE> lista
        {
            get
            {
                return (List<Coveg.Entities.VEHICULO_BE>)Page.Session["CATVEHICULOCollection"];
            }
            set
            {
                Page.Session["CATVEHICULOCollection"] = value;
            }
        }

        #endregion

        #region Initializers
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtVehiculoID.Attributes.Add("readonly", "readonly");

            try
            {
                if (!IsPostBack)
                {
                    Page.Session["SortDirection"] = "Ascending";
                    Page.Session["SortExpression"] = "";
                    Refresh(true);
                    CargarCombos();
                }
            }
            catch
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }



        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            if (Session["Recarga"] != null)
                if (Convert.ToBoolean(Session["Recarga"]))
                {
                    Session["Recarga"] = false;
                    mpEditarElemento.Show();
                }

            panelMessageBox.Visible = false;
        }

        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Información", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void ClearControls()
        {
            txtVehiculoID.Text = String.Empty;
            txtCapacidad.Text = String.Empty;
            txtCilindros.Text = String.Empty;
            txtColor.Text = String.Empty;
            txtDescripcion.Text = String.Empty;
            txtMarca.Text = String.Empty;
            txtModelo.Text = String.Empty;
            txtNoPuertas.Text = String.Empty;
            txtNumeroSerie.Text = String.Empty;
            txtNumeroMotor.Text = String.Empty;
            txtPlacas.Text = String.Empty;
            txtTipo.Text = String.Empty;
            txtTransmision.Text = String.Empty;
            txtUsuario.Text = String.Empty;
            txtClave.Text = String.Empty;

            cboEstatus.SelectedIndex = -1;
            chkRequereSolicitud.Checked = false;
            Session["ACTIVOID"] = 0;
            GridView1.SelectedIndex = -1;
        }

        /// <summary>
        ///
        /// </summary>
        public void SelectRecord()
        {
            if (GridView1.SelectedRow != null)
            {
                selectedRow = GridView1.SelectedRow.DataItemIndex;

                if (selectedRow < lista.Count())
                {
                    Coveg.Entities.VEHICULO_BE be = lista[selectedRow];
                    Session["ACTIVOID"] = be.ACTIVOID;
                    txtVehiculoID.Text = be.VEHICULOID.ToString();
                    txtCapacidad.Text = be.CAPACIDAD.ToString();
                    txtCilindros.Text = be.CILINDROS.ToString();
                    txtColor.Text = be.COLOR;
                    txtDescripcion.Text = be.DESCRIPCION;
                    txtMarca.Text = be.MARCA;
                    txtModelo.Text = be.MODELO.ToString();
                    txtNoPuertas.Text = be.PUERTAS.ToString();
                    txtNumeroSerie.Text = be.NUMERO_SERIE;
                    txtNumeroMotor.Text = be.NUMERO_MOTOR;
                    txtPlacas.Text = be.PLACAS;
                    txtTipo.Text = be.TIPO;
                    txtTransmision.Text = be.TRANSMISION;
                    txtUsuario.Text = be.NUMNOMINA;
                    txtClave.Text = be.CLAVE;
                    cboEstatus.SelectedValue = be.ESTATUS.ToString();
                    chkRequereSolicitud.Checked = be.REQUIERE_SOLICITUD;
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Refresh(bool requery)
        {
            if (requery || lista == null)
            {
                lista = Controller.SelectAll(new VEHICULO_BE());
            }

            selectedRow = -1;

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }
        /// <summary>
        ///
        /// </summary>
        public void Filtrar()
        {
            Refresh(true);
            if (GridView1.DataSource is List<VEHICULO_BE>)
            {
                string strValue = txtSearch.Text.ToLower();

                lista = lista.FindAll(p => 
                     F.Text.Input.IsNothing(p.CAPACIDAD, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.CILINDROS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.COLOR, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.MARCA, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.MODELO, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.PUERTAS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NUMERO_SERIE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NUMERO_MOTOR, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.PLACAS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.TIPO, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.TRANSMISION, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.CLAVE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESCRIPCION, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NUMNOMINA, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.ESTATUS, "").ToString().ToLower().Contains(strValue)
                    );

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Delete()
        {
            if (selectedRow >= 0)
            {
                if (lista[selectedRow] != null)
                {
                    VEHICULO_BE objBE = new VEHICULO_BE();
                    objBE.ACTIVOID = lista[selectedRow].ACTIVOID;

                    if (Controller.Delete(objBE))
                    {
                        Refresh(true);
                        ClearControls();
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoEliminado"]);
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Save()
        {
            if (ValidateControls())
            {
                VEHICULO_BE be = new VEHICULO_BE();
                ACTIVO_BE beActivo = new ACTIVO_BE();

                if (txtVehiculoID.Text != String.Empty)
                {
                    be.VEHICULOID = Convert.ToInt32(txtVehiculoID.Text);
                    be.ACTIVOID = Convert.ToInt32(Session["ACTIVOID"]);
                    beActivo.ACTIVOID = Convert.ToInt32(Session["ACTIVOID"]);
                }

                ///Valores de tabla de vehiculo
                be.CAPACIDAD = txtCapacidad.Text;
                be.CILINDROS = Convert.ToInt32(txtCilindros.Text); 
                be.COLOR = txtColor.Text;
                be.MARCA = txtMarca.Text;
                be.MODELO = txtModelo.Text;
                be.PUERTAS = Convert.ToInt32(txtNoPuertas.Text);
                be.NUMERO_SERIE = txtNumeroSerie.Text;
                be.NUMERO_MOTOR = txtNumeroMotor.Text;
                be.PLACAS = txtPlacas.Text;
                be.TIPO = txtTipo.Text;
                be.TRANSMISION = txtTransmision.Text;

                ///Valores de tabla activo
                
                

                if (lista != null && lista.Find(entity => entity.ACTIVOID == be.ACTIVOID) != null)
                {
                    beActivo = Controller.GetActivo(lista[selectedRow].CLAVE);
                    beActivo.DESCRIPCION = txtDescripcion.Text;

                    EstablecerResponsable(beActivo);

                    beActivo.ESTATUS = Convert.ToInt32(cboEstatus.SelectedValue);
                    beActivo.REQUIERE_SOLICITUD = chkRequereSolicitud.Checked;
                    beActivo.CLAVE = txtClave.Text;

                    if (Controller.Update(be, beActivo))
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoActualizado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errAtualizarRegistro"]);
                    }
                    ClearControls();
                }
                else
                {
                    EstablecerResponsable(beActivo);
                    beActivo.ESTATUS = Convert.ToInt32(cboEstatus.SelectedValue);
                    beActivo.REQUIERE_SOLICITUD = chkRequereSolicitud.Checked;
                    beActivo.CLAVE = txtClave.Text;

                    if (Controller.Insert(be, beActivo) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoInsertado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errInsertarRegistro"]);
                    }
                    ClearControls();
                }
            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                mpEditarElemento.Show();
                Session["Recarga"] = true;
            }
        }

        private void EstablecerResponsable(ACTIVO_BE beActivo)
        {
            var tokens = Coveg.Common.Text.ObtenerTokens(txtUsuario.Text);

            if (tokens != null && tokens.Length > 0)
                beActivo.NUMNOMINA = tokens[0];
            else
                beActivo.NUMNOMINA = txtUsuario.Text;
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        public void LoadCollection(List<VEHICULO_BE> collection)
        {
            lista = collection;
            Refresh(false);
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            return (
                cboEstatus.SelectedIndex != -1
                && !String.IsNullOrEmpty(txtCapacidad.Text)
                && !String.IsNullOrEmpty(txtCilindros.Text)
                && !String.IsNullOrEmpty(txtColor.Text)
                && !String.IsNullOrEmpty(txtDescripcion.Text)
                && !String.IsNullOrEmpty(txtMarca.Text)
                && !String.IsNullOrEmpty(txtModelo.Text)
                && !String.IsNullOrEmpty(txtNoPuertas.Text)
                && !String.IsNullOrEmpty(txtNumeroMotor.Text)
                && !String.IsNullOrEmpty(txtNumeroSerie.Text)
                && !String.IsNullOrEmpty(txtPlacas.Text)
                && !String.IsNullOrEmpty(txtTipo.Text)
                && !String.IsNullOrEmpty(txtTransmision.Text)
                && !String.IsNullOrEmpty(txtUsuario.Text)
                );
        }
        #endregion

        #region Eventos
        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Filtrar();
        }
        /// <summary>
        /// Cancelar edición
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdCancelarPop_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Hide();
        }

        /// <summary>
        /// Mouse OVER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
            }
        }

        /// <summary>
        /// Nuevo elemento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Show();
            cmdEliminar.Visible = false;
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        /// <summary>
        /// Actulizar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearControls();
            Refresh(true);
        }

        

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (F.Text.Input.IsNumeric(ddlPageSize.SelectedValue))
            {
                int psize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (psize == 0)
                {
                    GridView1.AllowPaging = false;
                }
                else
                {
                    GridView1.AllowPaging = true;
                    GridView1.PageSize = psize;
                    GridView1.PageIndex = 0;
                }
                Refresh(true);
                ClearControls();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectRecord();
            mpEditarElemento.Show();
            cmdEliminar.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }


        #endregion

        #region Sorting Commands

        /// <summary>
        /// Ordenar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (lista == null || lista.Count == 0)
                return;

            if (e.SortExpression == "ACTIVOID")
            {
                Page.Session["SortExpression"] = "ACTIVOID";
                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                {
                    var v = lista.OrderBy(p => p.ACTIVOID);
                    lista = v.ToList<VEHICULO_BE>();
                    Page.Session["SortDirection"] = "Descending";
                }
                else
                {
                    var v = lista.OrderByDescending(p => p.ACTIVOID);
                    lista = v.ToList<VEHICULO_BE>();
                    Page.Session["SortDirection"] = "Ascending";
                }
            }
            else
                if (e.SortExpression == "CAPACIDAD")
                {
                    Page.Session["SortExpression"] = "CAPACIDAD";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.CAPACIDAD);
                        lista = v.ToList<VEHICULO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.CAPACIDAD);
                        lista = v.ToList<VEHICULO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "CILINDROS")
                    {
                        Page.Session["SortExpression"] = "CILINDROS";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.CILINDROS);
                            lista = v.ToList<VEHICULO_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.CILINDROS);
                            lista = v.ToList<VEHICULO_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "CLAVE")
                        {
                            Page.Session["SortExpression"] = "CLAVE";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.CLAVE);
                                lista = v.ToList<VEHICULO_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.CLAVE);
                                lista = v.ToList<VEHICULO_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "COLOR")
                            {
                                Page.Session["SortExpression"] = "COLOR";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.COLOR);
                                    lista = v.ToList<VEHICULO_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.COLOR);
                                    lista = v.ToList<VEHICULO_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "DESCRIPCION")
                                {
                                    Page.Session["SortExpression"] = "DESCRIPCION";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.DESCRIPCION);
                                        lista = v.ToList<VEHICULO_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.DESCRIPCION);
                                        lista = v.ToList<VEHICULO_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "ESTATUS")
                                    {
                                        Page.Session["SortExpression"] = "ESTATUS";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.ESTATUS);
                                            lista = v.ToList<VEHICULO_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.ESTATUS);
                                            lista = v.ToList<VEHICULO_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "MARCA")
                                        {
                                            Page.Session["SortExpression"] = "MARCA";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.MARCA);
                                                lista = v.ToList<VEHICULO_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.MARCA);
                                                lista = v.ToList<VEHICULO_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "MODELO")
                                            {
                                                Page.Session["SortExpression"] = "MODELO";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.MODELO);
                                                    lista = v.ToList<VEHICULO_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.MODELO);
                                                    lista = v.ToList<VEHICULO_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "NUMERO_MOTOR")
                                                {
                                                    Page.Session["SortExpression"] = "NUMERO_MOTOR";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.NUMERO_MOTOR);
                                                        lista = v.ToList<VEHICULO_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.NUMERO_MOTOR);
                                                        lista = v.ToList<VEHICULO_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "NUMERO_SERIE")
                                                    {
                                                        Page.Session["SortExpression"] = "NUMERO_SERIE";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.NUMERO_SERIE);
                                                            lista = v.ToList<VEHICULO_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.NUMERO_SERIE);
                                                            lista = v.ToList<VEHICULO_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "NUMNOMINA")
                                                        {
                                                            Page.Session["SortExpression"] = "NUMNOMINA";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.NUMNOMINA);
                                                                lista = v.ToList<VEHICULO_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.NUMNOMINA);
                                                                lista = v.ToList<VEHICULO_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }
                                                        else
                                                            if (e.SortExpression == "PLACAS")
                                                            {
                                                                Page.Session["SortExpression"] = "PLACAS";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.PLACAS);
                                                                    lista = v.ToList<VEHICULO_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.PLACAS);
                                                                    lista = v.ToList<VEHICULO_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }
                                                            else
                                                                if (e.SortExpression == "PUERTAS")
                                                                {
                                                                    Page.Session["SortExpression"] = "PUERTAS";
                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                    {
                                                                        var v = lista.OrderBy(p => p.PUERTAS);
                                                                        lista = v.ToList<VEHICULO_BE>();
                                                                        Page.Session["SortDirection"] = "Descending";
                                                                    }
                                                                    else
                                                                    {
                                                                        var v = lista.OrderByDescending(p => p.PUERTAS);
                                                                        lista = v.ToList<VEHICULO_BE>();
                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                    }
                                                                }
                                                                else
                                                                    if (e.SortExpression == "REQUIERE_SOLICITUD")
                                                                    {
                                                                        Page.Session["SortExpression"] = "REQUIERE_SOLICITUD";
                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                        {
                                                                            var v = lista.OrderBy(p => p.REQUIERE_SOLICITUD);
                                                                            lista = v.ToList<VEHICULO_BE>();
                                                                            Page.Session["SortDirection"] = "Descending";
                                                                        }
                                                                        else
                                                                        {
                                                                            var v = lista.OrderByDescending(p => p.REQUIERE_SOLICITUD);
                                                                            lista = v.ToList<VEHICULO_BE>();
                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                        }
                                                                    }
                                                                    else
                                                                        if (e.SortExpression == "RESPONSABLE")
                                                                        {
                                                                            Page.Session["SortExpression"] = "RESPONSABLE";
                                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                            {
                                                                                var v = lista.OrderBy(p => p.RESPONSABLE);
                                                                                lista = v.ToList<VEHICULO_BE>();
                                                                                Page.Session["SortDirection"] = "Descending";
                                                                            }
                                                                            else
                                                                            {
                                                                                var v = lista.OrderByDescending(p => p.RESPONSABLE);
                                                                                lista = v.ToList<VEHICULO_BE>();
                                                                                Page.Session["SortDirection"] = "Ascending";
                                                                            }
                                                                        }
                                                                        else
                                                                            if (e.SortExpression == "TIPO")
                                                                            {
                                                                                Page.Session["SortExpression"] = "TIPO";
                                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                {
                                                                                    var v = lista.OrderBy(p => p.TIPO);
                                                                                    lista = v.ToList<VEHICULO_BE>();
                                                                                    Page.Session["SortDirection"] = "Descending";
                                                                                }
                                                                                else
                                                                                {
                                                                                    var v = lista.OrderByDescending(p => p.TIPO);
                                                                                    lista = v.ToList<VEHICULO_BE>();
                                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                                }
                                                                            }
                                                                            else
                                                                                if (e.SortExpression == "TRANSMISION")
                                                                                {
                                                                                    Page.Session["SortExpression"] = "TRANSMISION";
                                                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                    {
                                                                                        var v = lista.OrderBy(p => p.TRANSMISION);
                                                                                        lista = v.ToList<VEHICULO_BE>();
                                                                                        Page.Session["SortDirection"] = "Descending";
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        var v = lista.OrderByDescending(p => p.TRANSMISION);
                                                                                        lista = v.ToList<VEHICULO_BE>();
                                                                                        Page.Session["SortDirection"] = "Ascending";
                                                                                    }
                                                                                }
                                                                                else
                                                                                    if (e.SortExpression == "VEHICULOID")
                                                                                    {
                                                                                        Page.Session["SortExpression"] = "VEHICULOID";
                                                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                                        {
                                                                                            var v = lista.OrderBy(p => p.VEHICULOID);
                                                                                            lista = v.ToList<VEHICULO_BE>();
                                                                                            Page.Session["SortDirection"] = "Descending";
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            var v = lista.OrderByDescending(p => p.VEHICULOID);
                                                                                            lista = v.ToList<VEHICULO_BE>();
                                                                                            Page.Session["SortDirection"] = "Ascending";
                                                                                        }
                                                                                    }


            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
            ClearControls();
        }


        #endregion

        #region Funciones

        /// <summary>
        /// Carga listado em combos
        /// </summary>
        private void CargarCombos()
        {
            List<ESTATUS_BE> lstEstatus = new List<ESTATUS_BE>();

            lstEstatus = Controller.ListaEstatusTipoEstatus(Coveg.Common.Constantes.TE_ACTIVO);

            cboEstatus.DataSource = lstEstatus;
            cboEstatus.DataTextField = "NOMBRE";
            cboEstatus.DataValueField = "ESTATUSID";
            cboEstatus.DataBind();
        }

        #endregion

        #region Search Methods
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerResponsables(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("SELECT NO_EMPLEADO+' | '+nombre+' '+apellidos EMPLEADO FROM vwUsuario  WITH(NOLOCK) WHERE NO_EMPLEADO LIKE '" + prefixText + "%' or NOMBRE LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["EMPLEADO"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerTransmisiones(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("SELECT DISTINCT TRANSMISION  FROM VEHICULO WITH(NOLOCK) WHERE TRANSMISION LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["TRANSMISION"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerMarcas(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("SELECT distinct MARCA  FROM VEHICULO with(nolock) WHERE MARCA LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["MARCA"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerTipoAuto(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("SELECT DISTINCT TIPO  FROM VEHICULO WITH(NOLOCK) WHERE TIPO LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["TIPO"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        
        #endregion

        

       

    }
}