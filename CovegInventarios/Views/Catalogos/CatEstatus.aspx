﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CatEstatus.aspx.cs" Inherits="CovegInventarios.Views.Catalogos.CatEstatus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
  <title>Control de Inventario</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
    <ContentTemplate>
        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

        <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pnl1" TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
            BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" Enabled="True" />
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>

        <asp:Panel ID="pnl1" runat="server"  CssClass="shadowPanel">
            <table cellpadding="2" cellspacing="0" style="border:solid 1px #333333; background-color:#ffffff">
                <tr class="cpHeader">
                    <td>
                        <asp:Image ID="imgUpdate" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Detalles del Estatus" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdGuardar" Text="Guardar" Font-Bold="True" onclick="btnSave_Click" CssClass="boton" />&nbsp;
                        <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" Font-Bold="True" OnClick="btnEliminar_Click" CssClass="boton"/>&nbsp;
                        <asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            <tr>
                <td colspan="2" align="left">
                    <table>
                        <tr style="display:none">
                            <td>
                                <asp:Label ID="lblEstatusID" runat="server" Text="ID" 
                                    CssClass="Etiquetas"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox  ID="txtID" runat="server" Height="16px" 
                                    Width="300px" CssClass="textbox" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr >
                            <td><asp:Label ID="lblTipoEstatus" runat="server" Text="Tipo Estatus" 
                                    CssClass="Etiquetas"></asp:Label>&nbsp;
                                <asp:Label ID="lblRequerido" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido"></asp:Label>
                                </td>
                            <td>
                                <asp:DropDownList ID="cboTipoEstatus" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>
                                <asp:ListSearchExtender ID="cboTipoEstatus_ListSearchExtender" runat="server" 
                                    Enabled="True" PromptPosition="Bottom" PromptText="Escribir para buscar" 
                                    TargetControlID="cboTipoEstatus">
                                </asp:ListSearchExtender>
                            </td>
                        </tr>
                        <tr >
                            <td>
                                <asp:Label ID="lblNombre" runat="server" Text="Nombre" 
                                    CssClass="Etiquetas"></asp:Label>&nbsp;<asp:Label ID="lblRequerido0" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido"></asp:Label>
                                </td>
                            <td><asp:TextBox ID="txtNombre" runat="server" Height="16px" 
                                    Width="300px" CssClass="textbox" ></asp:TextBox></td>
                        </tr>
                        <tr >
                            <td>
                                <asp:Label ID="lblDescripcion" runat="server" Text="Descripción" 
                                    CssClass="Etiquetas"></asp:Label>&nbsp;<asp:Label ID="lblRequerido1" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido"></asp:Label>
                                </td>
                            <td><asp:TextBox  ID="txtDescripcion" runat="server" Height="30px" 
                                    Width="300px" CssClass="textbox" TextMode="MultiLine" ></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><br /></td>
                            <td><br /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </asp:Panel>

        <table>
        <tr>
            <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" 
                                    />
                            </td>
                            <td width="60%" align="left">
                                
                                &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" 
                                    Text="Catálogo de Estatus" Font-Bold="True" Font-Size="13px" 
                                     /></td>
                            <td align="right">
                                
                                <asp:Button runat="server" ID="Button1" Text="Nuevo"  
                                    Font-Bold="True"
                                    onclick="btnNew_Click" CssClass="boton" 
                                    />
                                &nbsp;<asp:Button runat="server" ID="Button2" Text="Refrescar"  
                                    Font-Bold="True" OnClick="btnRefresh_Click" 
                                     CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        <tr>
            <td class="panel6" valign="middle">
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lbl_ExpresionBusqueda" runat="server" 
                    Style="font-weight: 700" Text="Expresión de búsqueda" CssClass="Etiquetas" 
                    ></asp:Label>
                &nbsp;&nbsp;<asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search" 
                    AutoPostBack="True" CssClass="textbox" 
                    Width="170px" Height="16px" ></asp:TextBox>
                &nbsp;&nbsp;<asp:ImageButton ID="ImageButton3" runat="server" 
                    ImageUrl="~/Images/search_find.png" onclick="ImageButton3_Click" Width="24px" />
&nbsp;<asp:Label ID="lbl_TamanoPag" runat="server" CssClass="Etiquetas" 
                    Font-Bold="True" Text="Tamaño de página" 
                    ></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" Width="48px" 
                    Height="20px" CssClass="comboSeleccion" 
                    >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="30" Value="30" ></asp:ListItem>
                    <asp:ListItem Text="50" Value="50" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                    <asp:ListItem Text="300" Value="300" ></asp:ListItem>
                    <asp:ListItem Text="500" Value="500" ></asp:ListItem>
                    <asp:ListItem Text="---" Value="0" ></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="center">
            <br />
            <div class="grid">
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        Width="590px"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound" 
                    EmptyDataText="No se encontraron registros">
                        <Columns>
                            <asp:BoundField DataField="ESTATUSID" HeaderText="ID" 
                                SortExpression="ESTATUSID" Visible="false" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" 
                                SortExpression="NOMBRE" />
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCIÓN" 
                                SortExpression="DESCRIPCION" />
                            <asp:BoundField DataField="DESC_TIPOESTATUS" HeaderText="TIPO DE ESTATUS" 
                                SortExpression="DESC_TIPOESTATUS" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Editar">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                        <EmptyDataRowStyle Font-Bold="True" Font-Size="13px" ForeColor="Red" 
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>

                </td>
            </tr>
        </table>

        </ContentTemplate>
  </asp:UpdatePanel>  
</div>
    
</asp:Content>
