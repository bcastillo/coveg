﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;
using Coveg.Catalogos;
using F;

namespace CovegInventarios.Views.Catalogos
{
    public partial class CatRol : System.Web.UI.Page
    {

        #region Properties
        Coveg.Catalogos.Controller.USUARIOROLController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Catalogos.Controller.USUARIOROLController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Catalogos.Controller.USUARIOROLController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.USUARIOROLBE> lista
        {
            get
            {
                return (List<Coveg.Entities.USUARIOROLBE>)Page.Session["CatUsuarioROLCollection"];
            }
            set
            {
                Page.Session["CatUsuarioROLCollection"] = value;
            }
        }
        #endregion

        #region Initializers
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtUsuarioRolID.Attributes.Add("readonly", "readonly");

            try
            {
                if (!IsPostBack)
                {
                    Page.Session["SortDirection"] = "Ascending";
                    Page.Session["SortExpression"] = "";
                    Refresh(true);
                }
            }
            catch
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }



        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            if (Session["Recarga"] != null)
                if (Convert.ToBoolean(Session["Recarga"]))
                {
                    Session["Recarga"] = false;
                    mpEditarElemento.Show();
                }

            panelMessageBox.Visible = false;
        }

        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Información", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void ClearControls()
        {
            txtUsuarioRolID.Text = string.Empty;
            txtUsuario.Text = string.Empty;
            cboRol.SelectedIndex = -1;
            GridView1.SelectedIndex = -1;
        }

        /// <summary>
        ///
        /// </summary>
        public void SelectRecord()
        {
            if (GridView1.SelectedRow != null)
            {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;
                    if (selectedRow < lista.Count())
                    {
                        Coveg.Entities.USUARIOROLBE be = lista[selectedRow];
                        txtUsuarioRolID.Text = be.USUARIOROLID.ToString();
                        txtUsuario.Text = be.USUARIO;
                        cboRol.SelectedValue = be.CVE_ROL;
                    }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Refresh(bool requery)
        {
            if (requery || lista == null)
                lista = Controller.SelectAll(new USUARIOROLBE());

            selectedRow = -1;

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }
        /// <summary>
        ///
        /// </summary>
        public void Filtrar()
        {
            Refresh(true);
            if (GridView1.DataSource is List<USUARIOROLBE>)
            {
                string strValue = txtSearch.Text.ToLower();

                lista = lista.FindAll(p => 
                     F.Text.Input.IsNothing(p.USUARIO, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESC_ROL, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.CVE_ROL, "").ToString().ToLower().Contains(strValue)
                    );

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Delete()
        {
            if (selectedRow >= 0)
            {
                if (lista[selectedRow] != null)
                {
                    USUARIOROLBE objBE = new USUARIOROLBE();
                    objBE.USUARIOROLID = lista[selectedRow].USUARIOROLID;

                    if (Controller.Delete(objBE))
                    {
                        Refresh(true);
                        ClearControls();
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoEliminado"]);
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Save()
        {
            if (ValidateControls())
            {
                USUARIOROLBE be = new USUARIOROLBE();

                if (txtUsuarioRolID.Text != String.Empty)
                    be.USUARIOROLID = Convert.ToInt32(txtUsuarioRolID.Text);


                var tokens = Coveg.Common.Text.ObtenerTokens(txtUsuario.Text);

                if (tokens != null && tokens.Length > 0)
                    be.USUARIO = tokens[0];
                else
                    be.USUARIO = txtUsuario.Text;

                
                be.CVE_ROL = cboRol.SelectedValue;


                if (lista != null && lista.Find(entity => entity.USUARIOROLID == be.USUARIOROLID) != null)
                {
                    if (Controller.Update(be) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoActualizado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errAtualizarRegistro"]);
                    }
                    ClearControls();
                }
                else
                {

                    if (Controller.Insert(be) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoInsertado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errInsertarRegistro"]);
                    }
                    ClearControls();
                }
            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                mpEditarElemento.Show();
                Session["Recarga"] = true;
            }
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        public void LoadCollection(List<USUARIOROLBE> collection)
        {
            lista = collection;
            Refresh(false);
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            return (
            !string.IsNullOrEmpty(txtUsuario.Text) 
            && cboRol.SelectedIndex != -1
                );
        }
        #endregion

        #region Eventos

        /// <summary>
        /// Cancelar edición
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdCancelarPop_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Hide();
        }

        /// <summary>
        /// Mouse OVER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
            }
        }

        /// <summary>
        /// Nuevo elemento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Show();
            cmdEliminar.Visible = false;
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        /// <summary>
        /// Actulizar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearControls();
            Refresh(true);
        }

       
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (F.Text.Input.IsNumeric(ddlPageSize.SelectedValue))
            {
                int psize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (psize == 0)
                {
                    GridView1.AllowPaging = false;
                }
                else
                {
                    GridView1.AllowPaging = true;
                    GridView1.PageSize = psize;
                    GridView1.PageIndex = 0;
                }
                Refresh(true);
                ClearControls();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectRecord();
            mpEditarElemento.Show();
            cmdEliminar.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Filtrar();
        }
        #endregion

        #region Sorting Commands

        /// <summary>
        /// Ordenar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (lista == null || lista.Count == 0)
                return;

            if (e.SortExpression == "USUARIOROLID")
            {
                Page.Session["SortExpression"] = "USUARIOROLID";
                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                {
                    var v = lista.OrderBy(p => p.USUARIOROLID);
                    lista = v.ToList<USUARIOROLBE>();
                    Page.Session["SortDirection"] = "Descending";
                }
                else
                {
                    var v = lista.OrderByDescending(p => p.USUARIOROLID);
                    lista = v.ToList<USUARIOROLBE>();
                    Page.Session["SortDirection"] = "Ascending";
                }
            }
            else
                if (e.SortExpression == "USUARIO")
                {
                    Page.Session["SortExpression"] = "USUARIO";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.USUARIO);
                        lista = v.ToList<USUARIOROLBE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.USUARIO);
                        lista = v.ToList<USUARIOROLBE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "DESC_ROL")
                    {
                        Page.Session["SortExpression"] = "DESC_ROL";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.DESC_ROL);
                            lista = v.ToList<USUARIOROLBE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.DESC_ROL);
                            lista = v.ToList<USUARIOROLBE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "CVE_ROL")
                        {
                            Page.Session["SortExpression"] = "CVE_ROL";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.CVE_ROL);
                                lista = v.ToList<USUARIOROLBE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.CVE_ROL);
                                lista = v.ToList<USUARIOROLBE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
            ClearControls();
        }


        #endregion

        #region Search Methods
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerUsuarios(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("select usuario + ' | ' + nombre +' ' + apellidos  usuario from vwUsuario  WITH(NOLOCK) WHERE usuario LIKE '" + prefixText + "%' or nombre LIKE '" + prefixText  + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["usuario"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch { 
                
            }


            return res.ToArray();
        }
        #endregion

       

    }
}