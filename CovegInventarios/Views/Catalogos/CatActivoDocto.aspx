﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CatActivoDocto.aspx.cs" Inherits="CovegInventarios.Views.Catalogos.CatActivoDocto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
<title>Control de Inventario</title>
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<center>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
    <ContentTemplate>
        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

        <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pnl1" TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
            BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" Enabled="True" />
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>
        <asp:Panel ID="pnl1" runat="server" CssClass="shadowPanel" >        
        <table cellpadding="2" cellspacing="0" style="border:solid 1px #333333; background-color:#ffffff" >
                <tr class="cpHeader">
                    <td>
                        <asp:Image ID="imgUpdate" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Detalles de Documento" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdGuardar" Text="Guardar" Font-Bold="True" onclick="btnSave_Click" CssClass="boton" />&nbsp;
                        <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" Font-Bold="True" OnClick="btnEliminar_Click" CssClass="boton"/>&nbsp;
                        <asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                <td colspan="2" align="left">
                    <table>
                        <tr style="display:none">
                            
                            <td colspan="6">
                                <asp:TextBox ID="txtDocumentoID" runat="server" CssClass="textbox" 
                                    Height="16px"></asp:TextBox>
                            </td>
                            
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblClave" runat="server" CssClass="Etiquetas" Text="Clave"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido22" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave" runat="server" CssClass="textbox" Height="16px" 
                                    Width="100px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaIngreso" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha Ingreso"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido23" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaIngreso" runat="server" CssClass="textbox" 
                                    Height="16px" Width="80px"></asp:TextBox>
                                <asp:CalendarExtender ID="calFechaIngreso" runat="server" Format="dd/MM/yyyy" 
                                    PopupButtonID="imgFechaIngreso" TargetControlID="txtFechaIngreso" />
                                &nbsp;<asp:ImageButton ID="imgFechaIngreso" runat="server" 
                                    ImageUrl="~/Images/calendar.png" Width="24px" />
                            </td>
                            <td>
                                <asp:Label ID="lblEstatus" runat="server" CssClass="Etiquetas" Text="Estatus"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido2" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstatus" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>
                            </td>
                        </tr>                        
                        <tr>
                            <td>
                                <asp:Label ID="lblDescripcion" runat="server" CssClass="Etiquetas" 
                                    Text="Descripción"></asp:Label>
                                &nbsp;
                                <asp:Label ID="lblRequerido21" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="textbox" 
                                    Height="30px" TextMode="MultiLine" Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTipo" runat="server" CssClass="Etiquetas" 
                                    Text="Tipo documento"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblRequerido4" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtTipo" runat="server"  Height="16px" 
                                    CssClass="textbox" Width="120px" ></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblArea" runat="server" CssClass="Etiquetas" Text="Area"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido5" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtArea" runat="server" CssClass="textbox" Height="16px" 
                                    Width="120px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblEstante" runat="server" CssClass="Etiquetas" Text="Estante"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido19" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtEstante" runat="server" CssClass="textbox" Height="16px" 
                                    Width="120px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                    <asp:Label ID="lblCaja" runat="server" CssClass="Etiquetas" 
                                    Text="Caja"></asp:Label>
&nbsp;<asp:Label ID="lblRequerido20" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCaja" runat="server" CssClass="textbox" Height="16px" 
                                    Width="120px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblCarpeta" runat="server" CssClass="Etiquetas" 
                                    Text="Carpeta/Leffort"></asp:Label>
                                <asp:Label ID="lblRequerido7" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtCarpeta" runat="server" CssClass="textbox" Height="16px" 
                                    Width="120px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblRequiere" runat="server" CssClass="Etiquetas" 
                                    Text="Requiere Solicitud"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkRequereSolicitud" runat="server" ForeColor="Black" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </asp:Panel>
        <table>
        <tr>
            <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" 
                                    />
                            </td>
                            <td width="60%" align="left">
                                
                                &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" 
                                    Text="Catalogo de Documentos" Font-Bold="True" Font-Size="13px" 
                                     /></td>
                            <td align="right">
                                
                                <asp:Button runat="server" ID="Button1" Text="Nuevo"  
                                    Font-Bold="True"
                                    onclick="btnNew_Click" CssClass="boton" 
                                    />
                                &nbsp;<asp:Button runat="server" ID="Button2" Text="Refrescar"  
                                    Font-Bold="True" OnClick="btnRefresh_Click" 
                                     CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        <tr>
            <td class="panel6" valign="middle">
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lbl_ExpresionBusqueda" runat="server" 
                    Style="font-weight: 700" Text="Expresión de búsqueda" CssClass="Etiquetas" 
                    ></asp:Label>
                &nbsp;&nbsp;<asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search" 
                    AutoPostBack="True" CssClass="textbox" 
                    Width="170px" Height="16px" ></asp:TextBox>
                &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" 
                    ImageUrl="~/Images/search_find.png" onclick="ImageButton3_Click" Width="24px" />
&nbsp;
                <asp:Label ID="lbl_TamanoPag" runat="server" CssClass="Etiquetas" 
                    Font-Bold="True" Text="Tamaño de página" 
                    ></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" Width="48px" 
                    CssClass="comboSeleccion" Height="20px" 
                    >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="30" Value="30" ></asp:ListItem>
                    <asp:ListItem Text="50" Value="50" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                    <asp:ListItem Text="300" Value="300" ></asp:ListItem>
                    <asp:ListItem Text="500" Value="500" ></asp:ListItem>
                    <asp:ListItem Text="---" Value="0" ></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="center">
            <br />
            <div class="grid">
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        Width="590px"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound" 
                    EmptyDataText="No se encontraron registros">
                        <Columns>
                            <asp:BoundField DataField="DOCUMENTOID" HeaderText="ID" SortExpression="DOCUMENTOID" Visible="false" />
                            <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" SortExpression="CLAVE" />                            
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                            <asp:BoundField DataField="TIPO" HeaderText="TIPO" SortExpression="TIPO" />
                            <asp:BoundField DataField="AREA" HeaderText="AREA" SortExpression="AREA" />
                            <asp:BoundField DataField="ESTANTE" HeaderText="ESTANTE" SortExpression="ESTANTE" />
                            <asp:BoundField DataField="CAJA" HeaderText="CAJA" SortExpression="CAJA" />
                            <asp:BoundField DataField="LEFFORT" HeaderText="LEFFORT" SortExpression="LEFFORT" />
                            <asp:BoundField DataField="DESC_ESTATUS" HeaderText="ESTATUS" SortExpression="DESC_ESTATUS" />
                            <asp:BoundField DataField="FECHA_INGRESO" HeaderText="FECHA DE INGRESO" SortExpression="FECHA_INGRESO" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Editar">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                         <EmptyDataRowStyle Font-Bold="True" Font-Size="13px" ForeColor="Red" 
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
            </div>
                </td>
            </tr>
        </table>

        </ContentTemplate>
  </asp:UpdatePanel> 
</center>
</asp:Content>
