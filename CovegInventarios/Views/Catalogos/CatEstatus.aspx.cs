﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;
using Coveg.Catalogos;
using F;

namespace CovegInventarios.Views.Catalogos
{
    public partial class CatEstatus : System.Web.UI.Page
    {

        #region Properties
        Coveg.Catalogos.Controller.ESTATUSController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Catalogos.Controller.ESTATUSController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Catalogos.Controller.ESTATUSController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.ESTATUS_BE> lista
        {
            get
            {
                return (List<Coveg.Entities.ESTATUS_BE>)Page.Session["CatEstatusCollection"];
            }
            set
            {
                Page.Session["CatEstatusCollection"] = value;
            }
        }
        #endregion

        #region Initializers
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtID.Attributes.Add("readonly", "readonly");

            try
            {
                if (!IsPostBack)
                {
                    Page.Session["SortDirection"] = "Ascending";
                    Page.Session["SortExpression"] = "";
                    Refresh(true);
                    CargarCombos();
                }
            }
            catch 
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }



        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            if(Session["Recarga"]!=null)
                if (Convert.ToBoolean(Session["Recarga"]))
                {
                    Session["Recarga"] = false;
                    mpEditarElemento.Show();
                }

            panelMessageBox.Visible = false;
        }

        #endregion

        #region ICatalogoView<> Members

            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error","Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void ClearControls()
            {
                txtDescripcion.Text = string.Empty;
                txtID.Text = string.Empty;
                txtNombre.Text = string.Empty;
                cboTipoEstatus.SelectedIndex = -1;
                GridView1.SelectedIndex = -1;
            }

        /// <summary>
        ///
        /// </summary>
        public void SelectRecord()
        {
            if (GridView1.SelectedRow != null)
            {
                selectedRow = GridView1.SelectedRow.DataItemIndex;
                if (selectedRow < lista.Count())
                {
                    Coveg.Entities.ESTATUS_BE be = lista[selectedRow];
                    txtDescripcion.Text = be.DESCRIPCION;
                    txtID.Text = be.ESTATUSID.ToString();
                    txtNombre.Text = be.NOMBRE;
                    cboTipoEstatus.SelectedValue = be.TIPOESTATUSID.ToString();
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Refresh(bool requery)
        {
            if (requery || lista == null)
                lista = Controller.SelectAll(new ESTATUS_BE());

            selectedRow = -1;

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }
        /// <summary>
        ///
        /// </summary>
        public void Filtrar()
        {
            Refresh(true);
            if (GridView1.DataSource is List<ESTATUS_BE>)
            {
                string strValue = txtSearch.Text.ToLower();

                lista = lista.FindAll(p =>F.Text.Input.IsNothing(p.DESC_TIPOESTATUS, "").ToString().ToLower().Contains(strValue) 
                    || F.Text.Input.IsNothing(p.DESCRIPCION, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.NOMBRE, "").ToString().ToLower().Contains(strValue)
                    
                    );

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Delete()
        {
            if (selectedRow >= 0)
            {
                if (lista[selectedRow] != null)
                {
                    ESTATUS_BE objBE = new ESTATUS_BE();
                    objBE.ESTATUSID = lista[selectedRow].ESTATUSID;
                    if (Controller.Delete(objBE))
                    {
                        Refresh(true);
                        ClearControls();
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoEliminado"]);
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Save()
        {
            if (ValidateControls())
            {
                ESTATUS_BE be = new ESTATUS_BE();

                if(txtID.Text !=String.Empty)
                    be.ESTATUSID = Convert.ToInt32(txtID.Text);

                be.DESCRIPCION = txtDescripcion.Text;
                be.NOMBRE = txtNombre.Text;
                be.TIPOESTATUSID = Convert.ToInt32(cboTipoEstatus.SelectedValue);

                if (lista != null && lista.Find(entity => entity.ESTATUSID == be.ESTATUSID) != null)
                {
                    if (Controller.Update(be) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoActualizado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errAtualizarRegistro"]);
                    }
                    ClearControls();
                }
                else
                {

                    if (Controller.Insert(be) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoInsertado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errInsertarRegistro"]);
                    }
                    ClearControls();
                }
            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                mpEditarElemento.Show();
                Session["Recarga"] = true;
            }
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        public void LoadCollection(List<ESTATUS_BE> collection)
        {
            lista = collection;
            Refresh(false);
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            return (
            !string.IsNullOrEmpty(txtDescripcion.Text)
            &&!String.IsNullOrEmpty(txtNombre.Text)
            && cboTipoEstatus.SelectedIndex!=-1
                );
        }
        #endregion

        #region Eventos

        /// <summary>
        /// Cancelar edición
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdCancelarPop_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Hide();
        }

        /// <summary>
        /// Mouse OVER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
            }
        }

        /// <summary>
        /// Nuevo elemento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Show();
            cmdEliminar.Visible = false;
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        /// <summary>
        /// Actulizar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearControls();
            Refresh(true);
        }

        
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (F.Text.Input.IsNumeric(ddlPageSize.SelectedValue))
            {
                int psize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (psize == 0)
                {
                    GridView1.AllowPaging = false;
                }
                else
                {
                    GridView1.AllowPaging = true;
                    GridView1.PageSize = psize;
                    GridView1.PageIndex = 0;
                }
                Refresh(true);
                ClearControls();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectRecord();
            mpEditarElemento.Show();
            cmdEliminar.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Filtrar();
        }

        #endregion

        #region Sorting Commands

        /// <summary>
        /// Ordenar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (lista == null || lista.Count == 0)
                return;

            if (e.SortExpression == "ESTATUSID")
            {
                Page.Session["SortExpression"] = "ESTATUSID";
                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                {
                    var v = lista.OrderBy(p => p.ESTATUSID);
                    lista = v.ToList<ESTATUS_BE>();
                    Page.Session["SortDirection"] = "Descending";
                }
                else
                {
                    var v = lista.OrderByDescending(p => p.ESTATUSID);
                    lista = v.ToList<ESTATUS_BE>();
                    Page.Session["SortDirection"] = "Ascending";
                }
            }
            else
                if (e.SortExpression == "DESC_TIPOESTATUS")
                {
                    Page.Session["SortExpression"] = "DESC_TIPOESTATUS";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.DESC_TIPOESTATUS);
                        lista = v.ToList<ESTATUS_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.DESC_TIPOESTATUS);
                        lista = v.ToList<ESTATUS_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "DESCRIPCION")
                    {
                        Page.Session["SortExpression"] = "DESCRIPCION";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.DESCRIPCION);
                            lista = v.ToList<ESTATUS_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.DESCRIPCION);
                            lista = v.ToList<ESTATUS_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "NOMBRE")
                        {
                            Page.Session["SortExpression"] = "NOMBRE";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.NOMBRE);
                                lista = v.ToList<ESTATUS_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.NOMBRE);
                                lista = v.ToList<ESTATUS_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "TIPOESTATUSID")
                            {
                                Page.Session["SortExpression"] = "TIPOESTATUSID";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.TIPOESTATUSID);
                                    lista = v.ToList<ESTATUS_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.TIPOESTATUSID);
                                    lista = v.ToList<ESTATUS_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
            ClearControls();
        }


        #endregion

        #region Funciones

        /// <summary>
        /// Carga listado em combos
        /// </summary>
        private void CargarCombos()
        {
            List<TIPOESTATUS_BE> lstTipoEstatus = new List<TIPOESTATUS_BE>();
            lstTipoEstatus = Controller.ListTipoEstatus(new TIPOESTATUS_BE());

            cboTipoEstatus.DataSource = lstTipoEstatus;
            cboTipoEstatus.DataTextField = "NOMBRE";
            cboTipoEstatus.DataValueField = "TIPOESTATUSID";
            cboTipoEstatus.DataBind();
        }

        #endregion

        
    }
}