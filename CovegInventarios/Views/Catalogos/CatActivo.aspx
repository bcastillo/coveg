﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CatActivo.aspx.cs" Inherits="CovegInventarios.Views.Catalogos.CatActivo" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
    <ContentTemplate>
        <center>
        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>
        <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pnl1" TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
            BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" Enabled="True" />
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>
        <asp:Panel ID="pnl1" runat="server"  CssClass="shadowPanel">
            <table width="100%" cellpadding="2" cellspacing="0" style="border:solid 1px #333333; background-color:#ffffff">
                <tr class="cpHeader">
                    <td>
                        <asp:Image ID="imgUpdate" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Detalles de Activo" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="20px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdGuardar" Text="Guardar" Font-Bold="True" onclick="btnSave_Click" CssClass="boton" />&nbsp;
                        <asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" Font-Bold="True" OnClick="btnEliminar_Click" CssClass="boton"/>&nbsp;
                        <asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>                
                    <tr>
                        <td colspan="2" align="left">
                    <table>
                        <tr style="display:none">
                            <td>
                                &nbsp;</td>
                            <td colspan="3">
                                <asp:TextBox  ID="txtActivoID" runat="server" Height="20px" 
                                    CssClass="textbox" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblClave" runat="server" Text="Clave" 
                                    CssClass="Etiquetas"></asp:Label>&nbsp;<asp:Label ID="lblRequerido9" 
                                    runat="server" CssClass="etiquetaRequerido" ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtClave" runat="server" CssClass="textbox" Height="16px" 
                                    Width="80px"></asp:TextBox>
                                &nbsp;<asp:Label ID="lblRequiere" runat="server" CssClass="Etiquetas" 
                                    Text="Requiere Solicitud"></asp:Label>
                                &nbsp;<asp:CheckBox ID="chkRequereSolicitud" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblNumeroSerie" runat="server" CssClass="Etiquetas" 
                                    Text="Número de Serie"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNumeroSerie" runat="server" CssClass="textbox" 
                                    Height="16px" Width="200px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblTipoActivo" runat="server" CssClass="Etiquetas" 
                                    Text="Tipo Activo"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboTipoActivo" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>                                
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblNombre" runat="server" CssClass="Etiquetas" Text="Nombre"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido1" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtNombre" runat="server" CssClass="textbox" Height="16px" 
                                    Width="300px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="Usuario" runat="server" CssClass="Etiquetas" 
                                    Text="Responsable"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido8" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td valign="middle">
                                <asp:TextBox ID="txtNumNomina" runat="server" CssClass="textbox" 
                                    Height="16px" Width="200px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="txtNumNomina_AutoCompleteExtender" runat="server" 
                                    CompletionInterval="100" 
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="20" 
                                    DelimiterCharacters=";, :" Enabled="True" MinimumPrefixLength="3" 
                                    ServiceMethod="ObtenerResponsables" 
                                    ShowOnlyCurrentWordInCompletionListItem="True" TargetControlID="txtNumNomina" 
                                    UseContextKey="True">
                                </asp:AutoCompleteExtender>
                            </td>
                            <td>
                                <asp:Label ID="lblUnidad" runat="server" CssClass="Etiquetas" Text="Unidad"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido5" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboUnidad" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>
                                <asp:ListSearchExtender ID="cboUnidad_ListSearchExtender" runat="server" 
                                    Enabled="True" PromptPosition="Bottom" PromptText="Escribir para buscar" 
                                    TargetControlID="cboUnidad">
                                </asp:ListSearchExtender>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td>                                
                                <asp:Label ID="lblDescripcion0" runat="server" CssClass="Etiquetas" 
                                    Text="Descripción"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido4" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcion" runat="server" CssClass="textbox" Height="30px" 
                                    Width="98%" TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="lblEstatus" runat="server" CssClass="Etiquetas" Text="Estatus"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido2" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="cboEstatus" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>                                
                            </td>
                        </tr>
                    </table>
                </td>
                    </tr>
                </table>
        </asp:Panel>
        <table>
        <tr>
            <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" 
                                    />
                            </td>
                            <td width="60%" align="left">
                                
                                &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" 
                                    Text="Catalogo de Activos" Font-Bold="True" Font-Size="13px" 
                                     /></td>
                            <td align="right">
                                
                                <asp:Button runat="server" ID="Button1" Text="Nuevo"  
                                    Font-Bold="True"
                                    onclick="btnNew_Click" CssClass="boton" 
                                    />
                                &nbsp;<asp:Button runat="server" ID="Button2" Text="Refrescar"  
                                    Font-Bold="True" OnClick="btnRefresh_Click" 
                                     CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>

        <tr>
            <td class="panel6" valign="middle">
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="lbl_ExpresionBusqueda" runat="server" 
                    Style="font-weight: 700" Text="Expresión de búsqueda" CssClass="Etiquetas" 
                    ></asp:Label>
                &nbsp;&nbsp;<asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search" 
                    AutoPostBack="True" CssClass="textbox" 
                    Width="170px" Height="16px" ></asp:TextBox>
                &nbsp;<asp:ImageButton ID="ImageButton3" runat="server" 
                    ImageUrl="~/Images/search_find.png" onclick="ImageButton3_Click" Width="24px" />
&nbsp;
                <asp:Label ID="lbl_TamanoPag" runat="server" CssClass="Etiquetas" 
                    Font-Bold="True" Text="Tamaño de página" 
                    ></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" Width="48px" 
                    CssClass="comboSeleccion" Height="20px" 
                    >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="30" Value="30" ></asp:ListItem>
                    <asp:ListItem Text="50" Value="50" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                    <asp:ListItem Text="300" Value="300" ></asp:ListItem>
                    <asp:ListItem Text="500" Value="500" ></asp:ListItem>
                    <asp:ListItem Text="---" Value="0" ></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>

        <tr>
            <td align="center">
            <br />
            <div class="grid" style="width:1000px; overflow:scroll;">
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        Width="965px"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound"
                        >
                        <Columns>                            
                            <asp:BoundField DataField="ACTIVOID" HeaderText="ID" SortExpression="ACTIVOID" />
                            <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" SortExpression="CLAVE" />
                            <asp:BoundField DataField="NUMEROSERIE" HeaderText="SERIE" SortExpression="NUMEROSERIE" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE" />
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                            <asp:BoundField DataField="DESC_TIPOACTIVO" HeaderText="TIPO DE ACTIVO" SortExpression="DESC_TIPOACTIVO" />
                            <asp:BoundField DataField="NUMNOMINA" HeaderText="RESGUARDANTE" SortExpression="NUMNOMINA" />
                            <asp:BoundField DataField="DESC_UNIDAD" HeaderText="UNIDAD" SortExpression="DESC_UNIDAD" />                            
                            <asp:BoundField DataField="DESC_ESTATUS" HeaderText="ESTATUS" SortExpression="ESTATUS" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Editar">
                                <ControlStyle CssClass="boton" />
                                <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                         <EmptyDataRowStyle Font-Bold="True" Font-Size="13px" ForeColor="Red" 
                            HorizontalAlign="Center" VerticalAlign="Middle" />
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
                </td>
            </tr>

        </table>
        </center>
        </ContentTemplate>
  </asp:UpdatePanel> 
</asp:Content>
