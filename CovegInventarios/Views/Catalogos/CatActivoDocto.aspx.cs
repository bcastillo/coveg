﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;
using Coveg.Catalogos;
using F;

namespace CovegInventarios.Views.Catalogos
{
    public partial class CatActivoDocto : System.Web.UI.Page
    {
        #region Properties

        Coveg.Catalogos.Controller.DOCUMENTOController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Catalogos.Controller.DOCUMENTOController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Catalogos.Controller.DOCUMENTOController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.DOCUMENTO_BE> lista
        {
            get
            {
                return (List<Coveg.Entities.DOCUMENTO_BE>)Page.Session["CATDOCUMENTOCollection"];
            }
            set
            {
                Page.Session["CATDOCUMENTOCollection"] = value;
            }
        }

        #endregion

        #region Initializers
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtDocumentoID.Attributes.Add("readonly", "readonly");

            try
            {
                if (!IsPostBack)
                {
                    Page.Session["SortDirection"] = "Ascending";
                    Page.Session["SortExpression"] = "";
                    Refresh(true);
                    CargarCombos();
                }
            }
            catch 
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }



        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            if (Session["Recarga"] != null)
                if (Convert.ToBoolean(Session["Recarga"]))
                {
                    Session["Recarga"] = false;
                    mpEditarElemento.Show();
                }

            panelMessageBox.Visible = false;
        }

        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Información", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void ClearControls()
        {
            txtArea.Text = String.Empty;
            txtCaja.Text = String.Empty;
            txtCarpeta.Text = String.Empty;
            txtDocumentoID.Text = String.Empty;
            txtEstante.Text = String.Empty;
            txtTipo.Text = String.Empty;
            txtFechaIngreso.Text = String.Empty;
            txtDescripcion.Text = String.Empty;
            txtClave.Text = String.Empty;
            

            cboEstatus.SelectedIndex = -1;
            chkRequereSolicitud.Checked = false;
            Session["ACTIVOID"] = 0;
            GridView1.SelectedIndex = -1;
        }

        /// <summary>
        ///
        /// </summary>
        public void SelectRecord()
        {
            if (GridView1.SelectedRow != null)
            {
                selectedRow = GridView1.SelectedRow.DataItemIndex;
                if (selectedRow < lista.Count())
                {
                    Coveg.Entities.DOCUMENTO_BE be = lista[selectedRow];
                    Session["ACTIVOID"] = be.ACTIVOID;
                    txtArea.Text = be.AREA;
                    txtCaja.Text = be.CAJA;
                    txtCarpeta.Text = be.LEFFORT;
                    txtDocumentoID.Text = be.DOCUMENTOID.ToString();
                    txtEstante.Text = be.ESTANTE;
                    txtTipo.Text = be.TIPO;
                    txtFechaIngreso.Text = be.FECHA_INGRESO.ToShortDateString();                    
                    txtClave.Text = be.CLAVE;
                    txtDescripcion.Text = be.DESCRIPCION;
                    cboEstatus.SelectedValue = be.ESTATUS.ToString();
                    chkRequereSolicitud.Checked = be.REQUIERE_SOLICITUD;
                }
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Refresh(bool requery)
        {
            if (requery || lista == null)
            {
                lista = Controller.SelectAll(new DOCUMENTO_BE());
            }

            selectedRow = -1;

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }
        /// <summary>
        ///
        /// </summary>
        public void Filtrar()
        {
            Refresh(true);
            if (GridView1.DataSource is List<DOCUMENTO_BE>)
            {
                string strValue = txtSearch.Text.ToLower();

                lista = lista.FindAll(p => 
                    F.Text.Input.IsNothing(p.AREA, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.CAJA, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.CLAVE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESCRIPCION, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DOCUMENTOID, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.ESTANTE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.ESTATUS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.FECHA_INGRESO, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.LEFFORT, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.TIPO, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.REQUIERE_SOLICITUD, "").ToString().ToLower().Contains(strValue)
                    );

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Delete()
        {
            if (selectedRow >= 0)
            {
                if (lista[selectedRow] != null)
                {
                    DOCUMENTO_BE objBE = new DOCUMENTO_BE();
                    objBE.ACTIVOID = lista[selectedRow].ACTIVOID;
                    objBE.DOCUMENTOID = lista[selectedRow].DOCUMENTOID;
                    if (Controller.Delete(objBE))
                    {
                        Refresh(true);
                        ClearControls();
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoEliminado"]);
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Save()
        {
            if (ValidateControls())
            {
                DOCUMENTO_BE be = new DOCUMENTO_BE();
                ACTIVO_BE beActivo = new ACTIVO_BE();

                if (txtDocumentoID.Text != String.Empty)
                {
                    be.DOCUMENTOID = Convert.ToInt32(txtDocumentoID.Text);
                    be.ACTIVOID = Convert.ToInt32(Session["ACTIVOID"]);
                    beActivo.ACTIVOID = Convert.ToInt32(Session["ACTIVOID"]);
                }

                ///Valores de tabla de documento
                be.AREA = txtArea.Text;
                be.CAJA = txtCaja.Text;
                be.ESTANTE = txtEstante.Text;
                be.ESTATUS = Convert.ToInt32(cboEstatus.SelectedValue);
                be.FECHA_INGRESO = Convert.ToDateTime(txtFechaIngreso.Text);
                be.LEFFORT = txtCarpeta.Text;
                be.REQUIERE_SOLICITUD = chkRequereSolicitud.Checked;
                be.TIPO = txtTipo.Text;
                
                

                if (lista != null && lista.Find(entity => entity.ACTIVOID == be.ACTIVOID) != null)
                {
                    ///Valores de tabla activo
                    beActivo = Controller.GetActivo(lista[selectedRow].CLAVE);
                    //Obtener primero los valores del activo actual y luego planchar los nuevos
                    beActivo.DESCRIPCION = txtDescripcion.Text;
                    beActivo.ESTATUS = Convert.ToInt32(cboEstatus.SelectedValue);
                    beActivo.REQUIERE_SOLICITUD = chkRequereSolicitud.Checked;
                    beActivo.CLAVE = txtClave.Text;

                
                    if (Controller.Update(be, beActivo))
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoActualizado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errAtualizarRegistro"]);
                    }
                    ClearControls();
                }
                else
                {
                    beActivo.DESCRIPCION = txtDescripcion.Text;
                    beActivo.ESTATUS = Convert.ToInt32(cboEstatus.SelectedValue);
                    beActivo.REQUIERE_SOLICITUD = chkRequereSolicitud.Checked;
                    beActivo.CLAVE = txtClave.Text;

                

                    if (Controller.Insert(be, beActivo) != null)
                    {
                        Refresh(true);
                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoInsertado"]);
                    }
                    else
                    {
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errInsertarRegistro"]);
                    }
                    ClearControls();
                }
            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                mpEditarElemento.Show();
                Session["Recarga"] = true;
            }
        }

        
        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        public void LoadCollection(List<DOCUMENTO_BE> collection)
        {
            lista = collection;
            Refresh(false);
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            return (
                cboEstatus.SelectedIndex != -1
                && !String.IsNullOrEmpty(txtArea.Text)
                && !String.IsNullOrEmpty(txtCaja.Text)
                && !String.IsNullOrEmpty(txtCarpeta.Text)
                && !String.IsNullOrEmpty(txtClave.Text)
                && !String.IsNullOrEmpty(txtDescripcion.Text)
                && !String.IsNullOrEmpty(txtEstante.Text)
                && !String.IsNullOrEmpty(txtFechaIngreso.Text)
                && !String.IsNullOrEmpty(txtTipo.Text)
                && cboEstatus.SelectedIndex != -1
                );
        }
        #endregion

        #region Eventos
        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Filtrar();
        }
        /// <summary>
        /// Cancelar edición
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdCancelarPop_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Hide();
        }

        /// <summary>
        /// Mouse OVER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                if (Convert.ToDateTime(e.Row.Cells[9].Text) == new DateTime())
                {
                    e.Row.Cells[9].Text = "--";
                }
                else
                {
                    e.Row.Cells[9].Text = Convert.ToDateTime(e.Row.Cells[9].Text).ToShortDateString();
                }

            }
        }

        /// <summary>
        /// Nuevo elemento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Show();
            cmdEliminar.Visible = false;
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        /// <summary>
        /// Actulizar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearControls();
            Refresh(true);
        }

       

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (F.Text.Input.IsNumeric(ddlPageSize.SelectedValue))
            {
                int psize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (psize == 0)
                {
                    GridView1.AllowPaging = false;
                }
                else
                {
                    GridView1.AllowPaging = true;
                    GridView1.PageSize = psize;
                    GridView1.PageIndex = 0;
                }
                Refresh(true);
                ClearControls();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectRecord();
            mpEditarElemento.Show();
            cmdEliminar.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }


        #endregion

        #region Sorting Commands

        /// <summary>
        /// Ordenar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (lista == null || lista.Count == 0)
                return;

            if (e.SortExpression == "ACTIVOID")
            {
                Page.Session["SortExpression"] = "ACTIVOID";
                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                {
                    var v = lista.OrderBy(p => p.ACTIVOID);
                    lista = v.ToList<DOCUMENTO_BE>();
                    Page.Session["SortDirection"] = "Descending";
                }
                else
                {
                    var v = lista.OrderByDescending(p => p.ACTIVOID);
                    lista = v.ToList<DOCUMENTO_BE>();
                    Page.Session["SortDirection"] = "Ascending";
                }
            }
            else
                if (e.SortExpression == "AREA")
                {
                    Page.Session["SortExpression"] = "AREA";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.AREA);
                        lista = v.ToList<DOCUMENTO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.AREA);
                        lista = v.ToList<DOCUMENTO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "CAJA")
                    {
                        Page.Session["SortExpression"] = "CAJA";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.CAJA);
                            lista = v.ToList<DOCUMENTO_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.CAJA);
                            lista = v.ToList<DOCUMENTO_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "CLAVE")
                        {
                            Page.Session["SortExpression"] = "CLAVE";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.CLAVE);
                                lista = v.ToList<DOCUMENTO_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.CLAVE);
                                lista = v.ToList<DOCUMENTO_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DESCRIPCION")
                            {
                                Page.Session["SortExpression"] = "DESCRIPCION";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.DESCRIPCION);
                                    lista = v.ToList<DOCUMENTO_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.DESCRIPCION);
                                    lista = v.ToList<DOCUMENTO_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "DOCUMENTOID")
                                {
                                    Page.Session["SortExpression"] = "DOCUMENTOID";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.DOCUMENTOID);
                                        lista = v.ToList<DOCUMENTO_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.DOCUMENTOID);
                                        lista = v.ToList<DOCUMENTO_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "ESTANTE")
                                    {
                                        Page.Session["SortExpression"] = "ESTANTE";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.ESTANTE);
                                            lista = v.ToList<DOCUMENTO_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.ESTANTE);
                                            lista = v.ToList<DOCUMENTO_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "ESTATUS")
                                        {
                                            Page.Session["SortExpression"] = "ESTATUS";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.ESTATUS);
                                                lista = v.ToList<DOCUMENTO_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.ESTATUS);
                                                lista = v.ToList<DOCUMENTO_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }
                                        else
                                            if (e.SortExpression == "FECHA_INGRESO")
                                            {
                                                Page.Session["SortExpression"] = "FECHA_INGRESO";
                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                {
                                                    var v = lista.OrderBy(p => p.FECHA_INGRESO);
                                                    lista = v.ToList<DOCUMENTO_BE>();
                                                    Page.Session["SortDirection"] = "Descending";
                                                }
                                                else
                                                {
                                                    var v = lista.OrderByDescending(p => p.FECHA_INGRESO);
                                                    lista = v.ToList<DOCUMENTO_BE>();
                                                    Page.Session["SortDirection"] = "Ascending";
                                                }
                                            }
                                            else
                                                if (e.SortExpression == "LEFFORT")
                                                {
                                                    Page.Session["SortExpression"] = "LEFFORT";
                                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                    {
                                                        var v = lista.OrderBy(p => p.LEFFORT);
                                                        lista = v.ToList<DOCUMENTO_BE>();
                                                        Page.Session["SortDirection"] = "Descending";
                                                    }
                                                    else
                                                    {
                                                        var v = lista.OrderByDescending(p => p.LEFFORT);
                                                        lista = v.ToList<DOCUMENTO_BE>();
                                                        Page.Session["SortDirection"] = "Ascending";
                                                    }
                                                }
                                                else
                                                    if (e.SortExpression == "REQUIERE_SOLICITUD")
                                                    {
                                                        Page.Session["SortExpression"] = "REQUIERE_SOLICITUD";
                                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                        {
                                                            var v = lista.OrderBy(p => p.REQUIERE_SOLICITUD);
                                                            lista = v.ToList<DOCUMENTO_BE>();
                                                            Page.Session["SortDirection"] = "Descending";
                                                        }
                                                        else
                                                        {
                                                            var v = lista.OrderByDescending(p => p.REQUIERE_SOLICITUD);
                                                            lista = v.ToList<DOCUMENTO_BE>();
                                                            Page.Session["SortDirection"] = "Ascending";
                                                        }
                                                    }
                                                    else
                                                        if (e.SortExpression == "TIPO")
                                                        {
                                                            Page.Session["SortExpression"] = "TIPO";
                                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                            {
                                                                var v = lista.OrderBy(p => p.TIPO);
                                                                lista = v.ToList<DOCUMENTO_BE>();
                                                                Page.Session["SortDirection"] = "Descending";
                                                            }
                                                            else
                                                            {
                                                                var v = lista.OrderByDescending(p => p.TIPO);
                                                                lista = v.ToList<DOCUMENTO_BE>();
                                                                Page.Session["SortDirection"] = "Ascending";
                                                            }
                                                        }

                                                        else
                                                            if (e.SortExpression == "DESC_ESTATUS")
                                                            {
                                                                Page.Session["SortExpression"] = "DESC_ESTATUS";
                                                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                                                {
                                                                    var v = lista.OrderBy(p => p.DESC_ESTATUS);
                                                                    lista = v.ToList<DOCUMENTO_BE>();
                                                                    Page.Session["SortDirection"] = "Descending";
                                                                }
                                                                else
                                                                {
                                                                    var v = lista.OrderByDescending(p => p.DESC_ESTATUS);
                                                                    lista = v.ToList<DOCUMENTO_BE>();
                                                                    Page.Session["SortDirection"] = "Ascending";
                                                                }
                                                            }

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
            ClearControls();
        }


        #endregion

        #region Funciones

        /// <summary>
        /// Carga listado em combos
        /// </summary>
        private void CargarCombos()
        {
            List<ESTATUS_BE> lstEstatus = new List<ESTATUS_BE>();

            lstEstatus = Controller.ListaEstatusTipoEstatus(Coveg.Common.Constantes.TE_ACTIVO);

            cboEstatus.DataSource = lstEstatus;
            cboEstatus.DataTextField = "NOMBRE";
            cboEstatus.DataValueField = "ESTATUSID";
            cboEstatus.DataBind();
        }

        #endregion

        
 
    }
}