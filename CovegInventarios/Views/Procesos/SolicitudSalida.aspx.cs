﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views.Procesos
{
    public partial class SolicitudSalida : System.Web.UI.Page
    {

        #region Properties
        Coveg.Procesos.Controller.SOLICITUDSALIDAController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Procesos.Controller.SOLICITUDSALIDAController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Procesos.Controller.SOLICITUDSALIDAController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }



        public ACTIVO_BUSQUEDA ActivoSeleccionadoBE
        {
            get
            {
                return (ACTIVO_BUSQUEDA)Page.Session["ActivoSeleccionadoBE"];
            }
            set
            {
                Page.Session["ActivoSeleccionadoBE"] = value;
            }
        }

        public String ResponsableSeleccionado
        {
            get
            {
                return (String)Page.Session["ResponsableSeleccionado"];
            }
            set
            {
                Page.Session["ResponsableSeleccionado"] = value;
            }
        }

        

        private List<Coveg.Entities.ACTIVO_BUSQUEDA> listaActivos
        {
            get
            {
                return (List<Coveg.Entities.ACTIVO_BUSQUEDA>)Page.Session["ActivosSolicitadosCollection"];
            }
            set
            {
                Page.Session["ActivosSolicitadosCollection"] = value;
            }
        }

        public int NoElementosLista
        {
            get {
                if (listaActivos == null)
                    return 0;
                else
                    return listaActivos.Count(); 
            }
        }

        public AjaxControlToolkit.ModalPopupExtender objExtender
        {
            get { return this.mpEditComment; }
        }


        #endregion

        #region Eventos

            /// <summary>
        /// Mouse OVER
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
            }
        }

            protected void Page_Load(object sender, EventArgs e)
            {
                

                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);

                txtFecha.Attributes.Add("readonly", "readonly");
                txtFechaSalida.Attributes.Add("readonly", "readonly");
                txtFechaRetorno.Attributes.Add("readonly", "readonly");

                
                txtUsuario.Text = Session["USUARIO"].ToString();
                

                try
                {
                    txtFecha.Text = System.DateTime.Now.ToString();

                    if (!IsPostBack)
                    {
                        listaActivos = null;
                        Page.Session["SortDirection"] = "Ascending";
                        Page.Session["SortExpression"] = "";
                        CargarActivos();


                       
                    }
                    

                    
                }
                catch 
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

            /// <summary>
            /// Actualizar lista 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void btnRefresh_Click(object sender, EventArgs e)
            {
                CargarGrid();
            }


            /// <summary>
            ///
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
            {
                SelectRecord();
            }

            /// <summary>
            ///
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                GridView1.PageIndex = e.NewPageIndex;
                GridView1.DataSource = listaActivos;
                GridView1.DataBind();
            }

            /// <summary>
            /// Ordenar grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (listaActivos == null || listaActivos.Count == 0)
                    return;

                if (e.SortExpression == "DESCRIPCION")
                {
                    Page.Session["SortExpression"] = "DESCRIPCION";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = listaActivos.OrderBy(p => p.DESCRIPCION);
                        listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = listaActivos.OrderByDescending(p => p.DESCRIPCION);
                        listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "CLAVE")
                    {
                        Page.Session["SortExpression"] = "CLAVE";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = listaActivos.OrderBy(p => p.CLAVE);
                            listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = listaActivos.OrderByDescending(p => p.CLAVE);
                            listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "RESPONSABLE")
                        {
                            Page.Session["SortExpression"] = "RESPONSABLE";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = listaActivos.OrderBy(p => p.RESPONSABLE);
                                listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = listaActivos.OrderByDescending(p => p.RESPONSABLE);
                                listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DESC_RFIDTAG")
                            {
                                Page.Session["SortExpression"] = "DESC_RFIDTAG";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = listaActivos.OrderBy(p => p.DESC_RFIDTAG);
                                    listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = listaActivos.OrderByDescending(p => p.DESC_RFIDTAG);
                                    listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "DESC_TIPOACTIVO")
                                {
                                    Page.Session["SortExpression"] = "DESC_TIPOACTIVO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = listaActivos.OrderBy(p => p.DESC_TIPOACTIVO);
                                        listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = listaActivos.OrderByDescending(p => p.DESC_TIPOACTIVO);
                                        listaActivos = v.ToList<ACTIVO_BUSQUEDA>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }

                GridView1.SelectedIndex = -1;
                GridView1.DataSource = listaActivos;
                GridView1.DataBind();
                ClearControls();

            }


            /// <summary>
            /// Guardar solicitud
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void btnSave_Click(object sender, EventArgs e)
            {
                List<SOLICITUDSALIDA_BE> lstSolicitud = new List<SOLICITUDSALIDA_BE>();
                SOLICITUDSALIDA_BE objSolicitud = new SOLICITUDSALIDA_BE();
                try
                {
                    if (ValidateControls())
                    {
                        if (listaActivos != null && listaActivos.Count > 0)
                        {
                            
                            objSolicitud.FECHA = System.DateTime.Now;
                            DateTime fecha;

                            if (DateTime.TryParse(txtFechaSalida.Text, out fecha))
                                objSolicitud.FECHASALIDA = Convert.ToDateTime(txtFechaSalida.Text);

                            if (DateTime.TryParse(txtFechaRetorno.Text, out fecha))
                                objSolicitud.FECHARETORNO = Convert.ToDateTime(txtFechaRetorno.Text);

                            if (

                                Coveg.Common.Date.DateWithoutTime(objSolicitud.FECHASALIDA) >= Coveg.Common.Date.DateWithoutTime(DateTime.Now)
                                && Coveg.Common.Date.DateWithoutTime(objSolicitud.FECHARETORNO) >= Coveg.Common.Date.DateWithoutTime(DateTime.Now)
                                && objSolicitud.FECHARETORNO >= objSolicitud.FECHASALIDA
                                
                            )
                            {
                                objSolicitud.MOTIVO = txtMotivo.Text;
                                objSolicitud.USUARIO = txtUsuario.Text;
                                objSolicitud.IDJEFEINMEDIATO = Convert.ToInt32(Session["IDJEFE"]);

                                if (Request.QueryString["Tipo"] == "Documento")
                                    objSolicitud.ESTATUS = Coveg.Common.Constantes.EST_SOL_DOCUMENTO;
                                else
                                    objSolicitud.ESTATUS = Coveg.Common.Constantes.EST_SOL_OTRO;

                                lstSolicitud = Controller.Insert(objSolicitud);
                                if (lstSolicitud != null)
                                {
                                    foreach (ACTIVO_BUSQUEDA objElemento in listaActivos)
                                    {
                                        Controller.InsertActivosSolicitados(objElemento, lstSolicitud[0].SOLICITUDSALIDAID);
                                    }
                                    NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgSolicitudEnviada"]);

                                    ResponsableSeleccionado = String.Empty;

                                    Controller.NotificarAutorizadores(lstSolicitud[0].SOLICITUDSALIDAID);

                                    ClearScreen();

                                }
                            }
                            else
                            {
                                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFechasSolicitudInvalidas"]);
                            }
                        }
                        else
                        {
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaAgregarActivosSolicitados"]);
                        }
                    }
                    else
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                }
                catch (Exception ex)
                {
                    RaiseUserError(ex.Message);
                }   
            }




        #endregion

        #region Funciones
            /// <summary>
            /// 
            /// </summary>
            private void AgregarActivoASolicitud()
            {
                ACTIVO_BUSQUEDA objNuevo = new ACTIVO_BUSQUEDA();

                if (ActivoSeleccionadoBE != null)
                {
                    if (ValidaActivoAgregado())
                    {
                        if (listaActivos == null)
                            listaActivos = new List<ACTIVO_BUSQUEDA>();

                        listaActivos.Add(ActivoSeleccionadoBE);

                        CargarGrid();
                        

                        ActivoSeleccionadoBE = null;

                    }
                    else
                        RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errElementoExiste"]);
                }
                else
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errSeleccioneActivo"]);
            }
            /// <summary>
            /// Recibe elemento seleccionado
            /// </summary>
            /// <param name="objActivo"></param>
            public void AsignarActivoGenerico(ACTIVO_BUSQUEDA objActivo)
            {
                if (objActivo != null)
                {
                    DetalleActivo1.Visible = true;
                    DetalleActivo1.Inicializar(objActivo);
                }
                
                ResponsableSeleccionado = objActivo.RESPONSABLE;

                ActivoSeleccionadoBE = objActivo;
                //bcd agrega en automatico un activo que se eligio
                AgregarActivoASolicitud();
            }
            
            
            

            
            
            /// <summary>
            /// Valida si el activo seleccionado es del mismo tipo que los anteriores
            /// </summary>
            /// <param name="intTipoActivo"></param>
            /// <returns></returns>
            public Boolean ValidaSolicitudResponsable(String strResponsable)
            {
                if (strResponsable == ResponsableSeleccionado)
                    return true;
                else
                {
                    NotifyUser(System.Configuration.ConfigurationManager.AppSettings["errMismoResponsable"]);
                    return false;
                }
            }


            public void BusquedaActivos_Cancelled(object sender, EventArgs e)
            {
                pnl1.Visible = false;
            }

            /// <summary>
            ///
            /// </summary>
            /// <returns></returns>
            public bool ValidateControls()
            {
                return (
                    !string.IsNullOrEmpty(txtUsuario.Text) &&
                !string.IsNullOrEmpty(txtMotivo.Text)
                
                    );
            }


            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error", "Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            /// Actualiza grid
            /// </summary>
            public void CargarGrid()
            {
                GridView1.DataSource = listaActivos;
                GridView1.DataBind();
                ClearControls();
            }

            /// <summary>
            /// Borrar elemento de la lista
            /// </summary>
            public void Delete()
            {
                if (selectedRow >= 0)
                {
                    if (listaActivos[selectedRow] != null)
                    {
                        listaActivos.RemoveAt(selectedRow);
                        ActivoSeleccionadoBE = null;
                        DetalleActivo1.Visible = false;
                    }
                }
            }

            private void CargarActivos()
            {
                List<ACTIVO_BUSQUEDA> lstActivos = new List<ACTIVO_BUSQUEDA>();
                ACTIVO_BE objActivo = new ACTIVO_BE();
                objActivo.REQUIERE_SOLICITUD = true;

                if (Request.QueryString["Tipo"] == "Activo")
                    lstActivos = Controller.ListActivosSolicitud(objActivo);
                if (Request.QueryString["Tipo"] == "Documento")
                    lstActivos = Controller.ListActivosSolicitudDoctos(objActivo);
                if (Request.QueryString["Tipo"] == "Automovil")
                    lstActivos = Controller.ListActivosSolicitudVehiculos(objActivo);
                
                BusquedaActivos1.Initilize(lstActivos);

            }

            /// <summary>
            ///
            /// </summary>
            public void SelectRecord()
            {
                if (GridView1.Rows.Count>0 && GridView1.SelectedRow != null)
                {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;

                    DetalleActivo1.Visible = true;
                    DetalleActivo1.Inicializar(listaActivos[selectedRow]);
                    ResponsableSeleccionado = listaActivos[selectedRow].RESPONSABLE;
                    ActivoSeleccionadoBE = listaActivos[selectedRow];

                    
                }
            }


            private void ClearScreen()
            {

                txtFechaSalida.Text = string.Empty;
                txtFechaRetorno.Text = string.Empty;
                txtMotivo.Text = string.Empty;
                selectedRow =- 1;

                ActivoSeleccionadoBE = null;
                ResponsableSeleccionado = String.Empty;
                listaActivos = null;

                GridView1.DataSource = listaActivos;
                GridView1.DataBind();

                ClearControls();
             
            }

            /// <summary>
            ///
            /// </summary>
            public void ClearControls()
            {
                GridView1.SelectedIndex = -1;
                selectedRow = -1;
            }          

            /// <summary>
            /// Valida si el activo que se desea agregar no ha sido agregado a la lista actual
            /// </summary>
            /// <returns></returns>
            private Boolean ValidaActivoAgregado()
            {
                if (listaActivos == null)
                    return true;
                else
                    if (ActivoSeleccionadoBE == null)
                        return true;
                    else
                        if(listaActivos.Find(p=>p.ACTIVOID == ActivoSeleccionadoBE.ACTIVOID) == null)
                            return true;
                        else
                            return false;

            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
            {
                if (e.CommandName.ToLower().Trim().Equals("delete"))
                {
                    if (F.Text.Input.IsNumeric(e.CommandArgument))
                    {
                        selectedRow = Convert.ToInt32(e.CommandArgument);
                        Delete();
                       
                    }
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
            {
                CargarGrid();
            }
        #endregion


    }
}