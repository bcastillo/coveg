﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views.Procesos
{
    public partial class InventarioProgramado : System.Web.UI.Page
    {
        #region Properties

        Coveg.Procesos.Controller.INVENTARIOPROGRAMADOController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Procesos.Controller.INVENTARIOPROGRAMADOController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Procesos.Controller.INVENTARIOPROGRAMADOController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.INVENTARIOPROGRAMADO_BE> lista
        {
            get
            {
                return (List<Coveg.Entities.INVENTARIOPROGRAMADO_BE>)Page.Session["INVENTARIOPROGRAMADO_BECollection"];
            }
            set
            {
                Page.Session["INVENTARIOPROGRAMADO_BECollection"] = value;
            }
        }
        #endregion

        #region Initializers
        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtInventarioProgramadoID.Attributes.Add("readonly", "readonly");
            txtAvance.Attributes.Add("readonly", "readonly");

            txtFechaFin.Attributes.Add("readonly", "readonly");
            txtFechaInicio.Attributes.Add("readonly", "readonly");
            txtFechaProgramada.Attributes.Add("readonly", "readonly");

            try
            {
                if (!IsPostBack)
                {
                    Page.Session["SortDirection"] = "Ascending";
                    Page.Session["SortExpression"] = "";
                    Refresh(true);
                    CargaAreas();
                }
            }
            catch 
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }



        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            if (Session["Recarga"] != null)
                if (Convert.ToBoolean(Session["Recarga"]))
                {
                    Session["Recarga"] = false;
                    mpEditarElemento.Show();
                }

            panelMessageBox.Visible = false;
        }

        #endregion

        #region ICatalogoView<> Members

        /// <summary>
        ///
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Información", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        public void ClearControls()
        {
            txtDescripcion.Text = String.Empty;
            txtAvance.Text = String.Empty;
            txtFechaFin.Text = String.Empty;
            txtFechaInicio.Text = String.Empty;
            txtFechaProgramada.Text = String.Empty;
            txtInventarioProgramadoID.Text = String.Empty;
            cboArea.SelectedIndex = -1;

            GridView1.SelectedIndex = -1;
        }

        /// <summary>
        ///
        /// </summary>
        public void SelectRecord()
        {
            if (GridView1.SelectedRow != null)
            {
                selectedRow = GridView1.SelectedRow.DataItemIndex;
                Coveg.Entities.INVENTARIOPROGRAMADO_BE be = lista[selectedRow];

                txtDescripcion.Text = be.DESCRIPCION;

                txtAvance.Text = Convert.ToString(be.AVANCE * 100) + "%";

                if (be.FECHAFIN != new DateTime())
                    txtFechaFin.Text = be.FECHAFIN.ToShortDateString();
                else
                    txtFechaFin.Text = String.Empty;

                txtFechaInicio.Text = be.fechainicio.ToShortDateString();
                txtFechaProgramada.Text = be.FECHAPROGRAMADA.ToShortDateString();
                txtInventarioProgramadoID.Text = be.INVENTARIOPROGRAMADOID.ToString();

                cboArea.SelectedValue = be.ID_AREA.ToString();

            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Refresh(bool requery)
        {
            if (requery || lista == null)
                lista = Controller.SelectAll(new INVENTARIOPROGRAMADO_BE());

            selectedRow = -1;

            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }
        /// <summary>
        ///
        /// </summary>
        public void Filtrar()
        {
            Refresh(true);
            if (GridView1.DataSource is List<INVENTARIOPROGRAMADO_BE>)
            {
                string strValue = txtSearch.Text.ToLower();

                lista = lista.FindAll(p => F.Text.Input.IsNothing(p.DESC_ESTATUS, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.DESCRIPCION, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.FECHAFIN, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.fechainicio, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.FECHAPROGRAMADA, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.INVENTARIOPROGRAMADOID, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.PENDIENTES, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.CANTIDAD, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.AVANCE, "").ToString().ToLower().Contains(strValue)
                    || F.Text.Input.IsNothing(p.AREA, "").ToString().ToLower().Contains(strValue)

                    );

                GridView1.DataSource = lista;
                GridView1.DataBind();
            }
        }
        /// <summary>
        ///
        /// </summary>
        public void Delete()
        {
            if (selectedRow >= 0)
            {
                if (lista[selectedRow] != null)
                {
                    INVENTARIOPROGRAMADO_BE objBE = new INVENTARIOPROGRAMADO_BE();
                    objBE.INVENTARIOPROGRAMADOID = lista[selectedRow].INVENTARIOPROGRAMADOID;
                    var dt = Coveg.Data.OpenData.OpenQuery(System.Configuration.ConfigurationManager.AppSettings["invEnEjecucion"] + objBE.INVENTARIOPROGRAMADOID.ToString ());

                    if (dt != null && dt.Rows.Count > 0 && Convert.ToInt32 ( F.Text.Input.IsNothing(dt.Rows[0]["INV_EN_EJECUCION"],0))>=1)
                    {

                        NotifyUser(System.Configuration.ConfigurationManager.AppSettings["errInvEnProgreso"]);
                    }
                    else
                    {

                        if (Controller.Delete(objBE))
                        {
                            Refresh(true);
                            ClearControls();
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoEliminado"]);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        public void Save()
        {
            if (ValidateControls())
            {
                INVENTARIOPROGRAMADO_BE be = new INVENTARIOPROGRAMADO_BE();

                if (txtInventarioProgramadoID.Text != String.Empty)
                    be.INVENTARIOPROGRAMADOID = Convert.ToInt32(txtInventarioProgramadoID.Text);


                be.DESCRIPCION = txtDescripcion.Text;
                be.fechainicio = Convert.ToDateTime(txtFechaInicio.Text);
                be.FECHAPROGRAMADA = Convert.ToDateTime(txtFechaProgramada.Text);
                be.ID_AREA = Convert.ToInt32(cboArea.SelectedValue);

                if (
                    Coveg.Common.Date.DateWithoutTime(be.FECHAPROGRAMADA) >= Coveg.Common.Date.DateWithoutTime(DateTime.Now)
                    && Coveg.Common.Date.DateWithoutTime(be.fechainicio) >= Coveg.Common.Date.DateWithoutTime(DateTime.Now)
                    && be.fechainicio >= be.FECHAPROGRAMADA
                    )
                {

                    if (lista != null && lista.Find(entity => entity.INVENTARIOPROGRAMADOID == be.INVENTARIOPROGRAMADOID) != null)
                    {
                        if (Controller.Update(be) != null)
                        {
                            Refresh(true);
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoActualizado"]);
                        }
                        else
                        {
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errAtualizarRegistro"]);
                        }
                        ClearControls();
                    }
                    else
                    {

                        if (Controller.Insert(be) != null)
                        {
                            Refresh(true);
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoInsertado"]);
                        }
                        else
                        {
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errInsertarRegistro"]);
                        }
                        ClearControls();
                    }
                }
                else
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFechasInventarioInvalidas"]);
                }


            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["msgFaltaElementosRequeridos"]);
                mpEditarElemento.Show();
                Session["Recarga"] = true;
            }
        }
        /// <summary>
        ///
        /// </summary>
        /// <param name="collection"></param>
        public void LoadCollection(List<INVENTARIOPROGRAMADO_BE> collection)
        {
            lista = collection;
            Refresh(false);
        }
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            return (
                cboArea.SelectedIndex != -1
                && !String.IsNullOrEmpty(txtDescripcion.Text)
                && !String.IsNullOrEmpty(txtFechaInicio.Text)
                && !String.IsNullOrEmpty(txtFechaProgramada.Text)
                );
        }
        #endregion

        #region Eventos

        protected void cmdCancelarPop_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpEditarElemento.Hide();
        }


        protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                e.Row.Cells[2].Text = Convert.ToDateTime(e.Row.Cells[2].Text).ToShortDateString();
                e.Row.Cells[4].Text = Convert.ToDateTime(e.Row.Cells[4].Text).ToShortDateString();

                if (Convert.ToDateTime(e.Row.Cells[5].Text) == new DateTime())
                {
                    e.Row.Cells[5].Text = "--";
                }
                else
                {
                    e.Row.Cells[5].Text = Convert.ToDateTime(e.Row.Cells[5].Text).ToShortDateString();
                }

                e.Row.Cells[8].Text = Convert.ToString(Convert.ToDouble(e.Row.Cells[8].Text) * 100) + "%";

            }

        }

        /// <summary>
        /// Nuevo elemento
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            lblAvance.Visible = false;
            txtAvance.Visible = false;
            lblFechaFin.Visible = false;
            txtFechaFin.Visible = false;
            
            ClearControls();
            mpEditarElemento.Show();
            cmdEliminar.Visible = false;
        }

        /// <summary>
        /// Guardar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        /// <summary>
        /// Eliminar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            Delete();
        }

        /// <summary>
        /// Actulizar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearControls();
            Refresh(true);
        }

        /// <summary>
        /// Busqueda por contenidode caja de texto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Filtrar();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (F.Text.Input.IsNumeric(ddlPageSize.SelectedValue))
            {
                int psize = Convert.ToInt32(ddlPageSize.SelectedValue);
                if (psize == 0)
                {
                    GridView1.AllowPaging = false;
                }
                else
                {
                    GridView1.AllowPaging = true;
                    GridView1.PageSize = psize;
                    GridView1.PageIndex = 0;
                }
                Refresh(true);
                ClearControls();
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblAvance.Visible = true;
            txtAvance.Visible = true;
            lblFechaFin.Visible = true;
            txtFechaFin.Visible = true;

            SelectRecord();
            mpEditarElemento.Show();
            cmdEliminar.Visible = true;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }


        #endregion

        #region Sorting Commands

        /// <summary>
        /// Ordenar grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (lista == null || lista.Count == 0)
                return;

            if (e.SortExpression == "AREA")
            {
                Page.Session["SortExpression"] = "AREA";
                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                {
                    var v = lista.OrderBy(p => p.AREA);
                    lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                    Page.Session["SortDirection"] = "Descending";
                }
                else
                {
                    var v = lista.OrderByDescending(p => p.AREA);
                    lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                    Page.Session["SortDirection"] = "Ascending";
                }
            }
            else
                if (e.SortExpression == "AREA")
                {
                    Page.Session["SortExpression"] = "AREA";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.AREA);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.AREA);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
            else
                if (e.SortExpression == "AVANCE")
                {
                    Page.Session["SortExpression"] = "AVANCE";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.AVANCE);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.AVANCE);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
            else
                if (e.SortExpression == "CANTIDAD")
                {
                    Page.Session["SortExpression"] = "CANTIDAD";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.CANTIDAD);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.CANTIDAD);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
            else
                if (e.SortExpression == "PENDIENTES")
                {
                    Page.Session["SortExpression"] = "PENDIENTES";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.PENDIENTES);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.PENDIENTES);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }

            else
                if (e.SortExpression == "DESCRIPCION")
                {
                    Page.Session["SortExpression"] = "DESCRIPCION";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = lista.OrderBy(p => p.DESCRIPCION);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = lista.OrderByDescending(p => p.DESCRIPCION);
                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "ESTATUS")
                    {
                        Page.Session["SortExpression"] = "ESTATUS";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = lista.OrderBy(p => p.ESTATUS);
                            lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = lista.OrderByDescending(p => p.ESTATUS);
                            lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "FECHAFIN")
                        {
                            Page.Session["SortExpression"] = "FECHAFIN";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = lista.OrderBy(p => p.FECHAFIN);
                                lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = lista.OrderByDescending(p => p.FECHAFIN);
                                lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "fechainicio")
                            {
                                Page.Session["SortExpression"] = "fechainicio";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = lista.OrderBy(p => p.fechainicio);
                                    lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = lista.OrderByDescending(p => p.fechainicio);
                                    lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "FECHAPROGRAMADA")
                                {
                                    Page.Session["SortExpression"] = "FECHAPROGRAMADA";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = lista.OrderBy(p => p.FECHAPROGRAMADA);
                                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = lista.OrderByDescending(p => p.FECHAPROGRAMADA);
                                        lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "ID_AREA")
                                    {
                                        Page.Session["SortExpression"] = "ID_AREA";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = lista.OrderBy(p => p.ID_AREA);
                                            lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = lista.OrderByDescending(p => p.ID_AREA);
                                            lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }
                                    else
                                        if (e.SortExpression == "INVENTARIOPROGRAMADOID")
                                        {
                                            Page.Session["SortExpression"] = "INVENTARIOPROGRAMADOID";
                                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                            {
                                                var v = lista.OrderBy(p => p.INVENTARIOPROGRAMADOID);
                                                lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                                Page.Session["SortDirection"] = "Descending";
                                            }
                                            else
                                            {
                                                var v = lista.OrderByDescending(p => p.INVENTARIOPROGRAMADOID);
                                                lista = v.ToList<INVENTARIOPROGRAMADO_BE>();
                                                Page.Session["SortDirection"] = "Ascending";
                                            }
                                        }


            GridView1.SelectedIndex = -1;
            GridView1.DataSource = lista;
            GridView1.DataBind();
            ClearControls();
        }


        #endregion


        #region CargarCombos

            private void CargaAreas()
            {
                DataTable dt = Coveg.Data.OpenData.OpenQuery(System.Configuration.ConfigurationManager.AppSettings["consAreasInv"].ToString());
                cboArea.DataSource = dt;
                cboArea.DataTextField = "nombre_area";
                cboArea.DataValueField = "id_area";
                cboArea.DataBind();
            }

        #endregion



    }
}