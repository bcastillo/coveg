﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views.Procesos
{
    public partial class AutorizacionSalida : System.Web.UI.Page
    {

        #region Properties

            Coveg.Procesos.Controller.AUTORIZACIONSALIDAController _controller;
            /// <summary>
            ///
            /// </summary>
            protected Coveg.Procesos.Controller.AUTORIZACIONSALIDAController Controller
            {
                get
                {
                    if (_controller == null)
                    {
                        _controller = new Coveg.Procesos.Controller.AUTORIZACIONSALIDAController();
                    }

                    return _controller;
                }
                set
                {
                    _controller = value;
                }
            }

            /// <summary>
            ///
            /// </summary>
            private int selectedRow
            {
                get
                {
                    if (Page.Session["SelectedROW"] == null)
                    {
                        Page.Session["SelectedROW"] = -1;
                    }
                    return (Int32)Page.Session["SelectedROW"];
                }
                set
                {
                    Page.Session["SelectedROW"] = value;
                }
            }
            /// <summary>
            ///
            /// </summary>
            private List<Coveg.Entities.SOLICITUDSALIDA_BE> listaSolicitudes
            {
                get
                {
                    return (List<Coveg.Entities.SOLICITUDSALIDA_BE>)Page.Session["SolicitudesCollection"];
                }
                set
                {
                    Page.Session["SolicitudesCollection"] = value;
                }
            }
        #endregion

        #region Eventos

            /// <summary>
            /// Cerrar pop
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void cmdCancelarPop_Click(object sender, EventArgs e)
            {
                ClearControls();
                mpEditarElemento.Hide();
            }

            /// <summary>
            /// Alternancia de mouse en grid de solicitudes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                }
            }

            /// <summary>
            /// Grid de activos
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
                }
            }

            protected void btnRechazar_Click(object sender, EventArgs e)
            {
                if (GridView1.SelectedIndex != -1)
                {
                    mpEditComment.Show();
                }
                else
                    NotifyUser("Seleccione un elemento");
            }

            /// <summary>
            /// Autorizar
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void btnAutorizar_Click(object sender, EventArgs e)
            {
                if (Request.QueryString["Opcion"] == "Resguardo")
                    AprobarRechazarResguardo(true);
                if (Request.QueryString["Opcion"] == "Jefe")
                    AprobarRechazarJefe(true);
                if (Request.QueryString["Opcion"] == "Director")
                    AprobarRechazarDirector(true);

            }


            /// <summary>
            /// Actulizar grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void btnRefresh_Click(object sender, EventArgs e)
            {
                Session["DETALLEAUTORIZA"] = null;
                ClearControls();
                Refresh(true);
            }


            private void InicializaControles()
            {
                String strOpcion = Request.QueryString["Opcion"];
                txtSolicitante.Attributes.Add("readonly", "readonly");
                txtFecha.Attributes.Add("readonly", "readonly");
                txtMotivo.Attributes.Add("readonly", "readonly");
                txtFechaRetorno.Attributes.Add("readonly", "readonly");
                txtFechaSalida.Attributes.Add("readonly", "readonly");

                if (strOpcion == "Resguardo")
                    lblTituloCatalogo.Text = "Autorizaciones Activos en Resguardo";

                if (strOpcion == "Jefe")
                    lblTituloCatalogo.Text = "Autorizaciones Activos en Jefe de Area";

                if (strOpcion == "Director")
                    lblTituloCatalogo.Text = "Autorizaciones Activos de Dirección";

 
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
                TextPopup1.ConfirmAccept += new EventHandler(TextPopup1_Accepted);
                TextPopup1.ConfirmCancel += new EventHandler(TextPopup1_Cancelled);
                TextPopup1.Initilize(string.Empty, "Rechazo", "Introduzca el motivo del rechazo");

                try
                {
                    InicializaControles();
                    if (!IsPostBack)
                    {
                        Page.Session["SortDirection"] = "Ascending";
                        Page.Session["SortExpression"] = "";
                        Refresh(true);
                    }
                }
                catch
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                }
            }

            /// <summary>
            /// Cancelar ventana de textpop
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void TextPopup1_Cancelled(object sender, EventArgs e)
            {
                TextPopup1.AsignarValor = String.Empty;
                mpEditarElemento.Show();
            }

            /// <summary>
            /// Aprobar ventana de textpop
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            void TextPopup1_Accepted(object sender, EventArgs e)
            {
                if (GridView1.SelectedIndex != -1)
                {
                    if (Request.QueryString["Opcion"] == "Resguardo")
                        AprobarRechazarResguardo(false);
                    if (Request.QueryString["Opcion"] == "Jefe")
                        AprobarRechazarJefe(false);
                    if (Request.QueryString["Opcion"] == "Director")
                        AprobarRechazarDirector(false);
                }
                else
                    NotifyUser("Se requiere seleccionar un elemento");
            }

            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

            /// <summary>
            ///
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
            {
                SelectRecord();
                mpEditarElemento.Show();
            }

            /// <summary>
            ///
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                GridView1.PageIndex = e.NewPageIndex;
                GridView1.DataSource = listaSolicitudes;
                GridView1.DataBind();
            }

            /// <summary>
            /// Ordenar grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (listaSolicitudes == null || listaSolicitudes.Count == 0)
                    return;

                if (e.SortExpression == "DESC_ESTATUS")
                {
                    Page.Session["SortExpression"] = "DESC_ESTATUS";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = listaSolicitudes.OrderBy(p => p.DESC_ESTATUS);
                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = listaSolicitudes.OrderByDescending(p => p.DESC_ESTATUS);
                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "FECHA")
                    {
                        Page.Session["SortExpression"] = "FECHA";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = listaSolicitudes.OrderBy(p => p.FECHA);
                            listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = listaSolicitudes.OrderByDescending(p => p.FECHA);
                            listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "MOTIVO")
                        {
                            Page.Session["SortExpression"] = "MOTIVO";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = listaSolicitudes.OrderBy(p => p.MOTIVO);
                                listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = listaSolicitudes.OrderByDescending(p => p.MOTIVO);
                                listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "SOLICITUDSALIDAID")
                            {
                                Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = listaSolicitudes.OrderBy(p => p.SOLICITUDSALIDAID);
                                    listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = listaSolicitudes.OrderByDescending(p => p.SOLICITUDSALIDAID);
                                    listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "USUARIO")
                                {
                                    Page.Session["SortExpression"] = "USUARIO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = listaSolicitudes.OrderBy(p => p.USUARIO);
                                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = listaSolicitudes.OrderByDescending(p => p.USUARIO);
                                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }
                                else
                                    if (e.SortExpression == "NOMBRE")
                                    {
                                        Page.Session["SortExpression"] = "NOMBRE";
                                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                        {
                                            var v = listaSolicitudes.OrderBy(p => p.NOMBRE);
                                            listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                            Page.Session["SortDirection"] = "Descending";
                                        }
                                        else
                                        {
                                            var v = listaSolicitudes.OrderByDescending(p => p.NOMBRE);
                                            listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                            Page.Session["SortDirection"] = "Ascending";
                                        }
                                    }


                GridView1.SelectedIndex = -1;
                GridView1.DataSource = listaSolicitudes;
                GridView1.DataBind();

                ClearControls();
                
            }


        #endregion

        #region Funciones

            /// <summary>
            ///
            /// </summary>
            public void ClearControls()
            {
                txtFecha.Text = String.Empty;
                txtFechaSalida.Text = String.Empty;
                txtFechaRetorno.Text = String.Empty;

                txtMotivo.Text = String.Empty;
                txtSolicitante.Text = String.Empty;
                
                GridView1.SelectedIndex = -1;

                GridView2.DataSource = null;
                GridView2.DataBind();
            }

            /// <summary>
            ///
            /// </summary>
            public void Refresh(bool requery)
            {
                String strOpcion = Request.QueryString["Opcion"];
                SOLICITUDSALIDA_BE objSolicitud = new SOLICITUDSALIDA_BE();

                //Variables para busqueda de solicitudes ---------------------
                objSolicitud.IDJEFEINMEDIATO = (int)Session["IDUSUARIO"];
                objSolicitud.USUARIO = Session["USUARIO"].ToString();
                //---------------------------------------------------------

                if (requery || listaSolicitudes == null)
                {
                    if (strOpcion == "Resguardo")
                        listaSolicitudes = Controller.SelectSolicitudesResguardo(objSolicitud);
                    if (strOpcion == "Jefe")
                        listaSolicitudes = Controller.SelectSolicitudesJefe(objSolicitud);
                    if (strOpcion == "Director")
                        listaSolicitudes = Controller.SelectSolicitudesDireccion(objSolicitud);
                }

                selectedRow = -1;

                if (Session["DETALLEAUTORIZA"] != null)
                {
                    if (listaSolicitudes != null)
                        listaSolicitudes = listaSolicitudes.FindAll(p => p.SOLICITUDSALIDAID == Convert.ToInt32(Session["DETALLEAUTORIZA"]));
                }

                if(listaSolicitudes!=null)
                    listaSolicitudes = listaSolicitudes.OrderByDescending(p => p.SOLICITUDSALIDAID).ToList();

                GridView1.SelectedIndex = -1;
                GridView1.DataSource = listaSolicitudes;
                GridView1.DataBind();
            }


            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error", "Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void SelectRecord()
            {
                if (GridView1.SelectedRow != null)
                {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;
                    Coveg.Entities.SOLICITUDSALIDA_BE be = listaSolicitudes[selectedRow];
                    Coveg.Entities.ACTIVOSSOLICITADOS_BE objActivos = new ACTIVOSSOLICITADOS_BE();
                    List<ACTIVOSSOLICITADOS_BE> listaActivos = new List<ACTIVOSSOLICITADOS_BE>();

                    txtSolicitante.Text = be.NOMBRE;
                    txtMotivo.Text = be.MOTIVO;
                    txtFecha.Text = be.FECHA.ToShortDateString();
                    txtFechaSalida.Text = be.FECHASALIDA.ToShortDateString();
                    txtFechaRetorno.Text = be.FECHARETORNO.ToShortDateString();
                    objActivos.SOLICITUDSALIDAID = be.SOLICITUDSALIDAID;

                    listaActivos = Controller.ListaActivosSolicitados(objActivos);
                    GridView2.DataSource = listaActivos;
                    GridView2.DataBind();
                }
            }


            /// <summary>
            /// Aprobar y rechazar solicitud por responsable de resguardo de activo
            /// </summary>
            /// <param name="bolAutoriza"></param>
            private void AprobarRechazarResguardo(Boolean bolAutoriza)
            {
                AUTORIZACIONSALIDA_BE objAutoriza = new AUTORIZACIONSALIDA_BE();

                if (GridView1.SelectedRow != null)
                {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;
                    Coveg.Entities.SOLICITUDSALIDA_BE be = listaSolicitudes[selectedRow];

                    objAutoriza.AUTORIZO = Session["USUARIO"].ToString();
                    objAutoriza.FECHAREVISION = System.DateTime.Now;
                    objAutoriza.SOLICITUDSALIDAID = be.SOLICITUDSALIDAID;

                    if (bolAutoriza)
                    {
                        if (Controller.AutorizaResguardo(objAutoriza) != null)
                        {
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoAutorizado"]);
                            Controller.NotificarAutorizadores(be.SOLICITUDSALIDAID);
                        }
                        else
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                    }
                    else
                    {
                        objAutoriza.COMENTARIOS = TextPopup1.TextPopupText;
                        if (Controller.RechazaResguardo(objAutoriza) != null)
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoRechazado"]);
                        else
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                    }

                    Refresh(true);
                    ClearControls();
                }
            }

            private void AprobarRechazarJefe(Boolean bolAutoriza)
            {
                AUTORIZACIONSALIDA_BE objAutoriza = new AUTORIZACIONSALIDA_BE();

                if (GridView1.SelectedRow != null)
                {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;
                    Coveg.Entities.SOLICITUDSALIDA_BE be = listaSolicitudes[selectedRow];

                    objAutoriza.AUTORIZO = Session["USUARIO"].ToString();
                    objAutoriza.FECHAREVISION = System.DateTime.Now;
                    objAutoriza.SOLICITUDSALIDAID = be.SOLICITUDSALIDAID;
                    

                    if (bolAutoriza)
                    {
                        if (Controller.AutorizaJefe(objAutoriza) != null)
                        {
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoAutorizado"]);
                            Controller.NotificarAutorizadores(be.SOLICITUDSALIDAID);
                        }
                        else
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                    }
                    else
                    {
                        objAutoriza.COMENTARIOS = TextPopup1.TextPopupText;
                        if (Controller.RechazaJefe(objAutoriza) != null)
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoRechazado"]);
                        else
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                    }

                    Refresh(true);
                    ClearControls();
                }
            }

            private void AprobarRechazarDirector(Boolean bolAutoriza)
            {
                AUTORIZACIONSALIDA_BE objAutoriza = new AUTORIZACIONSALIDA_BE();

                if (GridView1.SelectedRow != null)
                {
                    selectedRow = GridView1.SelectedRow.DataItemIndex;
                    Coveg.Entities.SOLICITUDSALIDA_BE be = listaSolicitudes[selectedRow];

                    objAutoriza.AUTORIZO = Session["USUARIO"].ToString();
                    objAutoriza.FECHAREVISION = System.DateTime.Now;
                    objAutoriza.SOLICITUDSALIDAID = be.SOLICITUDSALIDAID;
                    

                    if (bolAutoriza)
                    {
                        if (Controller.AutorizaDirector(objAutoriza) != null)
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoAutorizado"]);
                        else
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                    }
                    else
                    {
                        objAutoriza.COMENTARIOS = TextPopup1.TextPopupText;
                        if (Controller.RechazaDirector(objAutoriza) != null)
                            NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgElementoRechazado"]);
                        else
                            RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                    }

                    Refresh(true);
                    ClearControls();
                }
            }

        #endregion

    }
}