﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="InventarioProgramado.aspx.cs" Inherits="CovegInventarios.Views.Procesos.InventarioProgramado" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
    <ContentTemplate>
        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

        <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pnl1" 
            TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
            BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" 
            Enabled="True" />
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>


        <asp:Panel ID="pnl1" runat="server"  CssClass="shadowPanel">
            <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" >
            <table width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" 
                            Text="Detalles de Inventario Programado" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" Font-Size="13px" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdGuardar" Text="Guardar" Font-Bold="True" onclick="btnSave_Click" CssClass="boton" />
                        &nbsp;<asp:Button runat="server" ID="cmdEliminar" Text="Eliminar" Font-Bold="True" OnClick="btnEliminar_Click" CssClass="boton"/>
                        &nbsp;<asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
            <asp:Panel ID="pBody" runat="server" CssClass="cpBody">
            <table cellpadding="2" cellspacing="0" class="shadow" >
            <tr>
                <td colspan="2" align="left">
                    <table cellpadding="2" cellspacing="0">
                        <tr style="display:none">
                            <td>
                                <asp:Label ID="lblInventarioProgramadoID" runat="server" Text="ID" 
                                    CssClass="Etiquetas"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox  ID="txtInventarioProgramadoID" runat="server" Height="16px" 
                                    CssClass="textbox" ></asp:TextBox>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr  style="height:30px">
                            <td>
                                <asp:Label ID="lblDescripcion" runat="server" CssClass="Etiquetas" 
                                    Text="Descripción del inventario" Font-Size="11px"></asp:Label>&nbsp;
                                    <asp:Label ID="lblRequerido4" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido" Font-Bold="true"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtDescripcion" runat="server"  Height="30px" 
                                    CssClass="textbox" Width="350px" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr  style="height:30px">
                            <td>
                                
                                <asp:Label ID="lblFechaProgramada" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha programada" Font-Size="11px"></asp:Label>&nbsp;
                                    <asp:Label ID="lblRequerido5" runat="server" CssClass="etiquetaRequerido" 
                                    ForeColor="#CC3300" Text="*" Font-Bold="true"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaProgramada" runat="server" CssClass="textbox" 
                                    Width="80px" Height="16px"></asp:TextBox>
                                <asp:CalendarExtender ID="txtFechaProgramada_CalendarExtender" runat="server"  Format="dd/MM/yyyy"  
                                    Enabled="True" TargetControlID="txtFechaProgramada" PopupButtonID="imgFechaProgramada" OnClientDateSelectionChanged ="checkDate">
                                </asp:CalendarExtender>
                                &nbsp;<asp:ImageButton ID="imgFechaProgramada" runat="server" 
                                    ImageUrl="~/Images/calendar.png" Width="24px" />
                            </td>
                            <td>
                                <asp:Label ID="lblFechaInicio" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha de inicio" Font-Size="11px"></asp:Label>
                                <asp:Label ID="lblRequerido24" runat="server" CssClass="etiquetaRequerido" 
                                    Font-Bold="true" ForeColor="#CC3300" Text="*"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaInicio" runat="server" CssClass="textbox" 
                                    Height="16px" Width="80px"></asp:TextBox>
                                <asp:CalendarExtender ID="txtFechaInicio_CalendarExtender" runat="server"  Format="dd/MM/yyyy" 
                                    Enabled="True" TargetControlID="txtFechaInicio" PopupButtonID="imgFechaInicio" OnClientDateSelectionChanged="checkDate">
                                </asp:CalendarExtender>
                                &nbsp;<asp:ImageButton ID="imgFechaInicio" runat="server" 
                                    ImageUrl="~/Images/calendar.png" Width="24px" />
                            </td>
                        </tr>
                        
                        <tr  style="height:30px">
                            <td>
                                <asp:Label ID="lblArea" runat="server" CssClass="Etiquetas" Text="Área" 
                                    Font-Size="11px"></asp:Label>
                                &nbsp;<asp:Label ID="lblRequerido22" runat="server" CssClass="etiquetaRequerido" 
                                    Text="*" Font-Bold="true" ForeColor="#CC3300"></asp:Label>
                                
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="cboArea" runat="server" CssClass="comboSeleccion" 
                                    Height="20px">
                                </asp:DropDownList>
                                <asp:ListSearchExtender ID="cboArea_ListSearchExtender" runat="server" 
                                    Enabled="True" PromptPosition="Bottom" PromptText="Teclee para buscar" 
                                    TargetControlID="cboArea">
                                </asp:ListSearchExtender>
                                <br />
                            </td>
                        </tr>
                        <tr style="height:30px" >
                            <td>
                                
                                <asp:Label ID="lblAvance" runat="server" CssClass="Etiquetas" Text="Progreso" 
                                    Font-Size="11px"></asp:Label>
                                
                            </td>
                            <td>
                                <asp:Label ID="txtAvance" runat="server" Font-Size="11px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaFin" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha de final" Font-Size="11px"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaFin" runat="server"></asp:Label>
                                &nbsp;</td>
                        </tr>
                        <tr style="height:5px">
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </asp:Panel>
        </asp:Panel>

        <table width="810px">
        <tr>
                <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" 
                                    />
                            </td>
                            <td width="60%" align="left">
                                
                                &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" 
                                    Text="Programación de Inventarios" Font-Bold="True" Font-Size="13px" 
                                     /></td>
                            <td align="right">
                                
                                <asp:Button runat="server" ID="Button1" Text="Nuevo"  
                                    Font-Bold="True"
                                    onclick="btnNew_Click" CssClass="boton" 
                                    />
                                &nbsp;<asp:Button runat="server" ID="Button2" Text="Refrescar"  
                                    Font-Bold="True" OnClick="btnRefresh_Click" 
                                     CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        <tr>
            <td class="panel6" valign="middle">
                &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;<asp:Label ID="Label1" runat="server" 
                    Style="font-weight: 700" Text="Expresión de búsqueda" CssClass="Etiquetas" 
                    ></asp:Label>
                &nbsp;&nbsp;<asp:TextBox ID="txtSearch" runat="server" AutoCompleteType="Search" 
                    AutoPostBack="True" CssClass="textbox" OnTextChanged="txtSearch_TextChanged" 
                    Width="170px" ></asp:TextBox>
                &nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" CssClass="Etiquetas" 
                    Font-Bold="True" Text="Tamaño de página" 
                    ></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" 
                    OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged" Width="48px" 
                    CssClass="comboSeleccion" Height="20px" 
                    >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="30" Value="30" ></asp:ListItem>
                    <asp:ListItem Text="50" Value="50" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                    <asp:ListItem Text="300" Value="300" ></asp:ListItem>
                    <asp:ListItem Text="500" Value="500" ></asp:ListItem>
                    <asp:ListItem Text="---" Value="0" ></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="center">
            <br />
            <div class="grid" style='width:810px;'>
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        Width="800px"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound">
                        <Columns>
                            <asp:BoundField DataField="INVENTARIOPROGRAMADOID" HeaderText="ID" SortExpression="INVENTARIOPROGRAMADOID" Visible="false" >
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                            <asp:BoundField DataField="FECHAPROGRAMADA" HeaderText="FECHAPROGRAMADA" SortExpression="FECHAPROGRAMADA" />
                            <asp:BoundField DataField="AREA" HeaderText="AREA" SortExpression="AREA" />
                            <asp:BoundField DataField="FECHAINICIO" HeaderText="FECHAINICIO" SortExpression="FECHAINICIO" />
                            <asp:BoundField DataField="FECHAFIN" HeaderText="FECHAFIN" SortExpression="FECHAFIN" />
                            <asp:BoundField DataField="CANTIDAD" HeaderText="CANTIDAD" SortExpression="CANTIDAD" />
                            <asp:BoundField DataField="PENDIENTES" HeaderText="PENDIENTES" SortExpression="PENDIENTES" />
                            <asp:BoundField DataField="AVANCE" HeaderText="AVANCE" SortExpression="AVANCE" />
                            <asp:TemplateField HeaderText="">
                                    <itemtemplate>
                                        <asp:ImageButton ID="imgDetalleInventarioProg" runat="server" ToolTip="Usuarios de Inventario Programado" 
                                            ImageUrl="~/Images/User1.png" Width="20px"  onClientClick=<%# string.Format("window.open('../Reportes/ReporteInvProgDetallePop.aspx?INVENTARIOPROGRAMADOID={0}','Detalle','menubar=no,scrollbars=yes,status=no,location=no,height=500,width=1050');", Eval("INVENTARIOPROGRAMADOID")) %>/>
                                    </itemtemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                             </asp:TemplateField>
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Editar">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
            </div>    
        </td>
        </tr>
        </table>
        <script type="text/javascript">
            function checkDate(sender, args) {
                var now = new Date();
                now.setHours(0, 0, 0, 0);

                if (sender._selectedDate < now) {
                    alert("No puede elegir fechas anteriores al dia de hoy!");
                    sender._selectedDate = new Date();
                    // set the date back to the current date
                    sender._textbox.set_Value(sender._selectedDate.format(sender._format))
                }
            }
        </script>
        </ContentTemplate>
  </asp:UpdatePanel> 
</div>
</asp:Content>
