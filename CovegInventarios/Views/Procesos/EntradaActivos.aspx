﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EntradaActivos.aspx.cs" Inherits="CovegInventarios.Views.Procesos.EntradaActivos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/Utils/TextPopup.ascx" TagName="TextPopup" TagPrefix="uc2" %>
<%@ Register Src="~/Utils/BusquedaActivos.ascx" TagName="BusquedaActivos" TagPrefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" />  
    <style type="text/css">
        .style2
        {
            font-weight: bold;
            font-size: 13px;
             color: #3466B7;
        }
        .style3
        {
            font-size: 11px;
            color: Black;
        }
        .style4
        {
            font-weight: bold;
            font-size: 11px;
            color: Black;
        }
        .style5
        {
        	color: Black;
            font-size: 13px;
            font-weight: bold;
        }
        .style6
        {
            font-size: 13px;
            color: #3466B7;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">  
    <ContentTemplate>

        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>
        
        <asp:ModalPopupExtender ID="mpDetalleActivo"  PopupControlID="pnlConsulta" TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="ModalPopupBG" runat="server"/>
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>

        <asp:ModalPopupExtender ID="mpEditComentarios"  PopupControlID="pnlObservacionesEntrada" TargetControlID="lnkFake2" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="ModalPopupBG" runat="server"/>
        <asp:LinkButton ID="lnkFake2" runat="server" ></asp:LinkButton>

        <asp:Panel ID="pnlObservacionesEntrada" runat="server" Height="200" Width="200" CssClass="modalPopup">
            <uc2:TextPopup ID="txtPopObservaciones" runat="server"/>
        </asp:Panel>

        <asp:Panel ID="pnlHeaderConsulta" runat="server" CssClass="cpHeader" Width="100%" >
            <table width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/Tools.png" 
                            Width="20px" />
                        &nbsp;
                        <asp:Label ID="Label2" runat="server" Text="Entrada de activos" 
                            Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdConsultar" Text="Consultar" Font-Bold="True" 
                            onclick="cmdConsultar_Click" CssClass="boton" />
                    </td>
                </tr>
            </table>
            </asp:Panel>

        <asp:Panel ID="pnlBodyConsulta" runat="server" CssClass="cpBody" BackColor="White" Width="100%">
            <table cellpadding="0" cellspacing="0" class="shadow" width="100%">
            <tr>
                <td align="left">

                    <fieldset id="Fieldset1" runat="server"  style='background-color:White;'>
                    <legend class="style5">Activo a recibir</legend>
                    <table>
                        <tr>
                            <td >
                                <asp:Label ID="lblClaveBusqueda" runat="server" 
                                    Text="Búsqueda por clave" CssClass="Etiquetas"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox  ID="txtClaveActivoBusqueda" runat="server" CssClass="textbox" 
                                    Width="350px" Height="16px"></asp:TextBox>
                                 <asp:AutoCompleteExtender ID="txtClaveActivoBusqueda_AutoCompleteExtender" runat="server" 
                                    CompletionSetCount="20" CompletionInterval="100" 
                                    UseContextKey="True" 
                                    MinimumPrefixLength="3"
                                    Enabled="True"                                     
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListItemCssClass="autocomplete_listItem"
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    DelimiterCharacters=";, :"
                                    ShowOnlyCurrentWordInCompletionListItem="True" 
                                    ServiceMethod="ObtenerActivos"
                                    TargetControlID="txtClaveActivoBusqueda">
                                </asp:AutoCompleteExtender>
                                &nbsp;
                                <br /></td>
                        </tr>
                    </table>
                    </fieldset>
                 </td>
            </tr>
            </table>
        </asp:Panel>

        <asp:Panel ID="pnlConsulta" runat="server" CssClass ="shadowPanel">

            <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="100%" >
            <table width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Activo" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdEntrada" Text="Entrada" 
                            Font-Bold="True" onclick="cmdEntrada_Click" 
                             CssClass="boton" />
                        &nbsp;<asp:Button ID="cmdCancelar" runat="server" CssClass="boton" Font-Bold="True" 
                            onclick="cmdCancelar_Click" Text="Cancelar" />
                        &nbsp;</td>
                </tr>
            </table>
            </asp:Panel>

            <asp:Panel ID="pBody" runat="server" CssClass="cpBody" BackColor="#FFFFFF" Width="100%">
            <table cellpadding="0" cellspacing="0" class="shadow" width="100%">
            <tr>
                <td align="left">

                    <fieldset id="fsSolicitud" runat="server"  style='background-color:#FFFFFF;'>
                    <legend class="style6"><b>Generales de la solicitud</b></legend>
                    <table cellpadding="2" cellspacing="0" width="100%">
                        <tr style="background-color:#ECF3F8">
                            <td width="130px">
                                <asp:Label ID="lblUsuario" runat="server" CssClass="style3" Text="Solicitante" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtUsuarioSolicito" runat="server"></asp:Label>
                            </td>
                            <td width="130px">
                                <asp:Label ID="lblEstatusSolicitud" runat="server" CssClass="style3" 
                                    Text="Estatus de la solicitud" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusSolicitud" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFecha" runat="server" CssClass="style3" 
                                    Text="Fecha de la solicitud" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFecha" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaSalida" runat="server" CssClass="style3" 
                                    Text="Fecha de salida" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaSalida" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr >
                            <td width="130px">
                                <asp:Label ID="lblMotivo" runat="server" CssClass="style3" Text="Motivo" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td colspan="5">
                                <asp:Label ID="txtMotivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaRetorno" runat="server" CssClass="style3" 
                                    Text="Fecha de retorno" Width="100px" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaRetorno" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr  style="background-color:#ECF3F8">
                            <td width="130px">
                                <asp:Label ID="lblMotivo0" runat="server" CssClass="style3" 
                                    style="font-weight: bold" Text="Usuario que ingresa"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtUsuarioIngresa" runat="server" CssClass="textbox" 
                                    Height="16px"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="txtUsuario_AutoCompleteExtender" runat="server" 
                                    CompletionSetCount="20" CompletionInterval="100" 
                                    UseContextKey="True" 
                                    MinimumPrefixLength="3"
                                    Enabled="True"                                     
                                    CompletionListCssClass="autocomplete_completionListElement" 
                                    CompletionListItemCssClass="autocomplete_listItem"
                                    CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                    DelimiterCharacters=";, :"
                                    ShowOnlyCurrentWordInCompletionListItem="True" 
                                    ServiceMethod="ObtenerUsuarios"
                                    TargetControlID="txtUsuarioIngresa"></asp:AutoCompleteExtender>
                            </td>
                            <td>
                                <asp:Label ID="lblMotivo1" runat="server" CssClass="style3" 
                                    style="font-weight: bold" Text="Usuario de salida"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtUsuarioSalida" runat="server"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    </fieldset>

                    <fieldset id="fsActivo" runat="server" style='background-color:#FFFFFF'>
                    <legend class="style2">Generales del activo</legend>
                        <table cellpadding="2" cellspacing="0" width="100%">
                        <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="Label6" runat="server" CssClass="style3" Text="Clave" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtClaveActivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label4" runat="server" CssClass="style3" Text="Número de serie" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td class="style4">
                                <asp:Label ID="txtNumSerieActivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblNombre" runat="server" CssClass="style3" 
                                    Text="Nombre" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNombre" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEstatusActivo" runat="server" CssClass="style3" Text="Estatus del activo" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusActivo" runat="server"></asp:Label>
                            </td>
                        </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDescripcion0" runat="server" CssClass="style3" 
                                        Text="Descripción" style="font-weight: bold"></asp:Label>
                                </td>
                                <td colspan="7">
                                    <asp:Label ID="txtDescripcionActivo" runat="server"></asp:Label>
                                </td>
                            </tr>
                    </table>
                    </fieldset>

                    <fieldset id="fsDocumento" runat="server" style='background-color:#FFFFFF'>
                    <legend class="style6"><b>Generales del&nbsp;documento</b></legend>
                    <table cellpadding="2" cellspacing="0" width="100%">
                        <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblClave" runat="server" CssClass="style3" Text="Clave" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td >
                                <asp:Label ID="txtClave" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTipo" runat="server" CssClass="style4" Text="Tipo"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtTipo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaIngreso" runat="server" CssClass="style3" 
                                    Text="Fecha de ingreso" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaIngreso" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEstatusDocumento" runat="server" CssClass="style3" 
                                    Text="Estatus del documento" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusDocumento" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblArea" runat="server" CssClass="style3" Text="Área" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtArea" runat="server"></asp:Label>
                            </td>
                            <td width="80px">
                                <asp:Label ID="lblEstante" runat="server" CssClass="style4" Text="Estante"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstante" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCarpeta" runat="server" CssClass="style3" 
                                    Text="Carpeta/Leffort" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCarpeta" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCaja" runat="server" CssClass="style3" Text="Caja" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCaja" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    </fieldset>

                    <fieldset id="fsAutomovil" runat="server" style='background-color:#FFFFFF;'>
                    <legend class="style6"><b>Generales del vehículo</b></legend>
                    <table cellpadding="2" cellspacing="0" width="100%">
                    <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblNumeroSerie" runat="server" CssClass="style3" 
                                    Text="Número de serie" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNumeroSerie" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCilindros" runat="server" CssClass="style3" 
                                    Text="Número de cílindros" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCilindros" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblMarca" runat="server" CssClass="style3" Text="Marca" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtMarca" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Vehiculo" runat="server" CssClass="style3" 
                                    Text="Estatus vehículo" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtEstatusVehiculo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblNoMotor" runat="server" CssClass="style3" 
                                    Text="Número de motor" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNumeroMotor" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblModelo" runat="server" CssClass="style3" Text="Modelo" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtModelo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" CssClass="style3" Text="Tipo" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtTipoAutomovil" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPlacas" runat="server" CssClass="style3" Text="Placas" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtPlacas" runat="server"></asp:Label>
                            </td>
                        </tr>
                    <tr style="background-color:#ECF3F8">
                            <td>
                                <asp:Label ID="lblNoPuertas" runat="server" CssClass="style3" 
                                    Text="Número de puertas" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtNoPuertas" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCapacidad" runat="server" CssClass="style3" Text="Capacidad" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtCapacidad" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblColor" runat="server" CssClass="style3" Text="Color" 
                                    style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtColor" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTransmision" runat="server" CssClass="style3" 
                                    Text="Transmisión" style="font-weight: bold"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtTransmision" runat="server"></asp:Label>
                            </td>
                        </tr>
                    

                    </table>
                    </fieldset>


                </td>
            </tr>
        </table>

        </asp:Panel>

        </asp:Panel>

    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
