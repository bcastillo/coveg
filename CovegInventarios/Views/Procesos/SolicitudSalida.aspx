﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SolicitudSalida.aspx.cs" Inherits="CovegInventarios.Views.Procesos.SolicitudSalida" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/Utils/BusquedaActivos.ascx" TagName="BusquedaActivos" TagPrefix="uc3" %>

<%@ Register src="../../Utils/DetalleActivo.ascx" tagname="DetalleActivo" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" />  
    <style type="text/css">
        .style1
        {
        	color:#3466B7;
            font-size: 13px;
        }
        .style4
        {
        	color:#000000;
            font-size: 11px;
        }
        .style5
        {
            color: #3466B7;
            font-size: 13px;
            font-weight: bold;
        }
        .style7
        {
            color: #3466B7;
            font-size: 13px;
            font-family: Intro_Book;
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">  
    <ContentTemplate>

        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

        <asp:Panel ID="pnl1" runat="server" CssClass="shadowPanel">
            <div>
                <uc3:BusquedaActivos ID="BusquedaActivos1" runat="server" Visible="true" />
            </div>
        </asp:Panel>
        <asp:ModalPopupExtender ID="mpEditComment"  PopupControlID="pnl1" TargetControlID="btnBuscar" OkControlID="ButtonOk" CancelControlID="ButtonCancel" BackgroundCssClass="ModalPopupBG" runat="server"/>
        
        <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="100%" >
            <table width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" 
                            Width="20px" />
                        &nbsp;<asp:Label ID="lblText" runat="server" Text="Solicitud de salida" Font-Bold="True" 
                            Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button runat="server" ID="cmdGuardar" Text="Guardar" 
                            Font-Bold="True" onclick="btnSave_Click" 
                             CssClass="boton" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
            </asp:Panel>

        <asp:Panel ID="pBody" runat="server" CssClass="cpBody" BackColor="White" Width="100%">
            <table cellpadding="0" cellspacing="0" class="shadow" width="100%">
            <tr>
                <td align="left">

                    <fieldset id="fsSolicitud" runat="server"  style='background-color:#FFFFFF;'>
                    <legend class="style7"><b>Datos de la solicitud</b></legend>
                    <table>
                        <tr style="height:30px">
                            <td>
                                <asp:Label ID="lblUsuario" runat="server" Text="Solicitante" 
                                    CssClass="Etiquetas"></asp:Label>
                                <asp:Label ID="lblRequerido0" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido" Font-Bold ="true"></asp:Label>
                                
                            </td>
                            <td>
                                <asp:Label ID="txtUsuario" runat="server"></asp:Label>
                            </td>
                            <td >
                                <asp:Label ID="lblFecha" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFecha" runat="server"></asp:Label>
                            </td>
                            <td><asp:Label ID="lblFechaSalida" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha de salida"></asp:Label>
                                    &nbsp;
                                <asp:Label ID="lblRequerido1" runat="server" CssClass="etiquetaRequerido" 
                                    Font-Bold="true" ForeColor="#CC3300" Text="*"></asp:Label>
                                
                            </td>
                            <td>
                                <asp:TextBox ID="txtFechaSalida" runat="server" CssClass="textbox" 
                                    Height="16px" Width="80px"></asp:TextBox>
                                    <asp:CalendarExtender ID="txtFechaSalida_CalendarExtender" OnClientDateSelectionChanged="checkDate" runat="server"  Format="dd/MM/yyyy" 
                                    Enabled="True" TargetControlID="txtFechaSalida" PopupButtonID ="imgFechaSalida">
                                </asp:CalendarExtender>
                                    &nbsp;<asp:ImageButton ID="imgFechaSalida" runat="server" 
                                    ImageUrl="~/Images/Week.png" Width="24px" />
                            </td>
                            <td>
                                                                
                                    <asp:Label ID="lblFechaRetorno" runat="server" CssClass="Etiquetas" 
                                        Text="Fecha de retorno"></asp:Label>
                                    &nbsp;<asp:Label ID="lblRequerido2" runat="server" CssClass="etiquetaRequerido" 
                                        Font-Bold="true" ForeColor="#CC3300" Text="*"></asp:Label>
                                                                
                            </td>
                            <td>
                                
                                <asp:TextBox ID="txtFechaRetorno" runat="server" CssClass="textbox" 
                                    Height="16px" Width="80px"></asp:TextBox>
                                    <asp:CalendarExtender ID="txtFechaRetorno_CalendarExtender" OnClientDateSelectionChanged="checkDate" runat="server"  Format="dd/MM/yyyy"
                                    Enabled="True" TargetControlID="txtFechaRetorno" PopupButtonID="imgFechaRetorno">
                                </asp:CalendarExtender>
                                    &nbsp;
                                    <asp:ImageButton ID="imgFechaRetorno" runat="server" 
                                    ImageUrl="~/Images/Week.png" Width="24px" />
                            </td>
                        </tr>
                        <tr style="height:30px">
                            <td>
                                <asp:Label ID="lblMotivo" runat="server" CssClass="Etiquetas" 
                                    Text="Motivo de la salida"></asp:Label>&nbsp;
                                <asp:Label ID="lblRequerido" runat="server" ForeColor="#CC3300" Text="*" 
                                    CssClass="etiquetaRequerido" Font-Bold ="true"></asp:Label>
                            </td>
                            <td colspan="7">
                                <asp:TextBox ID="txtMotivo" runat="server"  Height="30px" 
                                    TextMode="MultiLine" Width="100%" CssClass="textbox"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                    <uc2:DetalleActivo ID="DetalleActivo1" runat="server" Visible="False" />
                </td>
            </tr>
        </table>

        <table width="100%">
        <tr>
        <td valign="middle" class="cpHeaderCatalogos" >
            <table width="100%">
            <tr>
                <td>
                    <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" />
                </td>
                <td width="40%" align="left">
                    &nbsp;<asp:Label ID="lblTituloCatalogo" runat="server" Text="Activos de Solicitud" Font-Bold="True" Font-Size="13px"/></td>
                <td align="right">
                    <asp:Button runat="server" ID="btnBuscar" Text="Agregar activo"  
                        Font-Bold="True" CssClass="boton"/>
                    &nbsp; 
                    </td>
            </tr>
            </table>
        </td>
        </tr>
        <tr>
            <td class="panel6" valign="middle">
            <div class="grid" align="center">
                <asp:GridView ID="GridView1" 
                            Width="98%"
                            runat="server" 
                            CellPadding="3" 
                            OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                            AllowPaging="True" 
                            AllowSorting="true"
                            OnPageIndexChanging="GridView1_PageIndexChanging"
                            onsorting="GridView1_Sorting" 
                            AutoGenerateColumns="False"  
                            BackColor="White" 
                            BorderColor="#CCCCCC" 
                            BorderStyle="None" 
                            BorderWidth="1px" 
                            onrowdatabound="GridView1_RowDataBound" 
                    onrowcommand="GridView1_RowCommand" onrowdeleting="GridView1_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" SortExpression="DESCRIPCION" />
                            <asp:BoundField DataField="DESC_RFIDTAG" HeaderText="RFIDTAG" SortExpression="DESC_RFIDTAG" />
                            <asp:BoundField DataField="DESC_TIPOACTIVO" HeaderText="TIPO ACTIVO" SortExpression="DESC_TIPOACTIVO" />
                            <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" SortExpression="CLAVE" />
                            <asp:BoundField DataField="RESPONSABLE" HeaderText="RESPONSABLE" SortExpression="RESPONSABLE" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">                            
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                            <asp:ButtonField ButtonType="Button" CommandName="Delete" Text="Eliminar"  >                            
                            <ControlStyle CssClass="boton" />                            
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
                    </div>
                </td>
            </tr>
        </table>

        </asp:Panel>
        <script type="text/javascript">
            function checkDate(sender, args) {
                var now = new Date();
                now.setHours(0, 0, 0, 0);

                if (sender._selectedDate < now) {
                    alert("No puede elegir fechas anteriores al dia de hoy!");
                    sender._selectedDate = new Date();
                    // set the date back to the current date
                    sender._textbox.set_Value(sender._selectedDate.format(sender._format))
                }
            }
    </script>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
