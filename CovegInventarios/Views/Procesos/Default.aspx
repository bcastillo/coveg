﻿<%@ Page Title="Control de Inventario" Language="C#" MasterPageFile="~/Site1.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CovegInventarios._Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" />  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
        <ContentTemplate>

        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

            <asp:Panel ID="pHeader" runat="server" CssClass="cpMainHeader" Width="100%" >
            <table width="100%">
            <tr>
                <td>
                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" Width="20px" />&nbsp;
                    <asp:Label ID="lblText" runat="server" Text="Resumen de Solicitudes" Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" CssClass="whitetitle" />
                </td>
            </tr>
            </table>
            </asp:Panel>

            <asp:Panel ID="pBody" runat="server"  Width="100%">
                <table width="98%" cellspacing="5">
                    <tr>
                        <td>
                            <asp:Panel ID="Panel1" runat="server" CssClass="cpSubHeader" Width="100%" >
                            <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblSolicitudes" runat="server" Text="Mis Solicitudes" Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" CssClass="whitetitle" />
                                </td>
                            </tr>
                            </table>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel ID="Panel2" runat="server" CssClass="cpSubHeader" Width="100%" >
                            <table width="100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblAutorizaciones" runat="server" Text="Mis Autorizaciones Pendientes" Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" CssClass="whitetitle" />
                                </td>
                            </tr>
                            </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td width="50%" valign="top">
                            <div class="grid" align="center"  >
                                <asp:GridView ID="GridView1" 
                                    runat="server" 
                                    CellPadding="3" 
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="GridView1_RowDataBound" 
                                    OnPageIndexChanging="GridView1_PageIndexChanging"
                                    onsorting="GridView1_Sorting"
                                    OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                    Font-Names="intro_book"
                                    Width="100%" EmptyDataText="Usted no tiene solicitudes abiertas" 
                                    onrowcommand="GridView1_RowCommand" onrowdeleting="GridView1_RowDeleting"
                                    >
                                    <Columns>
                                        <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID" Visible="false" />
                                        <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" />
                                        <asp:BoundField DataField="TIPO" HeaderText="TIPO SOLICITUD" SortExpression="TIPO" />
                                        <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                                        <asp:BoundField DataField="DESC_ESTATUS" HeaderText="ESTATUS" SortExpression="DESC_ESTATUS" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                            <ControlStyle CssClass="boton" />
                                            <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                        <asp:ButtonField ButtonType="Button" CommandName="Delete" Text="Cerrar">
                                            <ControlStyle CssClass="boton" />
                                            <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                    <EmptyDataRowStyle Font-Bold="True" Font-Size="13px" ForeColor="#CC3300" 
                                        HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                        </td>
                        <td width="50%" valign="top">
                            <div class="grid" align="center"  >
                                <asp:GridView ID="GridView2" 
                                    runat="server" 
                                    CellPadding="3" 
                                    AllowPaging="True" 
                                    AllowSorting="true"
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="GridView2_RowDataBound" 
                                    OnPageIndexChanging="GridView2_PageIndexChanging"
                                    onsorting="GridView2_Sorting"
                                    OnSelectedIndexChanged="GridView2_SelectedIndexChanged"
                                    Font-Names="intro_book"
                                    Width="100%" EmptyDataText="Usted no tiene autorizaciones pendientes"
                                    >
                                    <Columns>
                                        <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID" Visible="false" />
                                        <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" />
                                        <asp:BoundField DataField="TIPO" HeaderText="TIPO SOLICITUD" SortExpression="TIPO" />
                                        <asp:BoundField DataField="SOLICITANTE" HeaderText="SOLICITANTE" SortExpression="SOLICITANTE" />
                                        <asp:BoundField DataField="TIPO_AUTORIZACION" HeaderText="TIPO AUTORIZA" SortExpression="TIPO_AUTORIZACION" />
                                        <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Detalle">
                                            <ControlStyle CssClass="boton" />
                                            <ItemStyle CssClass="70px" Width="70px" />
                                        </asp:ButtonField>
                                    </Columns>
                                   <EmptyDataRowStyle Font-Bold="True" Font-Size="13px" ForeColor="#CC3300" 
                                        HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
