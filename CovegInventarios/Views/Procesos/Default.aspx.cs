﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;
using Coveg.Catalogos;
using F;

namespace CovegInventarios
{
    public partial class _Default : System.Web.UI.Page
    {
        #region Properties
        Coveg.Procesos.Controller.SOLICITUDSALIDAController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Procesos.Controller.SOLICITUDSALIDAController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Procesos.Controller.SOLICITUDSALIDAController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private int selectedRow
        {
            get
            {
                if (Page.Session["SelectedROW"] == null)
                {
                    Page.Session["SelectedROW"] = -1;
                }
                return (Int32)Page.Session["SelectedROW"];
            }
            set
            {
                Page.Session["SelectedROW"] = value;
            }
        }
        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.SOLICITUDSALIDA_BE> listaSolicitudes
        {
            get
            {
                return (List<Coveg.Entities.SOLICITUDSALIDA_BE>)Page.Session["listaSolicitudesUsuario"];
            }
            set
            {
                Page.Session["listaSolicitudesUsuario"] = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        private List<Coveg.Entities.SOLICITUDSALIDA_BE> listaAutorizaciones
        {
            get
            {
                return (List<Coveg.Entities.SOLICITUDSALIDA_BE>)Page.Session["listaAutorizacionesUsuario"];
            }
            set
            {
                Page.Session["listaAutorizacionesUsuario"] = value;
            }
        }

        #endregion

        #region Eventos
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void Page_Load(object sender, EventArgs e)
            {
                MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
                try
                {
                    if (!IsPostBack)
                    {
                        Page.Session["SortDirection"] = "Ascending";
                        Page.Session["SortExpression"] = "";
                        RefreshSolicitudesUsuario(true);
                        RefreshAutorizacionesUsuario();
                    }
                }
                catch
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
            {
                try
                {
                    selectedRow = -1;
                    GridView1.SelectedIndex = -1;
                    GridView1.DataSource = listaSolicitudes;
                    GridView1.DataBind();
                }
                catch { 
                
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
            {
                if (e.CommandName.ToLower().Equals("delete") )
                {
                    if (F.Text.Input.IsNumeric(e.CommandArgument))
                    {
                        int i = Convert.ToInt32(e.CommandArgument);
                        if (Controller.FinalizarSolicitud(listaSolicitudes[i]))
                        {
                            
                            listaSolicitudes.RemoveAt(i);
                        }
                    }
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                GridView1.PageIndex = e.NewPageIndex;
                GridView1.DataSource = listaSolicitudes;
                GridView1.DataBind();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                GridView2.PageIndex = e.NewPageIndex;
                GridView2.DataSource = listaAutorizaciones;
                GridView2.DataBind();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
            {
                selectedRow = GridView1.SelectedRow.DataItemIndex;
                Coveg.Entities.SOLICITUDSALIDA_BE be = listaSolicitudes[selectedRow];
                Session["SOLICITUDIDDETALLE"] = be.SOLICITUDSALIDAID;
                Response.Redirect("../Reportes/DetalleSolicitud.aspx");
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
            {
                selectedRow = GridView2.SelectedRow.DataItemIndex;
                Coveg.Entities.SOLICITUDSALIDA_BE be = listaAutorizaciones[selectedRow];

                Session["DETALLEAUTORIZA"] = be.SOLICITUDSALIDAID;

                if(be.TIPO_AUTORIZACION =="RESGUARDO")
                    Response.Redirect("AutorizacionSalida.aspx?Opcion=Resguardo");

                if (be.TIPO_AUTORIZACION == "JEFATURA")
                    Response.Redirect("AutorizacionSalida.aspx?Opcion=Jefe");
                
                if (be.TIPO_AUTORIZACION == "DIRECTOR")
                    Response.Redirect("AutorizacionSalida.aspx?Opcion=Director");
                    
                if (be.TIPO_AUTORIZACION == "AUTOMOVIL")
                    Response.Redirect("AutorizacionSalidaAutos.aspx");

                if (be.TIPO_AUTORIZACION == "ARCHIVO")
                    Response.Redirect("AutorizacionSalidaDoctos.aspx");
   
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                    e.Row.Cells[1].Text = Convert.ToDateTime(e.Row.Cells[1].Text).ToShortDateString();
                }
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                    e.Row.Cells[1].Text = Convert.ToDateTime(e.Row.Cells[1].Text).ToShortDateString();
                }
            }

        #endregion

        #region Sorting Commands

            /// <summary>
            /// Ordenar grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (listaSolicitudes == null || listaSolicitudes.Count == 0)
                    return;

                if (e.SortExpression == "SOLICITUDSALIDAID")
                {
                    Page.Session["SortExpression"] = "SOLICITUDSALIDAID";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = listaSolicitudes.OrderBy(p => p.SOLICITUDSALIDAID);
                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = listaSolicitudes.OrderByDescending(p => p.SOLICITUDSALIDAID);
                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "FECHA")
                    {
                        Page.Session["SortExpression"] = "FECHA";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = listaSolicitudes.OrderBy(p => p.FECHA);
                            listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = listaSolicitudes.OrderByDescending(p => p.FECHA);
                            listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }
                    else
                        if (e.SortExpression == "MOTIVO")
                        {
                            Page.Session["SortExpression"] = "MOTIVO";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = listaSolicitudes.OrderBy(p => p.MOTIVO);
                                listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = listaSolicitudes.OrderByDescending(p => p.MOTIVO);
                                listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }
                        else
                            if (e.SortExpression == "DESC_ESTATUS")
                            {
                                Page.Session["SortExpression"] = "DESC_ESTATUS";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = listaSolicitudes.OrderBy(p => p.DESC_ESTATUS);
                                    listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = listaSolicitudes.OrderByDescending(p => p.DESC_ESTATUS);
                                    listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }
                            else
                                if (e.SortExpression == "TIPO")
                                {
                                    Page.Session["SortExpression"] = "TIPO";
                                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                    {
                                        var v = listaSolicitudes.OrderBy(p => p.TIPO);
                                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                        Page.Session["SortDirection"] = "Descending";
                                    }
                                    else
                                    {
                                        var v = listaSolicitudes.OrderByDescending(p => p.TIPO);
                                        listaSolicitudes = v.ToList<SOLICITUDSALIDA_BE>();
                                        Page.Session["SortDirection"] = "Ascending";
                                    }
                                }

                GridView1.SelectedIndex = -1;
                GridView1.DataSource = listaSolicitudes;
                GridView1.DataBind();
            }

            protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
            {
                if (listaAutorizaciones == null || listaAutorizaciones.Count == 0)
                    return;

                if (e.SortExpression == "SOLICITANTE")
                {
                    Page.Session["SortExpression"] = "SOLICITANTE";
                    if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                    {
                        var v = listaAutorizaciones.OrderBy(p => p.SOLICITANTE);
                        listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                        Page.Session["SortDirection"] = "Descending";
                    }
                    else
                    {
                        var v = listaAutorizaciones.OrderByDescending(p => p.SOLICITANTE);
                        listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                        Page.Session["SortDirection"] = "Ascending";
                    }
                }
                else
                    if (e.SortExpression == "FECHA")
                    {
                        Page.Session["SortExpression"] = "FECHA";
                        if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                        {
                            var v = listaAutorizaciones.OrderBy(p => p.FECHA);
                            listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                            Page.Session["SortDirection"] = "Descending";
                        }
                        else
                        {
                            var v = listaAutorizaciones.OrderByDescending(p => p.FECHA);
                            listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                            Page.Session["SortDirection"] = "Ascending";
                        }
                    }

                    else
                        if (e.SortExpression == "TIPO_AUTORIZACION")
                        {
                            Page.Session["SortExpression"] = "TIPO_AUTORIZACION";
                            if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                            {
                                var v = listaAutorizaciones.OrderBy(p => p.TIPO_AUTORIZACION);
                                listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                                Page.Session["SortDirection"] = "Descending";
                            }
                            else
                            {
                                var v = listaAutorizaciones.OrderByDescending(p => p.TIPO_AUTORIZACION);
                                listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                                Page.Session["SortDirection"] = "Ascending";
                            }
                        }

                        else
                            if (e.SortExpression == "TIPO")
                            {
                                Page.Session["SortExpression"] = "TIPO";
                                if (Page.Session["SortDirection"].ToString() == SortDirection.Ascending.ToString())
                                {
                                    var v = listaAutorizaciones.OrderBy(p => p.TIPO);
                                    listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                                    Page.Session["SortDirection"] = "Descending";
                                }
                                else
                                {
                                    var v = listaAutorizaciones.OrderByDescending(p => p.TIPO);
                                    listaAutorizaciones = v.ToList<SOLICITUDSALIDA_BE>();
                                    Page.Session["SortDirection"] = "Ascending";
                                }
                            }

                GridView2.SelectedIndex = -1;
                GridView2.DataSource = listaAutorizaciones;
                GridView2.DataBind();
            }

            #endregion

        #region Funciones

            private void RefreshAutorizacionesUsuario()
            {
                List<SOLICITUDSALIDA_BE> lstActivos = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> lstDoctos = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> lstAutos = new List<SOLICITUDSALIDA_BE>();

                lstActivos = ObtenerListaPendientesActivos();
                lstDoctos = ObtenerListaPendientesDoctos();
                lstAutos = ObtenerListaPendientesAutos();

                if (listaAutorizaciones == null)
                    listaAutorizaciones = new List<SOLICITUDSALIDA_BE>();
                else
                    listaAutorizaciones.Clear();

                if (lstActivos != null && lstActivos.Count>0)
                    listaAutorizaciones.AddRange(lstActivos);

                if (lstDoctos != null && lstDoctos.Count >0)
                    listaAutorizaciones.AddRange(lstDoctos);

                if (lstAutos != null && lstAutos.Count >0)
                    listaAutorizaciones.AddRange(lstAutos);

                listaAutorizaciones = listaAutorizaciones.OrderByDescending(p => p.SOLICITUDSALIDAID).ToList();

                GridView2.DataSource = listaAutorizaciones;
                GridView2.DataBind();
            }

            /// <summary>
            ///
            /// </summary>
            public void RefreshSolicitudesUsuario(bool requery)
            {
                SOLICITUDSALIDA_BE objElemento = new SOLICITUDSALIDA_BE();
                objElemento.USUARIO = (Session["USUARIO"]??string.Empty ).ToString();

                if (requery || listaSolicitudes == null)
                    listaSolicitudes = Controller.SelectSolicitudesUsuarioNoDevueltas(objElemento);

                selectedRow = -1;

                GridView1.SelectedIndex = -1;
                GridView1.DataSource = listaSolicitudes;
                GridView1.DataBind();
            }

        #endregion

        #region ICatalogoView<> Members


            void MessageBox1_Accepted(object sender, EventArgs e)
            {
                panelMessageBox.Visible = false;
            }

            /// <summary>
            ///
            /// </summary>
            /// <param name="message"></param>
            public void RaiseUserError(string message)
            {
                MessageBox1.Initilize("Error", "Error", message);
                panelMessageBox.Visible = true;
            }

            /// <summary>
            ///
            /// </summary>
            public void NotifyUser(string message)
            {
                MessageBox1.Initilize("Information", "Información", message);
                panelMessageBox.Visible = true;
            }

        #endregion

        #region Lista de Autorizaciones Pendientes

            public List<SOLICITUDSALIDA_BE> ObtenerListaPendientesAutos()
            {
                List<SOLICITUDSALIDA_BE> listaAutos = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> listaAutosFinal = new List<SOLICITUDSALIDA_BE>();
                SOLICITUDSALIDA_BE objSolicitud = new SOLICITUDSALIDA_BE();

                int intPosicion, intPosicion2;

                intPosicion = (Session["ROL"]??string.Empty ).ToString().LastIndexOf("AUTO");
                intPosicion2 =  (Session["ROL"]??string.Empty ).ToString().LastIndexOf("ADMIN");

                if (intPosicion != -1 || intPosicion2 != -1)
                {
                    objSolicitud.IDJEFEINMEDIATO = (int)(Session["IDUSUARIO"] ?? "0");
                    objSolicitud.USUARIO = (Session["USUARIO"]??string.Empty ).ToString();

                    listaAutos = Controller.SelectSolicitudesAutos(objSolicitud);

                    if (listaAutos != null)
                        if (listaAutos.Count > 0)
                            listaAutosFinal = (from n in listaAutos
                                               select new SOLICITUDSALIDA_BE(n.SOLICITUDSALIDAID, n.FECHA, n.NOMBRE, "AUTOMOVIL", n.TIPO)).ToList();

                    return listaAutosFinal;

                }
                else
                    return null;



            }
            /// <summary>
            /// Pendientes de autorizar doctos
            /// </summary>
            /// <returns></returns>
            public List<SOLICITUDSALIDA_BE> ObtenerListaPendientesDoctos()
            {
                List<SOLICITUDSALIDA_BE> listaDoctos = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> listaDoctosFinal = new List<SOLICITUDSALIDA_BE>();
                SOLICITUDSALIDA_BE objSolicitud = new SOLICITUDSALIDA_BE();


                int intPosicion, intPosicion2;

                intPosicion = (Session["ROL"] ?? string.Empty).ToString().LastIndexOf("DOCTOS");
                intPosicion2 =  (Session["ROL"]??string.Empty ).ToString().LastIndexOf("ADMIN");

                if (intPosicion != -1 || intPosicion2 != -1)
                {

                    objSolicitud.IDJEFEINMEDIATO = (int)(Session["IDUSUARIO"] ?? "0");
                    objSolicitud.USUARIO = (Session["USUARIO"]??string.Empty ).ToString();

                    listaDoctos = Controller.SelectSolicitudesDocumentos(objSolicitud);

                    if (listaDoctos != null)
                        if (listaDoctos.Count > 0)
                            listaDoctosFinal = (from n in listaDoctos
                                                select new SOLICITUDSALIDA_BE(n.SOLICITUDSALIDAID, n.FECHA, n.NOMBRE, "ARCHIVO", n.TIPO)).ToList();

                    return listaDoctosFinal;
                }
                else
                    return null;
            }

            public List<SOLICITUDSALIDA_BE> ObtenerListaPendientesActivos()
            {
                List<SOLICITUDSALIDA_BE> listaRolResguardo = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> listaRolJefe = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> listaRolDirector = new List<SOLICITUDSALIDA_BE>();

                List<SOLICITUDSALIDA_BE> lista1 = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> lista2 = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> lista3 = new List<SOLICITUDSALIDA_BE>();
                List<SOLICITUDSALIDA_BE> listaFinal = new List<SOLICITUDSALIDA_BE>();

                SOLICITUDSALIDA_BE objSolicitud = new SOLICITUDSALIDA_BE();

                //Variables para busqueda de solicitudes ---------------------
                objSolicitud.IDJEFEINMEDIATO = (int)(Session["IDUSUARIO"] ?? "0");
                objSolicitud.USUARIO = (Session["USUARIO"]??string.Empty ).ToString();
                //---------------------------------------------------------


                listaRolResguardo = Controller.SelectSolicitudesResguardo(objSolicitud);
                listaRolJefe = Controller.SelectSolicitudesJefe(objSolicitud);
                listaRolDirector = Controller.SelectSolicitudesDireccion(objSolicitud);

                if (listaRolResguardo != null)
                    if (listaRolResguardo.Count > 0)
                        lista1 = (from n in listaRolResguardo
                                   select new SOLICITUDSALIDA_BE(n.SOLICITUDSALIDAID,n.FECHA, n.NOMBRE, "RESGUARDO",n.TIPO)).ToList();

                if (listaRolJefe != null)
                    if (listaRolJefe.Count > 0)
                        lista2 = (from n in listaRolJefe
                                  select new SOLICITUDSALIDA_BE(n.SOLICITUDSALIDAID, n.FECHA, n.NOMBRE, "JEFATURA", n.TIPO)).ToList();

                if (listaRolDirector != null)
                    if (listaRolDirector.Count > 0)
                        lista3 = (from n in listaRolDirector
                                  select new SOLICITUDSALIDA_BE(n.SOLICITUDSALIDAID, n.FECHA, n.NOMBRE, "DIRECTOR", n.TIPO)).ToList();

                if (lista1 != null)
                    listaFinal.AddRange(lista1);

                if (lista2 != null)
                    listaFinal.AddRange(lista2);

                if (lista3 != null)
                    listaFinal.AddRange(lista3);

                return listaFinal;
            }

        #endregion

            

    }
}
