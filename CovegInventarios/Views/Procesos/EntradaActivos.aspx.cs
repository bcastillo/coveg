﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views.Procesos
{
    public partial class EntradaActivos : System.Web.UI.Page
    {
        #region Properties

            Coveg.Procesos.Controller.ENTRADAController _controller;
            /// <summary>
            ///
            /// </summary>
            protected Coveg.Procesos.Controller.ENTRADAController Controller
            {
                get
                {
                    if (_controller == null)
                    {
                        _controller = new Coveg.Procesos.Controller.ENTRADAController();
                    }

                    return _controller;
                }
                set
                {
                    _controller = value;
                }
            }

            private ACTIVO_BUSQUEDA ActivoBusquedaEntrada
            {
                get 
                {
                    return (ACTIVO_BUSQUEDA)Page.Session["ActivoBusquedaEntrada"]; 
                }
                set 
                { 
                        Page.Session["ActivoBusquedaEntrada"] = value; 
                }
            }

            private ACTIVO_BUSQUEDA InfoSolicitudEntrada
            {
                get 
                {
                    return (ACTIVO_BUSQUEDA)Page.Session["InfoSolicitudEntrada"]; 
                }
                set 
                {
                    Page.Session["InfoSolicitudEntrada"] = value; 
                }
            }

        

            private Boolean RecargarPantallaEntrada
            {
                get
                {
                    return (Boolean)Page.Session["RecargarPantallaEntrada"];
                }
                set
                {
                    Page.Session["RecargarPantallaEntrada"] = value;
                }
            }

        #endregion

        #region Eventos
        
        protected void Page_Load(object sender, EventArgs e)
        {
            MessageBox1.Accepted += new EventHandler(MessageBox1_Accepted);
            txtPopObservaciones.ConfirmAccept += new EventHandler(TextPopup1_Accepted);
            txtPopObservaciones.ConfirmCancel += new EventHandler(TextPopup1_Cancelled);
            txtPopObservaciones.Initilize(string.Empty, "Entrada", "Observaciones de Activo");
            

            
            try
            {
                if (!IsPostBack)
                {

                }
            }
            catch
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errConsulteAdministrador"]);
            }
        }
        
        /// <summary>
        /// Aceptar de caja de mensajes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MessageBox1_Accepted(object sender, EventArgs e)
        {
            panelMessageBox.Visible = false;
            if (RecargarPantallaEntrada)
            {
                mpDetalleActivo.Show();
                RecargarPantallaEntrada = false;
                txtUsuarioIngresa.Focus();
            }
        }

        /// <summary>
        /// Cancelar ingreso
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdCancelar_Click(object sender, EventArgs e)
        {
            ClearControls();
            mpDetalleActivo.Hide();   
        }


        /// <summary>
        /// Consultar información de activo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdConsultar_Click(object sender, EventArgs e)
        {
            Page.Session["RecargarPantallaEntrada"] = false;
            ClearControls();

            if (ValidateControls())
            {
                if (ActivoBusquedaEntrada == null)
                    ActivoBusquedaEntrada = new ACTIVO_BUSQUEDA();

                var tokens= Coveg.Common.Text.ObtenerTokens (txtClaveActivoBusqueda.Text);

                if (tokens != null && tokens.Length > 0)
                {
                    ActivoBusquedaEntrada.CLAVE = tokens[0].Trim ();
                }
                else
                {
                    ActivoBusquedaEntrada.CLAVE = txtClaveActivoBusqueda.Text;
                }
                

                ActivoBusquedaEntrada = Controller.ActivoEntrada(ActivoBusquedaEntrada);
                

                if (ActivoBusquedaEntrada != null)
                {
                    InfoSolicitudEntrada = Controller.InfoSolicitudEntrada(ActivoBusquedaEntrada);

                    AsignarSolicitud(InfoSolicitudEntrada);

                    if (ActivoBusquedaEntrada.DESC_TIPOACTIVO == "AUTO")
                        AsignarActivoAutomovil(ActivoBusquedaEntrada);
                    else
                        if (ActivoBusquedaEntrada.DESC_TIPOACTIVO == "DOCUMENTO")
                            AsignarActivoDocto(ActivoBusquedaEntrada);
                        else
                            AsignarActivo(ActivoBusquedaEntrada);


                    TipoSolicitudControles(ActivoBusquedaEntrada.DESC_TIPOACTIVO);
                    mpDetalleActivo.Show();
                }
                else
                {
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errNoEncontroActivo"]);
                    txtClaveActivoBusqueda.Text = String.Empty;
                   
                }
            }
            else
            {
                RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errIngreseCriterioBusqueda"]);
            }
        }

        /// <summary>
        /// Registrar ingreso de activo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void cmdEntrada_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidateControlIngreso())
                    mpEditComentarios.Show();
                else
                {
                    NotifyUser(System.Configuration.ConfigurationManager.AppSettings["errIngresePersonaIngresa"]);
                    mpDetalleActivo.Show();
                    RecargarPantallaEntrada = true;
                }
            }
            catch (Exception ex)
            {
                RaiseUserError(ex.Message);
            }
        }

        private Boolean ValidateControlIngreso()
        {
            if (string.IsNullOrEmpty(txtUsuarioIngresa.Text))
                return false;
            else
                return true;
        }

        /// <summary>
        /// Cancelar ventana de textpop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TextPopup1_Cancelled(object sender, EventArgs e)
        {
            txtPopObservaciones.AsignarValor = String.Empty;
            mpDetalleActivo.Show();
        }

        /// <summary>
        /// Aprobar ventana de textpop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void TextPopup1_Accepted(object sender, EventArgs e)
        {

            DETALLEENTRADA_BE objDetalleEntrada = new DETALLEENTRADA_BE();

            try
            {
                var tokens = Coveg.Common.Text.ObtenerTokens(txtUsuarioIngresa.Text);

                if (tokens != null && tokens.Length > 0)
                    objDetalleEntrada.USUARIO = tokens[0];
                else
                    objDetalleEntrada.USUARIO = txtUsuarioIngresa.Text;

                objDetalleEntrada.FECHA = DateTime.Now;                
                objDetalleEntrada.ACTIVOID = ActivoBusquedaEntrada.ACTIVOID;
                objDetalleEntrada.OBSERVACION = txtPopObservaciones.TextPopupText;

                if (Controller.RegistrarEntrada(objDetalleEntrada))
                    NotifyUser(System.Configuration.ConfigurationManager.AppSettings["msgEntradaRegistrada"]);
                else
                    RaiseUserError(System.Configuration.ConfigurationManager.AppSettings["errErrorEntrada"]);
            }
            catch (Exception ex)
            {
                RaiseUserError(ex.Message);
            }

        }

        #endregion

        #region Funciones
        

        /// <summary>
        /// Ver controles de tipo de solicitud
        /// </summary>
        private void TipoSolicitudControles(String strTipo)
        {
            if (strTipo == "DOCUMENTO")
            {
                fsActivo.Visible = false;
                fsAutomovil.Visible = false;
                fsDocumento.Visible = true;
            }
            else
            {
                if (strTipo == "AUTO")
                {
                    fsActivo.Visible = false;
                    fsAutomovil.Visible = true;
                    fsDocumento.Visible = false;
                }
                else
                {
                    fsActivo.Visible = true;
                    fsAutomovil.Visible = false;
                    fsDocumento.Visible = false;
                }
            }


        }

        

        /// <summary>
        /// Limpiar controles
        /// </summary>
        private void ClearControls()
        {
            txtUsuarioSolicito.Text=String.Empty;
            txtUsuarioIngresa.Text = String.Empty;
            txtUsuarioSalida.Text = String.Empty;

            txtFecha.Text=String.Empty;
            txtFechaSalida.Text=String.Empty;
            txtFechaRetorno.Text=String.Empty;
            txtMotivo.Text=String.Empty;
            txtNombre.Text=String.Empty;
            txtNumSerieActivo.Text=String.Empty;
            txtDescripcionActivo.Text=String.Empty;
            txtClaveActivo.Text=String.Empty;
            txtFechaIngreso.Text=String.Empty;
            txtTipo.Text=String.Empty;
            txtArea.Text=String.Empty;
            txtCaja.Text=String.Empty;
            txtClave.Text=String.Empty;
            txtEstante.Text=String.Empty;
            txtCarpeta.Text=String.Empty;
            txtNumeroSerie.Text=String.Empty;
            txtNumeroMotor.Text=String.Empty;
            txtNoPuertas.Text=String.Empty;
            txtCilindros.Text=String.Empty;
            txtCapacidad.Text=String.Empty;
            txtModelo.Text=String.Empty;
            txtPlacas.Text=String.Empty;
            txtMarca.Text=String.Empty;
            txtTipoAutomovil.Text=String.Empty;
            txtColor.Text=String.Empty;
            txtTransmision.Text = String.Empty;
        }

        /// <summary>
        /// Valida que exista un criterio de busqueda
        /// </summary>
        /// <returns></returns>
        public bool ValidateControls()
        {
            Boolean bolRes = false;

            bolRes = !string.IsNullOrEmpty(txtClaveActivoBusqueda.Text);

            return bolRes;

        }


        /// <summary>
        /// Mensaje de error
        /// </summary>
        /// <param name="message"></param>
        public void RaiseUserError(string message)
        {
            MessageBox1.Initilize("Error", "Error", message);
            panelMessageBox.Visible = true;
        }

        /// <summary>
        /// Notificación a usuario
        /// </summary>
        public void NotifyUser(string message)
        {
            MessageBox1.Initilize("Information", "Información", message);
            panelMessageBox.Visible = true;
        }


        /// <summary>
        /// Asignar datos de una solicitud a los controles
        /// </summary>
        /// <param name="objActivo"></param>
        public void AsignarSolicitud(ACTIVO_BUSQUEDA objActivo)
        {
            if (objActivo != null)
            {
                cmdEntrada.Visible = true;
                txtUsuarioSalida.Text = objActivo.USUARIO_SALIDA;
                txtUsuarioSolicito.Text = objActivo.USUARIO_SOLICITO;

                if (objActivo.FECHA_SOLICITUD != new DateTime())
                    txtFecha.Text = objActivo.FECHA_SOLICITUD.ToShortDateString();

                if (objActivo.FECHA_SALIDA != new DateTime())
                    txtFechaSalida.Text = objActivo.FECHA_SALIDA.ToShortDateString();

                if (objActivo.FECHARETORNO != new DateTime())
                    txtFechaRetorno.Text = objActivo.FECHARETORNO.ToShortDateString();

                txtMotivo.Text = objActivo.MOTIVO;
            }
            else
            {
                cmdEntrada.Visible = false;
            }
        }

        /// <summary>
        /// Asignar activo
        /// </summary>
        /// <param name="objActivo"></param>
        public void AsignarActivo(ACTIVO_BUSQUEDA objActivo)
        {
            txtNombre.Text = objActivo.NOMBRE;
            txtDescripcionActivo.Text = objActivo.DESCRIPCION;
            txtNumSerieActivo.Text = objActivo.NUMEROSERIE;
            txtClaveActivo.Text = objActivo.CLAVE;
        }

        /// <summary>
        /// Asignar valores de documento a controles
        /// </summary>
        /// <param name="objActivo"></param>
        public void AsignarActivoDocto(ACTIVO_BUSQUEDA objActivo)
        {
            if(objActivo.FECHA_INGRESO!= new DateTime())
                txtFechaIngreso.Text = objActivo.FECHA_INGRESO.ToShortDateString();

            txtTipo.Text = objActivo.TIPO_DOCUMENTO;
            txtArea.Text = objActivo.AREA;
            txtCaja.Text = objActivo.CAJA;
            txtClave.Text = objActivo.CLAVE;
            txtEstante.Text = objActivo.ESTANTE;
            txtCarpeta.Text = objActivo.LEFFORT;
        }

        /// <summary>
        /// Asignar valores de automoviles a controles
        /// </summary>
        /// <param name="objActivo"></param>
        public void AsignarActivoAutomovil(ACTIVO_BUSQUEDA objActivo)
        {
            txtNumeroSerie.Text = objActivo.NUMERO_SERIE_AUTOMOVIL;
            txtNumeroMotor.Text = objActivo.NUMERO_MOTOR;
            txtPlacas.Text = objActivo.PLACAS;
            txtNoPuertas.Text = objActivo.PUERTAS.ToString();
            txtMarca.Text = objActivo.MARCA;
            txtCilindros.Text = objActivo.CILINDROS.ToString();
            txtTipoAutomovil.Text = objActivo.TIPO;
            txtCapacidad.Text = objActivo.CAPACIDAD;
            txtColor.Text = objActivo.COLOR;
            txtModelo.Text = objActivo.MODELO;
            txtTransmision.Text = objActivo.TRANSMISION;
        }
        #endregion

        #region Search Methods
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerActivos(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("SELECT clave+' | '+descripcion CLAVE FROM activo WITH(NOLOCK) WHERE clave LIKE '" + prefixText + "%' or descripcion LIKE '" + prefixText + "%' ");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["CLAVE"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string[] ObtenerUsuarios(string prefixText, int count)
        {
            List<string> res = new List<string>();

            try
            {
                if (!string.IsNullOrEmpty(prefixText))
                {

                    var dt = Coveg.Data.OpenData.OpenQuery("select usuario + ' | ' + nombre +' ' + apellidos  usuario from vwUsuario  WITH(NOLOCK) WHERE usuario LIKE '" + prefixText + "%' or nombre LIKE '" + prefixText + "%'");

                    if (F.Data.Common.DataObjectsHelper.TableHasRecords(dt))
                    {
                        string itm = string.Empty;
                        res.Clear();
                        foreach (System.Data.DataRow row in dt.Rows)
                        {
                            itm = F.Text.Input.IsNothing(row["usuario"], string.Empty).ToString();

                            if (!String.IsNullOrEmpty(itm))
                            {
                                res.Add(itm);
                            }
                        }
                    }

                }
            }
            catch
            {

            }


            return res.ToArray();
        }
        #endregion

        
    }
}