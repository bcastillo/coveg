﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AutorizacionSalidaDoctos.aspx.cs" Inherits="CovegInventarios.Views.Procesos.AutorizacionSalidaDoctos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/Utils/ConfirmBox.ascx" TagName="ConfirmBox" TagPrefix="uc2" %>
<%@ Register Src="~/Utils/TextPopup.ascx" TagName="TextPopup" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" />  
    <style type="text/css">
        .style1
        {
            width: 350px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
        <ContentTemplate>

        <asp:Panel ID="pnl1" runat="server" Height="200" Width="200" CssClass="modalPopup">
            <uc3:TextPopup ID="TextPopup1" runat="server"/>
        </asp:Panel>

        <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
            <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
        </asp:Panel>

        <asp:ModalPopupExtender ID="mpEditComment" PopupControlID="pnl1" TargetControlID="lnkPopup" 
                OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
                BackgroundCssClass="ModalPopupBG" runat="server"  />
        <asp:LinkButton ID="lnkPopup" runat="server" Visible="true"></asp:LinkButton>
        
        <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pblDetalle" TargetControlID="lnkFake" 
                OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
                BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" Enabled="True" />
        <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>

        <asp:Panel ID="pblDetalle" runat="server"  CssClass="shadowPanel">
            <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="800px" >
            <table width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" Width="20px" />&nbsp;
                        <asp:Label ID="lblText" runat="server" Text="Autorización de salida de un documento" 
                            Font-Bold="True" Font-Italic="False" Font-Underline="False" Height="16px" 
                            CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button ID="Button1" runat="server"  Text="Autorizar"  Font-Bold="True" onclick="btnAutorizar_Click" CssClass="boton" />&nbsp;
                        <asp:Button ID="Button2" runat="server" Text="Rechazar" Font-Bold="True" CssClass="boton"  onclick="btnRechazar_Click"  />&nbsp;
                        <asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                    </td>
                </tr>
            </table>
        </asp:Panel>
            
            <asp:Panel ID="pBody" runat="server" CssClass="cpBody" Width="800px">
                <table cellpadding="2" cellspacing="0" class="shadow">
            <tr>
                <td align="left">
                    <table>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblSolicitante" runat="server" Text="Solicitante" 
                                    CssClass="Etiquetas" 
                                    style="font-weight: bold; font-size: 11px; color: #000000;"></asp:Label>
                            </td>
                            <td class="style1">
                                <asp:Label ID="txtSolicitante" runat="server"></asp:Label>
                                </td>
                            <td>
                                <asp:Label ID="lblFecha" runat="server" CssClass="Etiquetas" Text="Fecha" 
                                    style="font-weight: bold; font-size: 11px; color: #000000;"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFecha" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaSalida" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha de salida" 
                                    style="font-weight: bold; font-size: 11px; color: #000000;"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaSalida" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblMotivo" runat="server" CssClass="Etiquetas" Text="Motivo" 
                                    style="font-weight: bold; font-size: 11px; color: #000000;"></asp:Label>
                            </td>
                            <td class="style1" colspan="3">
                                <asp:Label ID="txtMotivo" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaRetorno" runat="server" CssClass="Etiquetas" 
                                    Text="Fecha de retorno" 
                                    style="font-weight: bold; font-size: 11px; color: #000000;"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaRetorno" runat="server"></asp:Label>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblActivos" runat="server" CssClass="Etiquetas" Text="Activos" 
                                    style="font-weight: bold; font-size: 11px; color: #000000;"></asp:Label>
                            </td>
                            <td colspan="5">
                                <br />
                                <div align="center" class="grid" style="width:670px">
                                    <asp:GridView ID="GridView2" runat="server" AllowPaging="True" 
                                        AllowSorting="false" AutoGenerateColumns="False" BackColor="White" 
                                        BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                                        Font-Names="intro_book" onrowdatabound="GridView2_RowDataBound" Width="660px">
                                        <Columns>
                                            <asp:BoundField DataField="ACTIVO_DESC" HeaderText="ACTIVO" 
                                                SortExpression="ACTIVO_DESC" />
                                            <asp:BoundField DataField="AREA" HeaderText="AREA" SortExpression="AREA" />
                                            <asp:BoundField DataField="CAJA" HeaderText="CAJA" SortExpression="CAJA" />
                                            <asp:BoundField DataField="ESTANTE" HeaderText="ESTANTE" 
                                                SortExpression="ESTANTE" />
                                            <asp:BoundField DataField="LEFFORT" HeaderText="LEFFORT" 
                                                SortExpression="LEFFORT" />
                                            <asp:BoundField DataField="TIPO_DOCUMENTO" HeaderText="TIPO DOCTO" 
                                                SortExpression="TIPO_DOCUMENTO" />
                                        </Columns>
                                        <FooterStyle BackColor="White" Font-Names="Intro_Book" ForeColor="#000066" />
                                        <HeaderStyle CssClass="myheader" Font-Bold="True" Font-Names="Intro_Book" 
                                            Font-Size="9pt" ForeColor="#33333f" />
                                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                        <RowStyle Font-Names="Intro_Book" Font-Size="9pt" ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" Font-Names="Intro_Book" 
                                            ForeColor="000066" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                    </asp:GridView>
                                </div>
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
            </asp:Panel>
        </asp:Panel>

    <table>
        <tr>
        <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" />
                            </td>
                            <td width="70%" align="left">&nbsp;
                                <asp:Label ID="lblTituloCatalogo" runat="server" 
                                    Text="Solicitudes de documentos por Autorizar" Font-Bold="True" 
                                    Font-Size="13px" /></td>
                            <td align="right">
                                <asp:Button ID="btnRefresh" runat="server"  Text="Refrescar"  Font-Bold="True" OnClick="btnRefresh_Click" CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        <tr>
            <td align="center">
            <br />
            <div class="grid">
            <tr>
                <td class="panel6" valign="middle">
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound" 
                        Font-Names="intro_book">
                        <Columns>
                             <asp:TemplateField HeaderText="">
                                    <itemtemplate>
                                        <asp:ImageButton ID="imgGraficaSCR" runat="server" ImageUrl="~/Images/report.gif"  onClientClick=<%# string.Format("window.open('../Reportes/ReporteDetalleSolicitudPop.aspx?SOLICITUDID={0}','Detalle','menubar=no,scrollbars=yes,status=no,location=no,height=500,width=930');", Eval("SOLICITUDSALIDAID")) %>/>
                                    </itemtemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID" />
                            <asp:BoundField DataField="USUARIO" HeaderText="USUARIO" SortExpression="USUARIO" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE" />
                            <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" />
                            <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                            <asp:BoundField DataField="DESC_ESTATUS" HeaderText="DESC_ESTATUS" SortExpression="DESC_ESTATUS" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Seleccionar">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
                </td>
            </tr>        
            </div>
        </td>
        </tr>

    </table>
                
</ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>
