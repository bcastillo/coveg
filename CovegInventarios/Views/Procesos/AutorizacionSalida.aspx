﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AutorizacionSalida.aspx.cs" Inherits="CovegInventarios.Views.Procesos.AutorizacionSalida" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Utils/MessageBox.ascx" TagName="MessageBox" TagPrefix="uc1" %>
<%@ Register Src="~/Utils/TextPopup.ascx" TagName="TextPopup" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headContent" runat="server">
    <title>Control de Inventario</title>
    <link href="/Styles/MasterSheet1.css" rel="stylesheet" type="text/css" />
    <link href="/Styles/Style1.css" rel="Stylesheet" type="text/css" />  
    <style type="text/css">
        .style1
        {
            font-family: Intro_Book;
            font-size: 11px;
            color: Black;
            font-weight: bold;
        }
        .style2
        {
            text-align: left;
        }
        .style3
        {
           
        }
        .style4
        {
          width:350px;
        }
        .style5
        {
        }
        .Etiquetas
        {
            font-weight: 700;
            font-size: 11px;
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div align="center">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">  
        <ContentTemplate>

    <asp:Panel ID="panelMessageBox" runat="server" Visible="false">
        <uc1:MessageBox ID="MessageBox1" runat="server" Visible="true" />
    </asp:Panel>

    <asp:Panel ID="pnl1" runat="server" Height="200" Width="200" CssClass="modalPopup">
        <uc3:TextPopup ID="TextPopup1" runat="server"/>
    </asp:Panel>
    
    <asp:ModalPopupExtender ID="mpEditComment" PopupControlID="pnl1" TargetControlID="lnkPopup" 
        OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
        BackgroundCssClass="ModalPopupBG" runat="server"  />
    <asp:LinkButton ID="lnkPopup" runat="server" Visible="true"></asp:LinkButton>

   <asp:ModalPopupExtender ID="mpEditarElemento"  PopupControlID="pblDetalle" 
            TargetControlID="lnkFake" OkControlID="ButtonOk" CancelControlID="ButtonCancel" 
            BackgroundCssClass="ModalPopupBG" runat="server" DynamicServicePath="" 
            Enabled="True" />
   <asp:LinkButton ID="lnkFake" runat="server" ></asp:LinkButton>

        <asp:Panel ID="pblDetalle" runat="server"  CssClass="shadowPanel">
            <asp:Panel ID="pHeader" runat="server" CssClass="cpHeader" Width="800px" >
            <table width="100%">
                <tr>
                    <td>
                        <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/Update.png" Width="20px" />&nbsp;
                        <asp:Label ID="lblText" runat="server" 
                            Text="Autorización de salida de un activo" Font-Bold="True" Font-Italic="False" 
                            Font-Underline="False" Height="16px" CssClass="whitetitle" />
                    </td>
                    <td align="right">
                        <asp:Button ID="btnAutorizar" runat="server"  Text="Autorizar"  Font-Bold="True" onclick="btnAutorizar_Click" CssClass="boton" />&nbsp;
                        <asp:Button ID="btnRechazar" runat="server" Text="Rechazar" Font-Bold="True" CssClass="boton"  onclick="btnRechazar_Click"  />&nbsp;
                        <asp:Button runat="server" ID="cmdCancelarPop" Text="Cancelar" Font-Bold="True" OnClick="cmdCancelarPop_Click" CssClass="boton"/>
                    </td>
                </tr>
            </table>
        </asp:Panel>
            
            <asp:Panel ID="pBody" runat="server" CssClass="cpBody" Width="800px">
                <table cellpadding="2" cellspacing="0" class="shadow">
                    <tr>
                <td align="left">
                    <table>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblSolicitante" runat="server" Text="Solicitante" 
                                    CssClass="style1"></asp:Label>
                            </td>
                            <td >
                                <asp:Label ID="txtSolicitante" runat="server"></asp:Label>
                                </td>
                            <td class="style2">
                                <asp:Label ID="lblFecha" runat="server" CssClass="style1" Text="Fecha"></asp:Label>
                            </td>
                            <td class="style2">
                                <asp:Label ID="txtFecha" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblFechaSalida" runat="server" CssClass="style1" 
                                    Text="Fecha de salida"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaSalida" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr style="height:35px">
                            <td>
                                <asp:Label ID="lblMotivo" runat="server" CssClass="style1" 
                                    Text="Motivo de la salida"></asp:Label>
                            </td>
                            <td class="style4" colspan="3">
                                <asp:Label ID="txtMotivo" runat="server"></asp:Label>
                            </td>
                            <td class="style2">
                                <asp:Label ID="lblFechaRetorno" runat="server" CssClass="style1" 
                                    Text="Fecha retorno"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="txtFechaRetorno" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblActivos" runat="server" CssClass="Etiquetas" Text="Activos"></asp:Label>
                            </td>
                            <td colspan="5">
                            <br />
                            <div class="grid" align="center" style='width:610px'>
                                <asp:GridView ID="GridView2" 
                                    runat="server" 
                                    CellPadding="3" 
                                    AllowPaging="True" 
                                    AllowSorting="false"
                                    AutoGenerateColumns="False"  
                                    BackColor="White" 
                                    BorderColor="#CCCCCC" 
                                    BorderStyle="None" 
                                    BorderWidth="1px" 
                                    onrowdatabound="GridView2_RowDataBound" 
                                    Font-Names="intro_book"
                                    Width="600px">
                                    <Columns>
                                        <asp:BoundField DataField="ACTIVO_DESC" HeaderText="ACTIVO" SortExpression="ACTIVO_DESC" />
                                        <asp:BoundField DataField="CLAVE" HeaderText="CLAVE" SortExpression="CLAVE" />
                                        <asp:BoundField DataField="TIPO_ACTIVO" HeaderText="TIPO DE ACTIVO" SortExpression="TIPO_ACTIVO" />
                                        <asp:BoundField DataField="RESPONSABLE" HeaderText="RESPONSABLE" SortExpression="RESPONSABLE" />
                                    </Columns>
                                    <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                                    <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                                    <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                                    <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                                    <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                                </asp:GridView>
                            </div>
                            <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
                </table>
            </asp:Panel>
        </asp:Panel>

        <table>
            <tr>
                <td valign="middle" class="cpHeaderCatalogos" >
                    <table width="100%">
                        <tr>
                            <td>
                            <asp:Image ID="imgTool" runat="server" ImageUrl="~/Images/Tools.png" Width="20px" />
                            </td>
                            <td width="70%" align="left">&nbsp;
                                <asp:Label ID="lblTituloCatalogo" runat="server" Text="Solicitudes por Autorizar" Font-Bold="True" Font-Size="13px" /></td>
                            <td align="right">
                                <asp:Button ID="btnRefresh" runat="server"  Text="Refrescar"  Font-Bold="True" OnClick="btnRefresh_Click" CssClass="boton"/>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
            <tr>
                <td align="center">
                <br />
                <div class="grid">
                <asp:GridView ID="GridView1" 
                        runat="server" 
                        CellPadding="3" 
                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                        AllowPaging="True" 
                        AllowSorting="true"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        onsorting="GridView1_Sorting" 
                        AutoGenerateColumns="False"  
                        BackColor="White" 
                        BorderColor="#CCCCCC" 
                        BorderStyle="None" 
                        BorderWidth="1px" 
                        onrowdatabound="GridView1_RowDataBound" 
                        Font-Names="intro_book">
                        <Columns>
                             <asp:TemplateField HeaderText="">
                                    <itemtemplate>
                                        <asp:ImageButton ID="imgReporte" runat="server" ImageUrl="~/Images/report.gif"  onClientClick=<%# string.Format("window.open('../Reportes/ReporteDetalleSolicitudPop.aspx?SOLICITUDID={0}','Detalle','menubar=no,scrollbars=yes,status=no,location=no,height=500,width=930');", Eval("SOLICITUDSALIDAID")) %>/>
                                    </itemtemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="SOLICITUDSALIDAID" HeaderText="ID" SortExpression="SOLICITUDSALIDAID" />
                            <asp:BoundField DataField="USUARIO" HeaderText="USUARIO" SortExpression="USUARIO" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" SortExpression="NOMBRE" />
                            <asp:BoundField DataField="FECHA" HeaderText="FECHA" SortExpression="FECHA" />
                            <asp:BoundField DataField="MOTIVO" HeaderText="MOTIVO" SortExpression="MOTIVO" />
                            <asp:BoundField DataField="DESC_ESTATUS" HeaderText="ESTAUTS" SortExpression="DESC_ESTATUS" />
                            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Seleccionar">
                            <ControlStyle CssClass="boton" />
                            <ItemStyle CssClass="70px" Width="70px" />
                            </asp:ButtonField>
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                        <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                        <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                        <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                        <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                    </asp:GridView>
                </div>
                </td>
            </tr>
        </table>

    </ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>
