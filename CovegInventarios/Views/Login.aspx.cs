﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Views
{
    public partial class Login : System.Web.UI.Page
    {

        #region Properties
        Coveg.Procesos.Controller.LOGINController _controller;
        /// <summary>
        ///
        /// </summary>
        protected Coveg.Procesos.Controller.LOGINController Controller
        {
            get
            {
                if (_controller == null)
                {
                    _controller = new Coveg.Procesos.Controller.LOGINController();
                }

                return _controller;
            }
            set
            {
                _controller = value;
            }
        }

        #endregion

        #region Eventos

            protected void Page_Load(object sender, EventArgs e)
            {

            }

            protected void imgLogin_Click(object sender, EventArgs e)
            {
                LOGIN_BE objLogin = new LOGIN_BE();
                objLogin.Usuario = txtUser.Text;
                objLogin.Password =  txtPassword.Text;
                ValidaUsuario(objLogin);
            }

        #endregion


        #region Funciones

            private void ValidaUsuario(LOGIN_BE objBE)
            {
                String strRoles=String.Empty;
                List<LOGIN_BE> objLogin;
                LOGIN_BE objJefeInmediato;

                if (txtUser.Text != String.Empty)
                {
                    if (txtPassword.Text != String.Empty)
                    {
                        objLogin = Controller.SelectAll(objBE);
                        
                        if (objLogin!= null)
                        {
                            objJefeInmediato = Controller.ObtenerJefeInmediato(objLogin[0]);

                            Session["IDUSUARIO"] = objLogin[0].id_usuario;
                            Session["USUARIO"] = objLogin[0].Usuario;
                            Session["NOMBRE"] = objLogin[0].Nombre_Completo;

                            if(objJefeInmediato!=null)
                                Session["IDJEFE"] = objJefeInmediato.id_usuario;

                            //Generar lista de roles
                            foreach (LOGIN_BE item in objLogin)
                            {
                                if (strRoles == String.Empty)
                                    strRoles = item.Rol;
                                else
                                    strRoles = strRoles + "," + item.Rol;
                            }

                            if (strRoles == null)
                                Session["ROL"] = "USUARIO";
                            else
                                Session["ROL"] = strRoles;

                            Response.Redirect("~/Views/Procesos/Default.aspx");
                        }
                        else
                        {
                            ///Denegar acceso
                            lblMensaje.Text = "Usuario/Password incorrecto";
                        }
                    }
                }
            }

        #endregion


    }
}