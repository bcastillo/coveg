﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace PLA.Web.Views.Utils
{
     

    public partial class TextPopup : System.Web.UI.UserControl
    {
        public event EventHandler ConfirmAccept;
        public event EventHandler ConfirmCancel;
        private string CurrentCultureCode = string.Empty;


        public String AsignarValor
        {
            set { txtText.Text = value; }
        }

        public String TextPopupText
        {
            get
            {
                return Session["TextPopupText"] as String;
            }
            set
            {
                Session["TextPopupText"] = value;

            }
        }


        public void Initilize(string CurrentCultureCode, string TitleText, string RequestText)
        {
            this.CurrentCultureCode = CurrentCultureCode;
            lblMsgTitle.Text = TitleText;
            lblMsgText.Text = RequestText;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            TextPopupText = "";
        }

        protected void b_btnConfirmOK_Click(object sender, EventArgs e)
        {
            if (txtText.Text != "")
            {
                TextPopupText = txtText.Text;
                txtText.Text = "";
                if (ConfirmAccept != null)
                {
                    ConfirmAccept(sender, e);
                }
            }
        }


        protected void b_btnConfirmCancel_Click(object sender, EventArgs e)
        {
            if (ConfirmCancel != null)
            {
                ConfirmCancel(sender, e);
            }
        }
    }
}