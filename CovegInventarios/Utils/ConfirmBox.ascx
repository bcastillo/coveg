﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfirmBox.ascx.cs" Inherits="ConfirmBox" %>
<table border="0"  style="width:100%;
height:100%; 
 position:fixed;
 top:0;
 left:0;
 z-index:99999; 
">
<tr><td align=center valign=middle>
<table 
    style="
     border: solid 1px #000000;
     background-image: url('../Images/bg_vertical.jpg'); background-repeat:repeat; " 
    cellspacing="0">
    <tr>
        
        <td align="center" colspan="2" 
            style="
            background-image: url('../Images/bg_tile.jpg'); background-repeat:repeat-x;
            color:#000000;
	        font:15px Calibri,Arial,sans-serif;
	        border:1px solid transparent; font-weight: bold;" valign="middle">
            <asp:Label ID="lblMsgTitle" runat="server"></asp:Label>
            <asp:Label ID="lblGama" runat="server" Text="Label"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle" style="width:50px">
            <asp:Image ID="imgQuestion" runat="server" ImageUrl="~/Images/msgQuestion.png" />
        </td>
        <td align="left" 
            style="            
            color:#3B3B3B;
	        font:15px Calibri,Arial,sans-serif;
	        border:1px solid transparent;
            
            padding-right: 10px;" valign="middle" >
            <asp:Label ID="lblMsgText" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td><asp:Button ID="Button1" runat="server" Text="Aceptar" CssClass="gray_button"
                            onclick="b_btnConfirmOK_Click" CausesValidation="False" /></td>
                    <td><asp:Button ID="Button2" runat="server" Text="Cancelar" CssClass="gray_button"
                            onclick="b_btnConfirmCancel_Click" CausesValidation="False" /></td>
    </tr>
</table>
</td>
</tr>
</table>