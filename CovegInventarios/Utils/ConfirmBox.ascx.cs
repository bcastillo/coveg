﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

    public partial class ConfirmBox : System.Web.UI.UserControl
    {
        public event EventHandler ConfirmAccept;
        public event EventHandler ConfirmCancel;
        private string CurrentCultureCode = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CurrentCultureCode"></param>
        /// <param name="TitleText"></param>
        /// <param name="QuestionText"></param>
        public void Initilize(string CurrentCultureCode, string TitleText, string QuestionText)
        {
            this.CurrentCultureCode = CurrentCultureCode;
            lblMsgTitle.Text = TitleText;
            lblMsgText.Text = QuestionText;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void b_btnConfirmOK_Click(object sender, EventArgs e)
        {
            if (ConfirmAccept != null)
            {
                ConfirmAccept(sender, e);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void b_btnConfirmCancel_Click(object sender, EventArgs e)
        {
            if (ConfirmCancel != null)
            {
                ConfirmCancel(sender, e);
            }
        }

    }