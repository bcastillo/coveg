﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Coveg.Entities;

namespace CovegInventarios.Utils
{
    public partial class BusquedaActivos : System.Web.UI.UserControl
    {
        #region Propiedades


        private List<ACTIVO_BE> lstActivos;

        public List<ACTIVO_BUSQUEDA> ListaActivosBusqueda
        {
            get { return (List<ACTIVO_BUSQUEDA>)Session["ListaActicosBusqueda_Collection"]; }
            set { Session["ListaActicosBusqueda_Collection"] = value; }
        }


        #endregion

        #region Eventos


            protected void dgvActivo_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='#ECF3F8'";
                    e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";

                }
            }
            /// <summary>
            /// Page load
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void Page_Load(object sender, EventArgs e)
            {

            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void bt_btFiltrar_Click(object sender, ImageClickEventArgs e)
            {
                dgvActivo.DataSource = Refresh();
                dgvActivo.DataBind();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void btn_CloseControl_Click(object sender, ImageClickEventArgs e)
            {
                CovegInventarios.Views.Procesos.SolicitudSalida myPage = (CovegInventarios.Views.Procesos.SolicitudSalida)this.Page;
                myPage.objExtender.Hide();
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void dgvActivo_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                CovegInventarios.Views.Procesos.SolicitudSalida myPage = (CovegInventarios.Views.Procesos.SolicitudSalida)this.Page;
                dgvActivo.PageIndex = e.NewPageIndex;
                dgvActivo.DataSource = Refresh();
                dgvActivo.DataBind();
                myPage.objExtender.Show();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void dgvActivo_SelectedIndexChanged(object sender, EventArgs e)
            {

                CovegInventarios.Views.Procesos.SolicitudSalida myPage = (CovegInventarios.Views.Procesos.SolicitudSalida)this.Page;
                Int32 intPosicionSeleccionada;
                Boolean bolRes = true;


                intPosicionSeleccionada = dgvActivo.SelectedRow.DataItemIndex;

                if (myPage.NoElementosLista > 0)
                    bolRes = myPage.ValidaSolicitudResponsable(Refresh()[intPosicionSeleccionada].RESPONSABLE);


                if (bolRes)
                    myPage.AsignarActivoGenerico(Refresh()[intPosicionSeleccionada]);
            }
        #endregion

        #region Funciones
            /// <summary>
            /// 
            /// </summary>
            /// <param name="lstActivos"></param>
            public void Initilize(List<ACTIVO_BUSQUEDA> lstActivos)
            {
                ListaActivosBusqueda = lstActivos;

                dgvActivo.DataSource = ListaActivosBusqueda;
                dgvActivo.DataBind();
            }
            /// <summary>
            /// 
            /// </summary>
            /// <returns></returns>
            private List<ACTIVO_BUSQUEDA> Refresh()
            {
                List<ACTIVO_BUSQUEDA> result = new List<ACTIVO_BUSQUEDA> ();

                if (ListaActivosBusqueda != null && ListaActivosBusqueda.Count > 0)
                {
                    result = ListaActivosBusqueda.FindAll(p => p.CLAVE.ToString().ToUpper().Contains(txtValor.Text.ToUpper()) || p.DESCRIPCION.ToString().ToUpper().Contains(txtValor.Text.ToUpper()) || p.RESPONSABLE.ToString().ToUpper().Contains(txtValor.Text.ToUpper()));
                }

                return result;
            }

        #endregion

    }
}