﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageBox.ascx.cs" Inherits="MessageBox" %>
<table border="0"  style="
width:100%;
height:100%; 
 position:fixed;
 top:0;
 left:0;
 z-index:99999; 
">
<tr><td align=center valign=middle>
<table class="shadowPanel"
    style="
     border: solid 1px #cfcfcf;
     background-color:#ddddff" 
    cellspacing="0">
    <tr>
        
        <td align="center" colspan="2" 
            style="
            background-color:royalblue;
            color:#FFFFFF;
	        font:15px Calibri,Arial,sans-serif;
	        border:1px solid transparent; font-weight: bold;" valign="middle" 
            bordercolor="#999999">
            <asp:Label ID="lblMsgTitle" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center" valign="middle" style="width:50px">
            <asp:Image ID="imgError" runat="server" Visible="False" 
                ImageUrl="~/Images/msgError.png" />
            <asp:Image ID="imgWarning" runat="server" Visible="False" 
                ImageUrl="~/Images/msgExclamation.png" />
            <asp:Image ID="imgInformation" runat="server" Visible="False" 
                ImageUrl="~/Images/msgInformation.png" />
        </td>
        <td align="left" 
            style="            
            color:#3B3B3B;
	        font:15px Calibri,Arial,sans-serif;
	        border:1px solid transparent;
            
            padding-right: 10px;" valign="middle" >
            <asp:Label ID="lblMsgText" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="2" style="padding-bottom: 10px">
            <asp:Button ID="b_btnConfirmOK" runat="server" Text="Aceptar" 
                CausesValidation="false" CssClass ="boton" onclick="b_btnAccept_Click" />
        </td>
    </tr>
</table>
</td>
</tr>
</table>