﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BusquedaActivos.ascx.cs" Inherits="CovegInventarios.Utils.BusquedaActivos" %>
<link href="../../Content/css/Style.css" rel="stylesheet" type="text/css" />
<link href="../../Content/css/Style1.css" rel="stylesheet" type="text/css" />

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:UpdatePanel ID="upp_Activo" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        
                <table cellspacing="0">
                <tr bgcolor="#F6F6F6">
            <td >
                <table cellspacing="0" style="width:100%">
                <tr>
                <td valign="middle" bgcolor="#619DDB">
                    <asp:Label ID="l_ActivosFilter" runat="server" Text="Búsqueda de activos" 
                        style="font-size: 12px; font-weight: bold; color: White" Font-Size="8pt" ></asp:Label>
                </td>
                <td align="right" bgcolor="#619DDB">         
                        <asp:ImageButton ID="btn_CloseControl" runat="server" ImageUrl="~/Images/close.png" Width="20px" onclick="btn_CloseControl_Click" />
                </td>
                </tr>
                </table>
                    </td>
            </tr>
                <tr>
            <td bgcolor="#FFFFF">
                <asp:Label ID="lblValorFiltro" runat="server" Text="Digite la clave, descripción o el responsable" 
                    CssClass="labelbox" Font-Size="8pt"></asp:Label>
                <asp:TextBox ID="txtValor" runat="server" Width="200px" 
                    CssClass="edit_input"></asp:TextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" 
                    ImageUrl="~/Images/search_find.png" onclick="bt_btFiltrar_Click" 
                    Width="30px" />
            </td>   
            </tr>
                <tr>
                   <td>
                        <asp:GridView ID="dgvActivo" 
                            Width="500px"
                            runat="server" 
                            CellPadding="3" 
                            AllowPaging="True" 
                            AllowSorting="false"
                            OnPageIndexChanging="dgvActivo_PageIndexChanging"
                            onrowdatabound="dgvActivo_RowDataBound"
                            OnSelectedIndexChanged="dgvActivo_SelectedIndexChanged"
                            AutoGenerateColumns="False"  
                            BackColor="White" 
                            BorderColor="#CCCCCC" 
                            BorderStyle="None" 
                            BorderWidth="1px">
                            <Columns>
                                <asp:BoundField DataField="ACTIVOID" HeaderText="ACTIVOID" Visible=false/>
                                <asp:BoundField DataField="CLAVE" HeaderText="Clave"/>
                                <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" />
                                <asp:BoundField DataField="RESPONSABLE" HeaderText="Responsable" />
                                <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Elegir">
                                <ControlStyle CssClass="boton" />
                                <ItemStyle CssClass="70px" Width="70px" />
                                </asp:ButtonField>
                                </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" Font-Names="Intro_Book" />
                            <HeaderStyle Font-Bold="True" ForeColor="#33333f" Font-Size="9pt" Font-Names="Intro_Book" CssClass="myheader"/>
                            <PagerStyle BackColor="#FCFCFC" ForeColor="#000066" HorizontalAlign="Center" />
                            <RowStyle ForeColor="#000066" Font-Size="9pt" Font-Names="Intro_Book"/>
                            <SelectedRowStyle BackColor="#ECF3F8" Font-Bold="True" ForeColor="000066" Font-Names="Intro_Book" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" Font-Names="Intro_Book" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" Font-Names="Intro_Book" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" Font-Names="Intro_Book" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" Font-Names="Intro_Book" />
                        </asp:GridView>
                   </td>                    
               </tr>
            </table>
            
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="dgvActivo" />
    </Triggers>
</asp:UpdatePanel>
