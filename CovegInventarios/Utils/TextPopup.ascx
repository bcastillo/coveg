﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextPopup.ascx.cs" Inherits="PLA.Web.Views.Utils.TextPopup" %>
<link href="../../Content/css/Style.css" rel="stylesheet" type="text/css" />
<link href="../../Content/css/Style1.css" rel="stylesheet" type="text/css" />
<table border="0"  style="
width:100%;
height:100%; 
 position:fixed;
 top:0;
 left:0;
 z-index:99990; 
">
<tr><td align="center" valign="middle">
<table class="shadowPanel"
    style="
     border: solid 1px #cfcfcf;
     background-color:#ddddff" 
    cellspacing="0" width="350px">
    <tr>
        <td align="center" colspan="2"  bgcolor="#619DDB" style="font-family: Intro Book; font-size: 12px;
            font-weight: bold; color: #000000; height:32px"  valign="middle" >
            <asp:Label ID="lblMsgTitle" runat="server" ForeColor="White"></asp:Label>
        </td>
    </tr>
    <tr bgcolor="#F6F6F6">
        <td align="center" valign="middle">
            <asp:Image ID="imgQuestion" runat="server" ImageUrl="~/Images/msgExclamation.png" Width="64px" />
        </td>
        <td align="left" style="font-family: Verdana; font-size: 12px; font-weight: bold; color: #FF0000; overflow: auto" valign="middle">
            <asp:Label ID="lblMsgText" runat="server"></asp:Label>
        </td>
    </tr>
      <tr bgcolor="#F6F6F6">
       
        <td align="center" style="font-family: Verdana; font-size: 12px; font-weight: bold;color: #FF0000; overflow: auto" valign="middle" colspan="2">
            <asp:Textbox ID="txtText" runat="server" TextMode="MultiLine" CssClass="textbox" Height="50px" Width="330px"></asp:Textbox>
        </td>
    </tr>
    <tr bgcolor="#F6F6F6">
        <td align="center" colspan="2">
            <table>
                <tr>                    
                    <td>
                        <asp:Button ID="b_btnConfirmOK" runat="server"  Text="Aceptar"  Font-Bold="True" OnClick="b_btnConfirmOK_Click" CssClass="boton"/>
                    <td>
                        <asp:Button ID="b_btnConfirmCancel" runat="server"  Text="Cancelar"  Font-Bold="True" OnClick="b_btnConfirmCancel_Click" CssClass="boton"/>
                </tr>
            </table>
        </td>
    </tr>
</table>
</td>
</tr>
</table>