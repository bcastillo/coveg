﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class MessageBox : System.Web.UI.UserControl
{

    public event EventHandler Accepted;
    private string CurrentCultureCode = string.Empty;
    /// <summary>
    /// 
    /// </summary>
    /// <param name="MessageType">Type of message box (Warning, Error, Information, Exclamation)</param>
    /// <param name="MessageText">Mesaage text to display</param>
    public void Initilize(string MessageType, string TitleText, string MessageText)
    {
        switch (MessageType)
        {
            case "Warning":
                imgWarning.Visible = true;
                imgError.Visible = false;
                imgInformation.Visible = false;
                break;
            case "Error":
                imgWarning.Visible = false;
                imgError.Visible = true;
                imgInformation.Visible = false;
                break;
            case "Information":
                imgWarning.Visible = false;
                imgError.Visible = false;
                imgInformation.Visible = true;
                break;
            case "Exclamation":
                imgWarning.Visible = true;
                imgError.Visible = false;
                imgInformation.Visible = false;
                break;
            default:
                imgWarning.Visible = true;
                imgError.Visible = false;
                imgInformation.Visible = false;
                break;
        }
        
        lblMsgTitle.Text = TitleText;
        lblMsgText.Text = MessageText;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void b_btnAccept_Click(object sender, EventArgs e)
    {
        if (Accepted != null)
        {
            Accepted(sender, e);
        }
    }

}